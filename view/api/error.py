# -*- coding:utf-8 -*-

from werkzeug.exceptions import HTTPException
from flask import jsonify

SYSTEM_ERROR = (1000, '系统错误', 400)
INVALID_API_KEY_OR_SECRET = (1001, '错误的APIKEY和SECRET', 401)
MISSING_AUTHORIZATION = (1002, '缺少Authorization参数', 403)
INVALID_ACCESS_TOKEN = (1003, '无效的access token', 403)

INVALID_PHONE_NUMBER = (1100, '手机号码不正确', 403)
SEND_SMS_VERIFY_EXCEPTION = (1101, '验证码发送失败！', 403)
MISMATCHED_PASSWORD = (1102, '账号或密码错误', 403)
USER_NOT_FOUND = (1103, '用户不存在', 404)
ALBUM_NOT_FOUND = (1104, '没有找到任何照片', 404)
UPDATE_USER_ERROR = (1105, '更新用户信息失败', 403)
PHOTO_NOT_FOUND = (1106, '照片不存在', 404)
PHOTO_NOT_VALID = (1107, '上传照片格式不正确', 403)

REGISTERED_PHONE_NUMBER = (1110, '该手机号已被注册', 403)
REGISTERED_FAIL = (1112, '注册失败', 400)
INVALID_VERIFY_CODE = (1111, '验证码错误', 403)
ACCOUNT_HAS_TELEPHONE = (1113, '您的账号已经绑定其他手机号', 403)

MISSING_PARAM = (4999, '缺少参数', 400)
HTTP_METHOD_ERROR = (4998, '非法的请求方式', 400)
INVALID_URL = (4997, '非法的请求URL', 400)

ORDER_NOT_FOUND = (2000, '订单不存在', 404)
ORDER_TYPE_ERROR = (2001, '订单类型错误', 400)
ORDER_EXPIRED = (2002, '订单过期', 403)
ORDER_VERIFY_ERROR = (2003, '订单验证失败', 405)
ORDER_CANNOT_REFOUNDABLE = (2004, '订单不能再退款', 405)
ORDER_NOT_MERCHANT = (2005, '不是商家创建的订单', 403)
ORDER_WEIXIN_REFUND_ERROR = (2006, '微信退款失败', 405)
ORDER_NOT_IN_REFUNDING = (2007, '未申请退款', 400)

GAME_NOT_REQUIRED = (2010, '该订场不能约球', 403)
GAME_REQUIRE_FULL = (2011, '该订场参与人数已到人数限制', 405)
GAME_NOT_FOUND = (2012, '比赛不存在', 404)
COURT_CANNOT_BOOK = (2013, '场地不能被预定', 403)
FORBIDDEN = (2014, '用户没有权限操作', 403)
GAME_OVER = (2015, '比赛已经结束', 405)
USER_NOT_MEET_REQUIRE = (2016, '用户不符合约球的条件', 403)
USER_ALREADY_PARTICIPANT = (2017, '用户已经参与约球', 403)
USER_NOT_MEET_REQUIRE_LEVEL = (2018, '抱歉,您的等级不符合要求', 403)
USER_NOT_MEET_REQUIRE_GENDER = (2019, '抱歉,您的性别不符合要求', 403)

NOT_ENOUGH_BALANCE = (3001, '余额不足', 403)
ACCOUNT_NOT_FOUND = (3002, '用户账户不存在', 404)
ACCOUNT_VIP_IS_SERVING = (3003, '用户已经是VIP会员', 405)

VENUE_NOT_FOUND = (5001, '网球馆不存在', 404)
DEVICE_NOT_REGISTER = (5002, '用户设备没有注册', 404)

REQUIRE_NOT_FOUND = (6001, '约球不存在', 404)

COMMENT_NOT_FOUND = (7001, '评论不存在', 404)

class APIError(HTTPException):

    def __init__(self, error):
        response = jsonify(self.api_error_dict(error))
        response.status_code = error[2]

        desc = 'TenCourt API Error'
        HTTPException.__init__(self, desc, response)

    def api_error_dict(self, error):
        return {
            'code': error[0],
            'mesg': error[1],
            'status': error[2]
        }
