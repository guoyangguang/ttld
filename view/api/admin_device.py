from flask import request, jsonify
from flask.views import MethodView

from model.manager import Manager
from model.admin_device import AdminDevice
from model.consts import IOS, ANDROID
from libs.decorator import check_api_key, check_access_token
from view.api.error import APIError, MISSING_PARAM, USER_NOT_FOUND, DEVICE_NOT_REGISTER

class AdminDeviceAPI(MethodView):
    @check_api_key
    @check_access_token
    def post(self):
        user_id = request.json.get('manager_id')
        device_token = request.json.get('device_token')

        if not (user_id and device_token):
            return APIError(MISSING_PARAM)

        if not Manager.get(user_id):
            return APIError(USER_NOT_FOUND)

        os = ANDROID if request.android else IOS
        device = AdminDevice.register(user_id, device_token, os)
        if not device:
            device = AdminDevice.get_by_uid(user_id)
        if not device:
            return APIError(DEVICE_NOT_REGISTER)
        return jsonify(code=200, device=device.jsonify())

    @check_api_key
    @check_access_token
    def get(self):
        manager_id = request.args.get('manager_id')
        device = AdminDevice.get_by_uid(manager_id)

        if not device:
            return APIError(DEVICE_NOT_REGISTER)

        return jsonify(code=200, device=device.jsonify())

    @check_api_key
    @check_access_token
    def delete(self):
        manager_id = request.args.get('manager_id')
        result = AdminDevice.remove(manager_id)
        return jsonify(code=200, result=result)
