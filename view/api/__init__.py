# coding: utf-8

from view.api.verify import VerifyCodeAPI, VerifyPhoneAPI, ResetPasswordVerifyAPI, ManagerVerifyPhoneAPI
from view.api.user import (
    UserAPI, UserLoginAPI, ResetPasswordAPI, UserRegisterAPI,UserChangeCityAPI,
    UserApkUpdate, UserWXLogin, UserAddTelephone, UserMergeAccount, UserGameOrderAPI 
)
from view.api.followship import(FollowedsAPI, FollowingsAPI, FollowUnfollowAPI)
from view.api.notify import (
    UserNotifyAPI, UserNotifyCountAPI, ManagerNotifyCountAPI, ManagerNotifyAPI, ManageNotifyReadAPI,
    ManagerNotifyAPI, NotifyReadAPI)
from view.api.require import RequireAPI, RequireSearchAPI, RequireDescAPI
from view.api.comment import CommentsAPI
from view.api.venue import (
    NearbyVenueAPI, UsualVenueAPI, VenuePhotoView, VenueScheduleAPI, VenueSearchAPI, VenueCityAPI,
    ManagerVenueSchedule, VenuePhotoAPI, SetAlbumCover
)
from view.api.order import (
    BookCourtOrderAPI, VIPOrderAPI, PendingOrderAPI,
    VerifyOrderAPI, ParticipateOrderAPI, VIPOrderAPI,
    ChargeOrderAPI, BookCourtAndAccountPayOrderAPI,
    ParticipateAndAccountPayOrderAPI, AccountPayOrderAPI,
    VIPAndAccountPayOrderAPI, RefundOrderAPI, GameOrderListAPI, OrderConfirm,
    UserRefundGameorderFromAlipayCallbackAPI
)
from view.api.game import UserGameAPI, GamesNotifyAPI, GameAPI, ManagerGameAPI, GameParticipate
from view.api.alipay import AlipayNotifyAPI
from view.api.order import AccountPayOrderAPI
from view.api.wxpay import WxPayNotifyAPI, WxServiceVerifyAPI
from view.api.device import RegisterDeviceAPI, UnRegisterDeviceAPI
from view.api.manager import (
    ManagerLoginAPI, ManagerChangePhoneAPI, ManagerResetPasswordAPI,
    ManagerChangePasswordAPI, ManagerNotifyConfig, ManagerUserInfo,
    ManagerAppUpdate
)
from view.api.admin_court import ManagerBookCourtAPI,ManagerConfirmBook
from view.api.admin_device import AdminDeviceAPI

def load_api_views(app):
    login = UserLoginAPI.as_view('login')
    app.add_url_rule('/api/user/login/', view_func=login, methods=['POST'])

    app.add_url_rule('/api/verify/phone/',
                     view_func=VerifyPhoneAPI.as_view('verify_phone'),
                     methods=['POST'])
    app.add_url_rule('/api/verify/code/',
                     view_func=VerifyCodeAPI.as_view('verify_code'),
                     methods=['POST'])

    app.add_url_rule('/api/verify/reset-password/',
                     view_func=ResetPasswordVerifyAPI.as_view('verify_reset_password'),
                     methods=['POST'])

    app.add_url_rule('/api/user/reset-password/',
                     view_func=ResetPasswordAPI.as_view('reset_password'),
                     methods=['POST'])


    app.add_url_rule('/api/followeds/', 
                     view_func=FollowedsAPI.as_view('followeds_api'), methods=['GET'])
    app.add_url_rule('/api/followings/', 
                     view_func=FollowingsAPI.as_view('followings_api'), methods=['GET'])
    ff_dispatcher = FollowUnfollowAPI.as_view('follow_unfollow_api')
    app.add_url_rule('/api/followeds/', view_func=ff_dispatcher, methods=['POST'])
    app.add_url_rule('/api/followeds/<int:followed_uid>/', view_func=ff_dispatcher, methods=['DELETE'])


    user_view = UserAPI.as_view('user_api')
    app.add_url_rule('/api/user/',
                     view_func=user_view, methods=['POST'])
    app.add_url_rule('/api/user/<int:uid>/', view_func=user_view,
                     methods=['GET', 'PUT'])

    book_order = BookCourtOrderAPI.as_view('book_order')
    app.add_url_rule('/api/order/book/<int:order_id>',
                     view_func=book_order, methods=['GET'])
    app.add_url_rule('/api/order/book/',
                     view_func=book_order, methods=['POST'])

    book_account_pay = BookCourtAndAccountPayOrderAPI.as_view('book_account_pay')
    app.add_url_rule('/api/order/account/book/', view_func=book_account_pay,
                     methods=['POST'])

    part_order = ParticipateOrderAPI.as_view('part_order')
    app.add_url_rule('/api/order/participate/',
                     view_func=part_order, methods=['POST'])

    part_account_pay = ParticipateAndAccountPayOrderAPI.as_view('part_account_pay')
    app.add_url_rule('/api/order/account/participate/', view_func=part_account_pay,
                     methods=['POST'])

    vip_order = VIPOrderAPI.as_view('vip_order')
    app.add_url_rule('/api/order/vip/', view_func=vip_order, methods=['POST'])

    vip_account_pay = VIPAndAccountPayOrderAPI.as_view('vip_account_pay')
    app.add_url_rule('/api/order/account/vip/', view_func=vip_account_pay,
                     methods=['POST'])

    charge_order = ChargeOrderAPI.as_view('charge_order')
    app.add_url_rule('/api/order/charge/', view_func=charge_order, methods=['POST'])

    pending_order = PendingOrderAPI.as_view('pending_order')
    app.add_url_rule('/api/order/user/pending/',
                     view_func=pending_order, methods=['GET'])

    verify_order = VerifyOrderAPI.as_view('verify_order')
    app.add_url_rule('/api/order/verify/', view_func=verify_order, methods=['POST'])

    require = RequireAPI.as_view('require')
    app.add_url_rule('/api/game/require/', view_func=require, methods=['POST'])
    
    comment_dispatcher = CommentsAPI.as_view('comment')
    app.add_url_rule(
        '/api/game/require/<int:id>/comments/', 
        view_func=comment_dispatcher, 
        methods=['GET', 'POST']
    )

    nearby = NearbyVenueAPI.as_view('nearby')
    app.add_url_rule('/api/venue/nearby/', view_func=nearby, methods=['GET'])

    usual_venue = UsualVenueAPI.as_view('usual_venue')
    app.add_url_rule('/api/venue/usual/', view_func=usual_venue, methods=['GET'])

    venue_photo = VenuePhotoView.as_view('venue_photo')
    app.add_url_rule('/api/venue/<int:vid>/photos/', view_func=venue_photo, methods=['GET'])

    schedule = VenueScheduleAPI.as_view('venue_schedule')
    app.add_url_rule('/api/venue/schedule/', view_func=schedule, methods=['GET'])

    venue_search = VenueSearchAPI.as_view('venue_search')
    app.add_url_rule('/api/venue/search/', view_func=venue_search, methods=['GET'])

    user_game = UserGameAPI.as_view('user_game')
    app.add_url_rule('/api/game/user/<int:uid>/<pay_type>/',
                     view_func=user_game, methods=['GET', 'POST'])

    game_search = RequireSearchAPI.as_view('game_search')
    app.add_url_rule('/api/game/search/', view_func=game_search, methods=['GET'])

    game_fetch = GameAPI.as_view('game_fetch')
    app.add_url_rule('/api/game/fetch/', view_func=game_fetch, methods=['GET'])

    vip_req = VIPOrderAPI.as_view('vip_req')
    app.add_url_rule('/api/order/vip/', view_func=vip_req, methods=['PUT', 'POST'])

    alipay_notify = AlipayNotifyAPI.as_view('alipay_notify')
    app.add_url_rule('/api/alipay/notify/', view_func=alipay_notify, methods=['POST'])

    payment_by_account = AccountPayOrderAPI.as_view('payment_by_account')
    app.add_url_rule('/api/order/payment/account/',
                     view_func=payment_by_account, methods=['POST'])

    app.add_url_rule('/api/order/refund/',
                     view_func=RefundOrderAPI.as_view('refund'),
                     methods=['POST'])
    app.add_url_rule(
        '/api/alipay/refund/callback/',
        view_func=UserRefundGameorderFromAlipayCallbackAPI.as_view('user_refund_gameorder_from_alipay_callback'),
        methods=['POST']
    )
    venue_city = VenueCityAPI.as_view('venue_city')
    app.add_url_rule('/api/venue/city/', view_func=venue_city, methods=['GET'])

    games_notify = GamesNotifyAPI.as_view('games_notify')
    app.add_url_rule('/api/game/notify/', view_func=games_notify, methods=['GET'])

    user_register = UserRegisterAPI.as_view('user_register')
    app.add_url_rule('/api/user/register/', view_func=user_register, methods=['POST'])

    user_city = UserChangeCityAPI.as_view('user_city')
    app.add_url_rule('/api/user/city/', view_func=user_city, methods=['POST'])

    user_notify_count = UserNotifyCountAPI.as_view('user_notify_count')
    app.add_url_rule('/api/user/notify/count/', view_func=user_notify_count, methods=['GET'])

    user_notify = UserNotifyAPI.as_view('user_notify')
    app.add_url_rule('/api/user/notify/', view_func=user_notify, methods=['GET'])

    user_wx_login = UserWXLogin.as_view('user_wx_login')
    app.add_url_rule('/api/user/wxlogin/', view_func=user_wx_login, methods=['POST'])

    user_add_telephone = UserAddTelephone.as_view('user_add_telephone')
    app.add_url_rule('/api/user/add-telephone/', view_func=user_add_telephone, methods=['POST'])

    user_merge = UserMergeAccount.as_view('user_merge')
    app.add_url_rule('/api/user/merge/', view_func=user_merge, methods=['POST'])

    wxpay_notify = WxPayNotifyAPI.as_view('wxpay_notify')
    app.add_url_rule('/api/wxpay/notify/', view_func=wxpay_notify, methods=['POST'])

    wx_verify = WxServiceVerifyAPI.as_view('wx_verify')
    app.add_url_rule('/api/wx/verify/', view_func=wx_verify, methods=['GET'])

    user_app_update = UserApkUpdate.as_view('user_app_update')
    app.add_url_rule('/api/user/update/', view_func=user_app_update, methods=['GET'])

    register_device = RegisterDeviceAPI.as_view('register_device')
    app.add_url_rule('/api/device/', view_func=register_device, methods=['GET'])
    app.add_url_rule('/api/device/register/', view_func=register_device, methods=['POST'])

    un_register_device = UnRegisterDeviceAPI.as_view('un_register_device')
    app.add_url_rule('/api/device/unregister/', view_func=un_register_device, methods=['POST'])

    game_order_list = GameOrderListAPI.as_view('game_order_list')
    app.add_url_rule('/api/order/game/list/', view_func=game_order_list, methods=['GET'])

    user_game_order = UserGameOrderAPI.as_view('user_game_order')
    app.add_url_rule('/api/my_order/', view_func=user_game_order, methods=['GET'])

    order_confirm = OrderConfirm.as_view('order_confirm')
    app.add_url_rule('/api/order/confirm/', view_func=order_confirm, methods=['POST'])

    notify_read = NotifyReadAPI.as_view('notify_read')
    app.add_url_rule('/api/notify/read/', view_func=notify_read, methods=['POST'])

    # 微信页面报名
    game_participate = GameParticipate.as_view('game_participate')
    app.add_url_rule('/api/game/participate/', view_func=game_participate, methods=['POST'])

    require_desc_update = RequireDescAPI.as_view('require_desc_update')
    app.add_url_rule('/api/require/desc/update/', view_func=require_desc_update, methods=['POST'])

    ## 商家后台
    app.add_url_rule('/api/manager/login/', view_func=ManagerLoginAPI.as_view('manager_login'), methods=['POST'])
    app.add_url_rule('/api/manager/verify/', view_func=ManagerVerifyPhoneAPI.as_view('manager_verify_phone'), methods=['POST'])
    app.add_url_rule('/api/manager/telephone/', view_func=ManagerChangePhoneAPI.as_view('manager_change_phone'), methods=['POST'])
    app.add_url_rule('/api/manager/reset_pwd/', view_func=ManagerResetPasswordAPI.as_view('manager_reset_pass'), methods=['POST'])
    app.add_url_rule('/api/manager/modify_pwd/', view_func=ManagerChangePasswordAPI.as_view('manager_change_pass'), methods=['POST'])
    app.add_url_rule('/api/manager/userinfo/', view_func=ManagerUserInfo.as_view('manager_userinfo'), methods=['GET'])
    app.add_url_rule('/api/manager/schedule/', view_func=ManagerVenueSchedule.as_view('manager_venue_schedule'), methods=['GET'])
    app.add_url_rule('/api/manager/games/', view_func=ManagerGameAPI.as_view('manager_game'), methods=['GET'])
    app.add_url_rule('/api/manager/preorder/', view_func=ManagerBookCourtAPI.as_view('manager_book_court'), methods=['POST', 'DELETE'])
    app.add_url_rule('/api/manager/confirm_preorder/', view_func=ManagerConfirmBook.as_view('manager_confirm_preorder'), methods=['POST'])
    app.add_url_rule('/api/manager/photos/', view_func=VenuePhotoAPI.as_view('manager_venue_photo'), methods=['GET', 'POST'])
    app.add_url_rule('/api/manager/album_cover/', view_func=SetAlbumCover.as_view('set_album_cover'), methods=['POST'])
    app.add_url_rule('/api/manager/noti_config/', view_func=ManagerNotifyConfig.as_view('manager_notify_config'), methods=['POST'])
    app.add_url_rule('/api/manager/device/', view_func=AdminDeviceAPI.as_view('manager_device'), methods=['GET', 'POST', 'DELETE'])
    app.add_url_rule('/api/manager/notify_count/', view_func=ManagerNotifyCountAPI.as_view('manager_notify_count'), methods=['GET'])
    app.add_url_rule('/api/manager/notify/', view_func=ManagerNotifyAPI.as_view('manager_notify'), methods=['GET'])
    app.add_url_rule('/api/manager/notify_read/', view_func=ManageNotifyReadAPI.as_view('manager_notify_read'), methods=['POST'])
    app.add_url_rule(
        '/api/manager/app/update/',
        view_func=ManagerAppUpdate.as_view('manager_app_update'),
        methods=['GET']
    )
