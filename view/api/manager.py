# coding: utf-8

from flask import request, jsonify
from flask.views import MethodView
from libs.decorator import check_api_key, check_manager_venue, check_access_token
from view.utils import check_manager_password, gen_access_token, check_phone_number, gen_manager_access_token
from view.api.error import APIError, MISSING_PARAM, MISMATCHED_PASSWORD, INVALID_PHONE_NUMBER, \
                           SYSTEM_ERROR, INVALID_VERIFY_CODE, USER_NOT_FOUND
from model.manager import Manager, ManagerNotifyConfig
from model.verify import AdminVerify
from model.token import ManagerToken
from config import MANAGER_APP_VERSION, MANAGER_APP_DOWNLOAD_PATH

class ManagerLoginAPI(MethodView):

    @check_api_key
    def post(self):
        phone = request.json.get('telephone')
        password = str(request.json.get('password'))
        app_key = request.json.get('app_key')

        if not (phone or password or app_key):
            raise APIError(MISSING_PARAM)

        if check_manager_password(phone, password):
            manager = Manager.get_by_name_or_tel(phone)
            #access_token = ManagerToken.get_by_uid(manager.id)
            #if access_token == None:
            access_token = gen_manager_access_token(manager.id, password)
            #ManagerToken.add(access_token,manager.id)
            return jsonify(
                code=200,
                access_token=access_token,
                user=manager.jsonify(),
            )

        raise APIError(MISMATCHED_PASSWORD)

class ManagerUserInfo(MethodView):

    @check_api_key
    def get(self):
        fetch_user_id = request.args.get('fetch_user_id')

        if not fetch_user_id :
            raise APIError(MISSING_PARAM)

        manager = Manager.get(fetch_user_id )
        return jsonify (
            code=200,
            user=manager.jsonify(),
        )


class ManagerChangePhoneAPI(MethodView):

    @check_access_token
    @check_manager_venue
    def post(self):
        verify_code = request.json.get('verify_code')
        phone = request.json.get('telephone')
        manager_id = request.json.get('manager_id')
        password = request.json.get('password')

        if not verify_code or not phone or not manager_id or not password:
            raise APIError(MISSING_PARAM)

        manager = Manager.get(manager_id)

        if not manager:
            raise APIError(USER_NOT_FOUND)

        if manager.password != password:
            raise APIError(MISMATCHED_PASSWORD)

        if str(verify_code) == AdminVerify.get_code(phone):
            try:
                manager.update(telephone=phone)
                return jsonify(code=200, new_tele=phone)
            except:
                raise APIError(SYSTEM_ERROR)

        raise APIError(INVALID_VERIFY_CODE)


class ManagerResetPasswordAPI(MethodView):

    @check_access_token
    def post(self):
        password = request.json.get('password')
        phone = request.json.get('telephone')
        verify_code = request.json.get('verify_code')
        app_key = request.json.get('app_key')

        if not (app_key and password and phone and verify_code):
            raise APIError(MISSING_PARAM)

        manager = Manager.get_by_telephone(phone)

        if str(verify_code) == AdminVerify.get_code(phone):
            try:
                manager.update(password=password)
                access_token = gen_manager_access_token(manager.id, password)
                return jsonify(
                    code=200,
                    access_token=access_token,
                    user=manager.jsonify()
                )
            except:
                raise APIError(SYSTEM_ERROR)

        raise APIError(INVALID_VERIFY_CODE)


class ManagerChangePasswordAPI(MethodView):

    @check_access_token
    @check_manager_venue
    def post(self):
        old_password = request.json.get('old_password')
        new_password = request.json.get('new_password')
        manager_id = request.json.get('manager_id')

        if not old_password or not new_password or not manager_id:
            return APIError(MISSING_PARAM)

        manager = Manager.get(manager_id)

        if old_password == manager.password:
            try:
                manager.update(password=new_password)
                access_token = gen_manager_access_token(manager.id, new_password)
                return jsonify(
                    code=200,
                    access_token=access_token,
                    user=manager.jsonify()
                )
            except:
                raise APIError(SYSTEM_ERROR)

        raise APIError(MISMATCHED_PASSWORD)


class ManagerNotifyConfig(MethodView):

    @check_access_token
    @check_manager_venue
    def post(self):
        new_order_config = request.json.get('noti_new_order')
        manager_id = request.json.get('manager_id')

        if not new_order_config:
            raise APIError(MISSING_PARAM)

        try:
            manager_notify_config = ManagerNotifyConfig.get_by_manager(manager_id)

            if not manager_notify_config:
                ManagerNotifyConfig.add(manager_id, new_order_config)
            else:
                manager_notify_config.update(new_order_config=new_order_config)

            return jsonify(
                code=200
            )
        except:
            raise APIError(SYSTEM_ERROR)



class ManagerAppUpdate(MethodView):

    @check_api_key
    def get(self):
        version_code = request.args.get('version_code')

        if not version_code:
            raise APIError(MISSING_PARAM)
 
        return jsonify(
            code=200,
            last_version=MANAGER_APP_VERSION,
            file_path=MANAGER_APP_DOWNLOAD_PATH
        )
