# -*- coding: utf-8 -*-

import json
from datetime import datetime, timedelta 
from flask import request, jsonify, abort 
from flask.views import MethodView 
from libs.decorator import check_api_key 
from model.user import User 
from model.manager import Manager 
from model.account import Account
from model.court import Court
from model.game import Game
from model.order import (
    Order, OrderNotFoundException, AccountNotFoundException,
    AccountBalanceNotEnoughException
)
from model.order.game import GameOrder
from model.order.participate import ParticipateOrder
from model.order.vip import VIPOrder
from model.order.charge import ChargeOrder
from model.order.trade_handler import pay_by_account_balance
from model.order.alipay_refund import AlipayRefund
from model.participates import Participates
from model.consts import (
    BOOK_TYPE_USER, COURT_STATUS_PARTICIPATE, GRADE_VIP_SLIVER,
    GRADE_VIP_GOLDEN, PRICE_VIP_SLIVER, PRICE_VIP_GOLDEN,
    PAYTYPE_GAME, PAYMETHOD_UNSET, PAYMETHOD_WEIXIN, GATEWAY_SECURITY,
    STATUS_PENDING, STATUS_COMPLETE, PAYTYPE_CHARGE, PAYTYPE_PARTICIPATE,
    VIP_PRICES, VIP_GRADES, PAYTYPE_VIP, STATUS_CONFIRMING, STATUS_REFUNDED, STATUS_REFUND_PENDING,
    ROLE_SYSADMIN, STATUS_DICT, PAYMETHOD_ALIPAY, REQUIRE_PAY_OFFLINE)

from view.api.error import (
    APIError, MISSING_PARAM, USER_NOT_FOUND, GAME_NOT_FOUND,
    ORDER_NOT_FOUND, ORDER_TYPE_ERROR, ORDER_EXPIRED,
    GAME_NOT_REQUIRED, GAME_REQUIRE_FULL, FORBIDDEN,
    NOT_ENOUGH_BALANCE, ACCOUNT_NOT_FOUND,
    COURT_CANNOT_BOOK, SYSTEM_ERROR, ACCOUNT_VIP_IS_SERVING,
    GAME_OVER, ORDER_VERIFY_ERROR, USER_NOT_MEET_REQUIRE, USER_NOT_MEET_REQUIRE_GENDER,
    USER_NOT_MEET_REQUIRE_LEVEL, USER_ALREADY_PARTICIPANT, ORDER_CANNOT_REFOUNDABLE
)
from config import TENCOURT
from model.wxpay import build_package
from model.wxpay.consts import partnerId
from model.notify import Notify
from model.wxpay.order import refund_from_tenpay, fetch_refunding_order
from model.alipay import save_refund_data
from model.alipay.order import is_callback_from_alipay, check_callback_sign
from model.order.order_notify import OrderRefundNotify

def make_game_order_from_request(request):
    court_id = request.json.get('court_id', '')
    user_id = request.json.get('user_id', '')
    start_time = request.json.get('start_time')
    end_time = request.json.get('end_time')

    pay_method = request.json.get('pay_method')
    if pay_method == None:
        pay_method = PAYMETHOD_UNSET

    if not (court_id and user_id and start_time and end_time):
        raise APIError(MISSING_PARAM)
    if isinstance(court_id, basestring):
        court_id = int(court_id)
    if isinstance(user_id, basestring):
        user_id = int(user_id)
    start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
    end_time = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')

    creator = User.get(user_id)
    if not creator:
        raise APIError(USER_NOT_FOUND)

    order = GameOrder.get_user_exist_order(creator.id, court_id, start_time, end_time)
    if order:
        return order

    court = Court.get(court_id)
    if not (court and court.can_book(start_time, end_time)):
        raise APIError(COURT_CANNOT_BOOK)

    game = Game.add(court_id, user_id, BOOK_TYPE_USER, start_time, end_time)
    if not game:
        raise APIError(SYSTEM_ERROR)

    # TODO args: pay_method and gateway
    amount = court.price(start_time, end_time)
    order = GameOrder.new(user_id, amount, PAYTYPE_GAME, datetime.now(),
                          None, STATUS_PENDING, pay_method,
                          GATEWAY_SECURITY,
                          game.id, court.id, court.venue.id,
                          court.venue.manager_id)

    if not order:
        # TODO
        raise APIError(SYSTEM_ERROR)
    return order


class BookCourtOrderAPI(MethodView):

    @check_api_key
    def get(self, order_id):
        if not order_id:
            raise APIError(MISSING_PARAM)
        order = Order.get(order_id)
        if not order:
            raise APIError(ORDER_NOT_FOUND)
        return jsonify(code=200, order=order.jsonify())

    @check_api_key
    def post(self):
        pay_method = request.json.get('pay_method')
        order = make_game_order_from_request(request)

        if not order:
            return

        if pay_method and int(pay_method) == PAYMETHOD_WEIXIN:
            packages = build_package(init_wx_param(order,request.remote_addr))
            return jsonify(code=200, order=order.jsonify(), wx_packages=packages)
        else:
            return jsonify(code=200, order=order.jsonify())


class BookCourtAndAccountPayOrderAPI(MethodView):

    @check_api_key
    def post(self):
        order = make_game_order_from_request(request)
        if not order:
            return
        try:
            pay_by_account_balance(order.id)
        except OrderNotFoundException:
            raise APIError(ORDER_NOT_FOUND)
        except AccountNotFoundException:
            raise APIError(ACCOUNT_NOT_FOUND)
        except AccountBalanceNotEnoughException:
            raise APIError(NOT_ENOUGH_BALANCE)
        order = Order.get(order.id)
        if not order:
            raise APIError(ORDER_NOT_FOUND)
        return jsonify(code=200, order=order.jsonify(), result='success')


class PendingOrderAPI(MethodView):

    @check_api_key
    def get(self):
        # TODO 验证用户身份
        uid = request.args.get('user_id')
        start = request.args.get('start', type=int, default=0)
        limit = request.args.get('count', type=int, default=10)
        end = start + limit

        if not uid:
            raise APIError(MISSING_PARAM)

        orders = Order.get_user_pending_orders(uid)
        return jsonify(
            code=200,
            orders=[o.jsonify() for o in orders[start: end]],
            start = start,
            count = limit,
        )


def make_participate_order_from_request(request):
    game_id = request.json.get('game_id')
    user_id = request.json.get('user_id')
    price = request.json.get('price')
    pay_method = request.json.get('pay_method')
    if pay_method == None:
        pay_method = PAYMETHOD_UNSET

    if not (game_id and user_id):
        raise APIError(MISSING_PARAM)
    user = User.get(user_id)
    if not user:
        raise APIError(USER_NOT_FOUND)
    game = Game.get(game_id)
    if not game:
        raise APIError(GAME_NOT_FOUND)

    if game.finished:
        raise APIError(GAME_OVER)

    if user.id == game.creator_id:
        raise APIError(FORBIDDEN)

    require = game.require
    if not require:
        raise APIError(GAME_NOT_REQUIRED)

    if user in Participates.get_participates_by_game(game_id):
        raise APIError(USER_ALREADY_PARTICIPANT)

    if not game.can_participate(user):
        raise APIError(GAME_REQUIRE_FULL)

    if require.pay_type == REQUIRE_PAY_OFFLINE:
        Participates.add(user_id, user.telephone, game_id)
        return

    exist_order = ParticipateOrder.get_user_exist_order(game_id, user_id)
    if exist_order:
        return exist_order

    # 比赛的约球会算上还未成交的订单，所以前面要先判断exist_order
    # if user in game.participates:
    #     raise APIError(USER_ALREADY_PARTICIPANT)
    # if not game.can_participate(user):
    #     raise APIError(GAME_REQUIRE_FULL)

    # TODO args: pay_method and gateway
    court = game.court
    if not price:
        price = game.require.price
    if game.court_id:
        venue_id = game.venue.id
        manager_id = game.venue.manager_id
    else:
        venue_id = None
        manager_id = None
    order = ParticipateOrder.new(user_id, price, PAYTYPE_PARTICIPATE, datetime.now(), None, STATUS_PENDING,
                pay_method,GATEWAY_SECURITY, game.id, game.court_id, venue_id, manager_id)

    if not order:
        raise APIError(SYSTEM_ERROR)
    return order


class ParticipateOrderAPI(MethodView):

    @check_api_key
    def post(self):
        pay_method = request.json.get('pay_method')
        order = make_participate_order_from_request(request)

        if not order:
            return jsonify(
                code=200,
            )

        if pay_method and int(pay_method) == PAYMETHOD_WEIXIN:
            packages = build_package(init_wx_param(order,request.remote_addr))
            return jsonify(code=200, order=order.jsonify(), wx_packages=packages)
        else:
            return jsonify(code=200, order=order.jsonify())


class ParticipateAndAccountPayOrderAPI(MethodView):

    @check_api_key
    def post(self):
        order = make_participate_order_from_request(request)
        if not order:
            return
        try:
            pay_by_account_balance(order.id)
        except OrderNotFoundException:
            raise APIError(ORDER_NOT_FOUND)
        except AccountNotFoundException:
            raise APIError(ACCOUNT_NOT_FOUND)
        except AccountBalanceNotEnoughException:
            raise APIError(NOT_ENOUGH_BALANCE)
        order = Order.get(order.id)
        if not order:
            raise APIError(ORDER_NOT_FOUND)
        return jsonify(code=200, order=order.jsonify(), result='success')


def make_vip_order_from_request(request):
    user_id = request.json.get('user_id')
    grade = request.json.get('vip_type')
    pay_method = request.json.get('pay_method')
    if pay_method == None:
        pay_method = PAYMETHOD_UNSET

    if not (user_id and grade):
        raise APIError(MISSING_PARAM)
    if not User.get(user_id):
        raise APIError(USER_NOT_FOUND)
    if not grade in VIP_GRADES:
        raise APIError(MISSING_PARAM)

    account = Account.get_or_add(user_id)
    if account.is_vip_serve:
        raise APIError(ACCOUNT_VIP_IS_SERVING)

    grade = VIP_GRADES[grade]
    amount = VIP_PRICES[grade]
    # TODO vip time delta
    effective_time = datetime.now()
    expire_time = effective_time + timedelta(365)
    order = VIPOrder.new(account.user_id, amount, PAYTYPE_VIP, datetime.now(),
                         None, STATUS_PENDING, pay_method,
                         GATEWAY_SECURITY, grade, effective_time,
                         expire_time)
    if not order:
        raise APIError(SYSTEM_ERROR)
    return order


class VIPOrderAPI(MethodView):

    @check_api_key
    def post(self):
        order = make_vip_order_from_request(request)
        if not order:
            return
        return jsonify(code=200, order=order.jsonify())


class VIPAndAccountPayOrderAPI(MethodView):

    @check_api_key
    def post(self):
        order = make_vip_order_from_request(request)
        if not order:
            return
        try:
            pay_by_account_balance(order.id)
        except OrderNotFoundException:
            raise APIError(ORDER_NOT_FOUND)
        except AccountNotFoundException:
            raise APIError(ACCOUNT_NOT_FOUND)
        except AccountBalanceNotEnoughException:
            raise APIError(NOT_ENOUGH_BALANCE)
        order = Order.get(order.id)
        if not order:
            raise APIError(ORDER_NOT_FOUND)
        return jsonify(code=200, order=order.jsonify(), result='success')

def init_wx_param(order,ip):
    parameter = {
        'bank_type': 'WX',
        'fee_type': '1',
        'input_charset': 'UTF-8',
        'notify_url': '%s/api/wxpay/notify/' % TENCOURT,
        'out_trade_no': order.id,
        'body' : order.body_desc(),
        #微信的支付单位是分
        'total_fee': int(order.price * 100),
        'partner': partnerId,
        'sign_type': 'MD5',
        'spbill_create_ip':ip
    }

    return parameter

def make_charge_order_from_request(request):
    user_id = request.json.get('user_id')
    amount = request.json.get('amount')
    pay_method = request.json.get('pay_method')
    if pay_method == None:
        pay_method = PAYMETHOD_UNSET

    if not (user_id and amount):
        raise APIError(MISSING_PARAM)
    if not User.get(user_id):
        raise APIError(USER_NOT_FOUND)
    account = Account.get_or_add(user_id)

    order = ChargeOrder.new(account.user_id, amount, PAYTYPE_CHARGE, datetime.now(),
                            None, STATUS_PENDING, pay_method,
                            GATEWAY_SECURITY)
    if not order:
        raise APIError(SYSTEM_ERROR)
    return order


class ChargeOrderAPI(MethodView):

    @check_api_key
    def post(self):
        order = make_charge_order_from_request(request)
        pay_method = request.json.get('pay_method')
        if not order:
            return

        if pay_method and int(pay_method) == PAYMETHOD_WEIXIN:
            packages = build_package(init_wx_param(order,request.remote_addr))
            return jsonify(code=200, order=order.jsonify(), wx_packages=packages)
        else:
            return jsonify(code=200, order=order.jsonify())

class VerifyOrderAPI(MethodView):

    @check_api_key
    def post(self):
        user_id = request.json.get('user_id', '')
        order_id = request.json.get('order_id', '')
        pay_method = request.json.get('pay_method')
        if not (user_id and order_id):
            raise APIError(MISSING_PARAM)
        user = User.get(user_id)
        if not user:
            raise APIError(USER_NOT_FOUND)
        account = Account.get_or_add(user_id)
        if not account:
            raise APIError(USER_NOT_FOUND)
        order = Order.get(order_id)
        if not order:
            raise APIError(ORDER_NOT_FOUND)
        if not order.verify(user):
            raise APIError(ORDER_VERIFY_ERROR)
        dct = {
            'code': 200,
            'order': order.jsonify(),
            'price': float(order.price),
            'account_balance': float(account.flush_get_balance())
        }
        if pay_method and int(pay_method) == PAYMETHOD_WEIXIN:
            packages = build_package(init_wx_param(order,request.remote_addr))
            dct['wx_packages'] = packages
        return jsonify(dct)


class AccountPayOrderAPI(MethodView):

    @check_api_key
    def post(self):
        order_id = request.json.get('order_id')
        user_id = request.json.get('user_id')

        if not (order_id and user_id):
            raise APIError(MISSING_PARAM)

        user = User.get(user_id)
        if not user:
            raise APIError(USER_NOT_FOUND)

        order = Order.get(order_id)
        if not order:
            raise APIError(ORDER_NOT_FOUND)

        if not order.pay_verify(user):
            raise APIError(ORDER_VERIFY_ERROR)

        try:
            pay_by_account_balance(order.id)
        except OrderNotFoundException:
            raise APIError(ORDER_NOT_FOUND)
        except AccountNotFoundException:
            raise APIError(ACCOUNT_NOT_FOUND)
        except AccountBalanceNotEnoughException:
            raise APIError(NOT_ENOUGH_BALANCE)
        order = Order.get(order.id)
        # TODO
        # if not order
        return jsonify(code=200, result='success', order=order.jsonify())


class RefundOrderAPI(MethodView):

    @check_api_key
    def post(self):
        game_id = request.json.get('game_id')
        user_id = request.json.get('user_id')
        reason = int(request.json.get('reason', 1))
        if not (game_id and user_id):
            raise APIError(MISSING_PARAM)

        game = Game.get(game_id)
        if not (game and str(game.creator_id) == str(user_id)):
            raise APIError(ORDER_NOT_FOUND)

        order = game.get_game_order()
        if not (order and order.pay_type == PAYTYPE_GAME):
            raise APIError(ORDER_NOT_FOUND)

        if not order.is_refundable(reason):
            raise APIError(ORDER_CANNOT_REFOUNDABLE)
        order.refund(reason=reason)
        account = Account.get(order.creator_id)
        dct = {
            'code': 200,
            'order': order.jsonify(),
            'price': float(order.price),
            'account_balance': float(account.flush_get_balance())
        }
        return jsonify(dct)


class GameOrderListAPI(MethodView):

    @check_api_key
    def get(self):
        start = request.args.get('start', type=int, default=0)
        limit = request.args.get('count', type=int, default=10)
        total_counts = GameOrder.total_counts()
        orders = GameOrder.gets_all(start, limit)

        return jsonify(
            recordCount=total_counts,
            data=[order.jsonify() for order in orders if order]
        )

class OrderConfirm(MethodView):

    @check_api_key
    def post(self):
        game_id = request.json.get('game_id')
        user_id = request.json.get('user_id')
        order_id = request.json.get('order_id')
        if not (game_id and user_id and order_id):
            raise APIError(MISSING_PARAM)

        order = Order.get(order_id)
        if order.game_id != game_id and order.creator_id != user_id and order.status != STATUS_CONFIRMING:
            raise APIError(FORBIDDEN)

        try:
            GameOrder.complete(order_id)
            return jsonify(
                code=200,
                msg=u'确认修改订单状态成功',
            )
        except Exception:
            raise APIError(SYSTEM_ERROR)


class UserRefundGameorderFromAlipayCallbackAPI(MethodView):

    def post(self):
        from tools.celery_logger import refund_logger_log
        notify_id=request.values.get('notify_id')
        if notify_id and is_callback_from_alipay(notify_id):
            data = dict(
               notify_time = request.form.get('notify_time'),
               notify_type = request.form.get('notify_type'),
               notify_id = request.form.get('notify_id'),
               batch_no = request.form.get('batch_no'),
               success_num = request.form.get('success_num'),
               result_details = request.form.get('result_details')
            )
            callback_sign = request.form.get('sign')
            if check_callback_sign(callback_sign, data):
                batch_no = unicode(request.form.get('batch_no'))
                result_details = unicode(request.form.get('result_details'))
                order_id = int(batch_no[8:])
                order = Order.get(order_id)
                if order and (result_details.find('^SUCCESS') != -1):
                    AlipayRefund.refund(order)
                    OrderRefundNotify.add(
                        order_id,
                        int(batch_no),
                        PAYMETHOD_ALIPAY,
                        json.dumps(request.form)
                    )
                save_refund_data(request.form)
                refund_logger_log.delay(20, 'alipay ' +  str(request.form))
                return 'success'
            else:
                abort(400)
        else:
            abort(400)

class AdminFetchRefundingOrderFromPaymentPlatform(MethodView):

    @check_api_key
    def get(self):
        manager_id = request.json.get('manager_id')
        order_id = request.json.get('order_id')
        if not (manager_id or order_id):
            raise APIError(MISSING_PARAM)
        manager = Manager.get(manager_id)
        if manager.role != ROLE_SYSADMIN:
            raise APIError(FORBIDDEN)
        order = Order.get(order_id)
        if not order: 
            raise APIError(ORDER_NOT_FOUND)
        if not (order.status in (STATUS_REFUND_PENDING, STATUS_REFUNDED, STATUS_CONFIRM_EXPIRED)):
            raise APIError(ORDER_NOT_IN_REFUNDING)

        if order.pay_method == PAYMETHOD_WEIXIN:
            r = fetch_refunding_order(order.transaction_id)
            if r:
                return jsonify(code=200, refunding_order_status=r)
            else:
                raise APIError(SYSTEM_ERROR)
        else:
            refunding_order_status = dict(msg=STATUS_DICT.get(order.status), refund_fee=order.amount)
            return jsonify(code=200, refunding_order_status=refunding_order_status)
