# -*- coding: utf-8 -*-

import datetime
from flask import request, jsonify
from flask.views import MethodView

from libs.decorator import check_api_key, login_required, check_access_token, check_manager_venue
from model.order import Order
from model.order.game import GameOrder
from model.order.participate import ParticipateOrder
from model.consts import PAYTYPE_GAME, PAYTYPE_PARTICIPATE, STATUS_COMPLETE, REQUIRE_PAY_OFFLINE, STATUS_PENDING, GATEWAY_SECURITY
from model.game import Game
from model.user import User
from model.participates import Participates
from .error import APIError, INVALID_URL, MISSING_PARAM, USER_NOT_FOUND, GAME_NOT_FOUND, GAME_OVER, GAME_NOT_REQUIRED, \
                    USER_ALREADY_PARTICIPANT, GAME_REQUIRE_FULL, SYSTEM_ERROR


GAME_TYPES = {
    'book': PAYTYPE_GAME,
    'participate': PAYTYPE_PARTICIPATE
}


class UserGameAPI(MethodView):

    @check_api_key
    def get(self, uid, pay_type):
        # TODO 检查用户身份
        start = request.args.get('start', type=int, default=0)
        limit = request.args.get('count', type=int, default=10)
        end = start + limit

        if not (uid and pay_type):
            raise APIError(MISSING_PARAM)
        if pay_type not in GAME_TYPES:
            raise APIError(INVALID_URL)

        pay_type = GAME_TYPES[pay_type]
        book_query = {
            'creator_id': uid,
            'status': STATUS_COMPLETE,
            'pay_type': PAYTYPE_GAME,
        }
        participate_query = {
            'creator_id': uid,
            'status': STATUS_COMPLETE,
            'pay_type': PAYTYPE_PARTICIPATE,
        }
        if pay_type == PAYTYPE_PARTICIPATE:
            ps = Order.filter(start=None, limit=None, **participate_query)
            gs = Order.filter(start=None, limit=None, **book_query)

            orders = filter(lambda o: o.game and o.game.require, ps + gs)
        else:
            gs = Order.filter(start=None, limit=None, **book_query)

            orders = filter(lambda o: o.game and not o.game.require, gs)

        games = [order.game for order in orders if order and order.game and order.game.venue]
        games = sorted(games, key=lambda g: g.start_time, reverse=True)
        games = games[start: end]
        games = [game.jsonify() for game in games]

        return jsonify(
            code=200,
            games=games,
            start=start,
            count=limit,
        )

class GameAPI(MethodView):
    
    @check_api_key
    def get(self):
        gid = request.args.get('game_id')
        
        if not gid:
            raise APIError(MISSING_PARAM)
        game = Game.get(gid)
        if not game:
            raise APIError(GAME_NOT_FOUND)   
        return jsonify(
            code=200,
            game=game.jsonify(),
        ) 


class GamesNotifyAPI(MethodView):

    @check_api_key
    def get(self):
        rst = {}
        uid = request.args.get('user_id')
        count = request.args.get('uid', type=int, default=1)

        if not uid:
            raise APIError(MISSING_PARAM)

        if not User.get(uid):
            raise APIError(USER_NOT_FOUND)

        now = datetime.datetime.now()
        rst['book'], rst['participate'] = Game.get_lasted_game(uid, now, count)
        pending_orders = Order.get_user_pending_orders(uid)

        return jsonify(
            code=200,
            games=rst,
            pending_orders=[order.jsonify() for order in pending_orders],
        )


class ManagerGameAPI(MethodView):

    @check_access_token
    @check_manager_venue
    def get(self):
        start = request.args.get('start')
        limit = request.args.get('limit')
        venue_id = request.args.get('venue_id')

        if not venue_id:
            raise APIError(MISSING_PARAM)

        games = GameOrder.get_effective_games_by_venue(venue_id, start, limit)

        return jsonify(
            games=[game.jsonify() for game in games]
        )


class GameParticipate(MethodView):

    @check_api_key
    def post(self):
        telephone = request.json.get('telephone')
        game_id = request.json.get('game_id')
        price = request.json.get('price')
        pay_method = request.json.get('pay_method')

        if not (game_id):
            raise APIError(MISSING_PARAM)

        game = Game.get(game_id)
        if not (game):
            raise APIError(GAME_NOT_FOUND)

        if game.finished:
            raise APIError(GAME_OVER)

        require = game.require
        if not require:
            raise APIError(GAME_NOT_REQUIRED)

        if Participates.is_participate_game(telephone, game_id):
            raise APIError(USER_ALREADY_PARTICIPANT)

        if require.max_users == len(game.participates):
            raise APIError(GAME_REQUIRE_FULL)

        if require.pay_type == REQUIRE_PAY_OFFLINE:
            Participates.add(None, telephone, game_id)
            return

        exist_order = ParticipateOrder.get_user_exist_order_by_tel(game_id, telephone)
        if exist_order:
            return exist_order

        if game.court_id:
            venue_id = game.venue.id
            manager_id = game.venue.manager_id
        else:
            venue_id = None
            manager_id = None

        order = ParticipateOrder.new(None, price, PAYTYPE_PARTICIPATE, datetime.datetime.now(),
                                     None, STATUS_PENDING, pay_method, GATEWAY_SECURITY,
                                     game.id, game.court_id, venue_id, manager_id, telephone)

        if not order:
            raise APIError(SYSTEM_ERROR)


