# coding: utf-8

from datetime import datetime
from flask import request, jsonify
from flask.views import MethodView
from libs.decorator import check_manager_venue, check_access_token
from view.api.error import APIError, INVALID_URL, MISSING_PARAM, COURT_CANNOT_BOOK, SYSTEM_ERROR, GAME_NOT_FOUND, \
                           FORBIDDEN, ORDER_NOT_MERCHANT, ORDER_NOT_FOUND
from model.court import Court
from model.admin_order import AdminOrder
from model.consts import BOOK_TYPE_MANAGER
from model.game import Game
from model.member import Member
from model.consts import BOOK_TYPE_USER, BOOK_TYPE_MANAGER, STATUS_PENDING

MerchantBook = 1
MerchantReserve = 2

class ManagerConfirmBook(MethodView):

    @check_access_token
    def post(self):
        game_id = request.json.get('game_id')
        game = Game.get(game_id)
        if not game:
            raise APIError(ORDER_NOT_FOUND)
        if game.book_type != BOOK_TYPE_MANAGER:
            raise APIError(ORDER_NOT_MERCHANT)

        admin_order = AdminOrder.get_by_game(game.id)
        if not admin_order:
            raise  APIError(ORDER_NOT_FOUND)
        admin_order.complete()
        return jsonify(
            code=200,
            game=game.jsonify()
        )

class ManagerBookCourtAPI(MethodView):

    @check_access_token
    @check_manager_venue
    def post(self):
        book_type =int(request.json.get('type'))
        venue_id = request.json.get('venue_id')
        court_id = int(request.json.get('court_id'))
        user_info = request.json.get('user_info')
        intro = request.json.get('intro')
        start_time = request.json.get('start_time')
        end_time = request.json.get('end_time')
        manager_id = int(request.json.get('manager_id'))

        if not (book_type and venue_id and court_id and start_time and end_time and manager_id):
            raise APIError(MISSING_PARAM)

        if book_type not in (MerchantBook, MerchantReserve):
            raise APIError(INVALID_URL)

        start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
        end_time = datetime.strptime(end_time, '%Y-%m-%d %H:%M:%S')

        court = Court.get(court_id)

        if not court or (court and not court.can_book(start_time, end_time)):
            raise APIError(COURT_CANNOT_BOOK)

        game = Game.add(court_id, manager_id, BOOK_TYPE_MANAGER, start_time, end_time, comment=intro)

        if(book_type == MerchantBook):

            member = Member.get_by_venue_and_identity(venue_id, user_info)

            if member:
                amount = court.price(start_time, end_time, member_type_id=member.member_type_id)
                AdminOrder.add(member.id, user_info, game.id, amount, STATUS_PENDING)
            else:
                amount = court.price(start_time, end_time)
                AdminOrder.add(None, user_info, game.id, amount, STATUS_PENDING)

        return jsonify(
            code=200,
            game=game.jsonify()
        )

    @check_access_token
    @check_manager_venue
    def delete(self):
        try:
            game_id = request.args.get('game_id')
            manager_id = request.args.get('manager_id')

            if not game_id:
                raise APIError(MISSING_PARAM)

            game = Game.get(game_id)

            if not game:
                raise APIError(GAME_NOT_FOUND)

            if game.book_type != BOOK_TYPE_MANAGER or (game.creator_id != int(manager_id)):
                raise APIError(FORBIDDEN)


            order = AdminOrder.get_by_game(game_id)
            if order:
                #删除订场订单
                order.refund()
            else:
                #删除预留
                game.delete()

            return jsonify(
                code=200
            )
        except:
            raise APIError(SYSTEM_ERROR)




