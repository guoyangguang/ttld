# -*- coding:utf-8 -*-

from datetime import datetime
from flask import request, jsonify
from flask.views import MethodView
from libs.decorator import check_api_key
from model.user import User
from model.game import Game
from model.require import Require
from model.city import City
from model.consts import TENNIS_ITEM,ANY_SPORTS_ITEM, BOOK_TYPE_NOT_BOOKING, REQUIRE_PAY_ONLINE
from tools.postman import notify_recommend_game
from view.utils import is_new_platform_version
from view.api.error import (
    MISSING_PARAM, USER_NOT_FOUND, GAME_NOT_FOUND,
    FORBIDDEN, SYSTEM_ERROR, GAME_OVER, APIError
)


class RequireAPI(MethodView):

    @check_api_key
    def post(self):
        creator_id = request.json.get('user_id')
        game_id = request.json.get('game_id')
        max_users = request.json.get('max_users')
        tennis_level = request.json.get('tennis_level')
        age = request.json.get('age')
        gender = request.json.get('gender')
        tennis_age = request.json.get('tennis_age')
        is_owner_play = request.json.get('is_owner_play')
        desc = request.json.get('desc')
        price = request.json.get('price')
        app_version = request.json.get('app_version')
        address = request.json.get('address')
        sports_type = request.json.get('sports_type', TENNIS_ITEM)
        pay_type = request.json.get('pay_type', REQUIRE_PAY_ONLINE)
        start_time = request.json.get('start_time')
        end_time = request.json.get('end_time')

        if max_users and str(max_users).isdigit():
            max_users = int(str(max_users))
        else:
            max_users = 0

        if not creator_id:
            raise APIError(MISSING_PARAM)

        user = User.get(creator_id)
        if not user:
            raise APIError(USER_NOT_FOUND)

        if game_id:
            game = Game.get(game_id)
            if not game:
                raise APIError(GAME_NOT_FOUND)

            if game.finished:
                raise APIError(GAME_OVER)

            if str(game.creator_id) != str(creator_id):
                raise APIError(FORBIDDEN)
        else:
            game = Game.add(None, creator_id, BOOK_TYPE_NOT_BOOKING, start_time, end_time)

        require = game.require
        if not require:
            if price < 0:
                price = Require.compute_price(game.price, max_users)
            require = Require.new(creator_id, game.id, age, gender, tennis_age,
                                  tennis_level, price, is_owner_play, desc, pay_type, address, sports_type, max_users)
            notify_recommend_game.delay(game)
            if not require:
                raise APIError(SYSTEM_ERROR)

        if app_version:
            return jsonify(
                code=200,
                game=game.jsonify()
            )
        else:
            return jsonify(
                code=200,
                require=require.jsonify()
            )


class RequireSearchAPI(MethodView):

    @check_api_key
    def get(self):
        age = request.args.get('age')
        gender = request.args.get('gender')
        tennis_age = request.args.get('tennis_age', type=int)
        tennis_level = request.args.get('tennis_level', type=float)
        start_time = request.args.get('start_time')
        hour = request.args.get('hour', type=int)
        stage = request.args.get('stage', type=int)
        user_id = request.args.get('user_id')
        city_id = request.args.get('city_id', type=int, default=1)
        sports_type = request.args.get('sports_type')
        #若无参数时默认为网球
        sports_type = ANY_SPORTS_ITEM if is_new_platform_version() else TENNIS_ITEM
        start = request.args.get('start', type=int, default=0)
        limit = request.args.get('count', type=int, default=10)
        end = start + limit
        
        current_user = User.get(user_id) if user_id else None

        if start_time:
            try:
                start_time = datetime.strptime(start_time, '%Y-%m-%d')
            except ValueError:
                start_time = None

        requires = Require.search(age, gender, tennis_age, tennis_level,
                                  start_time, hour, stage, current_user, city_id, sports_type)
        city = City.get(city_id)

        if requires:
            games = [require.game for require in requires]
        else:
            games = []

        return jsonify(
            code=200,
            games=[game.jsonify() for game in games[start: end]],
            city=city.name,
            start=start,
            count=limit,
        )


class RequireDescAPI(MethodView):

    @check_api_key
    def post(self):
        user_id = request.json.get('user_id')
        require_id = request.json.get('require_id')
        desc = request.json.get('desc')
        if not (user_id and require_id):
            raise APIError(MISSING_PARAM)

        require = Require.get(require_id)
        if require.creator_id != user_id:
            raise APIError(FORBIDDEN)

        require.update_desc(desc)

        return jsonify(
            code=200,
            require=require.jsonify(),
        )
