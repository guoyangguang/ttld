# coding: utf-8

import json
import time
from flask import request, abort, make_response
from flask.views import MethodView
from model.order import Order, get_order_class
from model.wxpay import save_notify_data
from model.wxpay.consts import WEIXIN_SERVICE_TOKEN
from model.consts import PAYMETHOD_WEIXIN, PAYTYPE_GAME
from model.order.order_notify import OrderPayNotify
from libs.decorator import check_api_key
import hashlib


class WxPayNotifyAPI(MethodView):

    def post(self):
        out_trade_no = request.args.get('out_trade_no', type=int)
        trade_state = request.args.get('trade_state', type=int)
        order = Order.get(out_trade_no)

        if not order:
            abort(400)

        if trade_state == 0:
            order_cls = get_order_class(order.pay_type)
            if order.pay_type == PAYTYPE_GAME:
                order_cls.received_payment(order.id)
            else:
                order_cls.complete(order.id)

            OrderPayNotify.add(out_trade_no, PAYMETHOD_WEIXIN, json.dumps(request.args))
        save_notify_data(request.args)
        return 'success'

class WxServiceVerifyAPI(MethodView):

    def get(self):
        token = WEIXIN_SERVICE_TOKEN
        query = request.args
        signature = query.get('signature')
        timestamp = query.get('timestamp')
        nonce = query.get('nonce')
        echostr = query.get('echostr')

        s = [timestamp, nonce, token]
        s.sort()
        s= ''.join(s)
        if(hashlib.sha1(s).hexdigest() == signature):
            return echostr
        return
