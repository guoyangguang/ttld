from flask import request, jsonify
from flask.views import MethodView
from model.notify import Notify
from model.admin_notify import AdminNotify, ADMIN_NEW_BOOK_NOTIFY, ADMIN_CANCEL_BOOK_NOTIFY
from model.user import User
from model.consts import READ_STATUS
from libs.decorator import check_api_key, check_access_token
from view.api.error import (
    APIError, MISSING_PARAM,USER_NOT_FOUND, FORBIDDEN
)

class ManagerNotifyCountAPI(MethodView):
    @check_api_key
    @check_access_token
    def get(self):
        manager_id = request.args.get('manager_id')
        notify_count = AdminNotify.get_unread_count(manager_id)
        return  jsonify(
            code=200,
            manager_id=manager_id,
            notify_count=notify_count,
        )

class ManagerNotifyAPI(MethodView):
    @check_api_key
    @check_access_token
    def get(self):
        manager_id = request.args.get('manager_id')
        type = int(request.args.get('type'))
        notifies = None
        if(type == ADMIN_NEW_BOOK_NOTIFY):
            notifies = AdminNotify.get_unread_book_notifies(manager_id)
        elif(type == ADMIN_CANCEL_BOOK_NOTIFY):
            notifies = AdminNotify.get_unread_cancel_notifies(manager_id)
        else:
            APIError(MISSING_PARAM)
        AdminNotify.read_notifies(notifies)
        return jsonify(
            code=200,
            manager_id=manager_id,
            notifies=[notify.jsonify() for notify in notifies if notify] if notifies else []
        )

class ManageNotifyReadAPI(MethodView):
    @check_api_key
    @check_access_token
    def post(self):
        notify_id = request.json.get('notify_id')
        AdminNotify.read_notifies_id([notify_id])

        return jsonify(
            code=200,
        )


class UserNotifyCountAPI(MethodView):

    @check_api_key
    def get(self):
        user_id = request.args.get('user_id')
        if not user_id:
            raise APIError(MISSING_PARAM)

        if not User.get(user_id):
            raise APIError(USER_NOT_FOUND)

        count = Notify.get_user_unread_notify_count(user_id)

        return jsonify(
            code=200,
            user_id=user_id,
            notify_count=count,
        )


class UserNotifyAPI(MethodView):

    @check_api_key
    def get(self):
        user_id = request.args.get('user_id')

        if not user_id:
            raise APIError(MISSING_PARAM)

        if not User.get(user_id):
            raise APIError(USER_NOT_FOUND)

        notifies = Notify.get_user_notify(user_id, start=0, limit=20)

        return jsonify(
            code=200,
            user_id=user_id,
            notifies=[notify.jsonify() for notify in notifies] if notifies else [],
        )


class NotifyReadAPI(MethodView):

    @check_api_key
    def post(self):
        notify_id = request.json.get('notify_id')
        user_id = request.json.get('user_id')

        if not notify_id or not user_id:
            raise APIError(MISSING_PARAM)

        notify = Notify.get(notify_id)

        if notify.user_id != int(user_id):
            raise APIError(FORBIDDEN)

        notify.update(status=READ_STATUS['read'])
        return jsonify(
            code=200,
        )

