# -*- coding: utf-8 -*-

import json
import requests
from flask import request, abort
from flask.views import MethodView

from model.order import Order, get_order_class
from model.alipay import save_notify_data
from model.alipay.consts import (ALIPAYAPI_PAYMENT_GATEWAY, ALIPAYAPI_PARTNER_ID, ALIPAYAPI_ORIGINAL_PARTNER_ID)
from model.consts import PAYMETHOD_ALIPAY, PAYTYPE_GAME
from model.order.order_notify import OrderPayNotify


class AlipayNotifyAPI(MethodView):

    def post(self):
        # 基本参数
        notify_id = request.form.get('notify_id')
        notify_time = request.form.get('notify_time')
        notify_type = request.form.get('notify_type')
        sign_type = request.form.get('sign_type')
        sign = request.form.get('sign')

        # 业务参数
        out_trade_no = request.form.get('out_trade_no')
        subject = request.form.get('subject')
        payment_type = request.form.get('payment_type')
        trade_no = request.form.get('trade_no')
        trade_status = request.form.get('trade_status')
        seller_id = request.form.get('seller_id')
        seller_email = request.form.get('seller_email')
        buyer_id = request.form.get('buyer_id')
        buyer_email = request.form.get('buyer_email')
        total_fee = request.form.get('total_fee')
        quantity = request.form.get('quantity')
        price = request.form.get('price')
        body = request.form.get('body')
        gmt_create = request.form.get('gmt_create')
        gmt_payment = request.form.get('gmt_payment')
        is_total_fee_adjust = request.form.get('is_total_fee_adjust')
        use_coupon = request.form.get('use_coupon')
        discount = request.form.get('discount')

        order = Order.get(out_trade_no)

        if not check_notify_id(notify_id=notify_id, partner_id=seller_id) or not order:
            abort(400)

        if not request.form.get('refund_status'): # 确保是充值的逻辑
            if trade_status in ('TRADE_SUCCESS', 'TRADE_FINISHED'):
                order_cls = get_order_class(order.pay_type)
                if order.pay_type == PAYTYPE_GAME:
                    order_cls.received_payment(order.id)
                else:
                    order_cls.complete(order.id)

                OrderPayNotify.add(out_trade_no, order.pay_type, json.dumps(request.form)) # 只写入交易成功的订单

        save_notify_data(request.form)
        return 'success'


def check_notify_id(notify_id, partner_id=ALIPAYAPI_PARTNER_ID):
    if partner_id not in [ALIPAYAPI_ORIGINAL_PARTNER_ID,ALIPAYAPI_PARTNER_ID]:
        return False
    params = {
        'service': 'notify_verify',
        'partner': partner_id,
        'notify_id': notify_id,
    }

    query = '&'.join('='.join([str(k), str(v)])
                     for k, v in params.iteritems())

    verify_url = '%s%s' % (ALIPAYAPI_PAYMENT_GATEWAY, query)

    res = requests.get(verify_url)

    return str(res.text) == 'true'
