#encoding=utf-8

from flask.views import MethodView
from flask import request, json, jsonify
from model.user import User
from model.require import Require
from model.comment import Comment
from .error import (
    APIError, REQUIRE_NOT_FOUND, MISSING_PARAM, USER_NOT_FOUND, COMMENT_NOT_FOUND
)
from libs.decorator import check_api_key

COMMENT_FETCH_LIMIT = 10

class CommentsAPI(MethodView):
    
    @check_api_key
    def post(self, id):
        body = request.json.get('body')
        user_id = request.json.get('user_id')
        comment_id = request.json.get('comment_id')
        if not(body and user_id): raise APIError(MISSING_PARAM)
        require = Require.get(id) 
        if not require: raise APIError(REQUIRE_NOT_FOUND)
        current_user = User.get(id=user_id)
        if not current_user: raise APIError(USER_NOT_FOUND)

        new_comment = Comment(body=body)
        if comment_id:
            if not Comment.get(comment_id): raise(COMMENT_NOT_FOUND)
            new_comment.comment_id = comment_id
        comment = Comment.create(current_user, require, new_comment)
        return jsonify(
            code=200,
            #TODO 暂时为了支持老版本，仍然返回comments，在未来的版本会删掉
            comments=Comment.get_comments_dict(require=require),
            new_comment = comment.jsonify()
        )

    @check_api_key
    def get(self, id): 
        require = Require.get(id)
        start = int(request.args.get('start')) if request.args.get('start') else 0
        limit = int(request.args.get('count')) if request.args.get('count') else 100
        if require:
            comments = Comment.get_comments_dict(require=require, start=start, limit=limit)
            return jsonify(
                code=200,
                comments=comments
            )
        else: 
            raise APIError(REQUIRE_NOT_FOUND)
