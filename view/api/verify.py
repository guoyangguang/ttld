# -*- coding:utf-8 -*-

from flask import request, jsonify
from flask.views import MethodView

from libs.decorator import check_api_key, login_required, check_access_token
from model.verify import Verify, AdminVerify
from model.user import User
from model.manager import Manager
from view.utils import check_phone_number
from view.api.error import (APIError, MISSING_PARAM, HTTP_METHOD_ERROR,
                            INVALID_VERIFY_CODE, INVALID_PHONE_NUMBER,
                            REGISTERED_PHONE_NUMBER, SEND_SMS_VERIFY_EXCEPTION,
                            USER_NOT_FOUND, SYSTEM_ERROR)


class VerifyCodeAPI(MethodView):

    @check_api_key
    def get(self):
        raise APIError(HTTP_METHOD_ERROR)

    @check_api_key
    def post(self):
        phone = request.json.get('telephone', '')
        verify_code = request.json.get('verify_code', '')

        if not phone or not verify_code:
            raise APIError(MISSING_PARAM)

        if str(verify_code) == Verify.get_code(phone):
            return jsonify(code='200', telephone=phone, r=0)

        raise APIError(INVALID_VERIFY_CODE)


class VerifyPhoneAPI(MethodView):

    @check_api_key
    def get(self):
        raise APIError(HTTP_METHOD_ERROR)

    @check_api_key
    def post(self):
        phone = request.json.get('telephone', '')
        if not check_phone_number(str(phone)):
            raise APIError(INVALID_PHONE_NUMBER)

        if User.is_phone_active(phone):
            raise APIError(REGISTERED_PHONE_NUMBER)

        #TODO set send interval time, check api key

        verify_code = Verify.gen_code()
        try:
            Verify.add(phone, verify_code)
            return jsonify(code='200', telephone=phone, verify_code=str(verify_code))
        except Exception:
            raise APIError(SEND_SMS_VERIFY_EXCEPTION)


class ResetPasswordVerifyAPI(MethodView):

    @check_api_key
    def post(self):
        phone = request.json.get('telephone', '')
        if not check_phone_number(str(phone)):
            raise APIError(INVALID_PHONE_NUMBER)
        if User.is_phone_active(phone):
            verify_code = Verify.gen_code()
            try:
                Verify.add(phone, verify_code)
                return jsonify(code='200', telephone=phone, verify_code=str(verify_code))
            except Exception:
                raise APIError(SEND_SMS_VERIFY_EXCEPTION)
        return APIError(USER_NOT_FOUND)


class ManagerVerifyPhoneAPI(MethodView):

    @check_access_token
    def post(self):
        manager_id = request.json.get('manager_id', '')
        phone = request.json.get('telephone')

        if not manager_id:
            raise APIError(MISSING_PARAM)

        manager = Manager.get(manager_id)

        if not manager:
            raise APIError(USER_NOT_FOUND)



        verify_code = AdminVerify.gen_code()
        AdminVerify.add(phone, verify_code)
        return jsonify(
            code=200,
            verify={
                'telephone': phone,
                'verify_code': str(verify_code),
            }
        )
        raise APIError(SEND_SMS_VERIFY_EXCEPTION)
