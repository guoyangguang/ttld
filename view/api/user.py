# coding: utf-8

from datetime import datetime

from flask import request, jsonify, current_app
from flask.views import MethodView
from model.user import User
from model.user_info import UserInfo
from model.photo import Photo, allowed_file
from model.verify import Verify
from model.token import Token
from model.consts import USER_STATUS_ACTIVE, K_USER, USER_GENDERS

from libs.decorator import check_api_key
from view.utils import (
    check_user_password, gen_access_token, encrypt,
    check_phone_number, get_full_user_dict, is_age_str_format_version
)
from view.api.error import (
    APIError, MISSING_PARAM, MISMATCHED_PASSWORD,
    USER_NOT_FOUND, INVALID_VERIFY_CODE,
    REGISTERED_PHONE_NUMBER, REGISTERED_FAIL,
    INVALID_PHONE_NUMBER, UPDATE_USER_ERROR,
    SYSTEM_ERROR, ACCOUNT_HAS_TELEPHONE, ACCOUNT_NOT_FOUND
)
from config import USER_APP_VERSION, USER_APP_DOWNLOAD_PATH

class UserLoginAPI(MethodView):

    @check_api_key
    def post(self):
        phone = request.json.get('telephone', '')
        password = str(request.json.get('password', ''))
        app_key = request.json.get('app_key')

        if not (phone and password and app_key):
            raise APIError(MISSING_PARAM)

        if check_user_password(phone, password):
            user = User.get_by_telephone(phone)
            access_token = gen_access_token(user.id, app_key)
            return jsonify(code='200', access_token=access_token,
                           user=get_full_user_dict(user))
        raise APIError(MISMATCHED_PASSWORD)


class ResetPasswordAPI(MethodView):

    @check_api_key
    def post(self):
        # 密码在客户端已加密
        app_key = request.json.get('app_key')
        password = request.json.get('password')
        phone = request.json.get('telephone')
        verify_code = request.json.get('verify_code')

        if not (app_key and password and phone and verify_code):
            raise APIError(MISSING_PARAM)

        user = User.get_by_telephone(phone)
        if not user:
            raise APIError(USER_NOT_FOUND)

        if str(verify_code) == Verify.get_code(phone):
            user.set_password(password)
            access_token = gen_access_token(user.id, app_key)
            return jsonify(code='200', access_token=access_token,
                           user=get_full_user_dict(user))
        raise APIError(INVALID_VERIFY_CODE)

class UserWXLogin(MethodView):
    @check_api_key
    def post(self):
        app_key = request.json.get('app_key')
        wx_user_id = str(request.json.get('wx_user_id'))
        wx_gender = request.json.get('wx_gender')

        if not wx_user_id or (wx_gender and wx_gender not in USER_GENDERS) :
            raise  APIError(MISSING_PARAM)
        new_account = False
        user = User.get_by_wx(wx_user_id)
        if not user:
            try:
                user = User.add(wx_id=wx_user_id, gender=wx_gender, status=USER_STATUS_ACTIVE)
                #已设默认名字，但此处要返回空名字，让客户端展示资料补充界面给用户
                user.name = None
                new_account = True
            except Exception as e:
                raise APIError(REGISTERED_FAIL)

        access_token = gen_access_token(user.id, app_key)
        Token.add(access_token, user.id, app_key)
        return jsonify(code='200', access_token=access_token, user=get_full_user_dict(user), new_account=new_account)

#Not USE
class UserAddTelephone(MethodView):
    @check_api_key
    def post(self):
        verify_code = request.json.get('verify_code')
        user_id = request.json.get('user_id')
        phone = request.json.get('telephone')
        password = request.json.get('password', '')
        user = User.get(user_id)


        if not (user_id and phone and verify_code):
            raise APIError(MISSING_PARAM)

        if verify_code != Verify.get_code(phone):
            raise APIError(INVALID_VERIFY_CODE)

        if not check_phone_number(phone):
            raise APIError(INVALID_PHONE_NUMBER)

        if not user:
            raise APIError(ACCOUNT_NOT_FOUND)

        if user.telephone:
            raise  APIError(ACCOUNT_HAS_TELEPHONE)

        user = user.update(telephone=phone)
        if password != None:
            user.set_password(password)
        return jsonify(code='200', user=get_full_user_dict(user))

class UserMergeAccount(MethodView):
    @check_api_key
    def post(self):
        merged_user_id = request.json.get('merged_userid')
        host_phone = request.json.get('host_telephone')
        host_password = request.json.get('host_password')

        if not (merged_user_id and host_phone and host_password):
            raise APIError(MISSING_PARAM)

        if check_user_password(host_phone, host_password):
            merged_user = User.get(merged_user_id)
            if merged_user.telephone:
                raise APIError(ACCOUNT_HAS_TELEPHONE)
            host_user = User.get_by_telephone(host_phone)
            User.merge(merged_user.id,host_user.id)
            host_user = User.get(host_user.id)
            return jsonify(code='200',user=get_full_user_dict(host_user))
        else:
            raise APIError(MISMATCHED_PASSWORD)


class UserAPI(MethodView):

    @check_api_key
    def get(self, uid=None):
        user = User.get(uid)
        if not user:
            raise APIError(USER_NOT_FOUND)
        return jsonify(code='200', user=get_full_user_dict(user))

    @check_api_key
    def put(self, uid):
        user = User.get(uid)
        if not user:
            raise APIError(USER_NOT_FOUND)

        name = request.form.get('name', '')
        birth = request.form.get('birth', '')
        gender = request.form.get('gender', '')
        intro = request.form.get('intro', '')

        height = request.form.get('height', '')
        weight = request.form.get('weight', '')

        tennis_age = request.form.get('tennis_age')
        tennis_level = request.form.get('tennis_level')

        badminton_age = request.form.get('badminton_age')
        football_age = request.form.get('football_age')
        football_position = request.form.get('football_position')
        # TODO user gender error
        if gender and gender not in USER_GENDERS:
            raise APIError(USER_NOT_FOUND)

        if is_age_str_format_version():
            tennis_age = UserInfo.make_real_age(tennis_age)
            badminton_age = UserInfo.make_real_age(badminton_age)
            football_age = UserInfo.make_real_age(football_age)


        # TODO validate birth format
        if birth:
            birth = datetime.strptime(birth, '%Y-%m-%d')

        user = user.update(name=name, birth=birth,
                           gender=gender, height=height, weight=weight,
                           intro=intro)
        user.update_info(tennis_age=tennis_age, tennis_level=tennis_level, badminton_age=badminton_age,
                              football_age=football_age, football_position=football_position)

        if not user:
            raise APIError(UPDATE_USER_ERROR)
        avatar = request.files.get('avatar')
        if avatar and allowed_file(avatar.filename):
            Photo.add(avatar.stream.read(), uid, K_USER, uid)

        return jsonify(code=200, user=get_full_user_dict(user))


class UserChangeCityAPI(MethodView):

    @check_api_key
    def post(self):
        user_id = request.json.get('user_id')

        if not user_id:
            raise  APIError(MISSING_PARAM)

        if not User.get(user_id):
            raise APIError(USER_NOT_FOUND)

        user = User.get(user_id)
        if not user:
            return APIError(USER_NOT_FOUND)
        city_id = request.json.get('city_id', '')
        user = user.update(city_id=city_id)
        if not user:
            return APIError(UPDATE_USER_ERROR)
        return jsonify(code=200)


class UserRegisterAPI(MethodView):

    @check_api_key
    def post(self):
        app_key = request.json.get('app_key')
        phone = request.json.get('telephone', '')
        password = request.json.get('password', '')
        verify_code = request.json.get('verify_code', '')

        if not (phone and password and verify_code):
            raise APIError(MISSING_PARAM)

        if not check_phone_number(phone):
            raise APIError(INVALID_PHONE_NUMBER)

        if User.is_phone_active(phone):
            raise APIError(REGISTERED_PHONE_NUMBER)

        if verify_code != Verify.get_code(phone):
            raise APIError(INVALID_VERIFY_CODE)

        try:
            user = User.add(telephone=phone, status=USER_STATUS_ACTIVE)
            user.set_password(password)
        except Exception as e:
            raise APIError(REGISTERED_FAIL)

        access_token = gen_access_token(user.id, app_key)
        Token.add(access_token, user.id, app_key)
        return jsonify(code='200', access_token=access_token, user=user.jsonify(), user_id=user.id)


class UserApkUpdate(MethodView):

    @check_api_key
    def get(self):
        version_code = request.args.get('version_code')

        if not version_code:
            raise APIError(MISSING_PARAM)
 
        return jsonify(
            code=200,
            last_version=USER_APP_VERSION,
            file_path=USER_APP_DOWNLOAD_PATH
        )

class UserGameOrderAPI(MethodView):

    @check_api_key
    def get(self):
        from model.order.game import GameOrder
        user_id = request.args.get('user_id')
        start = request.args.get('start', 0)
        limit = request.args.get('count', 10)
        orders = GameOrder.gets_by_user(user_id, int(start), int(limit))

        return jsonify(
            code=200,
            orders=[order.jsonify() for order in orders] if orders else []
        )

def make_real_tennis_age_level(tennis_age):
    if not tennis_age:
        return 0
    if tennis_age == '1-':
        return -1
    if tennis_age == '5+':
        return 5
    if tennis_age.isdigit():
        r = int(tennis_age)
        return r if r < 5 else 5
    return 0
