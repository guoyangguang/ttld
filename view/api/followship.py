#encoding=utf-8

from flask.views import MethodView
from flask import request, json, jsonify
from model.user import User
from model.followship import Followship
from .error import APIError, FORBIDDEN, MISSING_PARAM, USER_NOT_FOUND
from libs.decorator import check_api_key

class FollowedsAPI(MethodView):

    @check_api_key
    def get(self):
        user_id = request.values.get('user_id')
        if not user_id: raise APIError(MISSING_PARAM)
        current_user = User.get(user_id)
        if not current_user: raise APIError(USER_NOT_FOUND)
        followeds = Followship.followeds(current_user)
        body = list()
        for user in followeds:
            body.append(user.jsonify())
        return (json.dumps(body), 200, {'Content-Type': 'application/json'})

class FollowingsAPI(MethodView):

    @check_api_key
    def get(self):
        user_id = request.values.get('user_id')
        if not user_id: raise APIError(MISSING_PARAM)
        current_user = User.get(user_id)
        if not current_user: raise APIError(USER_NOT_FOUND)
        followings = Followship.followings(current_user)
        body = list()
        for user in followings:
            body.append(user.jsonify())
        return (json.dumps(body), 200, {'Content-Type': 'application/json'})

class FollowUnfollowAPI(MethodView):

    @check_api_key
    def post(self):
        user_id, followed_uid = request.json.get('user_id'), request.json.get('followed_uid')
        if not (user_id and followed_uid): raise APIError(MISSING_PARAM)
        current_user, user = User.get(user_id), User.get(followed_uid)
        if not (current_user and user): raise APIError(USER_NOT_FOUND)
        if (current_user.id != user.id):
            Followship.follow(current_user, user)
            response = jsonify()
            response.status_code = 201
            return response
        else:
            raise APIError(FORBIDDEN)

    @check_api_key
    def delete(self, followed_uid):
        user_id = request.json.get('user_id')
        if not user_id: raise APIError(MISSING_PARAM)
        current_user = User.get(user_id)
        user = User.get(followed_uid)
        if not (current_user and user): raise APIError(USER_NOT_FOUND)
        Followship.unfollow(current_user, user)
        response = jsonify()
        response.status_code = 204
        return response
