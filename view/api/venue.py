# -*- coding: utf-8 -*-

from datetime import datetime, timedelta, time
from flask import request, jsonify
from flask.views import MethodView

from libs.decorator import check_api_key, check_manager_venue, check_access_token
from model.consts import TENNIS_ITEM
from model.album import Album
from model.venue import Venue, search_venues
from model.game import Game
from model.user import User
from model.photo import Photo, allowed_file
from .error import APIError, MISSING_PARAM, INVALID_URL, ALBUM_NOT_FOUND, VENUE_NOT_FOUND, PHOTO_NOT_FOUND, \
                   FORBIDDEN, SYSTEM_ERROR, PHOTO_NOT_VALID, USER_NOT_FOUND
from model.admin_order import AdminOrder
from model.user import User

VENUE_FETCH_LIMIT = 10
MCKEY_VENUE_BOOK_PERCENTAGE = 'venue:%s:book_percentage'

class NearbyVenueAPI(MethodView):

    @check_api_key
    def get(self):
        user_id = request.args.get('user_id')
        latitude = request.args.get('latitude')
        longitude = request.args.get('longitude')
        city_id = request.args.get('city_id')
        start = request.args.get('start', type=int, default=0)
        limit = request.args.get('count', type=int, default=VENUE_FETCH_LIMIT)
        user_id = request.args.get('user_id')

        st = request.args.get('sports_type')
        sports_type = int(st) if st else TENNIS_ITEM

        end = start + limit

        venues = search_venues(user_id=user_id, latitude=latitude, longitude=longitude, city_id=city_id, sports_type=sports_type)
        venues = venues[start: end]
        rst = []
        
        for venue in venues:
            r = venue.jsonify()
            r.update({'recent_court_situation': venue.recent_court_situation})
            rst.append(r)

        return jsonify(
            code=200,
            venues=rst,
            start=start,
            count=limit,
        )


class UsualVenueAPI(MethodView):

    @check_api_key
    def get(self):
        uid = request.args.get('uid', type=int)
        start = request.args.get('start', type=int, default=0)
        limit = request.args.get('count', type=int, default=VENUE_FETCH_LIMIT)
        end = start + limit

        if not uid:
            raise APIError(MISSING_PARAM)
        venues = Venue.get_regular_venues(uid)

        return jsonify(
            code=200,
            venues=[venue.jsonify() for venue in venues[start: end]],
            start=start,
            count=limit,
        )


class VenuePhotoView(MethodView):

    @check_api_key
    def get(self, vid):
        venue = Venue.get(vid)
        if not venue:
            raise APIError(VENUE_NOT_FOUND)

        start = request.args.get('start', type=int, default=0)
        limit = request.args.get('count', type=int, default=1)

        # 一个场馆只有一个相册
        albums = Album.gets_by_venue(venue.id, limit=1)

        if not albums:
            raise APIError(ALBUM_NOT_FOUND)

        album = albums[0]
        photos = album.get_photos(start, limit)
        return jsonify(
            code=200,
            photos=[p.url('original') for p in photos],
            start=start,
            count=limit,
        )


class VenueScheduleAPI(MethodView):

    @check_api_key
    def get(self):
        venue_id = request.args.get('venue_id', type=int)
        date = request.args.get('date')

        if not (venue_id and date):
            raise APIError(MISSING_PARAM)

        venue = Venue.get(venue_id)
        if not venue:
            raise APIError(VENUE_NOT_FOUND)

        date = datetime.strptime(date, '%Y-%m-%d')
        date = date.date()
        courts = venue.scheduleInfo(date,is_for_merchant = False)

        return jsonify(schedule={'date': date.strftime('%Y-%m-%d'),
                                 'courts': courts,
                                 'current_time': date.strftime('%Y-%m-%d %H:%M:%S')},
                       code=200)


class VenueSearchAPI(MethodView):

    @check_api_key
    def get(self):
        user_id = request.args.get('user_id')
        q = request.args.get('q')
        start_time = request.args.get('start_time')
        latitude = request.args.get('latitude')
        longitude = request.args.get('longitude')
        city_id = request.args.get('city_id')

        st = request.args.get('sports_type')
        sports_type = int(st) if st else TENNIS_ITEM

        start = request.args.get('start', type=int, default=0)
        limit = request.args.get('count', type=int, default=VENUE_FETCH_LIMIT)
        end = start + limit
        
        if start_time:
            try:
                start_time = datetime.strptime(start_time, '%Y-%m-%d %H:%M:%S')
            except ValueError:
                try:
                    start_time = datetime.strptime(start_time, '%Y-%m-%d')
                except ValueError:
                    return jsonify(
                        code=200,
                        venues=[],
                        start=start,
                        count=limit,
                        total=0,
                    )

        venues = search_venues(user_id=user_id, q=q, start_time=start_time, latitude=latitude,
                               longitude=longitude, city_id=city_id, sports_type=sports_type)

        if venues:
            venues = venues[start: end]

        return jsonify(
            code=200,
            venues=[venue.jsonify() for venue in venues],
            start=start,
            count=limit,
        )


class VenueCityAPI(MethodView):

    @check_api_key
    def get(self):
        start = request.args.get('start', type=int, default=0)
        limit = request.args.get('count', type=int, default=10)
        cities = Venue.get_cities(start, limit)

        return jsonify(
            code=200,
            cities=[city.jsonify() for city in cities],
            start=start,
            count=limit,
        )


class ManagerVenueSchedule(MethodView):

    @check_access_token
    @check_manager_venue
    def get(self):
        venue_id = request.args.get('venue_id', type=int)
        date = request.args.get('date')

        if not (venue_id and date):
            raise APIError(MISSING_PARAM)
        venue = Venue.get(venue_id)
        if not venue:
            raise APIError(INVALID_URL)

        date = datetime.strptime(date, '%Y-%m-%d')
        date = date.date()
        courts = venue.scheduleInfo(date,is_for_merchant = True)

        return jsonify(
            code=200,
            schedule={
                'date': date.strftime('%Y-%m-%d'),
                'courts': courts,
                'current_time': date.strftime('%Y-%m-%d %H:%M:%S')
            }
        )




class VenuePhotoAPI(MethodView):

    @check_access_token
    @check_manager_venue
    def get(self):
        venue_id = request.args.get('venue_id')
        venue = Venue.get(venue_id)
        start = request.args.get('start', type=int)
        limit = request.args.get('limit', type=int)

        # 一个场馆只有一个相册
        albums = Album.gets_by_venue(venue.id, limit=1)

        if not albums:
            raise APIError(ALBUM_NOT_FOUND)

        album = albums[0]
        photos = album.get_photos(start, limit)

        return jsonify(
            code=200,
            venue_photos={
                'photos': [p.url('original') for p in photos],
                'photo_id': [p.id for p in photos],
                'start': start,
                'count': limit,
            }
        )

    @check_access_token
    @check_manager_venue
    def post(self):
        venue_id = request.form.get('venue_id')
        manager_id = request.form.get('manager_id')
        photo = request.files.get('photo')

        if not (venue_id and manager_id and photo):
            raise APIError(MISSING_PARAM)

        albums = Album.gets_by_venue(venue_id, limit=1)

        if not albums:
            raise APIError(ALBUM_NOT_FOUND)

        album = albums[0]

        if photo and allowed_file(photo.filename):
            p = Photo.add(photo.stream.read(), album.id, Album.kind, venue_id)

            return jsonify(
                code=200,
                venue_photo={
                    'photo_url': p.url('original'),
                    'photo_id': p.id,
                }
            )
        else:
            raise APIError(PHOTO_NOT_VALID)


class SetAlbumCover(MethodView):

    @check_access_token
    def post(self):
        stadium_id = request.json.get('stadium_id')
        photo_id = request.json.get('photo_id')

        if not stadium_id or not photo_id:
            raise APIError(MISSING_PARAM)

        photo = Photo.get(photo_id)
        albums = Album.gets_by_venue(stadium_id)

        if not photo:
            raise APIError(PHOTO_NOT_FOUND)

        if not albums:
            raise APIError(ALBUM_NOT_FOUND)

        album = albums[0]

        if photo.target_id != album.id:
            raise APIError(FORBIDDEN)

        try:
            album.update(photo_id=photo_id)
            return jsonify(
                code=200
            )
        except:
            raise APIError(SYSTEM_ERROR)



