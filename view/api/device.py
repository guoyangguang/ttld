# -*- coding:utf-8 -*-

from flask import request, jsonify
from flask.views import MethodView

from model.user import User
from model.device import Device
from model.consts import IOS, ANDROID
from libs.decorator import check_api_key
from view.api.error import APIError, MISSING_PARAM, USER_NOT_FOUND, DEVICE_NOT_REGISTER


class RegisterDeviceAPI(MethodView):

    @check_api_key
    def post(self):
        user_id = request.json.get('user_id')
        device_token = request.json.get('device_token')

        if not (user_id and device_token):
            return APIError(MISSING_PARAM)

        if not User.get(user_id):
            return APIError(USER_NOT_FOUND)

        Device.remove(user_id)
        os = ANDROID if request.android else IOS
        device = Device.register(user_id, device_token, os)
        if not device:
            return APIError(DEVICE_NOT_REGISTER)
        return jsonify(code=200, device=device.jsonify())

    @check_api_key
    def get(self):
        user_id = request.args.get('user_id')

        if not user_id:
            return APIError(MISSING_PARAM)

        if not User.get(user_id):
            return APIError(USER_NOT_FOUND)

        device = Device.get_by_uid(user_id)
        if not device:
            return APIError(DEVICE_NOT_REGISTER)

        return jsonify(code=200, device=device.jsonify())

class UnRegisterDeviceAPI(MethodView):
    @check_api_key
    def post(self):
        user_id = request.json.get('user_id')

        if not (user_id):
            return APIError(MISSING_PARAM)

        if not User.get(user_id):
            return APIError(USER_NOT_FOUND)

        result = Device.remove(user_id)
        return jsonify(code=200, result=result)
