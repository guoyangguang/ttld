# coding: utf-8

import hmac
import base64
import binascii
import hashlib
import random

from datetime import datetime, time, timedelta
from model.consts import ORDER_EXPIRED_DELTA
from flask import request
from config import TENCOURT 

ENCRYPT_SECRET = 'BititInDifferentWayTencourt'

NUMBER_HEAD = """130 131 132 133 134 135 136 137 138 139 147 150 151 152 153
                 155 156 157 158 159 180 181 182 183 185 186 187 188 189""".split()


def check_phone_number(phone):
    return phone.isdigit() and phone[0:3] in NUMBER_HEAD and len(phone) == 11


def check_user_password(phone, password):
    from model.user import User

    user = User.get_by_telephone(phone)
    if not user:
        return False

    if password == user.get_password():
        return True
    return False

def check_manager_password(phone, password):
    from model.manager import Manager

    manager = Manager.get_by_name_or_tel(phone)

    if not manager:
        return False

    if password == manager.password:
        return True

    return False

def encrypt(password):
    if isinstance(password, unicode):
        password = password.encode('utf-8')
    hash = hmac.new(ENCRYPT_SECRET, password, hashlib.sha1)
    return binascii.b2a_base64(hash.digest())[:-1]

def gen_access_token(user_id, api_key):
    #TODO modify access token generate method
    s = '%stencount%s' % (user_id, api_key)
    return base64.encodestring(s).replace('\n', '')

def gen_manager_access_token(manager_id, password):
    s = '%stencourt%s' % (manager_id, password)
    return base64.encodestring(s).replace('\n', '')

def get_int_key(str32_key):
    if len(str32_key) == 32:
        s1 = str32_key[0:16]
        try:
            k1 = int(s1, 16)
            if k1 >= 0 and gen_public_key(k1) == str32_key:
                return k1
        except ValueError:
            return None


def gen_public_key(int64_key):
    k1 = int64_key
    k2 = (3 * int64_key) & 0xffffffffffffffff
    s1 = '%016x' % k1
    s2 = '%016x' % k2
    return s1 + s2


def standard_datetime(dtstr):
    fmt = '%Y-%m-%d %H:%M:%S'
    return datetime.strptime(dtstr, fmt)


def standard_time(tmstr):
    h, m = tmstr.split(':')
    return time(int(h), int(m))


def standard_date(dtstr):
    return datetime.strptime(dtstr, '%Y-%m-%d').date()


def dt2timestamp(dt):
    return dt.strftime('%s')


def format_timestamp(ts):
    ts = float(ts)
    return datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

def fetch_app_version():
    app_version1 = request.args and request.args.get('app_version')
    app_version2 = request.json and request.json.get('app_version')
    app_version3 = request.form and request.form.get('app_version')
    app_version = app_version1 or app_version2 or app_version3
    if app_version:
        app_version = int(app_version)
        return app_version
    else:
        return (17 if request.android else 20300)

def is_age_str_format_version():
    app_version = fetch_app_version()
    return False if (request.android and app_version<18) else True

def is_new_platform_version():
    app_version = fetch_app_version()
    if request.android:
        return app_version > 18
    else:
        return app_version >= 20400

def get_full_user_dict(user):
    user_dict = user.full_jsonify()
    if not is_age_str_format_version():
        user_dict.update({'tennis_age': user.tennis_age})
    return user_dict

def get_normal_user_dict(user):
    user_id = user.id or user.user_id # 有的报名用户只有电话号码
    if user_id:
        from model.user import User
        user = User.get(user_id)
        user_dict = user.jsonify()
        if not is_age_str_format_version():
            user_dict.update({'tennis_age': user.tennis_age})
        return user_dict
    else:
        return user.jsonify()

def init_color(game, order):
    if game.is_created_by_manager():
        return 'red'
    elif order and order.completed:
        return 'green'
    elif order and order.status == 'P':
        return 'gray'
    else:
        return 'white'

def init_title(game, order):
    if game.is_created_by_manager():
        return '预留'
    elif order and order.status == 'P':
        order_create_time = order.create_time
        expire_time = order_create_time + timedelta(seconds=ORDER_EXPIRED_DELTA)
        expire_hour = expire_time.hour
        expire_minute = expire_time.minute
        return '用户正在预定中,请预留到%s点%s分' % (expire_hour, expire_minute)
    elif order and order.complete:
        return '%s预定了场地' % game.creator.name
    else:
        return '有%s人参加' % len(game.participates)

def get_a_random_avatar():
    avatar_template = TENCOURT + '/static/pic/mobile/default_avatar/avatar%s.png'
    default_avatart_urls = [avatar_template % i for i in range(1, 9)]
    return random.choice(default_avatart_urls)
