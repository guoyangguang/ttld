# -*- coding:utf-8 -*-

from flask import request, Blueprint
from flask.ext.mako import render_template


home = Blueprint('home', __name__)


@home.route('/')
def index():
    return render_template('home.plim')


@home.route('/about/')
def about():
    return render_template('about.plim')


@home.route('/jobs/')
def jobs():
    return render_template('jobs.plim')

@home.route('/privacy_policy/')
def privacy_policy():
    return render_template('privacy_policy.plim')
