# -*- coding:utf-8 -*-

from flask import request, Blueprint
from flask.ext.mako import render_template

from model.game import Game


gamebp = Blueprint('game', __name__)


@gamebp.route('/game/<int:game_id>/')
def game(game_id):
    game = Game.get(game_id)
    if not game:
        request.redirect('/')
    return render_template('game.plim', game=game)
