# -*- coding:utf-8 -*-

from flask import request, Blueprint
from flask.ext.mako import render_template


mobile = Blueprint('mobile', __name__)


@mobile.route('/', methods=['GET'])
def app():
    return render_template('mobile/app.html')
