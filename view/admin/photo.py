# -*- coding: utf-8 -*-

import os

from flask import request, abort, send_file
from flask.ext.login import login_required, current_user
from flask.ext.mako import render_template

from libs.filestore import FileStore
from model.album import Album
from model.photo import Photo, allowed_file
from model.consts import K_ALBUM

from . import admin


@admin.route('/<domain>/<category>/<filename>')
@admin.route('/<domain>/<filename>')
def render_photo(domain, filename, category=None):
    fs = FileStore(domain)
    path = fs.filepath(filename, category)
    if os.path.exists(path):
        return send_file(path)
    else:
        return ''


@admin.route('/album/<int:album_id>/photo/', methods=['GET', 'POST'])
@login_required
def photo(album_id):
    album = Album.get(album_id)
    if not album:
        abort(404)
    manager = current_user
    if request.method == 'POST':
        file = request.files['file']
        if file and allowed_file(file.filename):
            Photo.add(file.stream.read(), album_id, K_ALBUM, manager.id)
    venue_id = album.venue_id
    start = request.args.get('start', default=0, type=int)
    photos = album.get_photos(start, 20)
    return render_template('admin/photo/list.html', **locals())


@admin.route('/album/<int:album_id>/photo/add/')
@login_required
def add_photo(album_id):
    album = Album.get(album_id)
    if not album:
        abort(404)
    venue_id = album.venue_id
    return render_template('admin/photo/add.html', **locals())
