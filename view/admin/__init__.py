# coding:utf-8

from flask import Blueprint

admin = Blueprint('admin', __name__)

import index
import manager
import venue
import court
import album
import photo
import game
import report
