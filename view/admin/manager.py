# -*- coding: utf-8 -*-

from flask import request, redirect, url_for, abort
from flask.views import MethodView
from flask.ext.login import login_required, current_user
from flask.ext.mako import render_template

from model.manager import Manager
from model.photo import allowed_file, Photo
from model.consts import ROLE_MERCHANT, ROLE_NAME_MAPPING

from . import admin


@admin.route('/manager/', methods=['GET', 'POST'])
@login_required
def managers():
    if request.method == 'POST':
        name = request.form.get('name', default=None)
        password = request.form.get('pwd', default=None)
        password_confirm = request.form.get('pwd-confirm', default=None)
        role = request.form.get('role', default=None, type=int)
        telephone = request.form.get('telephone', default=0, type=int)
        company = request.form.get('company', default='')
        desc_message = request.form.get('desc', default='')

        error_msg = ''

        if not name or not password or not password_confirm or not role or not telephone:
            error_msg = '用户名、密码、身份、手机号均不能为空！'
        elif password != password_confirm:
            error_msg = '两次密码输入不一致！'
        elif role not in ROLE_NAME_MAPPING.keys():
            error_msg = '身份不合法！'

        if error_msg:
            return render_template('admin/manager/manager.html', error_msg=error_msg)
        else:
            Manager.add(name, role, password, telephone, company, desc_message)
            return redirect(url_for('admin.managers'))

    start = request.args.get('start', default=0, type=int)
    limit = request.args.get('limit', default=20, type=int)
    role = request.args.get('role', default=ROLE_MERCHANT, type=int)
    managers = Manager.gets_by_role(role, start, limit)
    return render_template('admin/manager/list.html', **locals())


@admin.route('/manager/add/')
@login_required
def add_manager():
    context = {
        'manager': None,
        'edit_mode': False,
        'error_msg': None,
    }
    return render_template('admin/manager/manager.html', **context)


class ManagerView(MethodView):

    decorators = [login_required]

    def prepare_manager(self, manager_id):
        self.manager = Manager.get(manager_id)

    def prepare_context(self, error_msg=None):
        self.context = {
            'manager': self.manager,
            'edit_mode': True,
            'error_msg': error_msg
        }

    def _check_permission(self):
        if not self.manager:
            abort(404)

        if self.manager.id != current_user.id:
            abort(403)

    def get(self, manager_id):
        self.prepare_manager(manager_id)
        self._check_permission()
        self.prepare_context()

        return render_template('admin/manager/manager.html', **self.context)

    def post(self, manager_id):
        self.prepare_manager(manager_id)
        self._check_permission()

        manager = self.manager

        name = request.form.get('name', default=manager.name)
        password = request.form.get('pwd', default=manager.password)
        password_confirm = request.form.get('pwd-confirm', default=manager.password)
        telephone = request.form.get('telephone', default=manager.telephone, type=int)
        company = request.form.get('company', default=manager.company)
        desc_message = request.form.get('desc', default=manager.desc_message)
        avatar = request.files.get('avatar')

        error_msg = ''

        if not name or not password or not password_confirm or not telephone:
            error_msg = '用户名、密码、手机号均不能为空！'
        elif password != password_confirm:
            error_msg = '两次密码输入不一致！'

        if error_msg:
            self.prepare_context(error_msg=error_msg)
            return render_template('admin/manager/manager.html', **self.context)
        else:
            manager.update(
                name=name,
                role=manager.role,
                password=password,
                telephone=telephone,
                company=company,
                desc_message=desc_message
            )

            if avatar and allowed_file(avatar.filename):
                Photo.add(avatar.stream.read(), manager.id, manager.kind, manager.id)
            return redirect(url_for('admin.manager', manager_id=manager.id))


admin.add_url_rule(
    '/manager/<int:manager_id>/',
    view_func=ManagerView.as_view('manager'),
    methods=['GET', 'POST']
)
