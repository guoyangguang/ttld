# coding: utf8

import sys
import json

from datetime import datetime

from flask import request, redirect, url_for, abort, Response, flash
from flask.ext.login import login_required, current_user
from flask.ext.mako import render_template

from model.manager import Manager
from model.game import Game
from model.venue import Venue
from model.photo import allowed_file, Photo
from model.consts import ROLE_MERCHANT
from model.order.game import GameOrder

from view.utils import format_timestamp, standard_time, init_color, init_title
from . import admin


@admin.route('/venue/', methods=['GET', 'POST'])
@login_required
def venue():
    if request.method == 'POST':
        venue_id = request.form.get('venue_id', default=0)
        merchant_id = request.form.get('merchant', default=None)
        name = request.form.get('name', default=None)
        address = request.form.get('address', default=None)
        telephone = request.form.get('telephone', default=None, type=int)
        traffic_info = request.form.get('traffic_info', default='')
        latitude = request.form.get('latitude', type=float, default=0.0)
        longitude = request.form.get('longitude', type=float, default=0.0)

        open_time = request.form.get('open_time', default='', type=standard_time)
        close_time = request.form.get('close_time', default='', type=standard_time)
        intro = request.form.get('intro', default='')
        avatar = request.files.get('avatar')

        error_msg = ''
        if not all([merchant_id, name, address, telephone]):
            error_msg = '商家、场馆名称、地址、电话都不能为空！'
        elif not Manager.get(merchant_id):
            error_msg = '商家不存在！'

        if error_msg:
            merchants = Manager.gets_by_role(ROLE_MERCHANT, start=0, limit=1000)
            return render_template('admin/venue/add.html', merchants=merchants,
                                   error=error_msg)
        else:
            venue = Venue.get(venue_id)
            if not venue:
                venue = Venue.add(merchant_id, name, address, telephone,
                                  traffic_info, latitude, longitude,
                                  open_time, close_time, intro)
            else:
                venue.update(name, address, telephone, traffic_info,
                             latitude, longitude, open_time, close_time, intro)

            if avatar and allowed_file(avatar.filename):
                Photo.add(avatar.stream.read(), venue.id, venue.kind, merchant_id)

            return redirect(url_for('admin.venue'))
    page = request.args.get('page', default=0, type=int)
    size = request.args.get('size', default=20, type=int)
    start = page * size
    total_counts = Venue.total_counts()
    total_page = total_counts / size + 1
    manager = current_user
    if manager.is_admin():
        venues = Venue.get_venues(start, size)
    else:
        venues = Venue.gets_by_manager(manager.id)

    return render_template('admin/venue/list.html', **locals())


@admin.route('/venue/add/')
@login_required
def add_venue():
    context = {
        'merchant': current_user,
        'venue': None,
    }
    if current_user.is_admin():
        merchants = Manager.gets_by_role(ROLE_MERCHANT, start=0, limit=sys.maxint)
        context['merchants'] = merchants
    return render_template('admin/venue/add.html', **context)


@admin.route('/venue/<int:vid>/edit/')
@login_required
def edit_venue(vid):
    venue = Venue.get(vid)
    if not venue:
        abort(404)
    if venue.manager_id != current_user.id and not current_user.is_admin():
        abort(403)

    context = {
        'venue': venue,
        'merchant': current_user,
    }
    if current_user.is_admin():
        merchants = Manager.gets_by_role(ROLE_MERCHANT, start=0, limit=sys.maxint)
        context['merchants'] = merchants
    return render_template('admin/venue/add.html', **context)


@admin.route('/venue/<int:vid>/delete/')
@login_required
def delete_venue(vid):
    venue = Venue.get(vid)
    if not venue:
        abort(404)
    if venue.manager_id != current_user.id and not current_user.is_admin():
        abort(403)

    if not venue.delete():
        flash('场馆删除失败，请先删除绑定的场地和相册')

    return redirect(url_for('admin.venue'))


@admin.route('/venue/<int:vid>/games/')
@login_required
def games_by_venue(vid):
    venue = Venue.get(vid)
    if not venue:
        abort(404)

    manager = current_user
    if not venue.is_owner(manager):
        abort(403)

    start = request.args.get('start')
    end = request.args.get('end')

    if not (start and end):
        abort(403)

    start_time = format_timestamp(start)
    end_time = format_timestamp(end)
    fmt = '%Y-%m-%d %H:%M:%S'
    start_time = datetime.strptime(start_time, fmt)
    end_time = datetime.strptime(end_time, fmt)
    games = Game.gets_by_venue_and_date(venue.id, start_time, end_time)
    data = []

    for g in games:
        gid = g.id
        game_order = GameOrder.get_by_game(gid)

        # 商家预留的场地是不会产生订单的
        if not g.is_created_by_manager() and game_order and game_order.is_expire:
            continue

        title = init_title(g, game_order)
        color = init_color(g, game_order)
        data.append({
            'id': g.id,
            'start': g.start_timestamp,
            'end': g.end_timestamp,
            'title': title,
            'allDay': False,
            'color': color,
            'resource': g.court.id,
        })

    return Response(json.dumps(data), mimetype='application/json')


@admin.route('/venue/<int:vid>/courts/')
@login_required
def courts_by_venue(vid):
    venue = Venue.get(vid)
    if not venue:
        abort(404)

    manager = current_user
    if not venue.is_owner(manager):
        abort(403)

    courts = venue.courts
    data = [{
        'name': c.name,
        'id': c.id,
    } for c in courts]

    return Response(json.dumps(data), mimetype='application/json')
