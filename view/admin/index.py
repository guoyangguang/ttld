# coding: utf-8

from flask import request, redirect, url_for
from flask.ext.login import (login_user, login_required,
        current_user, logout_user)
from flask.ext.mako import render_template

from model.manager import Manager
from model.venue import Venue
from view.utils import encrypt
from . import admin


@admin.route('/')
@login_required
def home():
    manager = current_user
    venues = Venue.gets_by_manager(manager.id)

    context = {
        'venues': venues
    }
    return render_template('admin/home.html', **context)


@admin.route('/logout/')
@login_required
def logout():
    logout_user()
    return redirect(url_for('admin.login'))


@admin.route('/login/', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username', '')
        password = request.form.get('password', '')

        if not username:
            error = '请输入用户名'
        elif not password:
            error = '请输入用户密码'
        else:
            manager = Manager.get_by_name(username) or Manager.get_by_telephone(username)
            if not manager:
                error = '该用户不存在，请重新输入！'
            elif encrypt(password) != manager.password:
                error = '密码错误，请重新输入！'
            else:
                login_user(manager)
                return redirect(url_for('admin.home'))

    return render_template('admin/login.html', **locals())
