# coding: utf-8

import json

from flask import request, redirect, url_for, abort, Response, jsonify
from flask.ext.login import login_required, current_user
from flask.ext.mako import render_template

from model.venue import Venue
from model.court import Court
from model.game import Game
from model.consts import (
    COURT_TYPES, ALL_COURT, COURT_ENVS, INDOOR,
    BOOK_TYPE_MANAGER
)

from view.utils import standard_datetime, format_timestamp
from . import admin


@admin.route('/venue/<int:vid>/court_list/', methods=['GET', 'POST'])
@login_required
def courts(vid=0):
    venue = Venue.get(vid)
    if not venue:
        abort(404)

    if request.method == 'POST':
        type = request.form.get('type', default=ALL_COURT, type=int)
        env = request.form.get('env', default=INDOOR, type=int)
        seq = request.form.get('seq')
        has_light = request.form.get('has_light', default='on')

        has_light = 1 if has_light == 'on' else 0

        Court.add(vid, type, env, seq, has_light)

        return redirect(url_for('admin.courts', vid=vid))

    start = request.args.get('start', default=0, type=int)
    limit = request.args.get('limit', default=20, type=int)
    courts = Court.get_by_venue(vid, start, limit)
    manager = current_user

    context = {
        'limit': limit,
        'courts': courts,
        'venue': venue,
        'manager': manager,
    }
    return render_template('admin/court/list.html', **context)


@admin.route('/venue/<int:vid>/court/new/')
@login_required
def new_court(vid=0):
    venue = Venue.get(vid)
    if not venue:
        abort(404)

    context = {
        'venue': venue,
        'types': COURT_TYPES,
        'envs': COURT_ENVS,
    }
    return render_template('admin/court/add.html', **context)


@admin.route('/venue/<int:vid>/court/<int:cid>/', methods=['DELETE'])
@login_required
def court(vid=0, cid=0):
    venue = Venue.get(vid)
    court = Court.get(cid)
    if not venue or not court:
        abort(404)

    manager = current_user
    if not venue.is_owner(manager) or venue != court.venue:
        abort(403)

    court.delete()
    return '', 204


@admin.route('/court/<int:cid>/games/', methods=['GET', 'POST'])
@login_required
def games_by_court(cid=0):
    court = Court.get(cid)
    if not court:
        abort(404)

    manager = current_user
    venue = court.venue
    if not venue.is_owner(manager):
        abort(403)

    if request.method == 'POST':
        start_time = request.json.get('start_time')
        end_time = request.json.get('end_time')
        comment = request.json.get('comment')

        start_time = standard_datetime(start_time)
        end_time = standard_datetime(end_time)

        reason = court.check_can_book(start_time, end_time)
        if reason:
            return jsonify(dict(code=403, reason=reason))

        creator_id = current_user.id
        game = Game.add(cid, creator_id, BOOK_TYPE_MANAGER,
                        start_time, end_time, comment=comment)

        if not game:
            return jsonify(dict(code=403, reason='请重试'))

        res = {
            'code': 200,
            'game_id': game.id
        }
        return jsonify(res)

    # FIXME verify start and end
    start = request.args.get('start')
    end = request.args.get('end')

    start_time = format_timestamp(start)
    end_time = format_timestamp(end)
    games = Game.gets_by_court_and_date(cid, start_time, end_time)

    data = []

    for g in games:
        _title = '有%s人参加' % len(g.participates)
        title = '预留' if g.is_created_by_manager() else _title
        color = 'red' if g.is_created_by_manager() else 'green'
        data.append({
            'id': g.id,
            'start': g.start_timestamp,
            'end': g.end_timestamp,
            'title': title,
            'allDay': False,
            'color': color,
        })

    return Response(json.dumps(data), mimetype='application/json')
