# -*- coding: utf-8 -*-

from flask import request, redirect, url_for, abort
from flask.ext.login import login_required, current_user
from flask.ext.mako import render_template

from model.venue import Venue
from model.album import Album
from model.consts import K_ALBUM

from . import admin


@admin.route('/venue/<int:venue_id>/album/', methods=['GET', 'POST'])
@login_required
def album(venue_id):
    venue = Venue.get(venue_id)
    if not venue:
        abort(404)
    manager = current_user
    if not venue.can_manage(manager):
        abort(403)

    if request.method == 'POST':
        title = request.form.get('title', default=None)
        intro = request.form.get('intro', default='')
        error = ''
        if not title:
            error = '相册名称不能为空！'
            render_template('admin/album/add.html', **locals())
        else:
            Album.add(title, intro, K_ALBUM, manager.id, venue_id)
            return redirect(url_for('admin.album', venue_id=venue_id))

    start = request.args.get('start', default=0, type=int)
    limit = request.args.get('limit', default=20, type=int)
    albums = Album.gets_by_venue(venue_id, start, limit)
    return render_template('admin/album/list.html', **locals())


@admin.route('/venue/<int:venue_id>/album/add/')
@login_required
def add_album(venue_id):
    venue = Venue.get(venue_id)
    if not venue:
        abort(404)
    return render_template('admin/album/add.html', **locals())


@admin.route('/venue/<int:venue_id>/album/<int:album_id>/delete/')
@login_required
def delete_album(venue_id, album_id):
    album = Album.get(album_id)
    if album and album.venue and album.venue.can_manage(current_user):
        album.delete()
    return redirect(url_for('admin.album', venue_id=venue_id))
