# -*- coding: utf-8 -*-

from datetime import date, timedelta

from flask import request, flash, redirect, url_for
from flask.ext.login import login_required, current_user
from flask.ext.mako import render_template

from . import admin
from view.utils import standard_date
from model.stats import MerchantStats


@admin.route('/report/', methods=['GET'])
@login_required
def report():
    default_end_date = date.today()
    default_start_date = default_end_date - timedelta(days=7)
    start_date = request.args.get('start_date',
                                  type=standard_date,
                                  default=default_start_date)
    end_date = request.args.get('end_date',
                                type=standard_date,
                                default=default_end_date)

    if start_date > end_date:
        flash('结束日期不能大于开始日期！')
        return redirect(url_for('admin.report'))

    statses = []

    for i in xrange((end_date - start_date).days + 1):
        date_ = start_date + timedelta(days=i)
        stats = MerchantStats(current_user.id, date_)
        statses.append(stats)

    context = {
        'start_date': start_date,
        'end_date': end_date,
        'statses': statses,
    }

    return render_template('admin/report/report.html', **context)


@admin.route('/report/order/', methods=['GET'])
@login_required
def order_report():
    return render_template('admin/report/order.html', **locals())
