# coding: utf-8

import json

from flask import jsonify, abort, request, Response
from flask.ext.login import login_required, current_user

from model.game import Game

from view.utils import format_timestamp
from . import admin


@admin.route('/game/<int:gid>/', methods=['GET', 'DELETE'])
@login_required
def game(gid=0):
    game = Game.get(gid)
    if not game:
        abort(404)

    if request.method == 'DELETE':
        if game.can_delete:
            game.delete()
            return '', 200
        else:
            abort(405)

    user_data = []

    if not game.is_created_by_manager():
        for o in game.orders:
            p = o.creator
            user_data.append({
                'avatar': p.avatar_url,
                'name': p.name,
                'status': o.status_cn,
                'create_time': o.create_time.strftime('%Y-%m-%d %H:%M:%S'),
            })

    court = game.court

    res = {
        'game_id': game.id,
        'court_name': court.name,
        'participates': user_data,
        'start_time': game.start_time.strftime('%Y-%m-%d %H:%M:%S'),
        'end_time': game.end_time.strftime('%Y-%m-%d %H:%M:%S'),
        'comment': game.comment,
    }

    return jsonify(res)


@admin.route('/games/')
def all_games():
    start = request.args.get('start')
    end = request.args.get('end')

    start_time = format_timestamp(start)
    end_time = format_timestamp(end)

    _games = Game.gets_by_manager_and_date(current_user.id, start_time, end_time)

    games = []
    for game in _games:
        if game.is_created_by_manager():
            games.append(game)
            continue
        order = game.get_game_order()
        if order and (not order.is_expire) and (not order.is_refunded):
            games.append(game)

    data = [{
        'id': g.id,
        'start': g.start_timestamp,
        'end': g.end_timestamp,
        'title': '场馆：%s, 场地：%s，有%s人参加' % (
                g.venue.name, g.court.name, len(g.orders)),
        'allDay': False,
    } for g in games]

    return Response(json.dumps(data), mimetype='application/json')


@admin.route('/alipay_refunds/', methods=['GET'])
@login_required
def admin_get_alipay_refunds():
    from model.order.alipay_refund import AlipayRefund
    from flask import render_template, abort

    page = request.args.get('page', type=int, default=1)
    alipay_refunds = [alipay_refund.presenter() for alipay_refund in AlipayRefund.all(page=page)]
    return (
        render_template('/admin/alipay_refund.html', alipay_refunds=alipay_refunds),
        200,
        {'Content-Type': 'text/html'}
    )
