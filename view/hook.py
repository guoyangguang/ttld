# -*- coding:utf-8 -*- ＃

from flask.views import MethodView
from flask import request
import os
import json


class Hook(MethodView):

    def get(self):
        return str(os.popen('git log -1').readlines())

    def post(self):
        payload = request.form['payload']

        if payload:
            data = json.loads(payload)
            if str(data['ref']).find('release'):
                os.popen('git pull')
