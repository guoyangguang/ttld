-- MySQL dump 10.13  Distrib 5.1.63, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: tencourt
-- ------------------------------------------------------
-- Server version 5.1.63-0ubuntu0.10.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `grade` tinyint(1) NOT NULL,
  `balance` decimal(9,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_user` (`user_id`,`grade`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='账户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_auth`
--

DROP TABLE IF EXISTS `app_auth`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_key` bigint(20) unsigned NOT NULL,
  `secret` bigint(20) unsigned NOT NULL,
  `app_name` varchar(50) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_app_key` (`app_key`),
  KEY `idx_app_name` (`app_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `charge_order`
--

DROP TABLE IF EXISTS `charge_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `charge_order` (
  `id` int(11) NOT NULL,
  `last_balance` decimal(9,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='充值订单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `city` varchar(50) CHARACTER SET ucs2 DEFAULT NULL,
  `weather_code` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `city` (`city`),
  KEY `idx_city` (`pid`),
  CONSTRAINT `idx_city` FOREIGN KEY (`pid`) REFERENCES `province` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='城市表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `court`
--

DROP TABLE IF EXISTS `court`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `court` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `size_type` int(2) NOT NULL DEFAULT '0',
  `env_type` int(2) NOT NULL DEFAULT '0',
  `sports_type` int(2) DEFAULT '0',
  `ground_type` int(2) DEFAULT '0',
  `seq` varchar(50) CHARACTER SET ucs2 NOT NULL,
  `has_light` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `court_ibfk` (`venue_id`),
  CONSTRAINT `court_ibfk` FOREIGN KEY (`venue_id`) REFERENCES `venue` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000010 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `token` varchar(128) NOT NULL,
  `os` tinyint(1) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_token` (`token`,`os`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='用户设备';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device`
--

DROP TABLE IF EXISTS `admin_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `token` varchar(128) NOT NULL,
  `os` tinyint(1) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_user_token` (`token`,`os`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='商家用户设备';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `court_id` int(11),
  `creator_id` int(11) NOT NULL,
  `book_type` tinyint(1) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_user` (`creator_id`,`book_type`),
) ENGINE=InnoDB AUTO_INCREMENT=1000019 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `game_order`
--

DROP TABLE IF EXISTS `game_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `game_order` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `court_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `manager_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_game` (`game_id`),
  KEY `idx_court` (`court_id`),
  KEY `idx_venue` (`venue_id`),
  KEY `idx_manager` (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='订场订单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET ucs2 NOT NULL,
  `role` smallint(5) unsigned NOT NULL,
  `password` varchar(64) NOT NULL,
  `telephone` varchar(30) NOT NULL,
  `company` varchar(100) CHARACTER SET ucs2 DEFAULT '',
  `desc_message` varchar(255) CHARACTER SET ucs2 DEFAULT '',
  `parent_id` int(11) DEFAULT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT 1,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name_role` (`name`,`role`)
) ENGINE=InnoDB AUTO_INCREMENT=1000003 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notify`
--

DROP TABLE IF EXISTS `notify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notify_type` int(3),
  `user_id` int(11) NOT NULL,
  `game_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `participant_id` int(11) DEFAULT NULL,
  `msg` varchar(100) CHARACTER SET ucs2 NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='通知表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_notify`
--

DROP TABLE IF EXISTS `admin_notify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_notify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) NOT NULL DEFAULT '1',
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `manager_id` int(11) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `msg` varchar(100) CHARACTER SET ucs2 NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='商家通知表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `participate_order`
--

DROP TABLE IF EXISTS `participate_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participate_order` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `court_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `manager_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_game` (`game_id`),
  KEY `idx_court` (`court_id`),
  KEY `idx_venue` (`venue_id`),
  KEY `idx_manager` (`manager_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='约球订单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `photo`
--

DROP TABLE IF EXISTS `photo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `target_id` int(11) unsigned NOT NULL,
  `target_kind` smallint(5) unsigned NOT NULL,
  `author_id` int(11) unsigned NOT NULL DEFAULT '0',
  `seq` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_target` (`target_id`,`target_kind`)
) ENGINE=InnoDB AUTO_INCREMENT=1000006 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `photo_album`
--

DROP TABLE IF EXISTS `photo_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo_album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET ucs2 DEFAULT '',
  `intro` varchar(255) DEFAULT NULL,
  `kind` int(6) NOT NULL,
  `manager_id` int(11) NOT NULL,
  `venue_id` int(11) NOT NULL,
  `photo_id` int(11),
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `idx_album_manager` (`manager_id`),
  KEY `idx_album_venue` (`venue_id`),
  CONSTRAINT `idx_album_manager` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`),
  CONSTRAINT `idx_album_venue` FOREIGN KEY (`venue_id`) REFERENCES `venue` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000002 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `province`
--

DROP TABLE IF EXISTS `province`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province` varchar(50) CHARACTER SET ucs2 DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `province` (`province`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='省份表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `require`
--

DROP TABLE IF EXISTS `require`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `require` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `age` mediumint(3) DEFAULT '0',
  `gender` char(1) DEFAULT '',
  `tennis_age` mediumint(3) DEFAULT '0',
  `tennis_level` tinyint(2) DEFAULT '0',
  `price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `max_users` tinyint(1) NOT NULL DEFAULT '1',
  `is_owner_play` tinyint(1) NOT NULL DEFAULT '1',
  `desc` varchar(255) CHARACTER SET ucs2 DEFAULT '',
  `pay_type` tinyint(1),
  `address` varchar(50) CHARACTER SET ucs2,
  `sports_type` int(3) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_game` (`game_id`),
  KEY `idx_user` (`creator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000006 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `require_id` int(11) NOT NULL,
  `comment_id` int(11),
  `body` varchar(255) CHARACTER SET ucs2 DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `user_ibfk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `require_ibfk` FOREIGN KEY (`require_id`) REFERENCES `require` (`id`)
  /*CONSTRAINT `comment_ibfk` FOREIGN KEY (`comment_id`) REFERENCES `comment` (`id`)*/
) ENGINE=InnoDB AUTO_INCREMENT=1000006 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `recommend`
--

DROP TABLE IF EXISTS `recommend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recommend` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000001 DEFAULT  CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `telephone_verify`
--

DROP TABLE IF EXISTS `telephone_verify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telephone_verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telephone` varchar(30) NOT NULL,
  `code` mediumint(6) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_telephone` (`telephone`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_token` varchar(60) NOT NULL,
  `user_id` int(11) NOT NULL,
  `app_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `trade`
--

DROP TABLE IF EXISTS `trade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) NOT NULL,
  `amount` decimal(9,2) NOT NULL,
  `pay_type` tinyint(1) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pay_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` char(1) NOT NULL,
  `pay_method` tinyint(1) NOT NULL,
  `gateway` tinyint(1) NOT NULL,
  `refund_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `telephone` varchar(30),
  PRIMARY KEY (`id`),
  KEY `idx_creator` (`creator_id`,`status`,`pay_type`)
) ENGINE=InnoDB AUTO_INCREMENT=1000015 DEFAULT CHARSET=latin1 COMMENT='订单基本信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `alipay_refund`
--

DROP TABLE IF EXISTS `alipay_refund`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `alipay_refund` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_ar_order_id` (`order_id`),
  CONSTRAINT `fk_ar_trade` FOREIGN KEY (`order_id`) REFERENCES `trade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telephone` varchar(30),
  `wx_id` varchar(100),
  `status` tinyint(2) DEFAULT '0',
  `name` varchar(255) CHARACTER SET ucs2 DEFAULT '',
  `birth` DATE NOT NULL DEFAULT '0000-00-00 00:00:00',
  `gender` char(1) DEFAULT '',
  `tennis_age` tinyint(3) DEFAULT '0',
  `tennis_level` float(2,1) DEFAULT '1.0',
  `member_type` char(1) DEFAULT '',
  `city_id` int(11) DEFAULT 1,
  `height` int(3),
  `weight` int(3),
  `reg_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_telephone` (`telephone`),
  UNIQUE KEY `uk_wx_id` (`wx_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000004 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `user_info`;

CREATE TABLE `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `tennis_age` tinyint(3) DEFAULT '0',
  `tennis_level` float(2,1) DEFAULT '1.0',
  `badminton_age` tinyint(3) DEFAULT '0',
  `football_age` tinyint(3) DEFAULT '0',
  `football_position` varchar(20),
  `default_avatar` varchar(255),
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique` (`user_id`),
  CONSTRAINT `user_cons` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000004 DEFAULT CHARSET=latin1;

--
-- Table structure for table `followship`
--

DROP TABLE IF EXISTS `followship`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `followship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `followed_id` int(11) NOT NULL,
  `following_id` int(11) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `fed_cons` FOREIGN KEY (`followed_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fing_cons` FOREIGN KEY (`following_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000004 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_password`
--

DROP TABLE IF EXISTS `user_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `password` varchar(64) DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `venue`
--

DROP TABLE IF EXISTS `venue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manager_id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET ucs2 DEFAULT NULL,
  `address` varchar(255) CHARACTER SET ucs2 NOT NULL DEFAULT '',
  `telephone` varchar(20) NOT NULL DEFAULT '',
  `cellphone` varchar(20) NOT NULL DEFAULT '',
  `traffic_info` varchar(255) CHARACTER SET ucs2 NOT NULL DEFAULT '',
  `latitude` double(10,6) DEFAULT NULL,
  `longitude` double(10,6) DEFAULT NULL,
  `open_time` time NOT NULL DEFAULT '00:00:00',
  `close_time` time NOT NULL DEFAULT '00:00:00',
  `intro` varchar(255) CHARACTER SET ucs2 NOT NULL DEFAULT '',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `city_id` int(11) NOT NULL DEFAULT '1',
  `sports_type` int(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `venue_ibfk` (`manager_id`),
  KEY `city_ibfk` (`city_id`),
  CONSTRAINT `venue_ibfk` FOREIGN KEY (`manager_id`) REFERENCES `manager` (`id`),
  CONSTRAINT `city_ibfk` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000003 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `venue_price`
--

DROP TABLE IF EXISTS `venue_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venue_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `env_type` int(2) NOT NULL DEFAULT '1',
  `ground_type` int(2) NOT NULL DEFAULT '1',
  `point` tinyint(2) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `kind` tinyint(1) NOT NULL,
  `price_type` int(2) NOT NULL DEFAULT 2,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_venue_point` (`venue_id`,`env_type`,`ground_type`,`point`,`kind`,`price_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='场馆价格';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `vip_order`
--

DROP TABLE IF EXISTS `vip_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vip_order` (
  `id` int(11) NOT NULL,
  `grade` tinyint(1) NOT NULL,
  `effective_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expire_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='购买VIP订单';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Table structure for table `register`
--

DROP TABLE IF EXISTS `register`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `register` (
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `city_id` int(11),
  `venue_name` varchar(50) CHARACTER SET ucs2 NOT NULL,
  `address` varchar(50) CHARACTER SET ucs2 NOT NULL,
  `telephone` varchar(30) NOT NULL,
  `courtInfo` varchar(250) CHARACTER SET ucs2 NOT NULL,
  `reg_pic` varchar(250) NOT NULL,
  `activate` tinyint(1) NOT NULL DEFAULT 1,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
)ENGINE=InnoDB AUTO_INCREMENT=1000000 DEFAULT CHARSET=latin1 COMMENT='注册验证表';
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Table structure for table `admin_telephone_verify`
--

DROP TABLE IF EXISTS `admin_telephone_verify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_telephone_verify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `telephone` varchar(30) NOT NULL,
  `code` mediumint(6) NOT NULL DEFAULT '0',
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_telephone` (`telephone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `admin_venue_price`
--

DROP TABLE IF EXISTS `admin_venue_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_venue_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `member_type_id` int(11),
  `env_type` int(2) NOT NULL DEFAULT '1',
  `size_type` int(2) NOT NULL DEFAULT '1',
  `point` tinyint(2) NOT NULL,
  `price` decimal(9,2) NOT NULL,
  `kind` tinyint(1) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_venue_point` (`venue_id`, `member_type_id`, `env_type`, `size_type`, `point`, `kind`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='场馆会员价格';
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `venue_bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venue_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11) NOT NULL,
  `account_name` varchar(100) CHARACTER SET ucs2 NOT NULL,
  `bank_name` varchar(100)  CHARACTER SET ucs2 NOT NULL,
  `account` varchar(100) NOT NULL,
  `city_id` int(11) NOT NULL,
  `create_time` timestamp default '0000-00-00 00:00:00',
  `update_time` timestamp default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `id_venue_bank_id` FOREIGN KEY(`venue_id`) REFERENCES `venue`(`id`),
  CONSTRAINT `idx_venue_bank_city` FOREIGN KEY(`city_id`) REFERENCES `city`(`id`),
  UNIQUE KEY `idx_venue_bank` (`venue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='账户银行信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `member_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11),
  `member_type` varchar(20) CHARACTER SET ucs2 NOT NULL,
  `court_price` longtext,
  `create_time` timestamp default '0000-00-00 00:00:00',
  `update_time` timestamp default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_member_type` (`venue_id`, `member_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='场馆会员类型表';
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20)  CHARACTER SET ucs2,
  `cellphone` varchar(20) NOT NULL DEFAULT '',
  `gender` char(1) DEFAULT '',
  `member_type_id` int(11),
  `number` varchar(30),
  `venue_id` int(11),
  `expire_time` timestamp default '0000-00-00 00:00:00',
  `create_time` timestamp default '0000-00-00 00:00:00',
  `update_time` timestamp default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='场馆会员表';
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `admin_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11),
  `identity` varchar(20),
  `game_id` varchar(250),
  `amount` decimal(9,2) NOT NULL,
  `status` char(1) NOT NULL,
  `pay_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `create_time` timestamp default '0000-00-00 00:00:00',
  `update_time` timestamp default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='后台订单表';
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `venue_price_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `venue_price_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `venue_id` int(11),
  `price_info` longtext,
  `create_time` timestamp default '0000-00-00 00:00:00',
  `update_time` timestamp default now() on update now(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_venue_id` (`venue_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='场馆价格信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `order_pay_notify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_pay_notify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  pay_method tinyint(1) NOT NULL,
  notify longtext,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `idx_order_pay_notify` FOREIGN KEY (`order_id`) REFERENCES `trade`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='订单支付信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `order_refund_notify`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_refund_notify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `refund_id` int(20) NOT NULL,
  pay_method tinyint(1) NOT NULL,
  notify longtext,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `idx_order_refund_notify` FOREIGN KEY (`order_id`) REFERENCES `trade`(`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='订单退款信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `book_regular`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book_regular` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `court_id` int(11) NOT NULL,
  `week` int(2) NOT NULL,
  `start_point` int(2) NOT NULL,
  `end_point` int(2) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `update_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='场地订场规则表';

DROP TABLE IF EXISTS `participates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `participates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11),
  `telephone` varchar(30),
  `game_id` int(11),
  `participate_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  CONSTRAINT `idx_participates_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `idx_participates_game` FOREIGN KEY (`game_id`) REFERENCES `game` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='活动报名表';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-26 23:10:15
