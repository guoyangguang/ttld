from config import HOST, PORT, GUNICORN_LOG_DIR
from os import path

bind = '%s:%s' % (HOST, PORT)
debug = False
daemon = False
loglevel = 'error'
errorlog = path.join(GUNICORN_LOG_DIR, 'error.log')
accesslog = path.join(GUNICORN_LOG_DIR, 'access.log')
workers = 4
