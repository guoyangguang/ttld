# coding: utf-8

from datetime import datetime
from config import TENCOURT

from libs.sqlstore import store
from libs.filestore import fs


DEFAULT_AVATAR_URL = '%s/static/pic/tennis.jpg' % TENCOURT


class Photo(object):

    table = 'photo'
    select_all = 'select id, time, target_id, target_kind, author_id, seq' +\
                 ' from ' + table

    def __init__(self, id, time, target_id, target_kind, author_id, seq):
        self.id = str(id)
        self.create_time = time
        self.target_id = target_id
        self.target_kind = target_kind
        self.author_id = author_id
        self.seq = seq

    def __repr__(self):
        return 'Photo(id=%s)' % self.id

    @property
    def filename(self):
        return "p%s.jpg" % self.id

    def url(self, category=None):
        path = fs.path(self.filename, category)
        return "%s/admin/%s" % (TENCOURT, path)

    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + " where id=%s", id)
        return r and cls(*r)

    @classmethod
    def gets(cls, ids):
        photos = [cls.get(id) for id in ids if id]
        photos = filter(None, photos)
        return photos 

    @classmethod
    def gets_by_target(cls, target_id, target_kind, start=None, limit=None):
        if start and limit:
            r = store.fetchall(cls.select_all +
                          " where target_id=%s and target_kind=%s "
                          "order by time desc limit %s, %s", (target_id, target_kind, start, limit))
        else:
            r = store.fetchall(cls.select_all +
                          " where target_id=%s and target_kind=%s "
                          "order by time desc", (target_id, target_kind))
        if isinstance(r, tuple):
            return [cls(*photo) for photo in r]

    @classmethod
    def add(cls, content, target_id, target_kind, author_id=0, seq=0):
        r = store.transac('insert into ' + cls.table +
                          ' (target_id, target_kind, author_id, seq)'
                          ' values (%s, %s, %s, %s)',
                          (target_id, target_kind, author_id, seq))
        if r:
            photo = cls.get(r)
            fs.save(photo.filename, content, 'original')
            return photo

    def delete(self):
        fs.delete(self.filename)
        r = store.transac('delete from ' + self.table + ' where id=%s', self.id)
        return r


def allowed_file(filename):
    exts = ['jpg', 'gif', 'png', 'bmp', 'jpeg']
    return '.' in filename and filename.lower().rsplit('.', 1)[1] in exts
