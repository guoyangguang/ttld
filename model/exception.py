# -*- coding: utf-8 -*-

class CourtNotExistError(Exception):
    pass


class VenueNotOpenError(Exception):
    pass


class VenueAlreadyClosedError(Exception):
    pass


class CourtScheduledOverError(Exception):
    pass
