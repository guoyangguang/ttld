# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
from libs.sqlstore import store
from libs.mc import mc, cache, ONE_DAY
from model.user import User
from model.consts import ALL_STAGES,MORNING_STAGE,NOON_STAGE,NIGHT_STAGE,ANY_SPORTS_ITEM
from model.game import Game
MCKEY_REQUIRE = 'require:%s'

class Require(object):

    table = '`require`'
    select_all = "select id, creator_id, game_id, age, gender, tennis_age, tennis_level, " +\
                 "price, max_users, is_owner_play, `desc`, pay_type, address, sports_type, create_time, update_time from " + table

    select_inner_join_game = "select require.id, require.creator_id, require.game_id, require.age, require.gender," +\
                      "require.tennis_age, require.tennis_level, require.price, require.max_users, " +\
                      "require.is_owner_play, require.desc, require.pay_type, require.address, require.address, require.create_time, " \
                      "require.update_time from " + table + " inner join game on require.game_id = game.id"

    def __init__(self, id, creator_id, game_id, age, gender,
                 tennis_age, tennis_level, price, max_users, is_owner_play,
                 desc, pay_type, address, sports_type, create_time, update_time):
        self.id = id
        self.creator_id = creator_id
        self.game_id = game_id
        self.age = age
        self.gender = gender
        self.tennis_age = tennis_age
        self.tennis_level = tennis_level
        self.price = price
        self.max_users = max_users
        self.is_owner_play = is_owner_play
        self.desc = desc
        self.pay_type = pay_type
        self.address = address
        self.sports_type = sports_type
        self.create_time = create_time
        self.update_time = update_time

    @property
    def requirement(self):
        '''约球条件描述'''
        r = []
        if self.tennis_level:
            r.append('%s级' % self.tennis_level)
        if self.gender:
            if self.gender == 'm':
                r.append('男性')
            elif self.gender == 'f':
                r.append('女性')
        else:
            r.append('性别不限')
        return r

    @classmethod
    def compute_price(cls, game_price, users_count):
        '''约球定价算法'''
        if not users_count:
            return 10
        price = game_price / users_count
        tmp = round(price)
        if tmp < price:
            tmp += 1
        return tmp

    @classmethod
    def search(cls, age=None, gender=None, tennis_age=None,
               tennis_level=None, start_time=None, hour=None, stage=ALL_STAGES, user=None, city_id=1, only_future_requires=True, sports_type=ANY_SPORTS_ITEM):
        queries = []
        params = []
        if age:
            queries.append('require.age >= %s')
            params.append(age)
        if gender:
            queries.append('require.gender = %s')
            params.append(gender)
        if tennis_age:
            queries.append('require.tennis_age >= %s')
            params.append(tennis_age)
        if tennis_level:
            queries.append('require.tennis_level >= %s')
            params.append(tennis_level)
        if only_future_requires:
            queries.append('game.end_time > %s')
            params.append(datetime.now())

        if params and queries:
            r = store.fetchall(
                 cls.select_inner_join_game  + ' where ' +' and '.join(queries) + ' order by id desc',
                 params
            )
        else:
            r = store.fetchall(cls.select_inner_join_game + ' order by id desc')
        requires = ([cls(*require) for require in r] if isinstance(r, tuple) else list())
        basic_filter = lambda r: (r.game.is_participatable(user))
        requires = filter(basic_filter, requires)

        type_filter = lambda r:(r.game and r.game.court and r.game.court.sports_type & sports_type > 0)
        requires = filter(type_filter, requires)

        if start_time:
            date_filter = lambda r: (r.game and r.start_time.date() == start_time.date())
            requires = filter(date_filter, requires)

        if 0 <= hour < 24:
            hour_filter = lambda r: (r.game.start_time.hour <= hour and r.game.end_time.hour >= hour)
            requires = filter(hour_filter, requires)

        stage_filter = None
        if stage == MORNING_STAGE:
            stage_filter = lambda r: (r.game.start_time.hour < 12)
        elif stage == NOON_STAGE:
            stage_filter = lambda r: (r.game.start_time.hour >= 12 and r.game.start_time.hour < 18)
        elif stage == NIGHT_STAGE:
            stage_filter = lambda r: (r.game.start_time.hour >= 18)

        if stage_filter:
            requires = filter(stage_filter, requires)

        # 过滤可以参加的比赛。根据城市进行过滤。
        if city_id:
            city_filter = lambda r: r.game.city_id == city_id
            requires = filter(city_filter, requires)
        
        if not user or (user and not(user.id in User.get_ttld_user_ids())):
            requires = filter(lambda require: not(require.is_required_in_test_venue()), requires)
        return requires

    @classmethod
    def flush(cls, id):
        mc.delete(MCKEY_REQUIRE % id)

    @property
    def creator(self):
        return self.creator_id and User.get(self.creator_id)

    @property
    def game(self):
        from model.game import Game
        return self.game_id and Game.get(self.game_id)

    @property
    def start_time(self):
        return self.game and self.game.start_time

    @property
    def end_time(self):
        return self.game and self.game.end_time

    @property
    def court(self):
        return self.game and self.game.court

    @property
    def venue(self):
        return self.game and self.game.venue
    
    def is_required_in_test_venue(self):
        game = self.game
        court = game.court
        venue = court.venue
        return True if venue.name == '天天网球馆' else False

    @classmethod
    @cache(MCKEY_REQUIRE % '{id}', expire=ONE_DAY)
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', id)
        return r and cls(*r)

    @classmethod
    def gets(cls, ids):
        requires = [cls.get(id) for id in ids if id]
        requires = filter(None, requires)
        return requires 

    @classmethod
    def delete(cls, id):
        r = store.transac('delete from ' + cls.table + ' where id=%s', id)
        if r:
            cls.flush(id)


    @classmethod
    def new(cls, creator_id, game_id, age, gender, tennis_age, tennis_level,
            price, is_owner_play, desc, pay_type, address, sports_type, max_users=0):
        game = Game.get(game_id)

        if not game:
            return None

        r = store.transac(
            'insert into ' + cls.table + ' (creator_id, game_id, '
            'age, gender, tennis_age, tennis_level, price, max_users, '
            'is_owner_play, `desc`, pay_type, address, sports_type, create_time) '
            'values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
            (creator_id, game_id, age, gender, tennis_age,
            tennis_level, price, max_users, is_owner_play, desc, pay_type, address, sports_type, datetime.now())
        )

        if r:
            if game.court_id:
                venue_id = game.venue.id
                Game.flush_weekly_particapte_count(venue_id, datetime.now().date())
            return cls.get(r)

    @classmethod
    def get_by_game(cls, game_id):
        r = store.fetchone(cls.select_all +' where game_id=%s', game_id)
        return r and cls(*r)

    def meet(self, user):
        '''判断一个用户是否符合约球条件'''
        return True
        # return self.meet_gender(user) and self.meet_level(user)

    def meet_gender(self, user):
        gender_options = ['m', 'f']
        if self.gender in gender_options and self.gender != user.gender:
            return False
        return True

    def meet_level(self, user):
        if self.tennis_level and self.tennis_level > user.user_info.tennis_level:
            return False
        return True

    def update_desc(self, desc):
        sql = 'update {table} set desc=%s where id=%s'.format(table=self.table)
        r = store.transac(sql, (desc, self.id))
        return r

    def jsonify(self):
        from view.utils import get_normal_user_dict
        return {
            'id': self.id,
            'creator': self.creator and get_normal_user_dict(self.creator) or '',
            'start_time': self.start_time.strftime('%Y-%m-%d %H:%M:%S'),
            'end_time': self.end_time.strftime('%Y-%m-%d %H:%M:%S'),
            'court': self.court and self.court.jsonify(),
            'venue_id': self.venue and self.venue.id or '',
            'venue_name': self.venue and self.venue.name,
            'age': self.age,
            'gender': self.gender,
            'tennis_age': self.tennis_age,
            'tennis_level': self.tennis_level,
            'price': float(self.price),
            'max_users': self.max_users,
            'participates': [get_normal_user_dict(user) for user in self.game.participates],
            'create_time': self.create_time.strftime('%Y-%m-%d %H:%M:%S'),
            'is_owner_play': self.is_owner_play,
            'desc': self.desc,
            'pay_type': self.pay_type,
            'address': self.address
        }
