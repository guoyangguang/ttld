# coding: utf-8

from datetime import datetime
from random import randint

from libs.sqlstore import store
from tools.postman.sms import send_verify_code_sms

class Verify(object):

    table = 'telephone_verify'
    select_all = 'select id, telephone, code, create_time, update_time from ' + table 

    def __init__(self, id, telephone, code, create_time, update_time):
        self.id = str(id)
        self.telephone = telephone
        self.code = code
        self.create_time = create_time
        self.update_time = update_time

    @classmethod
    def gen_code(cls):
        return randint(100000, 999999)

    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', (id,))
        return r and cls(*r)

    @classmethod
    def add(cls, telephone, code):
        if not telephone or not code:
            return False
        r = store.transac(
            'insert into ' + cls.table + ' (telephone, code, create_time)' +\
            ' values (%s, %s, %s) on duplicate key update code=%s',
            (telephone, code, datetime.now(), code)
        )
        new_code = cls.get_code(telephone)
        send_verify_code_sms.delay(telephone,new_code)
        if r:
            return cls.get(r)

    @classmethod
    def get_code(cls, telephone):
        r = store.fetchone('select code from ' + cls.table +
                           ' where telephone=%s', (telephone,))
        if r:
            return str(r[0])


class AdminVerify(Verify):
    table = 'admin_telephone_verify'

