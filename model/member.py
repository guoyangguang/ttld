# coding: utf-8

import MySQLdb
from datetime import datetime
from libs.sqlstore import store
from config import MYSQL_CONFIG

class Member(object):

    table = 'member'
    select_all = 'select id, name, cellphone, gender, member_type_id, number, venue_id, expire_time,' +\
                 ' create_time, update_time from ' + table

    def __init__(self, _id, name, cellphone, gender, member_type_id, number, venue_id, expire_time,
                 create_time, update_time):
        self.id = _id
        self.name = name
        self.cellphone = cellphone
        self.gender = gender
        self.member_type_id = member_type_id
        self.number = number
        self.venue_id = venue_id
        self.expire_time = expire_time
        self.create_time = create_time
        self.update_time = update_time

    @property
    def expired(self):
        now = datetime.now()
        return self.expire_time < now

    @property
    def member_type(self):
        return MemberType.get(self.member_type_id).member_type

    @classmethod
    def get(cls, _id):
        r = store.fetchone(cls.select_all + ' where id=%s', _id)
        return r and cls(*r)

    @classmethod
    def gets(cls, ids):
        members = [cls.get(_id) for _id in ids]
        members = filter(None, members)
        return members 

    @classmethod
    def add(cls, name, cellphone, gender, member_type_id, number, venue_id, expire_time, create_time=datetime.now()):
        sql = 'insert into {table} (name, cellphone, gender, member_type_id, number, venue_id, ' \
              'expire_time, create_time) values (%s, %s, %s, %s, %s, %s, %s, %s)'.format(table=cls.table)

        r = store.transac(sql, (name, cellphone, gender, member_type_id, number, venue_id, expire_time, create_time))

        if r:
            cls.get(r)


    def update(self, name=None, cellphone=None, gender=None, member_type_id=None, number=None, expire_time=None):
        name = name or self.name
        cellphone = cellphone or self.cellphone
        gender = gender or self.gender
        member_type_id = member_type_id or self.member_type_id
        number = number or self.number
        expire_time = expire_time or self.expire_time
        sql = 'update {table} set name=%s, cellphone=%s, gender=%s, member_type_id=%s, number=%s, expire_time=%s where id=%s'.format(table=self.table)
        r = store.transac(sql, (name, cellphone, gender, member_type_id, number, expire_time, self.id))
        if r:
            return Member.get(self.id)
            

    @classmethod
    def gets_by_venue(cls, venue_id):
        r = store.fetchall(cls.select_all + ' where venue_id = %s', venue_id)
        if isinstance(r, tuple):
            return [cls(*member) for member in r] 

    @classmethod
    def get_by_venue_and_identity(cls, venue_id, identity):
        sql = cls.select_all + ' where venue_id=%s and (cellphone=%s or number=%s)'
        r = store.fetchone(sql, (venue_id, identity, identity))
        return r and cls(*r)

    @classmethod
    def get_by_member_type(cls, member_type_id):
        sql = cls.select_all + ' where member_type_id=%s'
        r = store.fetchall(sql, member_type_id)
        if isinstance(r, tuple):
            return [cls(*member) for member in r] 

class MemberType(object):

    table='member_type'
    select_all = 'select id, venue_id, member_type, court_price, create_time, update_time from ' + table

    def __init__(self, _id, venue_id, member_type, court_price, create_time, update_time):
        self.id = _id
        self.venue_id = venue_id
        self.member_type = member_type
        self.court_price = court_price
        self.create_time = create_time
        self.update_time = update_time

    @classmethod
    def add(cls, venue_id, member_type, court_price, create_time=datetime.now()):
        sql = 'insert into {table} (venue_id, member_type, court_price, create_time) ' \
              'values (%s, %s, %s, %s)'.format(table=cls.table)
        r = store.transac(sql, (venue_id, member_type, court_price, create_time))
        if r:
            return cls.get(r)


    def update(self, member_type=None, court_price=None):
        member_type = member_type or self.member_type
        court_price = court_price or self.court_price
        sql = 'update {table} set member_type=%s, court_price=%s where id=%s'.format(table=self.table)
        r = store.transac(sql, (member_type, court_price, self.id))
        if r:
            MemberType.get(r)


    def delete(self):
        conn = MySQLdb.connect(
            host=MYSQL_CONFIG['host'],
            user=MYSQL_CONFIG['user'],
            passwd=MYSQL_CONFIG['password'],
            db=MYSQL_CONFIG['db'],
            port=int(MYSQL_CONFIG['port']),
            charset='utf8'
        )
        cur = conn.cursor()
        try:
            sql = 'delete from {table} where id=%s'.format(table=self.table)
            cur.execute(sql, self.id)
            sql_pri = 'delete from {table} where member_type_id=%s'.format(table=AdminVenuePrice.table)
            cur.execute(sql_pri, self.id)
            conn.commit()
            return True
        except Exception as e:
            conn.rollback()
            return False
        finally:
            cur.close()
            conn.close()


    @classmethod
    def get(cls, _id):
        sql = cls.select_all + ' where id=%s'
        r = store.fetchone(sql, _id)
        return r and cls(*r)


    @classmethod
    def gets_by_venue(cls, venue_id):
        sql = cls.select_all + ' where venue_id=%s'
        r = store.fetchall(sql, venue_id)
        if isinstance(r, tuple):
            return [cls(*member_type) for member_type in r]


    @classmethod
    def gets(cls, ids):
        member_types = [cls.get(_id) for _id in ids]
        member_types = filter(None, member_types)
        return member_types 
