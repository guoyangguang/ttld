# -*- coding: utf-8 -*-

import sys

from datetime import datetime

from libs.sqlstore import store
from model.manager import Manager
from model.photo import Photo
from model.venue import Venue
from model.consts import K_ALBUM


class Album(object):

    table = 'photo_album'
    kind = K_ALBUM
    select_all = "select id, title, intro, kind, manager_id, " +\
                 "venue_id, photo_id, create_time, update_time from " + table

    def __init__(self, id, title, intro, kind,
                 manager_id, venue_id, photo_id, create_time, update_time):
        self.id = id
        self.title = title
        self.intro = intro
        self.kind = kind
        self.manager_id = manager_id
        self.venue_id = venue_id
        self.photo_id = photo_id
        self.create_time = create_time
        self.update_time = update_time

    @property
    def author(self):
        return Manager.get(self.manager_id)

    @property
    def venue(self):
        return Venue.get(self.venue_id)

    def can_manage(self, manager):
        return manager.can_manage(self.venue)

    def is_owner(self, manager):
        return str(manager.id) == str(self.manager_id)

    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + " where id=%s", id)
        return r and cls(*r)

    @classmethod
    def gets(cls, ids):
        albums = [cls.get(id) for id in ids if id]
        albums = filter(None, albums)
        return albums

    @classmethod
    def gets_by_venue(cls, venue_id, start=0, limit=1):
        r = store.fetchall(cls.select_all + " where venue_id=%s "
                      "order by update_time desc limit %s, %s", (venue_id, start, limit))
        if isinstance(r, tuple):
           return [cls(*album) for album in r]

    @classmethod
    def add(cls, title, intro, kind, manager_id, venue_id, photo_id=None):
        r = store.transac("insert into " + cls.table + " (title, intro, kind, "
                          "manager_id, venue_id, photo_id, create_time) values "
                          "(%s, %s, %s, %s, %s, %s, %s)",
                          (title, intro, kind, manager_id, venue_id, photo_id, datetime.now()))
                           
        if r:
            return cls.get(r)

    def update(self, title=None, intro=None, photo_id=None):
        self.title = title or self.title
        self.intro = intro or self.intro
        self.photo_id = photo_id or self.photo_id
        r = store.transac("update " + self.table + " "
                      "set title=%s, intro=%s, photo_id=%s where id=%s",
                      (self.title, self.intro, self.photo_id, self.id))
        if r:
            return self 

    def get_photos(self, start=None, limit=None):
        return Photo.gets_by_target(self.id, self.kind, start, limit)

    def delete(self):
        photos = self.get_photos(start=0, limit=sys.maxint)
        for photo in photos:
            photo.delete()
        r = store.transac('delete from ' + self.table + ' where id=%s', self.id)
        return r
