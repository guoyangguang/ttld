from libs.sqlstore import store
from libs.mc import mc, cache, ONE_DAY
from view.utils import get_a_random_avatar

MCKEY_USERINFO = 'userinfo:%s'

class UserInfo(object):

    table = 'user_info'
    select_all = 'select id, user_id, tennis_age, tennis_level, badminton_age, football_age, ' +\
                 'football_position, default_avatar from ' + table
                 
    def __init__(self, id, user_id, tennis_age, tennis_level, badminton_age,
                 football_age, football_position, default_avatar):
        self.id = id
        self.user_id = user_id
        self.tennis_age = tennis_age
        self.tennis_level = tennis_level
        self.badminton_age = badminton_age
        self.football_age = football_age
        self.football_position = football_position
        self.default_avatar = default_avatar

    @classmethod
    @cache(MCKEY_USERINFO % '{id}', expire=ONE_DAY)
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where user_id=%s', (id))
        return r and cls(*r)

    @classmethod
    def flush(cls, _id):
        mc.delete(MCKEY_USERINFO % _id)

    @classmethod
    def add(cls, user_id=None, tennis_age=None, tennis_level=None, badminton_age=None,
            football_age=None, football_position=0):
        r = store.transac(
            'insert ignore into ' + cls.table +\
            ' (user_id, tennis_age, tennis_level, badminton_age, football_age, football_position, default_avatar)' +\
            ' values (%s, %s, %s, %s, %s, %s, %s)',
            (user_id, tennis_age, tennis_level, badminton_age, football_age, football_position, get_a_random_avatar())
        )

    def update(self, tennis_age=None, tennis_level=None, badminton_age=None,
        football_age=None, football_position=None):
        self.tennis_age=tennis_age or self.tennis_age
        self.tennis_level=tennis_level or self.tennis_level
        self.badminton_age=badminton_age or self.badminton_age
        self.football_age=football_age or self.football_age
        self.football_position=football_position or self.football_position
        r=store.transac('update ' + self.table + ' set tennis_level=%s, tennis_age=%s, badminton_age=%s, '
                                                'football_age=%s, football_position=%s where user_id=%s',
            (self.tennis_level, self.tennis_age, self.badminton_age, self.football_age, self.football_position, self.user_id))
        if r:
            UserInfo.flush(self.user_id)

    @classmethod
    def convertAgeToString(cls, age):
        if age <= 0:
            return '1-'
        if age >= 5:
            return '5+'
        return str(age)

    @classmethod
    def make_real_age(cls, age):
        if not age:
            return 0
        if age == '1-':
            return -1
        if age == '5+':
            return 5
        if age.isdigit():
            r = int(age)
            return r if r < 5 else 5
        return 0

    def jsonify(self):
        dict = {
            'tennis_age': UserInfo.convertAgeToString(self.tennis_age),
            'tennis_level': float(self.tennis_level) if self.tennis_level else 0,
            'badminton_age': UserInfo.convertAgeToString(self.badminton_age),
            'football_age': UserInfo.convertAgeToString(self.football_age),
            'football_position': int(self.football_position) if self.football_position else 0,
            'default_avatar': self.default_avatar
        }
        return dict
