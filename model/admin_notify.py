# coding: utf-8

import datetime
from libs.sqlstore import store
from game import Game
from consts import BOOK_TYPE_USER,READ_STATUS

ADMIN_ALL_NOTIFY         = 0
ADMIN_NEW_BOOK_NOTIFY    = 1
ADMIN_CANCEL_BOOK_NOTIFY = 2

class AdminNotify(object):

    table = 'admin_notify'
    select_all = 'select id, type, status ,game_id, manager_id, msg, create_time, update_time from ' + table

    def __init__(self, id, type, status ,game_id, manager_id, msg, create_time, update_time):
        self.id = id
        self.type = type
        self.status = status
        self.game_id = game_id
        self.manager_id = manager_id
        self.msg = msg
        self.create_time = create_time
        self.update_time = update_time

    def __repr__(self):
        return 'AdminNotify(notify=%s)' % self.msg

    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', (id))
        return  r and cls(*r)

    @classmethod
    def gets(cls, ids):
        admin_notifies = [cls.get(id) for id in ids if id]
        admin_notifies = filter(None, admin_notifies) 
        return admin_notifies

    @classmethod
    def add(cls, type, status, game_id, manager_id, msg):
        r = store.transac('insert into {table} (type, status, game_id, manager_id, msg, create_time)'
                              'values (%s, %s, %s, %s ,%s, %s)'.format(table=cls.table),
                              (type, status, game_id, manager_id, msg, datetime.datetime.now()))
        if r:
            return cls.get(r)

    @classmethod
    def add_new_book_notify(cls, game_id, msg):
        game = Game.get(game_id)
        if(game.status == BOOK_TYPE_USER):
            r = cls.add(
                type=ADMIN_NEW_BOOK_NOTIFY,
                status=READ_STATUS['unread'],
                game_id=game_id,
                manager_id=game.manager.id,
                msg=msg
            )
            return r

    @classmethod
    def add_cancel_book_notify(cls, game_id, msg='有一个订单被取消'):
        game = Game.get(game_id)
        if(game.status == BOOK_TYPE_USER):
            r = cls.add(
                type=ADMIN_CANCEL_BOOK_NOTIFY,
                status=READ_STATUS['unread'],
                game_id=game_id,
                manager_id=game.manager.id,
                msg=msg
            )
            return r

    @classmethod
    def read_notifies(cls, notifies):
        if notifies:
            for notify in notifies:
                cls.__read_notify_id__(notify.id)

    @classmethod
    def read_notifies_id(cls, ids):
        for notify_id in ids:
            cls.__read_notify_id__(notify_id)

    @classmethod
    def __read_notify_id__(cls, id):
        r = store.transac(
            'update {table} set status=%s where id=%s'.format(table=cls.table),
            (READ_STATUS['read'], id)
        )
        return r

    @classmethod
    def get_unread_count(cls, manager_id):
        r = store.fetchone(
            'select count(id) from {table} where manager_id=%s and status=%s'.format(table=cls.table),
            (manager_id, READ_STATUS['unread'])
        )
        return r and r[0]

    @classmethod
    def get_unread_notifies(cls, manager_id):
        r = store.fetchall(
            cls.select_all + ' where manager_id=%s and status=%s',
            (manager_id, READ_STATUS['unread'])
        )
        if isinstance(r, tuple):
            return [cls(*notify) for notify in r]

    @classmethod
    def get_unread_book_notifies(cls, manager_id):
        r = store.fetchall(
            cls.select_all + ' where manager_id=%s and type=%s and status=%s',
            (manager_id, ADMIN_NEW_BOOK_NOTIFY, READ_STATUS['unread'])
        )
        if isinstance(r, tuple):
            notifies = [cls(*notify) for notify in r]
            return notifies

    @classmethod
    def get_unread_cancel_notifies(cls, manager_id):
        r = store.fetchall(
            cls.select_all + ' where manager_id=%s and type=%s and status=%s',
            (manager_id, ADMIN_CANCEL_BOOK_NOTIFY, READ_STATUS['unread'])
        )
        if isinstance(r, tuple):
            notifies = [cls(*notify) for notify in r]
            return notifies

    def update(self, **kwargs):
        params = ['%s="%s"' % (key, kwargs.get(key)) for key in kwargs]
        update_sql = ', '.join(params)
        r = store.transac('update admin_notify set %s where id=%s' % (update_sql, self.id))
        return r

    def jsonify(self):
        notify = {
            'id': self.id,
            'type': self.type,
            'status': self.status,
            'manager_id': self.manager_id,
            'msg': self.msg,
            'create_time': self.create_time,
            'update_time': self.update_time
        }
        if self.game_id:
            notify['game'] = Game.get(self.game_id).jsonify()

        return notify
