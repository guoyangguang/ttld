# -*- coding:utf-8 -*-

import os

from config import DEBUG
from model.consts import USER_APP, MERCHANT_APP
from apnsclient import Session, Message, APNs

import time
from apns import APNs, Frame, Payload

user_cert_path = os.path.join(os.path.dirname(__file__), 'files', 'wetennis_dev.pem' if DEBUG else 'wetennis_product.pem')
merchant_cert_path = os.path.join(os.path.dirname(__file__), 'files', 'wtmanager_dev.pem' if DEBUG else 'wtmanager_product.pem')

user_client = APNs(use_sandbox=True if DEBUG else False, cert_file=user_cert_path)
merchant_client = APNs(use_sandbox=True if DEBUG else False, cert_file=merchant_cert_path)

class ApnsClient(object):

    @classmethod
    def push(cls, app, device_token, alert, badge, extra):
        payload = Payload(alert=alert, badge=badge, custom={"extra":extra})
        if app == USER_APP:
            user_client.gateway_server.send_notification(device_token, payload)
        elif app == MERCHANT_APP:
            merchant_client.gateway_server.send_notification(device_token, payload)

    # @classmethod
    # def pushes(cls, app, device_tokens, alert, badge, extra):
    #     payload = Payload(alert=alert, badge=badge, custom=extra)


