# coding: utf-8

from datetime import datetime
from libs.sqlstore import store
from model.consts import STATUS_COMPLETE, STATUS_REFUNDED


class AdminOrder(object):

    table = 'admin_order'
    select_all = 'select id, member_id, identity, game_id, amount, status, pay_time,' +\
                 ' create_time, update_time from ' + table

    def __init__(self, _id, member_id, identity, game_id, amount, status, pay_time, create_time, update_time):
        self.id = _id
        self.member_id = member_id
        self.identity = identity
        self.game_id = game_id
        self.amount = amount
        self.status = status
        self.pay_time = pay_time
        self.create_time = create_time
        self.update_time = update_time

    @property
    def price(self):
        return float(self.amount)

    @classmethod
    def add(cls, member_id, identity, game_id, amount, status, create_time=datetime.now()):
        sql = 'insert into {table} (member_id, identity, game_id, amount, status, create_time) values (%s, %s, %s, %s, %s, %s)'\
                .format(table=cls.table)
        _id = store.transac(sql, (member_id, identity, game_id, amount, status, create_time))

        if _id:
            return cls.get(_id)

    @classmethod
    def get(cls, _id):
        r = store.fetchone(cls.select_all + ' where id=%s', _id)
        return r and cls(*r)

    @classmethod
    def get_by_game(cls, game_id):
        r = store.fetchone(cls.select_all + ' where game_id=%s', game_id)
        return r and cls(*r)

    @classmethod
    def gets(cls, ids):
        orders = [cls.get(_id) for _id in ids]
        orders = filter(None, orders)
        return orders 

    def update(self, **kwargs):
        update_params = ','.join('%s=%%s' % f for f in kwargs)
        sql = 'update {table} set '.format(table=self.table) + update_params + ' where id=%s'
        values = kwargs.values()
        values.append(self.id)
        r = store.transac(sql, values)
        return r

    def complete(self):
        self.update(status=STATUS_COMPLETE, pay_time=datetime.now())

    def refund(self):
        self.update(status=STATUS_REFUNDED)

    @property
    def is_refunded(self):
        return self.status == STATUS_REFUNDED
