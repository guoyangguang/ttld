# coding: utf-8

from datetime import datetime
from libs.sqlstore import store

class Province(object):

    table = 'province'

    def __init__(self, id, province):
        self.id = id
        self.province = province

    @property
    def name(self):
        return self.province

    def __repr__(self):
        return '<Province:province=%s>' % self.province

    @classmethod
    def get(cls, id):
        r = store.fetchone('select * from {table} where id=%s'.format(table=cls.table), id)
        return r and cls(*r)

    @classmethod
    def gets(cls, ids):
        provinces = [cls.get(id) for id in ids]
        provinces = filter(None, provinces)
        return provinces 

    @classmethod
    def save(cls, province):
        r = store.transac(
            'insert into {table} (province) values (%s)'.format(table=cls.table),
            province
        )
        if r:
            return cls.get(r)
