import sys

from model.manager import Manager
from model.game import Game
from model.order.game import GameOrder
from model.order.participate import ParticipateOrder
from model.consts import STATUS_COMPLETE


class MerchantStats(object):

    def __init__(self, merchant_id, date):
        self.date = date
        self.merchant_id = merchant_id

    @property
    def merchant(self):
        return Manager.get(self.merchant_id)

    @property
    def reserved_game_count(self):
        return Game.get_manager_book_game_count_by_date(self.merchant_id, self.date)

    def _valid_orders(self, order_cls):
        orders = order_cls.gets_by_manager(self.merchant_id, limit=sys.maxint)
        valid_orders = [order for order in orders
                        if order.status == STATUS_COMPLETE
                        and order.create_time.date() == self.date]
        return valid_orders

    @property
    def valid_order_count(self):
        return len(self._valid_orders(GameOrder))

    @property
    def valid_participate_count(self):
        return len(self._valid_orders(ParticipateOrder))

    @property
    def estimated_revenue(self):
        orders = self._valid_orders(GameOrder)
        return sum(order.amount for order in orders)
