# -*- coding: utf-8 -*-
from model.user import User
from model.game import Game
from model.device import Device
from model.require import Require
from datetime import datetime
from libs.sqlstore import store

# Recommend a match
class Recommend(object):

    table = 'recommend'
    select_all = 'select id, user_id, game_id, create_time, update_time from ' + table 

    def __init__(self, id, user_id, game_id, create_time, update_time):
        self.id = id
        self.user_id = user_id
        self.game_id = game_id
        self.create_time = create_time
        self.update_time = update_time

    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id = %s', id)
        return r and cls(*r)

    @classmethod
    def add(cls, user_id, game_id):
        r = store.transac('insert into ' + cls.table + ' (user_id, game_id, create_time) '
                          'values (%s, %s, %s)', (user_id, game_id, datetime.now()))
        if r:
            return cls.get(r)

    @classmethod
    def can_recommend(cls, user_id):
        r = store.fetchone(
            'select id from ' + cls.table + ' where DateDiff(%s,create_time) < 2 and user_id=%s',
            (datetime.now(), user_id)
        )
        return bool(r)

    @classmethod
    def devices_for_recommend(cls, game):
        r = store.fetchall('select device.id, device.uid, device.token, device.os, device.create_time from '
                           'user, device ' +
                           'where user.id=device.uid and user.id not like %s and user.city_id=%s order by rand() limit 30',
                      (game.creator_id,game.city_id))
        devices = []
        if isinstance(r, tuple):
            for t in r:
                device = Device(*t)
                if Recommend.can_recommend(device.uid):
                    devices.insert(0,device)
        return devices
