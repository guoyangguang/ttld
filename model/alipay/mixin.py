# -*- coding:utf-8 -*-

from .consts import ALIPAY_REFUND_NOTIFY_URL
from .interface import IPayAPI

class AlipayMixin(IPayAPI):

    @classmethod
    def refund_notify_url(cls):
        return ALIPAY_REFUND_NOTIFY_URL
