# -*- coding: utf-8 -*-

from datetime import datetime
from libs.sqlstore import store

class Sign(object):

    table = 'dut_sign'

    STATUS_SIGNED = 'S'    # 代扣
    STATUS_UNSIGNED = 'U'  # 解除代扣
    STATUS_EXPRESS = 'E'   # 不自动代扣但可以一键支付

    VALID_STATUS = (STATUS_SIGNED, STATUS_UNSIGNED)

    def __init__(self, id, user_id, alipay_account_name, alipay_account_id,
                 alipay_sign_id, mobile, status, update_time, plan_id,
                 init_trade_id):
        self.id = id
        self.user_id = user_id
        self.alipay_account_name = alipay_account_name
        self.alipay_account_id = alipay_account_id
        self.alipay_sign_id = alipay_sign_id
        self.mobile = mobile
        self.status = status
        self.update_time = update_time
        self.plan_id = plan_id
        self.init_trade_id = init_trade_id

    @property
    def is_enable(self):
        return self.status != self.STATUS_UNSIGNED

    @classmethod
    def gets_by_user_id(cls, user_id):
        store.execute('select id from ' + cls.table + ' where user_id=%s '
                      'order by update_time desc')
        rs = store.fetchall()
        return cls.gets([id for id, in rs if id])

    @classmethod
    def gets(cls, ids):
        return [cls.get(id) for id in ids if id]

    @classmethod
    def get(cls, id):
        store.execute('select id, user_id, alipay_account_name,'
                      ' alipay_account_id, alipay_sign_id, mobile, status,'
                      ' update_time, plan, init_trade_id from ' + cls.table +
                      ' where id=%s', id)
        r = store.fetchone()
        return r and cls(*r)
