from libs.db import db


NOTIFY_DATA_DB_KEY = '/tencourt/alipay/notify/data'
REFUND_DATA_DB_KEY = '/tencourt/alipay/refund/data'

def save_notify_data(data):
    db_data = db.get(NOTIFY_DATA_DB_KEY)
    if not db_data:
        db.lcreate(NOTIFY_DATA_DB_KEY)

    db.ladd(NOTIFY_DATA_DB_KEY, data)

def save_refund_data(data):
    db_data = db.get(REFUND_DATA_DB_KEY)
    if not db_data:
        db.lcreate(REFUND_DATA_DB_KEY)

    db.ladd(REFUND_DATA_DB_KEY, data)