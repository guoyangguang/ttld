# -*- coding:utf-8 -*-

class IPayAPI(object):

    @classmethod
    def sign(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def verify(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def pay_url(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def refund_url(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def pay_return(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def pay_notify(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def pay_error(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def refund_notify(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def refund_error(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def pay_return_url(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def pay_notify_url(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def pay_error_url(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def refund_notify_url(cls, *args, **kwargs):
        raise NotImplementedError

    @classmethod
    def refund_error_url(cls, *args, **kwargs):
        raise NotImplementedError
