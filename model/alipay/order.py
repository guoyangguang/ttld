#-*- coding: utf-8 -*-
import requests
import hashlib
from datetime import datetime
import urllib
from tools.celery_logger import refund_logger_log

ALIPAYAPI_REFUND_PARTNER_ID = '2088411660677514'
ALIPAYAPI_REFUND_MD5KEY = '84kfbp0suz31kn2do1iv3ceipaggmqat'
ALIPAYAPI_REFUND_SELLER_EMAIL = 'zhifubao@tiantianld.com'
ALIPAYAPI_REFUND_NOTIFY_URL = 'http://www.tiantianld.com/api/alipay/refund/callback/'

'''batch_orders, list, like [[trade_no1, refund_fee1]]
   batch_no_suffix, integer, like order.id
'''
def refund_from_alipay(batch_orders, batch_no_suffix):
    today = datetime.now()
    stringified_batch_orders = list() 
    for order in batch_orders:
        order[1] = str(order[1])
        order.append('negotiate')
        stringified_batch_orders.append('^'.join(order))
    detail_data = '#'.join(stringified_batch_orders) 

    data = dict(
      _input_charset='utf-8',
      service='refund_fastpay_by_platform_pwd',
      partner=ALIPAYAPI_REFUND_PARTNER_ID,
      seller_email=ALIPAYAPI_REFUND_SELLER_EMAIL,
      seller_user_id=ALIPAYAPI_REFUND_PARTNER_ID,
      batch_no=today.strftime('%Y%m%d') + str(batch_no_suffix), 
      batch_num=str(len(batch_orders)),
      detail_data=detail_data,
      refund_date=today.strftime('%Y-%m-%d %H:%M:%S'),
      notify_url=ALIPAYAPI_REFUND_NOTIFY_URL
    )
    return 'https://mapi.alipay.com/gateway.do?' + build_data(data)

def is_callback_from_alipay(notify_id):
    data = dict(
        service='notify_verify'.encode('utf-8'),
        partner=ALIPAYAPI_REFUND_PARTNER_ID.encode('utf-8'),
        notify_id=notify_id.encode('utf-8')
    ) 
    verify_url = 'https://mapi.alipay.com/gateway.do?' + urllib.urlencode(data)
    try:
        response = requests.request(method='GET', url=verify_url)
    except Exception as e:
        refund_logger_log.delay(40, str(e))
    else:
        return response.text == 'true'

def build_data(dic_data):
    keys = dic_data.keys()
    keys.sort()
    str_for_md5 = '&'.join(['%s=%s' % (key, dic_data[key]) for key in keys])
    sign = hashlib.md5((str_for_md5 + ALIPAYAPI_REFUND_MD5KEY).encode('utf-8')).hexdigest()
    dic_data['sign'] = sign
    dic_data['sign_type'] = 'MD5'
    return '&'.join(['%s=%s' % (key, urllib.quote(dic_data[key].encode('utf-8'))) for key in dic_data.keys()])

def check_callback_sign(callback_sign, params):
    keys =params.keys()
    keys.sort()
    str_for_md5 = '&'.join(['%s=%s' % (key, params[key]) for key in keys])
    new_sign = hashlib.md5((str_for_md5 + ALIPAYAPI_REFUND_MD5KEY).encode('utf-8')).hexdigest()
    return callback_sign == new_sign

if __name__ == '__main__':
    import decimal
    #val = refund_from_alipay([['2014112778822880', decimal.Decimal('120.00')]], 2003199)
    val = refund_from_alipay([['2014112842759823', decimal.Decimal('1.00')]], 2003392)
    print(val)
