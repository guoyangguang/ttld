# -*- coding: utf-8 -*-

__author__ = 'alex'

import os
from model.device import Device
from model.admin_device import AdminDevice
from model.apns import ApnsClient
from model.gtpns.gtpns_client import gtpns_client, admin_gtpns_client
from model.consts import IOS, ANDROID, NOTI_USER_NEW_PARTICIPATOR, NOTI_USER_RECOMMEND_GAME, NOTI_USER_ORDER_CONFIRMED, \
    NOTI_USER_ORDER_CANCELED, NOTI_USER_NEW_COMMENT, NOTI_USER_NEW_REPLY, NOTI_ADMIN_NEW_BOOK, NOTI_ADMIN_CANCEL_BOOK, USER_APP, MERCHANT_APP
from model.require import Require
from model.order import Order
import json

class PushClient(object):

    def notify_new_participator(self, order, alert):
        info = {'type':NOTI_USER_NEW_PARTICIPATOR,'game_id':order.game_id, 'owner_id':order.game.creator_id, 'new_player_id':order.creator_id}
        return self.__push_user_message__(info, alert, order.game.creator_id)

    def notify_recommend_game(self, game, device):
        # TODO implement real alert content
        alert = u'有球友发起了一场适合你的约球哦，来看看吧'
        info = {'type':NOTI_USER_RECOMMEND_GAME, 'game_id':game.id}
        return self.__push_user_device_message(info, alert, device)

    def notify_require_owner(self, comment, require):
        return self.__push_user_message__(
            info=dict(require_id=comment.require_id, game_id=require.game_id, type=NOTI_USER_NEW_COMMENT),
            alert=u'你发起的约球有一条新留言',
            user_id=(Require.get(comment.require_id)).creator_id
        )

    def notify_comment_owner(self, comment, require):
        replied_comment = comment.get(comment.comment_id)
        return self.__push_user_message__(
            info=dict(require_id=comment.require_id, game_id=require.game_id, type=NOTI_USER_NEW_REPLY),
            alert=u'有人回复了你在约球里的留言',
            user_id=replied_comment.user_id
        )

    def notify_game_order_confirmed(self, order):
        info = {'type':NOTI_USER_ORDER_CONFIRMED, "order_id":order.id, "owner_id":order.creator_id}
        alert = u'订场被确认'
        return self.__push_user_message__(
            info,alert,order.creator_id
        )

    def notify_game_order_confirm_failed(self, order):
        info = {'type':NOTI_USER_ORDER_CANCELED, "order_id":order.id, "owner_id":order.creator_id}
        alert = u'订场被拒绝'
        return self.__push_user_message__(
            info,alert,order.creator_id
        )

    def notify_admin_new_book(self, order, alert):
        info = {'type':NOTI_ADMIN_NEW_BOOK, 'game_id':order.game_id}
        return self.__push_admin_message__(info, alert, order.manager_id)

    def notify_admin_cancel_book(self, order):
        alert = u'有一个订单被取消'
        info = {'type':NOTI_ADMIN_CANCEL_BOOK, 'game_id':order.game_id}
        return self.__push_admin_message__(info, alert, order.manager_id)


    def __push_admin_message__(self, info, alert, user_id, title=u'动起来商家'):
        device = AdminDevice.get_by_uid(user_id)
        if not device: return False
        if device.os == IOS:
            ApnsClient.push(MERCHANT_APP, device.token, alert, badge=1, extra=info)
        elif device.os == ANDROID:
            template = admin_gtpns_client.notificationTemplate(
                transContent=info, title=title, text=alert, isVibrate=False
            )
            admin_gtpns_client.pushMessageToSingle(template, device.token)
        return True

    def __push_user_message__(self, info, alert, user_id, title=u'动起来'):
        device = Device.get_by_uid(user_id)
        return self.__push_user_device_message(info, alert, device, title)

    def __push_user_device_message(self, info, alert, device, title=u'动起来'):
        if not device: return False
        if device.os == IOS:
            ApnsClient.push(USER_APP, device.token, alert, badge=1, extra=info)
        elif device.os == ANDROID:
            info['title'] = title
            info['text'] = alert
            template = gtpns_client.transmissionTemplate(transContent=json.dumps(info), transType=2)
            gtpns_client.pushMessageToSingle(template, device.token)
        return True

    @classmethod
    def instance(cls):
        if not hasattr(cls, '_instance'):
            cls._instance = cls()
        return getattr(cls, '_instance')

push_client = PushClient.instance()
