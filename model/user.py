# coding: utf-8

from datetime import datetime

from libs.db import db
from libs.sqlstore import store
from libs.mc import mc, cache, ONE_DAY
from view.utils import is_new_platform_version
from model.consts import USER_STATUS_INACTIVE, USER_STATUS_ACTIVE, K_USER, PAYTYPE_GAME, PAYTYPE_PARTICIPATE, \
    ANY_SPORTS_ITEM, TENNIS_ITEM, BADMINTON_ITEM, FOOTBALL_ITEM
from model.photo import Photo, DEFAULT_AVATAR_URL
from model.user_info import UserInfo
from config import MYSQL_CONFIG

MCKEY_USER = 'user:%s'
MCKEY_PLAYED_COUNT = 'played_count:%s:type%s'
USER_INTRO_DB_KEY = '/user/%s/intro'


class User(object):

    table = 'user'
    kind = K_USER
    select_all = 'select id, telephone, wx_id, status, name, birth, gender,' +\
                 ' member_type, reg_time , city_id, height, weight from ' + table

    def __init__(self, id, telephone, wx_id, status, name, birth, gender,
                  member_type, reg_time, city_id, height, weight):
        self.id = id
        self.telephone = telephone
        self.wx_id = wx_id
        self.status = status
        self.name = name
        self.birth = birth
        self.gender = gender or 'y'
        self.member_type = member_type or 'y'
        self.reg_time = reg_time
        self.city_id = city_id
        self.height = height
        self.weight = weight
        self.__user_info = None

    def __repr__(self):
        return '<User:id=%s,telephone=%s>' % (self.id, self.telephone)

    def __eq__(self, other):
        return self.id == other.id

    @property
    def avatar(self):
        p = Photo.gets_by_target(self.id, self.kind, limit=1)
        return p[0] if p else None

    @property
    def tennis_age(self):
        return self.user_info.tennis_age if self.user_info else None

    @property
    def tennis_level(self):
        return self.user_info.tennis_level if self.user_info else None

    @property
    def avatar_url(self):
        user_avatar = self.avatar
        if user_avatar:
            return (user_avatar.url('original'))
        else:
            user_info = UserInfo.get(self.id)
            return user_info.default_avatar if user_info else None
        
    @property
    def intro(self):
        return db.get(USER_INTRO_DB_KEY % self.id) or ''

    @property
    def user_info(self):
        self.__user_info = self.__user_info or UserInfo.get(self.id)
        return self.__user_info

    @property
    def account(self):
        from model.account import Account
        return Account.get_or_add(self.id)

    @property
    def account_grade(self):
        return self.account and self.account.grade

    @property
    def account_grade_name(self):
        return self.account and self.account.grade_name

    @property
    def account_balance(self):
        return self.account and self.account.flush_get_balance() or 0

    @property
    def vip_expire_time(self):
        return self.account and self.account.vip_expire_time

    @property
    def is_vip_serve(self):
        return self.account and self.account.is_vip_serve

    @property
    def tennis_age_level(self):
        if self.tennis_age == -1:
            return '1-'
        if self.tennis_age >= 5:
            return '5+'
        return str(self.tennis_age)

    def delete(self):
        r = store.transac('delete from ' + self.table +
                          ' where id=%s', self.id)
        if r:
            User.flush(self.id)
        return r

    # password should be reset when user change his(her) telephone
    def set_password(self, password):
        r = store.transac(
            'insert into user_password (user_id, password, create_time)' +\
            ' values (%s, %s, %s) on duplicate key update password=%s',
            (self.id, password, datetime.now(), password)
        )
        return r 

    def get_password(self):
        r = store.fetchone('select password from user_password'
                           ' where user_id=%s', (int(self.id),))
        return r and r[0]

    @classmethod
    def is_phone_active(cls, phone):
        user = cls.get_by_telephone(phone)
        if not user:
            return False
        return True if user.status == USER_STATUS_ACTIVE else False

    @classmethod
    def is_active(cls, phone, wx_id):
        user = cls.get_by_telephone(phone) or cls.get_by_wx(wx_id)
        if not user:
            return False
        return True if user.status == USER_STATUS_ACTIVE else False

    @classmethod
    @cache(MCKEY_USER % '{id}', expire=ONE_DAY)
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', (id))
        return r and cls(*r)

    @classmethod
    def get_by_telephone(cls, telephone):
        r = store.fetchone(cls.select_all + ' where telephone=%s', (telephone,))
        return r and cls(*r)

    @classmethod
    def get_by_wx(cls, wx_id):
        r = store.fetchone(cls.select_all + ' where wx_id=%s', (wx_id))
        return r and cls(*r)
    
    @classmethod
    def get_ttld_user_ids(cls):
        users = list()
        tels=['18611440935']
        for tel in tels:
            user = cls.get_by_telephone(tel)
            if user:
                users.append(user)
        ids = [user.id for user in users]
        return ids

    @classmethod
    def gets(cls, ids):
        users = [cls.get(id) for id in ids if id]
        users = filter(None, users)
        return users

    @classmethod
    def add(cls, telephone=None, wx_id=None, status=USER_STATUS_INACTIVE, name='球友',
            birth='1987-01-01 00:00:00', gender=None, member_type=None, intro=None, city_id=1, height=None, Weight=None):
        if not telephone and not wx_id:
            return None

        r = store.transac(
            'insert ignore into ' + cls.table +\
            ' (telephone, wx_id, status, name, birth, gender,' +\
            ' member_type, city_id, height, weight)'+\
            ' values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
            (telephone, wx_id, status, name, birth, gender,
            member_type, city_id, height, Weight)
        )

        if r:
            if intro:
                db.set(USER_INTRO_DB_KEY % r, intro)

            user = cls.get(r)
            UserInfo.add(user.id)
            return user

    @classmethod
    def flush(cls, _id):
        mc.delete(MCKEY_USER % _id)

    @classmethod
    def merge(cls, host_uid, merged_uid):
        import MySQLdb
        from model.game import Game
        from model.order import Order
        from model.require import Require
        from model.account import Account
        merged_account = Account.get(merged_uid)
        conn = MySQLdb.connect(
            host=MYSQL_CONFIG['host'],
            user=MYSQL_CONFIG['user'],
            passwd=MYSQL_CONFIG['password'],
            db=MYSQL_CONFIG['db'],
            port=int(MYSQL_CONFIG['port']),
            charset='utf8'
        )
        cur = conn.cursor()

        try:
            sql = 'update {table} set creator_id=%s where creator_id=%s'.format(table=Game.table)
            cur.execute(sql, (host_uid, merged_uid))
            sql = 'update {table} set creator_id=%s where creator_id=%s'.format(table=Require.table)
            cur.execute(sql, (host_uid, merged_uid))
            sql = 'update {table} set creator_id=%s where creator_id=%s'.format(table=Order.table)
            cur.execute(sql, (host_uid, merged_uid))
            sql = 'update {table} set balance=balance+%s where user_id=%s'.format(table=Account.table)
            cur.execute(sql, (merged_account.balance, host_uid))
            cur.execute('delete from {table} where user_id=%s'.format(table=Account.table), merged_uid)
            cur.execute('delete from {table} where user_id=%s'.format(table=User.table), merged_uid)
            conn.commit()
            User.flush(merged_uid)
        except:
            conn.rollback()
        finally:
            cur.close()
            conn.close()

    def update(self, telephone=None, wx_id=None, status=None, name=None,
               birth=None, gender=None, member_type=None, intro=None, city_id=None, height=None, weight=None):
        self.telephone = telephone or self.telephone
        self.wx_id = wx_id or self.wx_id
        self.status = status or self.status
        self.name = name or self.name
        self.birth = birth or self.birth
        self.gender = gender or self.gender
        self.member_type = member_type or self.member_type
        self.city_id = city_id or self.city_id
        self.height = height or self.height
        self.weight = weight or self.weight

        r = store.transac('update ' + self.table + ' set telephone=%s, wx_id=%s, status=%s, '
                          'name=%s, birth=%s, gender=%s, '
                          'member_type=%s, city_id=%s, height=%s, weight=%s '
                          'where id=%s',
                          (self.telephone, self.wx_id, self.status, self.name, self.birth,
                          self.gender, self.member_type, self.city_id, self.height, self.weight, self.id))
        if r:
            if intro != None:
                db.set(USER_INTRO_DB_KEY % self.id, intro)
            User.flush(self.id)
            return User.get(self.id)

    def update_info(self, tennis_age=None, tennis_level=None, badminton_age=None,
                    football_age=None, football_position=None):
        self.user_info.update(tennis_age=tennis_age, tennis_level=tennis_level, badminton_age=badminton_age,
                        football_age=football_age, football_position=football_position)

    def flush_played_amount(self, sports_type):
        mc.delete(MCKEY_PLAYED_COUNT % ('{self.id}','{sports_type}'))

    def get_user_book_games_amount(self):
        sql = 'select count(id) from trade where creator_id=%s and status="C" and pay_type=%s'
        r = store.fetchone(sql, (self.id, PAYTYPE_GAME))
        return r and r[0]

    def get_user_participate_games_amount(self):
        sql = 'select count(id) from trade where creator_id=%s and status="C" and pay_type=%s'
        r = store.fetchone(sql, (self.id, PAYTYPE_PARTICIPATE))
        return r and r[0]

    @cache(MCKEY_PLAYED_COUNT % ('{self.id}','{sports_type}'), expire=ONE_DAY)
    def get_user_all_games_amount(self, sports_type=ANY_SPORTS_ITEM):
        return self.get_user_book_games_amount(sports_type) + self.get_user_participate_games_amount(sports_type)

    def get_user_book_games_amount(self, sports_type=ANY_SPORTS_ITEM):
        sql = 'select count(game.id) from game inner join court on game.court_id = court.id where game.creator_id = %s and court.sports_type & %s'
        r = store.fetchone(sql, (self.id, sports_type))
        return r and r[0]


    def get_user_participate_games_amount(self, sports_type=ANY_SPORTS_ITEM):
        sql = 'select count(trade.id) from trade inner join participate_order on trade.id = participate_order.id ' \
              'inner join court on participate_order.court_id = court.id ' \
              'where trade.creator_id = %s and trade.status=%s and trade.pay_type=%s and court.sports_type & %s'
        r = store.fetchone(sql, (self.id, 'C', PAYTYPE_PARTICIPATE, sports_type))
        return r and r[0]

    def jsonify(self):
        dict = {
            'id': self.id,
            'telephone': self.telephone,
            'name': self.name,
            'avatar_url': self.avatar_url,
            'birth': self.birth and self.birth.strftime('%Y-%m-%d') or '',
            'gender': self.gender,
            'intro': self.intro,
        }

        if is_new_platform_version():
            dict['height'] = self.height or 0
            dict['weight'] = self.weight or 0
            dict['user_info'] = self.user_info.jsonify() if self.user_info else None
            dict['participate_count'] = {
              'tn': self.get_user_all_games_amount(TENNIS_ITEM),
              'bm': self.get_user_all_games_amount(BADMINTON_ITEM),
              'fb' : self.get_user_all_games_amount(FOOTBALL_ITEM),
            }
        else:
            #兼容天天网球老版本数据#
            dict['tennis_age'] = UserInfo.convertAgeToString(self.tennis_age)
            dict['tennis_level'] = self.tennis_level
            dict['book_games_amount'] = self.get_user_book_games_amount(TENNIS_ITEM)
            dict['participate_games_amount'] = self.get_user_participate_games_amount(TENNIS_ITEM)

        return dict

    def full_jsonify(self):
        dict1 = self.jsonify()
        dict2 = {
            'account_balance': float(self.account_balance),
            'has_password': self.get_password() is not None,
            'member_type': self.account_grade_name,
            'city_id': self.city_id,
        }
        if self.wx_id:
            dict2['wx_id'] = str(self.wx_id)

        return dict(dict1.items() + dict2.items())
