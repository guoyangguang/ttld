#encoding=utf-8

from datetime import datetime
from libs.sqlstore import store 
from model.user import User 

class Followship(object):
    
    table = 'followship'

    def __init__(self, id, followed_id, following_id, create_time, update_time):
        self.id = id
        self.followed_id = followed_id
        self.following_id = following_id
        self.create_time = create_time
        self.update_time = update_time

    @classmethod
    def followeds(cls, user):
        r = store.fetchall(
            'select followed_id from ' + cls.table + ' where following_id=%s', user.id
        )
        if isinstance(r, tuple):
            return User.gets(r) 

    @classmethod
    def followings(cls, user):
        r = store.fetchall(
            'select following_id from ' + cls.table + ' where followed_id=%s', user.id
        )
        if isinstance(r, tuple):
            return User.gets(r)

    @classmethod
    def follow(cls, following, followed):
        if followed not in cls.followeds(following):
            sql = 'insert into ' + cls.table +\
            ' (followed_id, following_id, create_time) values (%s, %s, %s)'
            r = store.transac(sql, (followed.id, following.id, datetime.now()))
            if r:
                return followed
        else:
            return followed 

    @classmethod
    def unfollow(cls, following, followed):
        if followed in cls.followeds(following):
            r = store.transac(
                'delete from ' + cls.table + ' where followed_id=%s and following_id=%s',
                (followed.id, following.id)
            )
            if r:
                return followed
