# -*- coding:utf-8 -*-

from libs.sqlstore import store
from model.consts import DEVICE_OS
from model.device import Device


class AdminDevice(Device):

    table = 'admin_device'

    def __repr__(self):
        return '<AdminDevice:uid=%s,token=%s>' % (self.uid, self.token)

    @property
    def os_str(self):
        return DEVICE_OS.get(self.os) or 'Other OS'
