# coding: utf-8
from datetime import datetime
from libs.sqlstore import store
from model.province import Province

class City(object):

    table = 'city'
    select_all = 'select id, pid, city, weather_code from ' + table

    def __init__(self, id, pid, city, weather_code):
        self.id = id
        self.pid = pid
        self.city = city
        self.weather_code = weather_code

    def __repr__(self):
        return '<City:city=%s>' % self.city

    @property
    def name(self):
        return self.city

    @property
    def province(self):
        return Province.get(self.pid).province

    @classmethod
    def get_by_name(cls, name):
        r = store.fetchone(cls.select_all + ' where city=%s', name)
        return r and cls(*r)

    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', id)
        return r and cls(*r)

    @classmethod
    def gets(self, ids):
        cities = [self.get(id) for id in ids if id]
        cities = filter(None, cities)
        return cities 

    @classmethod
    def save(cls, pid, city, weather_code):
        r = store.transac('insert into {table} (pid, city, weather_code)'
                          ' values (%s, %s, %s)'.format(table=cls.table),
                          (pid, city, weather_code))

        if r:
            return cls.get(r)

    def jsonify(self):
        return {
            'id': self.id,
            'province': self.province,
            'city': self.city,
            'weather_code': self.weather_code
        }
