#encoding=utf-8

from datetime import datetime

from libs.sqlstore import store
from model.user import User
from sets import Set


class Comment(object):

    table = 'comment'
    select_all = 'select id, user_id, require_id, comment_id, body, create_time,' +\
                 ' update_time from ' + table 

    def __init__(self, id=None, user_id=None, require_id=None, comment_id=None,\
                 body=None, create_time=None, update_time=None):
        self.id = id
        self.user_id = user_id
        self.require_id = require_id
        self.comment_id = comment_id
        self.body = body
        self.create_time = create_time
        self.update_time = update_time

    @classmethod
    def get(cls, id):
        sql = cls.select_all + ' where id=%s'
        r = store.fetchone(sql, id)
        if r:
            return cls(*r)

    @classmethod
    def get_comment_replies(cls, comment):
        sql = cls.select_all + ' where comment_id=%s'
        r = store.fetchall(sql, comment.id)
        if isinstance(r, tuple):
            return [cls(*comment) for comment in r]

    @classmethod
    def get_user_comments(cls, require, user):
        sql = cls.select_all + ' where require_id=%s and user_id=%s'
        r = store.fetchall(sql, (require.id, user.id))
        if isinstance(r, tuple):
            return [cls(*comment) for comment in r]
    
    @classmethod
    def get_comments_dict(cls, require, start=0, limit=100):
        comments = cls.__comments_of__(require=require, start=start, limit=limit)
        body = list()

        for comment in comments:
            comment_dict = comment.jsonify()
            body.append(comment_dict)
        return body

    def jsonify(self):
        user = User.get(self.user_id)

        comment_dict =  dict(
            id=self.id,
            require_id=self.require_id,
            user_id=self.user_id,
            comment_id=self.comment_id,
            user_name=user.name,
            user_avatar_url=user.avatar_url,
            body=self.body,
            create_time=self.create_time.strftime('%Y-%m-%d %H:%M:%S'),
        )

        if self.comment_id > 0:
            last_comment = Comment.get(self.comment_id)
            last_comment_owner = User.get(last_comment.user_id)
            comment_dict['comment_user_name'] = last_comment_owner.name
        return comment_dict


    @classmethod
    def __comments_of__(cls, require, start, limit):
        sql = cls.select_all + ' where require_id=%s order by create_time asc limit %s, %s'
        r = store.fetchall(
            sql, 
            (require.id,start,limit),
        )
        if isinstance(r, tuple):
            return [cls(*comment) for comment in r]

    @classmethod
    def create(cls, user, require, comment):
        sql = 'insert into ' + cls.table +\
              ' (user_id, require_id, comment_id, body, create_time) values (%s, %s, %s, %s, %s)'
        r = store.transac(
            sql, 
            (user.id, require.id, comment.comment_id, comment.body, datetime.now())
        )
        if r:
            comment = cls.get(r)
            from tools.postman import notify_new_comment
            notify_new_comment.delay(comment, require)
            return comment

    @classmethod
    def count_replying_users(cls, require, user): 
        comments = cls.get_user_comments(require, user)
        replies = list()
        for comment in comments:
            replies.extend(cls.get_comment_replies(comment))
        user_ids = [reply.user_id for reply in replies]
        return len(list(Set(user_ids)))
