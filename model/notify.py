# coding: utf-8

from datetime import datetime
from model.user import User
from model.game import Game
from model.order import Order
from model.comment import Comment
from libs.sqlstore import store
from consts import READ_STATUS


PARTICIPATE_MSG = u'%s加入了你的约球'
BEGINNING_BOOK_GAMES_MSG = u'有一个订场即将开始'
BEGINNING_PARTICIPATE_GAMES_MSG = u'有一个约球即将开始'
PENDING_ORDER_MSG = u'你还有未支付订单'
ORDER_CONFIRM_SUCCESS = u'订单确认成功'
ORDER_CONFIRM_FAIL = u'订单确认失败'

USER_NOTIFY_NEW_PARTICIPATOR  = 1
USER_NOTIFY_GAME_COMING       = 2
USER_NOTIFY_ORDER_CONFIRMED   = 3
USER_NOTIFY_ORDER_CANCELED    = 4
USER_NOTIFY_NEW_COMMENT       = 5
USER_NOTIFY_NEW_REPLY         = 6

class Notify(object):

    table = 'notify'
    select_all = 'select id, notify_type, user_id, game_id, order_id, participant_id, msg, ' +\
                 'status, create_time, update_time from ' + table

    def __init__(self, id, notify_type, user_id, game_id, order_id, participant_id, msg, status, create_time, update_time):
        self.id = id
        self.notify_type = notify_type
        self.user_id = user_id
        self.game_id = game_id
        self.order_id = order_id
        self.participant_id = participant_id
        self.msg = msg
        self.status = status
        self.create_time = create_time
        self.update_time = update_time

    def __repr__(self):
        return '<Notify:notify=%s>' % self.msg

    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', id)
        return r and cls(*r)

    @classmethod
    def gets(cls, ids):
        notifies = [cls.get(id) for id in ids]
        notifies = filter(None, notifies)
        return notifies 

    @classmethod
    def __add__(cls, notify_type, user_id, game_id, order_id, participant_id, msg, status=READ_STATUS['unread']):
        sql = 'insert into ' +  cls.table +\
              ' (notify_type , user_id, game_id, order_id, participant_id, msg, status, create_time)' +\
              ' values (%s, %s, %s, %s, %s, %s, %s, %s)'
        r = store.transac(
            sql,
            (notify_type, user_id, game_id, order_id, participant_id, msg, status, datetime.now())
        )
        if r:
            return cls.get(r)
        else:
            return r

    @classmethod
    def get_user_unread_notify_count(cls, user_id):
        cls.add_user_games_notify(user_id)
        # cls.add_user_pending_orders_notify(user_id)
        r = store.fetchone(
            'select count(id) from ' + cls.table + ' where user_id=%s and status=%s', 
            (user_id, READ_STATUS['unread'])
        )
        return r and r[0]

    @classmethod
    def get_user_unread_notify(cls, user_id, start=0, limit=20):
        ids = store.fetchall(
            'select id from {table} where user_id=%s and status=%s order by id desc limit %s, %s'
            .format(table=cls.table), (user_id, READ_STATUS['unread'], start, limit))
        if isinstance(ids, tuple):
            notifies = cls.sign_notify_read(ids)
            return notifies

    @classmethod
    def get_user_notify(cls, user_id, start=0, limit=20):
        rs = store.fetchall(cls.select_all + ' where user_id=%s order by id desc limit %s, %s', (user_id, start, limit))
        if isinstance(rs, tuple):
            return [cls(*r) for r in rs]

    @classmethod
    def add_user_games_notify(cls, user_id):
        games = Order.get_beginning_games(user_id)

        for game in games:
            game_order = game.get_game_order()
            if game.require:
                n = cls.get_by_user_game_msg(user_id, game.id, BEGINNING_BOOK_GAMES_MSG)
                if n:
                    n.update(msg=BEGINNING_PARTICIPATE_GAMES_MSG, status=READ_STATUS['unread'])
                elif not cls.has_games(user_id, game.id, BEGINNING_PARTICIPATE_GAMES_MSG):
                    cls.__add__(USER_NOTIFY_GAME_COMING, user_id, game.id, game_order.id, None, BEGINNING_PARTICIPATE_GAMES_MSG, READ_STATUS['unread'])
            else:
                if not cls.has_games(user_id, game.id, BEGINNING_BOOK_GAMES_MSG):
                    cls.__add__(USER_NOTIFY_GAME_COMING, user_id, game.id, game_order.id, None, BEGINNING_BOOK_GAMES_MSG, READ_STATUS['unread'])

        return

    # @classmethod
    # def add_user_pending_orders_notify(cls, user_id):
    #     orders = Order.get_user_pending_orders(user_id)
    #     for order in orders:
    #         if cls.has_pending_orders(user_id, order.id, PENDING_ORDER_MSG):
    #             cls.__add__(user_id, None, order.id, None, PENDING_ORDER_MSG, READ_STATUS['unread'])
    #
    #     return

    @classmethod
    def add_join_require_notify(cls, user_id, game_id, order_id, participant_id, alert):
        cls.__add__(
            notify_type=USER_NOTIFY_NEW_PARTICIPATOR,
            user_id=user_id,
            game_id=game_id,
            order_id=order_id,
            participant_id=participant_id,
            msg=alert,
            status=READ_STATUS['unread']
        )

    @classmethod
    def add_user_order_confirm_notify(cls, user_id, game_id, order_id):
        cls.__add__(USER_NOTIFY_ORDER_CONFIRMED, user_id, game_id, order_id, None, ORDER_CONFIRM_SUCCESS, READ_STATUS['unread'])

    @classmethod
    def add_user_order_confirm_fail_notify(cls, user_id, game_id, order_id):
        cls.__add__(USER_NOTIFY_ORDER_CANCELED, user_id, game_id, order_id, None, ORDER_CONFIRM_FAIL, READ_STATUS['unread'])

    @classmethod
    def add_comment(cls, require):
        cls.delete_comment_notify(require)
        cls.__add__(
            USER_NOTIFY_NEW_COMMENT,
            require.creator_id,
            require.game_id,
            None,
            None,
            '你发起的活动有新留言',
            status=READ_STATUS['unread']
        )
    
    @classmethod
    def delete_comment_notify(cls, require):
        r = store.transac(
                'delete from ' + cls.table + ' where user_id=%s and game_id=%s and notify_type=%s',
                (require.creator_id, require.game_id, USER_NOTIFY_NEW_COMMENT)
            )
        return r

    @classmethod
    def add_reply_of_comment(cls, require, comment):
        replied_comment = Comment.get(comment.comment_id)
        replied_user = User.get(replied_comment.user_id)
        replying_user = User.get(comment.user_id)
        num = Comment.count_replying_users(require, replied_user)
        cls.delete_reply_notify(require, replied_user)
        cls.__add__(
            USER_NOTIFY_NEW_REPLY,
            replied_user.id,
            require.game_id,
            None,
            None,
            replying_user.name + '...' + str(num) + '人回复了你',
            status=READ_STATUS['unread']
        )

    @classmethod
    def delete_reply_notify(cls, require, user):
        r = store.transac(
                'delete from ' + cls.table + ' where user_id=%s and game_id=%s and notify_type=%s',
                (user.id, require.game_id, USER_NOTIFY_NEW_REPLY)
            )
        return r

    @classmethod
    def has_games(cls, user_id, game_id, msg):
        r = store.fetchone(
            'select count(id) from notify where user_id=%s and game_id=%s and msg=%s',
            (user_id, game_id, msg)
        )
        return r and r[0]

    @classmethod
    def has_pending_orders(cls, user_id, order_id, msg):
        r = store.fetchone(
            'select count(id) from notify where user_id=%s and order_id=%s and msg=%s',
            (user_id, order_id, msg)
        )
        return r and r[0]

    @classmethod
    def sign_notify_read(cls, ids):
        for id in ids:
            r = store.transac('update {table} set status=1 where id=%s'.format(table=cls.table), id)
            if not r:
                ids.remove(id)
        return cls.gets(ids)


    @classmethod
    def get_by_user_game_msg(cls, user_id, game_id, msg):
        r = store.fetchone(
            cls.select_all + ' where user_id=%s and game_id=%s and msg=%s',
            (user_id, game_id, msg)
        )
        return r and cls(*r)

    def update(self, **kwargs):
        params = ['%s="%s"' % (key, kwargs.get(key)) for key in kwargs]
        update_sql = ', '.join(params)
        r = store.transac('update notify set %s where id=%s' % (update_sql, self.id))
        return r

    def jsonify(self):
        notify = {
            'id': self.id,
            'msg': self.msg,
            'type': self.notify_type,
            'status': self.status,
            'create_time': self.create_time
        }

        if self.game_id:
            game = Game.get(self.game_id)
            notify['game'] = game.jsonify() if game else []

        if self.order_id:
            order = Order.get(self.order_id)
            notify['order'] = order.jsonify() if order else []

        if self.participant_id:
            notify['participant_id'] = self.participant_id

        return notify
