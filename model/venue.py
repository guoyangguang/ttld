# -*- coding: utf-8 -*-

import sys
import math
from datetime import datetime, timedelta, time
from MySQLdb import IntegrityError
from libs.sqlstore import store
from libs.mc import mc, cache, HALF_HOUR, ONE_DAY, HALF_DAY, new_cache
from model.consts import K_VENUE, TT_PRICE
from model.photo import Photo
from model.city import City
from model.user import User
from model.manager import Manager
from model.consts import COURT_STATUS_FREE, COURT_STATUS_EXPIRED, TENNIS_ITEM, BADMINTON_ITEM, FOOTBALL_ITEM, ANY_SPORTS_ITEM

MCKEY_VENUE = 'venue:%s'
MCKEY_VENUE_RECENT_COURT_SITUATION = 'venue:%s:recent_court_situation'
MCKEY_VENUE_AVATAR_URL = 'venue:%s:avatar_url'
MCKEY_VENUE_MAX_PRICE = 'venue:%s:max_price'
MCKEY_VENUE_MIN_PRICE = 'venue:%s:min_price'
MCKEY_VENUE_COURTS = 'venue:%s:courts'
MCKEY_VENUE_CITYLIST = 'venue_citylist'
BOOK_PERCENTAGE_STANDARD = 0.5

class Venue(object):

    table = 'venue'
    kind = K_VENUE
    select_all = 'select id, manager_id, name, address, telephone, cellphone, traffic_info, latitude, ' +\
                 'longitude, open_time, close_time, intro, city_id, create_time, update_time, sports_type from ' +\
                 table

    def __init__(self, id, manager_id, name, address, telephone, cellphone, traffic_info,
                 latitude, longitude, open_time, close_time, intro, city_id, create_time, update_time, sports_type):
        self.id = id
        self.manager_id = manager_id
        self.name = name
        self.address = address
        self.telephone = telephone
        self.cellphone = cellphone
        self.traffic_info = traffic_info
        self.latitude = latitude
        self.longitude = longitude
        self.open_time = open_time
        self.close_time = close_time
        self.intro = intro
        self.city_id = city_id
        self.create_time = create_time
        self.update_time = update_time
        self.sports_type = sports_type

    def __repr__(self):
        return '<Venue:id=%s>' % self.id

    def __eq__(self, venue):
        return (venue and isinstance(venue, Venue)
                and (venue.id == self.id))

    def __ne__(self, venue):
        return not (venue and isinstance(venue, Venue)
                and (venue.id == self.id))

    def __hash__(self):
        return int(self.id)

    # 开馆时间可能不是整点
    @property
    def start_book_time(self):
        start = (datetime.min + self.open_time).time()
        if start.minute != 0 or start.second != 0:
            return timedelta(hours=start.hour + 1)
        return self.open_time

    # 打烊时间可能不是整点
    @property
    def end_book_time(self):
        end = (datetime.min + self.close_time).time()
        if end.minute != 0 or end.second != 0:
            return timedelta(hours=end.hour)
        return self.close_time

    @property
    def avatar(self):
        from model.album import Album

        albums = Album.gets_by_venue(self.id)

        if albums:
            album = albums[0]
            pid = album.photo_id

            if pid:
                p = Photo.get(pid)
                return p

        photo = Photo.gets_by_target(self.id, self.kind, limit=1)
        if photo:
            return photo[0]

        return None

    @property
    @new_cache(MCKEY_VENUE_AVATAR_URL, expire=ONE_DAY)
    def avatar_url(self):
        return (self.avatar.url('original') if self.avatar
                else None)

    @property
    def court_counts(self):
        r = store.fetchone('select count(id) from court where venue_id=%s', self.id)
        return r and r[0]

    @property
    def manager(self):
        return Manager.get(self.manager_id)

    @property
    @new_cache(MCKEY_VENUE_COURTS, expire=ONE_DAY)
    def courts(self):
        from model.court import Court
        return Court.get_by_venue(self.id, limit=sys.maxint)

    @property
    @new_cache(MCKEY_VENUE_RECENT_COURT_SITUATION, expire=ONE_DAY)
    def recent_court_situation(self):
        from model.game import Game
        venue_courts = self.court_counts
        open_hours = ((self.end_book_time - self.start_book_time).seconds / 3600)
        total_counts = open_hours * venue_courts * 7
        book_counts = 0
        start_time = datetime.combine(datetime.now().date(), time(0, 0))
        end_time = start_time + timedelta(weeks=1)

        games = Game.gets_by_venue_and_date(self.id, start_time, end_time)

        for game in games:
            if game.is_created_by_manager():
                book_counts = book_counts + (game.end_time - game.start_time).seconds / 3600
            else:
                game_order = game.get_game_order()
                if game_order and not game_order.is_expire:
                    book_counts = book_counts + (game.end_time - game.start_time).seconds / 3600
        book_percentage = 0 if total_counts <=0 else round(float(book_counts) / total_counts, 5)
        court_situation = u'充裕' if book_percentage < BOOK_PERCENTAGE_STANDARD else u'紧张'

        return court_situation

    @property
    @new_cache(MCKEY_VENUE_MAX_PRICE, expire=ONE_DAY)
    def maximum_price(self):
        venue_id = self.id
        sql = 'select max(price) from venue_price where venue_id=%s and price_type=%s'
        r = store.fetchone(sql, (venue_id, TT_PRICE))
        if r and r[0]:
            return int(r[0])

    @property
    @new_cache(MCKEY_VENUE_MIN_PRICE, expire=ONE_DAY)
    def minimum_price(self):
        venue_id = self.id
        sql = 'select min(price) from venue_price where venue_id=%s and price_type=%s'
        r = store.fetchone(sql, (venue_id, TT_PRICE))
        if r and r[0]:
            return int(r[0])

    def is_owner(self, merchant):
        if not merchant:
            return False
        return merchant.id == self.manager_id

    def can_manage(self, merchant):
        return self.is_owner(merchant) or merchant.is_admin()

    def isfull(self, date):
        for court in self.courts:
            if not court.isfull(date):
                return False
        return True

    def isAvailable(self, date):
        start = datetime.combine(date.date(), time())
        start_point = start + self.start_book_time
        if date < start_point:
            date = start_point
        end_time = datetime.combine(date.date(), time()) + self.end_book_time

        for court in self.courts:
            if court.can_book(date, end_time):
                return True
        return False

    def has_price(self):
        venue_id = self.id
        sql = 'select price from venue_price where venue_id=%s'
        r = store.fetchone(sql, venue_id)
        return bool(r)

    def hour_interval(self):
        return 2 if self.sports_type == FOOTBALL_ITEM else 1

    def update(self, name=None, address=None, telephone=None, cellphone=None,
               traffic_info=None, latitude=None, longitude=None,
               open_time=None, close_time=None, intro=None, city_id=None, sports_type=None):
        self.name = name or self.name
        self.address = address or self.address
        self.telephone = telephone or self.telephone
        self.cellphone = cellphone or self.cellphone
        self.traffic_info = traffic_info or self.traffic_info
        self.latitude = latitude or self.latitude
        self.longitude = longitude or self.longitude
        self.open_time = open_time or self.open_time
        self.close_time = close_time or self.close_time
        self.intro = intro or self.intro
        self.city_id = city_id or self.city_id
        self.sports_type = sports_type or self.sports_type

        r = store.transac('update ' + self.table + ' set name=%s, address=%s, '
                          'telephone=%s, cellphone=%s, traffic_info=%s, latitude=%s, '
                          'longitude=%s, open_time=%s, close_time=%s, intro=%s, city_id=%s ,sports_type=%s'
                          'where id = %s',
                          (self.name, self.address, self.telephone, self.cellphone,
                           self.traffic_info, self.latitude, self.longitude,
                           self.open_time, self.close_time, self.intro, self.city_id, self.id, self.sports_type))
        if r:
            self.flush()
            return Venue.get(self.id)

    def delete(self):
        r = store.transac('delete from ' + self.table + ' where id=%s', self.id)
        return r

    def scheduleInfo(self, date, is_for_merchant):

        courts = []
        for court in self.courts:
            schedule = court.schedule(date)

            start_point = datetime.combine(date, time()) + self.open_time
            end_point = datetime.combine(date, time()) + self.close_time

            dating = []

            start_hour = start_point.hour
            end_hour = end_point.hour if end_point.hour>0 else 24
            for point in range(start_hour, end_hour):

                if point%self.hour_interval() != 0 or point+self.hour_interval()>end_hour:
                    continue
                start_time = datetime.combine(date, time()) + timedelta(hours=point)
                end_time = start_time + timedelta(hours=self.hour_interval())

                expired = start_time < datetime.now()
                dct = None

                from model.game import Game
                start_time_str = start_time.strftime('%Y-%m-%d %H:%M:%S')

                for i in range(point, point + self.hour_interval()):
                    if i in schedule:
                        game_id = schedule.get(point)
                        game = Game.get(game_id)
                        if not game:
                            continue
                        dct = game.merchantScheduleInfo if is_for_merchant else game.scheduleInfo

                dct = dct or {
                    'price': court.price(start_time, end_time),
                    'game_id': '',
                    'court_id': int(court.id),
                    'status': COURT_STATUS_EXPIRED if expired else COURT_STATUS_FREE,
                }
                dct['time'] = start_time_str
                dct['in'] = self.hour_interval()
                if(is_for_merchant or not expired):
                    dating.append(dct)


            d = {
                'court': court.jsonify(),
                'schedule': dating
            }
            courts.append(d)
        return courts

    def jsonify(self):
        from model.game import Game
        weekly_participate_count = Game.get_weekly_participate_count_by_venue(self.id, datetime.now().date())
        return {
            'id': int(self.id),
            'name': self.name or '',
            'address': self.address or '',
            'telephone': self.telephone or self.cellphone or '',
            'traffic_info': self.traffic_info or '',
            'latitude': self.latitude or '',
            'longitude': self.longitude or '',
            'open_time': str(self.open_time) or '',
            'close_time': str(self.close_time) or '',
            'avatar_url': self.avatar_url or '',
            'intro': self.intro or '',
            'weekly_game_count': weekly_participate_count or 0,
            'court_count': len(self.courts) or 0,
            'maximum_price': self.maximum_price,
            'minimum_price': self.minimum_price,
            'sports_type': self.sports_type,
        }

#Class Methods#
    @classmethod
    @cache(MCKEY_VENUE % '{id}', expire=ONE_DAY)
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', id)
        return r and cls(*r)

    @classmethod
    def gets(cls, ids):
        venues = [cls.get(id) for id in ids if id]
        venues = filter(None, venues)
        return venues 

    @classmethod
    def gets_by_manager(cls, manager_id):
        r = store.fetchall(cls.select_all + ' where manager_id=%s', (manager_id,))
        if isinstance(r, tuple):
            return [cls(*venue) for venue in r]

    @classmethod
    def get_by_name(cls, name):
        r = store.fetchone(cls.select_all + ' where name=%s', name)
        return r and cls(*r)

    @classmethod
    def get_venues(cls, start=0, limit=20, sports_type=ANY_SPORTS_ITEM):
        r = store.fetchall(Venue.select_all + ' where `sports_type` & %s > 0' +' limit %s,%s', (sports_type, start, limit))
        if isinstance(r, tuple):
            return [cls(*venue) for venue in r]

    @classmethod
    def total_counts(cls, sports_type=ANY_SPORTS_ITEM):
        r = store.fetchone('select count(id) from venue' + ' where `sports_type` & %s > 0', (sports_type))
        return r and r[0]

    @classmethod
    def get_regular_venues(cls, user_id, sports_type=ANY_SPORTS_ITEM):
        from model.order.game import GameOrder
        from model.order.participate import ParticipateOrder
        game_orders = GameOrder.gets_by_user(user_id)
        participate_orders = ParticipateOrder.gets_by_user(user_id)
        venues = {}
        for order in game_orders + participate_orders:
            venue = order.venue
            if venue:
                if venue.id not in venues:
                    venues[venue.id] = (venue, 1)
                else:
                    v, count = venues[venue.id]
                    venues[venue.id] = (venue, count+1)
        venues = filter(lambda v: v.sports_type & sports_type == v.sports_type, venues)
        venues = sorted(venues.values(), key=lambda v: v[1], reverse=True)
        # TODO start limit
        return [v for v, count in venues]

    @classmethod
    def add(cls, manager_id, name, address, telephone, cellphone='', traffic_info='',
            latitude=0.0, longitude=0.0, open_time=None, close_time=None,
            intro='', city_id=1, create_time=datetime.now(), sports_type=0):
        if not manager_id or not name or not address or not telephone:
            return False
        r = store.transac('insert into ' + cls.table +
                          ' (manager_id, name, address, telephone, cellphone,'
                          ' traffic_info, latitude, longitude, open_time,'
                          ' close_time, intro, city_id, create_time, sports_type) '
                          'values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)',
                          (manager_id, name, address, telephone, cellphone,
                           traffic_info, latitude, longitude,
                           open_time, close_time, intro, city_id, create_time, sports_type))
        if r:
            return cls.get(r)

    @classmethod
    def flush(cls, venue_id):
        mc.delete(MCKEY_VENUE % venue_id)

    @classmethod
    def flush_avatar_url(cls, venue_id):
        mc.delete(MCKEY_VENUE_AVATAR_URL % venue_id)

    @classmethod
    @cache(MCKEY_VENUE_CITYLIST, expire=HALF_DAY)
    def get_cities(cls, start=None, limit=None):
        if start and limit:
            r = store.fetchall('select distinct venue.city_id from venue, venue_price where venue_price.venue_id like venue.id limit %s, %s'.format(start, limit))
        else:
            r = store.fetchall('select distinct venue.city_id from venue, venue_price where venue_price.venue_id like venue.id')
        if isinstance(r, tuple):
            return City.gets(r)

    @new_cache(MCKEY_VENUE_MAX_PRICE, expire=ONE_DAY)
    def get_maximum_price(self):
        venue_id = self.id
        sql = 'select max(price) from venue_price where venue_id=%s and price_type=%s'
        r = store.fetchone(sql, (venue_id, TT_PRICE))
        if r and r[0]:
            return int(r[0])

    @new_cache(MCKEY_VENUE_MIN_PRICE, expire=ONE_DAY)
    def get_minimum_price(self):
        venue_id = self.id
        sql = 'select min(price) from venue_price where venue_id=%s and price_type=%s'
        r = store.fetchone(sql, (venue_id, TT_PRICE))
        if r and r[0]:
            return int(r[0])
    
    @classmethod
    def total_counts(cls):
        sql = 'select count(id) from venue'
        r = store.fetchone(sql)
        return r and r[0]

    def has_price(self):
        venue_id = self.id
        sql = 'select price from venue_price where venue_id=%s'
        r = store.fetchone(sql, venue_id)
        return bool(r)

def get_venues(start=0, limit=20):
    r = store.fetchall(Venue.select_all + ' limit %s,%s', (start, limit))
    if isinstance(r, tuple):
        return [Venue(*venue) for venue in r] 

def get_regular_venues(user_id):
    from model.order.game import GameOrder
    from model.order.participate import ParticipateOrder
    game_orders = GameOrder.gets_by_user(user_id)
    participate_orders = ParticipateOrder.gets_by_user(user_id)
    venues = {}
    for order in game_orders + participate_orders:
        venue = order.venue
        if venue:
            if venue.id not in venues:
                venues[venue.id] = (venue, 1)
            else:
                v, count = venues[venue.id]
                venues[venue.id] = (venue, count+1)
    venues = sorted(venues.values(), key=lambda v: v[1], reverse=True)
    # TODO start limit
    return [v for v, count in venues]

def search_venues(user_id=None, q=None, start_time=None, latitude=None, longitude=None, city_id=None, sports_type=ANY_SPORTS_ITEM):
    query = []
    params = []

    if q:
        query.append('name like %s')
        params.append('%' + q + '%')

    if city_id:
        query.append('city_id = %s')
        params.append(city_id)


    if latitude and longitude:
        l_query = 'order by acos(sin(({latitude}*{pi})/180 ) * sin((latitude*{pi})/180) + cos(({latitude}*{pi})/180) * cos((latitude*{pi})/180) ' \
                  '* cos(({longitude}*{pi})/180-(longitude*{pi})/180)) * 6380 asc'.format(latitude=latitude,longitude=longitude,pi=math.pi)
    else:
        l_query = ''

    if q or city_id:
        sql = 'select id from venue where ' + ' and '.join(query) + l_query
    else:
        if l_query:
            sql = 'select id from venue %s' % l_query
        else:
            sql = 'select id from venue'

    ids = store.fetchall(sql, params)
    venues = Venue.gets(ids)
    venues = filter(lambda  v: v.has_price() and v.sports_type & sports_type == v.sports_type, venues)

    #选择了整点时间，现在客户端选“不限”的时候会传当前时间，先这么支持。以后考虑客户端改进
    if start_time and start_time.minute == 0 and start_time.second == 0:
        venues = filter(lambda v: v.isAvailable(start_time), venues)

    ttld_user_ids = User.get_ttld_user_ids()
    if not(user_id) or (user_id and not(user_id in ttld_user_ids)):
        venues = filter(lambda venue: venue.name != '天天网球馆', venues)
    return venues
