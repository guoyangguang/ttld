# -*- coding:utf-8 -*-

from datetime import datetime

from libs.sqlstore import store


class Token(object):

    table = "token"
    select_all = 'select id, access_token, user_id, app_id, create_time, update_time from ' + table 

    def __init__(self, id, access_token, user_id, app_id, create_time, update_time):
        self.id = id
        self.access_token = access_token
        self.user_id = user_id
        self.app_id = app_id
        self.create_time = create_time
        self.update_time = update_time

    @classmethod
    def add(cls, access_token, user_id, app_id=0):
        if not cls.get_by_uid(user_id):
            r = store.transac(
                'insert into ' + cls.table +\
                ' (access_token, user_id, app_id, create_time)' +\
                ' values(%s, %s, %s, %s)',
                (access_token, user_id, app_id, datetime.now())
            )
            if r:
                return cls.get(r)

    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', id)
        return r and cls(*r)

    @classmethod
    def get_by_uid(cls, user_id):
        r = store.fetchone(cls.select_all + ' where user_id=%s', user_id)
        return r and cls(*r)

    def delete(self):
        r = store.transac(
            'delete from ' + self.table + ' where user_id=%s', (self.user_id)
        )
        return r

class ManagerToken(Token):

    table = "ManagerToken"
