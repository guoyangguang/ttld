# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from libs.sqlstore import store
from model.user import User
from model.game import Game
from model.consts import (
    PAYMETHOD_ALIPAY, GATEWAY_SECURITY,
    PAYTYPE_GAME, PAYTYPE_PARTICIPATE, PAYTYPE_VIP, PAYTYPE_CHARGE,
    STATUS_PENDING, STATUS_COMPLETE, STATUS_FAIL,
    STATUS_SUSPEND, STATUS_REFUND_PENDING,
    STATUS_REFUNDED, VALID_STATUS, PENDING_STATUS,
    STATUS_DICT, STATUS_EN_DICT, ORDER_EXPIRED_DELTA, STATUS_CONFIRMING
)


class Order(object):

    table = 'trade'
    select_all = 'select id, creator_id, amount, pay_type, create_time, pay_time, '+\
                 'status, pay_method, gateway, refund_time, telephone from ' + table

    def __init__(self, id, creator_id, amount, pay_type,
                 create_time, pay_time, status, pay_method, gateway, refund_time=None, telephone=None):
        self.id = id
        self.creator_id = creator_id
        self.amount = amount
        self.pay_type = pay_type
        self.status = status
        self.create_time = create_time
        self.pay_time = pay_time
        self.pay_method = pay_method
        self.gateway = gateway
        self.refund_time = refund_time
        self.telephone = telephone

    @classmethod
    def new(cls, creator_id, amount, pay_type, create_time, pay_time=None,
            status=STATUS_PENDING, pay_method=PAYMETHOD_ALIPAY,
            gateway=GATEWAY_SECURITY, telephone=None):
        r = store.transac(
            'insert into ' + cls.table +\
            ' (creator_id, amount, pay_type, status, pay_method, gateway, telephone)' +\
            ' values (%s, %s, %s, %s, %s, %s, %s)',
            (creator_id, amount, pay_type, status,
             pay_method, gateway, telephone)
        )
        return r 

    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', id)
        if r: 
            cls = get_order_class(r[3])
            return cls and cls(*r)

    @classmethod
    def gets(cls, ids):
        orders = [cls.get(id) for id in ids]
        orders = filter(None, orders)
        return orders 

    @classmethod
    def update(cls, id, **kwargs):
        if 'status' in kwargs and kwargs['status'] not in VALID_STATUS:
            raise InvalidStatusException
        invalid_fields = ('id',)
        for field in invalid_fields:
            if field in kwargs:
                del kwargs[field]
        values = kwargs.values()
        values.append(id)
        r = store.transac('update ' + cls.table + ' set ' +
                          ','.join('%s=%%s' % f for f in kwargs) +
                          ' where id=%s', values)
        return r

    @classmethod
    def complete(cls, id):
        cls.update(id, **{'status': STATUS_COMPLETE})

    @classmethod
    def confirming(cls, id):
        cls.update(id, **{'status': STATUS_CONFIRMING,
                          'pay_time': datetime.now()})

    @classmethod
    def fail(cls, id):
        cls.update(id, **{'status': STATUS_FAIL})

    @classmethod
    def filter(cls, start=None, limit=None, **kwargs):
        if not kwargs:
            return []

        invalid_fields = ['creator_id', 'pay_type', 'status', 'create_time']
        for field in kwargs:
            if field not in invalid_fields:
                return []

        create_time = kwargs.pop('create_time') if 'create_time' in kwargs else None

        # 注意值的顺序
        values = kwargs.values()
        if create_time:
            values.append(create_time)

        # 创建时间使用 >= 条件
        wheres = ['%s=%%s' % key for key in kwargs]
        if create_time:
            wheres.append('create_time>=%s')

        if start and limit:
            values.append(start)
            values.append(limit)
            rs = store.fetchall('select id from ' + cls.table + ' where ' +
                                ' and '.join(wheres) + ' order by id desc '
                                'limit %s, %s', values)
        else:
            rs = store.fetchall('select id from ' + cls.table + ' where ' +
                                ' and '.join(wheres) + ' order by id desc', values)

        return [Order.get(r) for r in rs]

    @classmethod
    def get_user_pending_orders(cls, user_id):
        '''返回没有支付的订场和约球订单(未过期的)'''

        gs = cls.get_user_pending_game_orders(user_id)
        ps = cls.get_user_pending_participate_orders(user_id)
        return gs + ps

    @classmethod
    def get_user_pending_participate_orders(cls, user_id):
        '''返回没有支付的约球订单(未过期的)'''

        create_time = datetime.now() - timedelta(seconds=ORDER_EXPIRED_DELTA)
        query = {
            'creator_id': user_id,
            'status': STATUS_PENDING,
            'create_time': create_time,
            'pay_type': PAYTYPE_PARTICIPATE
        }
        return cls.filter(0, 100, **query)

    @classmethod
    def get_user_pending_part_orders_by_tel(cls, tel):
        create_time = datetime.now() - timedelta(seconds=ORDER_EXPIRED_DELTA)
        query = {
            'telephone': tel,
            'status': STATUS_PENDING,
            'create_time': create_time,
            'pay_type': PAYTYPE_PARTICIPATE
        }
        return cls.filter(0, 100, **query)

    @classmethod
    def get_user_pending_game_orders(cls, user_id):
        '''返回没有支付的订场订单(未过期的)'''

        create_time = datetime.now() - timedelta(seconds=ORDER_EXPIRED_DELTA)
        query = {
            'creator_id': user_id,
            'status': STATUS_PENDING,
            'create_time': create_time,
            'pay_type': PAYTYPE_GAME
        }
        return cls.filter(0, 100, **query)

    @classmethod
    def get_user_lastest_completed_order(cls, user_id, pay_type=PAYTYPE_VIP):
        rs = cls.filter(0, 1, **{'creator_id': user_id,
                                 'status': STATUS_COMPLETE,
                                 'pay_type': pay_type})
        return rs and rs[0]

    def verify(self, user=None):
        raise NotImplementedError

    def pay_verify(self, user=None):
        raise NotImplementedError

    def pay(self):
        pass
    
    def refund(self):
        pass
        
    def body_desc(self):
        return ''

    @property
    def creator(self):
        return User.get(self.creator_id)

    @property
    def status_cn(self):
        return STATUS_DICT.get(self.status, '未知状态')

    @property
    def status_en(self):
        return STATUS_EN_DICT.get(self.status, 'pending')

    @property
    def completed(self):
        return self.status in (STATUS_COMPLETE, STATUS_CONFIRMING, STATUS_REFUND_PENDING)

    @property
    def is_refunded(self):
        return self.status == STATUS_REFUNDED

    @classmethod
    def get_beginning_games(cls, user_id):
        gs = store.fetchall('select game_id from game_order a inner join trade b on a.id=b.id where creator_id=%s and status=%s', (user_id, 'C'))
        ps = store.fetchall('select game_id from participate_order a inner join trade b on a.id=b.id where creator_id=%s and status=%s', (user_id, 'C'))
        ids = gs + ps
        games = Game.gets(ids)
        now = datetime.now()
        one_day_seconds = 3600 * 24

        return filter(lambda g: g and 0 <= (g.start_time - now).total_seconds() <= one_day_seconds, games)


def get_order_class(pay_type):
    from model.order.game import GameOrder
    from model.order.participate import ParticipateOrder
    from model.order.vip import VIPOrder
    from model.order.charge import ChargeOrder

    d = {
        PAYTYPE_GAME: GameOrder,
        PAYTYPE_PARTICIPATE: ParticipateOrder,
        PAYTYPE_VIP: VIPOrder,
        PAYTYPE_CHARGE: ChargeOrder
    }
    if not pay_type in d:
        return None
    return d[pay_type]


class InvalidStatusException(Exception):
    pass


class AccountNotFoundException(Exception):
    pass


class OrderNotFoundException(Exception):
    pass


class AccountBalanceNotEnoughException(Exception):
    pass
