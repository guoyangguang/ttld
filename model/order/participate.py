# coding: utf-8

from datetime import datetime

from libs.sqlstore import store
from model.consts import ORDER_EXPIRED_DELTA, PAYTYPE_PARTICIPATE, \
                         STATUS_EXPIRED, STATUS_EN_DICT, STATUS_PASSED
from model.account import Account
from model.game import Game
from model.order import Order
from tools.postman import notify_join_game
from model.order.game import GameOrder

class ParticipateOrder(Order):

    table = 'participate_order'

    def __init__(self, id, creator_id, amount, pay_type, create_time,
                 pay_time, status, pay_method, gateway, refund_time=None, telephone=None):
        super(ParticipateOrder, self).__init__(id, creator_id, amount,
                pay_type, create_time, pay_time, status,
                pay_method, gateway, refund_time, telephone)
        r = store.fetchone('select game_id, court_id, venue_id, manager_id '
                           'from ' + self.table + ' where id=%s', self.id)
        game_id, court_id, venue_id, manager_id = r
        self.game_id = game_id
        self.court_id = court_id
        self.venue_id = venue_id
        self.manager_id = manager_id

    def __repr__(self):
        return '<ParticipateOrder:id=%s>' % self.id

    @classmethod
    def new(cls, creator_id, amount, pay_type, create_time, pay_time,
            status, pay_method, gateway, game_id, court_id,
            venue_id, manager_id, telephone=None):
        id = Order.new(creator_id, amount, pay_type, create_time,
                       pay_time, status, pay_method, gateway, telephone)
        if id:
            r = store.transac(
                'insert into ' + cls.table +\
                ' (id, game_id, court_id, venue_id, manager_id)' +\
                ' values (%s, %s, %s, %s, %s)',
                (id, game_id, court_id, venue_id, manager_id)
            )
            order = Order.get(id)
            if not order:
                Order.fail(id)
            return order

    @classmethod
    def complete(cls, id):
        order = Order.get(id)
        if order:
            if order.game:
                Game.flush(order.game.id)
            Order.complete(id)
            order.game.creator.flush_played_amount(order.game.sports_type)
            account = Account.get(order.game.creator_id)
            account.inc_balance(order.price)

            notify_join_game.delay(order)

            return True
        return False

    @classmethod
    def filter(cls, start=0, limit=20, ascend=False, **kwargs):
        if not kwargs:
            return []
        invalid_fields = ['court_id', 'game_id', 'venue_id', 'manager_id']
        for field in kwargs:
            if field not in invalid_fields:
                return []
        values = kwargs.values()
        values.append(start)
        values.append(limit)
        wheres = ['%s=%%s' % key for key in kwargs]
        order_type = 'asc' if ascend else 'desc'

        r = store.fetchall('select id from ' + cls.table + ' where ' +
                           ' and '.join(wheres) + ' order by id ' + order_type +
                           ' limit %s,%s', values)
        if isinstance(r, tuple):
            return [Order.get(id) for id in r]

    @classmethod
    def get_user_exist_order(cls, game_id, creator_id):
        '''返回用户已经存在约球订单(未过期)'''

        orders = Order.get_user_pending_participate_orders(creator_id)
        if orders:
            orders = filter(lambda o: o.game_id == game_id, orders)
            return orders and orders[0]

    @classmethod
    def get_user_exist_order_by_tel(cls, game_id, tel):
        orders = Order.get_user_pending_part_orders_by_tel(tel)
        if orders:
            orders = filter(lambda o: o.game_id == game_id, orders)
            return orders and orders[0]

    @classmethod
    def gets_by_user(cls, user_id, start=0, limit=10):
        return cls.filter(start, limit, ascend=False, **{'creator_id': user_id, 'pay_type': PAYTYPE_PARTICIPATE})

    @classmethod
    def gets_by_game(cls, game_id, start=0, limit=20):
        return cls.filter(start=start, limit=limit, ascend=True, **{'game_id': game_id})

    @classmethod
    def gets_by_court(cls, court_id, start=0, limit=20):
        return cls.filter(start, limit, ascend=False, **{'court_id': court_id})

    @classmethod
    def gets_by_venue(cls, venue_id, start=0, limit=20):
        return cls.filter(start, limit, ascend=False, **{'venue_id': venue_id})

    @classmethod
    def gets_by_manager(cls, manager_id, start=0, limit=20):
        return cls.filter(start, limit, ascend=False, **{'manager_id': manager_id})

    @property
    def game(self):
        return Game.get(self.game_id)

    @property
    def court(self):
        return self.game and self.game.court

    @property
    def venue(self):
        return self.game and self.game.venue

    @property
    def price(self):
        return self.amount

    @property
    def is_expire(self):
        if self.completed:
            return False

        delta = datetime.now() - self.create_time
        return delta.total_seconds() > ORDER_EXPIRED_DELTA

    def is_passed(self):
        now = datetime.now()
        return now > self.game.start_time

    @property
    def is_refundable(self):
        pass

    def body_desc(self):
        return '动起来约球费'

    def verify(self, user=None):
        # FIXME: 订单是否超时应该在这里判断，现在在API层判断
        if not (self.creator and self.game and self.court):
            return False
        if not self.game.can_participate(user):
            return False
        return True

    def pay_verify(self, user=None):
        if not (self.creator and self.game and self.court):
            return False
        if not GameOrder.verify_participate_payment(self, user):
            return False
        return True

    def verify_participant(self, participant):
        return self.game.can_participate(participant)

    def jsonify(self):
        if (not self.completed) and self.is_expire:
            status = STATUS_EN_DICT[STATUS_EXPIRED]
        elif self.is_passed():
            status = STATUS_EN_DICT[STATUS_PASSED]
        else:
            status = self.status_en
        return {
            'id': self.id,
            'creator_id': self.creator_id,
            'status': status,
            'price': float(self.price),
            'pay_type': 'participate',
            'game': self.game.jsonify()
        }
