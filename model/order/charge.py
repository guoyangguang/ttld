# -*- coding: utf-8 -*-

from datetime import datetime

from libs.sqlstore import store
from model.account import Account
from model.order import Order, AccountNotFoundException
from model.consts import PAYTYPE_CHARGE, STATUS_COMPLETE, STATUS_REFUND_PENDING
from tools.postman import notify_charge_success
from view.utils import get_normal_user_dict

PROMOTION = True

class ChargeOrder(Order):

    table = 'charge_order'

    def __init__(self, id, creator_id, amount, pay_type, create_time,
                 pay_time, status, pay_method, gateway, refund_time=None):
        super(ChargeOrder, self).__init__(id, creator_id, amount,
                pay_type, create_time, pay_time, status,
                pay_method, gateway, refund_time)

        r = store.fetchone('select last_balance from ' + self.table +
                           ' where id=%s', self.id)
        last_balance, = r
        self.last_balance = last_balance

    def __repr__(self):
        return '<ChargeOrder:id=%s>' % self.id

    @classmethod
    def new(cls, creator_id, amount, pay_type, create_time, pay_time,
            status, pay_method, gateway):
        id = Order.new(creator_id, amount, pay_type, create_time,
                       pay_time, status, pay_method, gateway)
        if id:
            account = Account.get_or_add(creator_id)
            if account:
                r = store.transac(
                    'insert into ' + cls.table + ' (id, last_balance) values (%s, %s)',
                    (id, account.flush_get_balance())
                )
            order = Order.get(id)
            if not order:
                Order.fail(id)
            return order

    @classmethod
    def gets_by_user(cls, user_id, start=0, limit=10):
        r = store.fetchall('select id from ' + Order.table + ' where creator_id=%s '
                           'and pay_type=%s order by id desc limit %s, %s',
                           (user_id, PAYTYPE_CHARGE, start, limit))
        if r:
            return [Order.get(id) for id in r]

    @classmethod
    def complete(cls, id):
        Order.update(id, **{'status': STATUS_COMPLETE,
                            'pay_time': datetime.now()})
        order = Order.get(id)
        if order and order.status in (STATUS_COMPLETE, STATUS_REFUND_PENDING):
            account = Account.get_or_add(order.creator_id)
            if not account:
                # TODO
                raise AccountNotFoundException

            amount = ChargeOrder.promotion_charge(order.amount)
            account.inc_balance(amount)
            promotion_amount = amount-float(order.amount)

            notify_charge_success.delay(order, promotion_amount)


    @classmethod
    def promotion_charge(cls,amount):
        new_amount = float(amount)
        if PROMOTION:
            if(new_amount<500):
                new_amount = new_amount*1.1
            elif(new_amount<1000):
                new_amount = new_amount*1.12
            elif(new_amount>=1000):
                new_amount = new_amount*1.15
            new_amount = float("%.2f" % new_amount)
        return new_amount

    @property
    def price(self):
        return self.amount

    @property
    def is_refundable(self):
        pass

    @property
    def account(self):
        return Account.get_or_add(self.creator_id)

    def body_desc(self):
        return '动起来充值'

    def verify(self, user=None):
        if not self.account:
            return False
        return True

    def pay_verify(self, user=None):
        return self.verify(user)

    def jsonify(self):
        return {
            'id': self.id,
            'creator_id': self.creator_id,
            'price': float(self.price),
            'status': self.status_en,
            'pay_type': 'charge',
            'user': get_normal_user_dict(self.creator)
        }
