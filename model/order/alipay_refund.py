#-*- coding: utf-8 -*-

from datetime import datetime
from libs.sqlstore import store
from model.order.order_notify import OrderPayNotify 
from model.order import Order
from model.alipay.order import refund_from_alipay

class AlipayRefund(object):

    table = 'alipay_refund'
    select_all = 'select id, order_id, status, create_time, update_time from ' + table 
                  
    per_page = 10

    def __init__(self, id=None, order_id=None, status=None, create_time=None, update_time=None):
        self.id = id
        self.order_id = order_id
        self.status = status 
        self.create_time = create_time
        self.update_time = update_time

    @classmethod
    def get(cls, id):
        sql = cls.select_all + ' where id=%s'
        r = store.fetchone(sql, id)
        if r:
            return cls(*r)

    @classmethod
    def get_by_order(cls, order):
        sql = cls.select_all + ' where order_id=%s'
        r = store.fetchone(sql, order.id)
        if r:
            return cls(*r)

    @classmethod
    def all(cls, page):
        r = store.fetchall(
            cls.select_all + ' order by create_time asc limit %s offset %s',
            (cls.per_page, (page-1)*cls.per_page)
        )
        if isinstance(r, tuple):
            return [cls(*alipay_refund) for alipay_refund in r]

    @classmethod
    def create(cls, order):
        sql = 'insert into ' + cls.table +\
              ' (order_id, status, create_time) values (%s, %s, %s)'
        r = store.transac(sql, (order.id, 'pending', datetime.now()))
        if r:
            return cls.get(r)

    @classmethod
    def refund(cls, order):
        sql = 'update ' + cls.table + ' set status=%s where order_id=%s'
        r = store.transac(sql, ('refunded', order.id))
        if r:
            return cls.get_by_order(order)

    def presenter(self):
        order = Order.get(self.order_id)
        order_pay_notify = OrderPayNotify.get_by_order_id(self.order_id)
        if self.status == 'pending':
            request = refund_from_alipay([[order_pay_notify.trade_no, order.amount]], order.id)
        elif self.status == 'refunded':
            request = None
        return dict(
            id=self.id,
            order_id=self.order_id,
            status=self.status,
            out_trade_no=order.id,
            amount=order.amount,
            trade_no=order_pay_notify.trade_no,
            request=request,
            create_time=self.create_time.strftime('%Y-%m-%d %H:%M:%S') if self.create_time else None,
            update_time=self.update_time.strftime('%Y-%m-%d %H:%M:%S') if self.update_time else None
        )
