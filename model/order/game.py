# coding: utf-8

import datetime
from libs.sqlstore import store
from model.consts import (
    ORDER_EXPIRED_DELTA, ORDER_REFUNDING_DELTA, PAYTYPE_GAME,
    STATUS_EXPIRED, STATUS_EN_DICT, STATUS_REFUNDED, STATUS_COMPLETE, STATUS_PENDING, STATUS_CONFIRMING,
    STATUS_CONFIRM_EXPIRED, STATUS_PASSED, STATUS_REFUND_PENDING, PAYMETHOD_WEIXIN,
    PAYMETHOD_ACCOUNT, PAYMETHOD_ALIPAY, USER_CANCEL_REASON, CONFIRM_FAILED_REASON, NEGOTIATE_REASON
)
from model.account import Account
from model.game import Game
from model.order import Order
from model.order.order_notify import OrderPayNotify
from model.user import User
from tools.postman import notify_cancel_game,notify_confirmed_game,notify_received_gameorder_payment
from model.wxpay.order import refund_from_tenpay
from model.order.alipay_refund import AlipayRefund
from model.alipay.consts import ALIPAYAPI_ORIGINAL_PARTNER_ID

class GameOrder(Order):

    table = 'game_order'

    def __init__(self, id, creator_id, amount, pay_type, create_time,
                 pay_time, status, pay_method, gateway, refund_time=None, telephone=None):
        super(GameOrder, self).__init__(id, creator_id, amount,
                pay_type, create_time, pay_time, status,
                pay_method, gateway, refund_time, telephone)
        r = store.fetchone('select game_id, court_id, venue_id, manager_id '
                           'from ' + self.table + ' where id=%s', id)
        game_id, court_id, venue_id, manager_id = r
        self.game_id = game_id
        self.court_id = court_id
        self.venue_id = venue_id
        self.manager_id = manager_id

    def __repr__(self):
        return '<GameOrder:id=%s>' % self.id

    @classmethod
    def new(cls, creator_id, amount, pay_type, create_time, pay_time,
            status, pay_method, gateway, game_id, court_id,
            venue_id, manager_id, telephone=None):
        id = Order.new(creator_id, amount, pay_type, create_time,
                       pay_time, status, pay_method, gateway, telephone)
        if id:
            r = store.transac(
                'insert into ' + cls.table +\
                ' (id, game_id, court_id, venue_id, manager_id)' +\
                ' values (%s, %s, %s, %s, %s)',
                (id, game_id, court_id, venue_id, manager_id)
            )
            order = Order.get(id)
            if not order:
                Order.fail(id)
            return order

    @classmethod
    def complete(cls, id):
        Order.complete(id)
        order = Order.get(id)
        if order and order.game:
            Game.flush(order.game.id)
            order.game.creator.flush_played_amount(order.game.sports_type)
        notify_confirmed_game(order)

    @classmethod
    def received_payment(cls, id):
        order = Order.get(id)
        Order.confirming(id)
        notify_received_gameorder_payment.delay(order)

    @classmethod
    def filter(cls, start=0, limit=20, **kwargs):
        if not kwargs:
            return []
        invalid_fields = ['court_id', 'game_id', 'venue_id', 'manager_id']
        for field in kwargs:
            if field not in invalid_fields:
                return []
        values = kwargs.values()
        values.append(start)
        values.append(limit)
        wheres = ['%s=%%s' % key for key in kwargs]
        r = store.fetchall('select id from ' + cls.table + ' where ' +
                           ' and '.join(wheres) + ' order by id desc '
                           'limit %s,%s', values)
        return [Order.get(id) for id in r]

    @classmethod
    def get_by_game(cls, game_id):
        r = store.fetchone('select id from ' + cls.table + ' where game_id=%s', game_id)
        return r and Order.get(r)

    @classmethod
    def get_user_exist_order(cls, creator_id, court_id, start_time, end_time):
        orders = Order.get_user_pending_game_orders(creator_id)
        if orders:
            orders = filter(lambda order: (order.court_id == court_id and
                                           order.game.start_time == start_time and
                                           order.game.end_time == end_time),
                            orders)
            return orders and orders[0]

    @classmethod
    def gets_by_user(cls, user_id, start=0, limit=10):
        r = store.fetchall(Order.select_all + ' where creator_id=%s '
                           'and pay_type=%s order by id desc limit %s, %s',
                           (user_id, PAYTYPE_GAME, start, limit))
        if isinstance(r, tuple):
            return [cls(*order) for order in r]

    @classmethod
    def gets_by_court(cls, court_id, start=0, limit=20):
        return cls.filter(start, limit, **{'court_id': court_id})

    @classmethod
    def gets_by_venue(cls, venue_id, start=0, limit=20):
        return cls.filter(start, limit, **{'venue_id': venue_id})

    @classmethod
    def gets_by_manager(cls, manager_id, start=0, limit=20):
        return cls.filter(start, limit, **{'manager_id': manager_id})

    @classmethod
    def gets_all(cls, start=0, limit=20):
        r = store.fetchall('select id from ' + cls.table + ' order by id desc '
                           'limit %s,%s', (start, limit))
        if isinstance(r, tuple):
            return [Order.get(id) for id in r]

    def body_desc(self):
        return '动起来订场费'

    @property
    def game(self):
        return Game.get(self.game_id)

    @property
    def court(self):
        return self.game and self.game.court

    @property
    def venue(self):
        return self.game and self.game.venue

    @property
    def price(self):
        return self.amount

    @property
    def is_expire(self):
        if self.completed:
            return False

        delta = datetime.datetime.now() - self.create_time
        return delta.total_seconds() > ORDER_EXPIRED_DELTA

    def is_confirming(self):
        return self.status == STATUS_CONFIRMING

    def is_confirm_expire(self):
        if self.is_confirming():
            return self.confirm_remain_seconds() < 0

    def is_passed(self):
        now = datetime.datetime.now()
        return now > self.game.start_time

    def pending_remain_seconds(self):
        return ORDER_EXPIRED_DELTA - (datetime.datetime.now() - self.create_time).seconds

    def confirm_remain_seconds(self):
        count_down_seconds = ORDER_EXPIRED_DELTA
        now = datetime.datetime.now()
        pay_time = self.pay_time
        order_date = datetime.datetime.combine(pay_time.date(), datetime.time(0, 0))
        delta_start = datetime.timedelta(hours=8, seconds=30 * 60)
        delta_end = datetime.timedelta(hours=21, seconds=30 * 60)
        delta_work = datetime.timedelta(hours=9)
        count_down_start = order_date + delta_start
        count_down_end = order_date + delta_end
        if (count_down_start <= pay_time <= count_down_end):
            remain_seconds = count_down_seconds - int((now - pay_time).total_seconds())
        elif pay_time < count_down_start:
            remain_seconds = int((order_date + delta_work - now).total_seconds())
        else:
            remain_seconds = int((order_date + datetime.timedelta(days=1, hours=9) - now).total_seconds())

        return remain_seconds

    def is_refundable(self, reason=USER_CANCEL_REASON):
        if not (self.creator and self.game and self.court):
            return False
        if not self.completed:
            return False
        delta = self.game.start_time - datetime.datetime.now()
        # 运营驳回或者协商退款都不受24小时限制
        if not reason in (CONFIRM_FAILED_REASON, NEGOTIATE_REASON):
            if delta.total_seconds() < ORDER_REFUNDING_DELTA:
                return False
        if self.game.require and len(self.game.get_participates_without_creator()):
            return False
        return True

    def verify(self, user=None):
        # FIXME: 订单是否超时应该在这里判断，现在在API层判断
        if not (self.creator and self.game and self.court):
            return False
        return True

    def pay_verify(self, user=None):
        return self.verify(user) and (not self.is_expire)

    def refund(self, reason=None):
        if not reason:
            return
        if self.pay_method == PAYMETHOD_WEIXIN:
            now = datetime.datetime.now().strftime('%Y%m%d')
            transaction_id = OrderPayNotify.get_by_order_id(self.id).transaction_id
            out_refund_no = now + str(self.id)
            result = refund_from_tenpay(self.id, transaction_id, out_refund_no, self.amount, self.amount)
            if not result or not result.get('is_refunded'):
                from view.api.error import (APIError, ORDER_WEIXIN_REFUND_ERROR)
                raise APIError(ORDER_WEIXIN_REFUND_ERROR)
        elif self.pay_method == PAYMETHOD_ACCOUNT:
            account = Account.get(self.creator_id)
            account.inc_balance(float(self.price))
        elif self.pay_method == PAYMETHOD_ALIPAY:
            order_pay_notify = OrderPayNotify.get_by_order_id(self.id)
            if order_pay_notify.seller_id == ALIPAYAPI_ORIGINAL_PARTNER_ID:
                account = Account.get(self.creator_id)
                account.inc_balance(float(self.price))
            else:
                AlipayRefund.create(self)
        Order.update(self.id, **{'status': STATUS_REFUNDED,
                                'refund_time': datetime.datetime.now()})
        notify_cancel_game.delay(self, reason)

    def refund_by_user(self):
        if not self.is_refundable():
            from view.api.error import (APIError, ORDER_CANNOT_REFOUNDABLE)
            raise APIError(ORDER_CANNOT_REFOUNDABLE)
        self.refund(reason=USER_CANCEL_REASON)

    @classmethod
    def get_effective_games_by_venue(cls, venue_id, start=0, limit=20):
        sql = 'select game_id from ' + cls.table + ' as a inner join ' + Order.table +\
              ' as b on a.id=b.id and a.venue_id=%s' +\
              ' and (b.status=%s or (b.status=%s and timestampdiff(second, b.create_time, now())<%s))' +\
              ' order by b.create_time limit %s, %s'
        rs = store.fetchall(
            sql,
            (venue_id, STATUS_COMPLETE, STATUS_PENDING, ORDER_EXPIRED_DELTA, int(start), int(limit))
        )
        return rs and Game.gets(rs)

    @classmethod
    def verify_participate_payment(cls, order, user):
        if not (order and user):
            return False

        game = order.game
        if not game:
            return False

        if game.creator_id == user.id:
            return False

        if not game.order_completed:
            return False

        if game.finished:
            return False

        if not game.require:
            return False

        if not game.require.meet(user):
            return False

        porders = game.get_participate_orders()
        porders = filter(lambda po: po.creator_id == user.id, porders)

        if len(porders) > 1 or len(porders) == 0:
            return False

        porder = porders[0]
        if porder.is_expire:
            return False

        participates = game.participates

        if user not in participates:
            return False

        if game.require.max_users and game.require.max_users <= len(participates) - 1:
            return False

        return True

    @classmethod
    def total_counts(cls):
        sql = 'select count(id) from {table}'.format(table=cls.table)
        r = store.fetchone(sql)
        return r and r[0]

    def analyze_create_time(self):
        count_down_seconds = 5 * 60
        now = datetime.datetime.now()
        create_time = self.create_time
        order_date = datetime.datetime.combine(create_time.date(), datetime.time(0, 0))
        delta_start = datetime.timedelta(hours=8, seconds=30*60)
        delta_end = datetime.timedelta(hours=21, seconds=30*60)
        delta_work = datetime.timedelta(hour=9)
        count_down_start = order_date + delta_start
        count_down_end = order_date + delta_end

        if (count_down_start <= create_time <= count_down_end):
            remain_seconds = count_down_seconds - (now - create_time).seconds
        elif create_time < count_down_start:
            remain_seconds = (order_date + delta_work - now).seconds
        else:
            remain_seconds = (order_date + datetime.timedelta(days=1, hours=9) - now).seconds

        return remain_seconds

    def jsonify(self):
        if self.status == STATUS_REFUNDED:
            status = STATUS_EN_DICT[STATUS_REFUNDED]
        elif (not self.completed) and self.is_expire:
            status = STATUS_EN_DICT[STATUS_EXPIRED]
        elif self.is_confirm_expire():
            status = STATUS_EN_DICT[STATUS_CONFIRM_EXPIRED]
        elif self.is_passed():
            status = STATUS_EN_DICT[STATUS_PASSED]
        else:
            status = self.status_en

        data = {
            'id': self.id,
            'creator_id': self.creator_id,
            'status': status,
            'price': float(self.price),
            'pay_type': 'book',
            'game': self.game.jsonify(),
            'create_time': self.create_time.strftime('%Y-%m-%d %H:%M:%S'),
            'pay_time': self.pay_time.strftime('%Y-%m-%d %H:%M:%S') if self.pay_time else '',
        }

        if status == STATUS_EN_DICT[STATUS_PENDING]:
            data['time_left'] = self.pending_remain_seconds()

        if status == STATUS_EN_DICT[STATUS_CONFIRMING]:
            data['time_left'] = self.confirm_remain_seconds()

        return data
