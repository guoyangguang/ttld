# -*- coding: utf-8 -*-

from MySQLdb import IntegrityError

from libs.sqlstore import store

from model.consts import PAYMETHOD_ACCOUNT, GATEWAY_ACCOUNT, PAYTYPE_GAME
from model.account import Account
from model.order import (
    Order, OrderNotFoundException, AccountNotFoundException,
    AccountBalanceNotEnoughException,
    get_order_class
)

def pay_by_account_balance(order_id):
    order = Order.get(order_id)
    if not order:
        raise OrderNotFoundException

    account = Account.get(order.creator_id)
    if not account:
        raise AccountNotFoundException

    if account.flush_get_balance() < order.price:
        raise AccountBalanceNotEnoughException

    try:
        account.reduce_balance(order.price)
    except IntegrityError:
        store.rollback()
        return

    # TODO
    # 可能会出现扣钱成功了，但是加钱失败的情况

    try:
        cls = get_order_class(order.pay_type)
        if order.pay_type == PAYTYPE_GAME:
            cls.received_payment(order.id)
        else:
            cls.complete(order.id)
        Order.update(order.id, **{'pay_method': PAYMETHOD_ACCOUNT,
                                  'gateway': GATEWAY_ACCOUNT})
    except IntegrityError:
        # TODO
        return

    return True

