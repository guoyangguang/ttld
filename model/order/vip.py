# -*- coding: utf-8 -*-

from datetime import datetime

from libs.sqlstore import store
from model.user import User
from model.account import Account
from model.order import Order, AccountNotFoundException
from model.consts import PAYTYPE_VIP, STATUS_COMPLETE, STATUS_REFUND_PENDING
from view.utils import get_normal_user_dict


class VIPOrder(Order):

    table = 'vip_order'

    def __init__(self, id, creator_id, amount, pay_type, create_time,
                 pay_time, status, pay_method, gateway, refund_time=None):
        super(VIPOrder, self).__init__(id, creator_id, amount,
                pay_type, create_time, pay_time, status,
                pay_method, gateway, refund_time)
        r = store.fetchone('select grade, effective_time, expire_time '
                           'from ' + self.table + ' where id=%s', self.id)
        grade, effective_time, expire_time = r
        self.grade = grade
        self.effective_time = effective_time
        self.expire_time = expire_time

    def __repr__(self):
        return '<VIPOrder:id=%s>' % self.id

    @classmethod
    def new(cls, creator_id, amount, pay_type, create_time, pay_time,
            status, pay_method, gateway, grade, effective_time, expire_time):
        id = Order.new(creator_id, amount, pay_type, create_time,
                       pay_time, status, pay_method, gateway)
        if id:
            r = store.transac(
                'insert into ' + cls.table +\
                ' (id, grade, effective_time, expire_time)' +\
                ' values (%s, %s, %s, %s)',
                (id, grade, effective_time, expire_time)
            )
            order = Order.get(id)
            if not order:
                Order.fail(id)
            return order

    @classmethod
    def complete(cls, id):
        Order.update(id, **{'status': STATUS_COMPLETE,
                            'pay_time': datetime.now()})
        order = Order.get(id)
        if order and order.status in (STATUS_COMPLETE, STATUS_REFUND_PENDING):
            account = Account.get_or_add(order.creator_id)
            if not account:
                # TODO
                raise AccountNotFoundException
            account.update_grade(order.grade)

    @classmethod
    def gets_by_user(cls, user_id, start=0, limit=10):
        r = store.fetchall(Order.select_all + ' where creator_id=%s '
                          'and pay_type=%s order by id desc limit %s, %s',
                          (user_id, PAYTYPE_VIP, start, limit))
        if isinstance(r, tuple):
            return [Order(*order) for order in r]

    @property
    def price(self):
        return self.amount

    @property
    def is_refundable(self):
        pass

    def verify(self, user=None):
        # TODO
        return True

    def pay_verify(self, user=None):
        return self.verify(user)

    def jsonify(self):
        return {
            'id': self.id,
            'creator_id': self.creator_id,
            'status': self.status_en,
            'price': float(self.price),
            'pay_type': 'vip',
            'user': get_normal_user_dict(self.creator)
        }
