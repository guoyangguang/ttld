# -*- coding: utf-8 -*-

import json
from datetime import datetime
from libs.sqlstore import store

class OrderPayNotify(object):

    table = 'order_pay_notify'
    select_all = 'select id, order_id, pay_method, notify, create_time, update_time from {table}'.format(table=table)

    def __init__(self, id, order_id, pay_method, notify, create_time, update_time):
        self.id = id
        self.order_id = order_id
        self.pay_method = pay_method
        self.notify = notify
        self.create_time = create_time
        self.update_time = update_time

    @property
    def transaction_id(self):
        notify = json.loads(self.notify)
        return notify and notify.get('transaction_id')

    @property
    def trade_no(self):
        notify = json.loads(self.notify)
        return notify and notify.get('trade_no')
    @property
    def seller_id(self):
        notify = json.loads(self.notify)
        return notify and notify.get('seller_id')

    @classmethod
    def get(cls, id):
        sql = cls.select_all + ' where id=%s' % id
        order_notify = store.fetchone(sql)
        return order_notify and cls(*order_notify)

    @classmethod
    def add(cls, order_id, pay_method, notify, create_time=datetime.now()):
        sql = 'insert into {table}(order_id, pay_method, notify, create_time) values (%s, %s, %s, %s)'.format(table=cls.table)
        r = store.transac(sql, (order_id, pay_method, notify, create_time))

        if r:
            return cls.get(r)

    @classmethod
    def get_by_order_id(cls, order_id):
        sql = cls.select_all + ' where order_id=%s'
        r = store.fetchone(sql, order_id)
        return r and cls(*r)

class OrderRefundNotify(object):

    table = 'order_refund_notify'
    select_all = 'select id, order_id, refund_id, pay_method, notify, create_time, update_time from {table}'.format(table=table)

    def __init__(self, id, order_id, refund_id, pay_method, notify, create_time, update_time):
        self.id = id
        self.order_id = order_id
        self.refund_id = refund_id
        self.pay_method = pay_method
        self.notify = notify
        self.create_time = create_time
        self.update_time = update_time

    @classmethod
    def get(cls, id):
        sql = cls.select_all + ' where id=%s' % id
        r = store.fetchone(sql)
        return r and cls(*r)

    @classmethod
    def add(cls, order_id, refund_id, pay_method, notify, create_time=datetime.now()):
        sql = 'insert into {table}(order_id, refund_id, pay_method, notify, create_time) values (%s, %s, %s, %s, %s)'.format(
            table=cls.table)
        store.transac(sql, (order_id, refund_id, pay_method, notify, create_time))
