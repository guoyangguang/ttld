# -*- coding: utf-8 -*-

from datetime import datetime
from libs.sqlstore import store
from model.venue import Venue
from model.consts import WORKDAY, WEEKEND, HOLIDAY, TT_PRICE

VENUE_PRICE_KEY = '/venue/%s/price'


class Price(object):

    table = 'venue_price'
    select_all = 'select id, venue_id, env_type, ground_type, point, price, kind, price_type from ' + table

    def __init__(self, id, venue_id, env_type, ground_type, point, price, kind, price_type):
        self.id = id
        self.venue_id = venue_id
        self.env_type = env_type
        self.ground_type = ground_type
        self.point = point
        self.price = price
        self.kind = kind
        self.price_type = price_type

    def __repr__(self):
        return '<Price:price=%s, venue=%s, env_type=%s, ground_type=%s>' % (self.price, self.venue_id, self.env_type, self.ground_type)

    @classmethod
    def new(cls, venue_id, env_type, ground_type, point, price, kind, price_type):
        r = store.transac(
            'insert into ' + cls.table +\
            ' (venue_id, env_type, ground_type, point, price, kind, price_type)' +\
            ' values (%s, %s, %s, %s, %s, %s, %s) on duplicate key' +\
            ' update price=%s, env_type=%s, ground_type=%s, kind=%s, price_type=%s',
            (venue_id, env_type, ground_type, point, price, kind, price_type,
            price, env_type, ground_type, kind, price_type)
        )
        return r

    @classmethod
    def get(cls, venue_id, env_type, ground_type, point, kind, price_type=TT_PRICE):
        r = store.fetchone(
            cls.select_all +\
            ' where venue_id=%s and env_type=%s and ground_type=%s and point=%s and kind=%s and price_type=%s',
            (venue_id, env_type, ground_type, point, kind, price_type)
        )
        return r and cls(*r)

    @classmethod
    def get_venue_default_price(cls, venue_id, kind):
        r = store.fetchone(
            cls.select_all + ' where venue_id=%s and kind=%s',
            (venue_id, kind)
        )
        return r and cls(*r)


class VenuePriceInfo(object):

    table = 'venue_price_info'
    select_all = 'select id, venue_id, price_info, create_time, update_time from {table}'.format(table=table)

    def __init__(self, _id, venue_id, price_info, create_time, update_time):
        self.id = _id
        self.venue_id = venue_id
        self.price_info = price_info
        self.create_time = create_time
        self.update_time = update_time

    @classmethod
    def add(cls, venue_id, price_info, create_time=datetime.now()):
        try:
            sql = 'insert into {table} (venue_id, price_info, create_time) values (%s, %s, %s) '\
                  'on duplicate key update price_info=%s'.format(table=cls.table)
            store.execute(sql, (venue_id, price_info, create_time, price_info))
            store.commit()
        except Exception as e:
            store.rollback()
            raise

    @classmethod
    def get_by_venue(cls, venue_id):
        sql = cls.select_all + ' where venue_id=%s'
        r = store.fetchone(sql, venue_id)

        return r and cls(*r)
