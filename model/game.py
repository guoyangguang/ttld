# coding: utf-8

from datetime import datetime, timedelta, time

from MySQLdb import IntegrityError

from config import TENCOURT
from libs.db import db
from libs.sqlstore import store
from libs.mc import mc, pcache, cache, ONE_HOUR, ONE_DAY

from model.court import Court
from model.user import User
from model.consts import MAX_PARTICIPATE_COUNT, BOOK_TYPE_MANAGER, COURT_STATUS_MERCHANT_MEMBER_PENDING, \
    COURT_STATUS_MERCHANT_MEMBER_BOOK, COURT_STATUS_MERCHANT_PENDING, COURT_STATUS_RESERVE, COURT_STATUS_BOOK, \
    COURT_STATUS_PARTICIPATE_FULL, COURT_STATUS_PARTICIPATE, COURT_STATUS_MERCHANT_BOOK, COURT_STATUS_FREE, \
    COURT_STATUS_PENDING, BOOK_TYPE_REGULAR
from model.admin_order import AdminOrder
from model.participates import Participates
from model.consts import STATUS_COMPLETE, STATUS_PENDING, STATUS_REFUND_PENDING, ORDER_EXPIRED_DELTA, REQUIRE_PAY_OFFLINE
from view.utils import dt2timestamp, get_normal_user_dict


GAME_COMMENT_DB_KEY = '/game/%s/comment'
MCKEY_GAME = 'game:%s'
MCKEY_COURT_GAMES = 'court:%s:date:%s'
MCKEY_VENUE_WEEKLY_GAMES_COUNT = 'venue:%s:date:%s'

class Game(object):

    table = 'game'
    select_all = 'select id, court_id, creator_id, book_type, start_time, ' +\
                 'end_time, create_time, update_time from ' + table

    def __init__(self, id, court_id, creator_id, book_type, start_time,
                 end_time, create_time, update_time):
        self.id = id
        self.court_id = court_id
        self.creator_id = creator_id
        self.book_type = book_type
        self.start_time = start_time
        self.end_time = end_time
        self.create_time = create_time
        self.update_time = update_time

    def __repr__(self):
        return '<Game:id=%s,creator=%s>' % (self.id, self.creator_id)

    @property
    def status(self):
        if self.book_type == BOOK_TYPE_MANAGER:
            admin_order = AdminOrder.get_by_game(self.id)
            if admin_order:
                if admin_order.member_id:
                    if admin_order.status == STATUS_COMPLETE:
                        return COURT_STATUS_MERCHANT_MEMBER_BOOK
                    elif admin_order.status == STATUS_PENDING:
                        return COURT_STATUS_MERCHANT_MEMBER_PENDING
                else:
                    if admin_order.status == STATUS_COMPLETE:
                        return COURT_STATUS_MERCHANT_BOOK
                    elif admin_order.status == STATUS_PENDING:
                        return COURT_STATUS_MERCHANT_PENDING

                return COURT_STATUS_FREE
            else:
                return COURT_STATUS_RESERVE

        else:
            from model.order.game import GameOrder
            game_order = GameOrder.get_by_game(self.id)
            if game_order and game_order.status == STATUS_PENDING:
                return COURT_STATUS_PENDING
            if self.require:
                if not self.can_participate():
                    return COURT_STATUS_PARTICIPATE_FULL
                else:
                    return COURT_STATUS_PARTICIPATE

            return COURT_STATUS_BOOK

    @property
    def active(self):
        from model.order.game import GameOrder
        if self.book_type == BOOK_TYPE_MANAGER:
            admin_order = AdminOrder.get_by_game(self.id)
            if admin_order:
                if admin_order.status == STATUS_COMPLETE or admin_order.status == STATUS_PENDING or admin_order.status == STATUS_REFUND_PENDING:
                    return True
            else:
                #预留
                return True
        else:
            order = GameOrder.get_by_game(self.id)
            if order:
                if order.status == STATUS_COMPLETE or order.status == STATUS_REFUND_PENDING:
                    return True
                if order.status == STATUS_PENDING:
                    pending_expired_time = datetime.now() - timedelta(seconds=ORDER_EXPIRED_DELTA)
                    return pending_expired_time < order.create_time
        return False

    @property
    def url(self):
        return '%s/game/%s/' % (TENCOURT, self.id)

    @property
    def comment(self):
        return db.get(GAME_COMMENT_DB_KEY % self.id)

    @property
    def court(self):
        return self.court_id and Court.get(self.court_id)

    @property
    def venue(self):
        return self.court and self.court.venue

    @property
    def creator(self):
        return self.creator_id and User.get(self.creator_id)

    @property
    def manager(self):
        return self.venue and self.venue.manager

    @property
    def start_timestamp(self):
        return dt2timestamp(self.start_time)

    @property
    def end_timestamp(self):
        return dt2timestamp(self.end_time)

    @property
    def scheduleInfo(self):
        dict = self.__basicScheduleInfo__
        if self.require:
            dict['per_price'] = float(self.require.price)
            dict['max_player'] = self.require.max_users
            dict['current_player'] = len(self.participates)
        return dict

    @property
    def merchantScheduleInfo(self):
        dict = self.__basicScheduleInfo__

        if self.book_type == BOOK_TYPE_MANAGER:
            order = AdminOrder.get_by_game(self.id)
            if order:
                dict['ct'] = order.identity
            else:
                dict['ct'] = self.comment 
        else:
            dict['ct'] = '****' + self.creator.telephone[-4:]
        return dict


    @property
    def __basicScheduleInfo__(self):
        dict = {
            'price':self.price,
            'game_id':self.id,
            'court_id':self.court_id,
            'status':self.status
        }
        return dict

    @property
    def desc(self):
        return '{start}-{end} {s_name}-{c_name}'.format(start=self.start_time.strftime('%Y-%m-%d %H:%M'),
                                                      end=self.end_time.strftime('%H:%M'),
                                                      s_name=self.venue.name.encode('utf-8'),
                                                      c_name=self.court.name.encode('utf-8'))

    @property
    def price(self):
        try:
            return self.court.price(self.start_time, self.end_time)
        except Exception:
            return None

    @property
    def orders(self):
        orders = self.get_participate_orders()
        game_order = self.get_game_order()
        orders.append(game_order)
        return orders

    @property
    def city_id(self):
        return self.court and self.court.city_id

    @property
    def sports_type(self):
        if self.court_id:
            return self.court and self.court.sports_type
        else:
            return self.require.sports_type

    def get_participate_orders(self):
        from model.order.participate import ParticipateOrder
        orders = ParticipateOrder.gets_by_game(self.id, start=0, limit=100)
        return filter(lambda o: o and not o.is_expire, orders)

    def get_game_order(self):
        from model.order.game import GameOrder
        return GameOrder.get_by_game(self.id)

    @property
    def participates(self):
        require = self.require
        if require and require.pay_type != REQUIRE_PAY_OFFLINE:
            orders = self.get_participate_orders()
            users = [order.creator for order in orders]
            if self.require.is_owner_play:
                users.append(self.creator)
            return users
        else:
            participates = Participates.get_participates_by_game(self.id)
            return participates

    # FIXME 聪聪非要要
    def get_participates_without_creator(self):
        require = self.require
        if require and require.pay_type != REQUIRE_PAY_OFFLINE:
            orders = self.get_participate_orders()
            return [order.creator for order in orders]
        else:
            participates = Participates.get_participates_by_game_without_creator(self.id, self.creator_id)
            return participates

    @property
    def order_completed(self):
        order = self.get_game_order()
        return order and order.completed

    @property
    def started(self):
        return self.start_time <= datetime.now()

    @property
    def finished(self):
        return self.end_time <= datetime.now()

    @property
    def require(self):
        from model.require import Require
        return Require.get_by_game(self.id)

    @property
    def can_delete(self):
        orders = self.get_participate_orders()
        if orders and any(order.completed for order in orders):
            return False
        return True

    def can_participate(self, user=None):
        if self.finished:
            return False

        if not self.require:
            return False

        if not self.order_completed:
            return False

        if user and not self.require.meet(user):
            return False

        if user and Participates.is_participate_game(user.id, self.id):
            return False

        if self.require.max_users and self.require.max_users <= len(self.participates):
            return False

        return True

    def is_participatable(self, user=None):
        if self.finished:
            return False

        if not self.require:
            return False

        if not self.order_completed:
            return False

        #if self.require.max_users == len(self.participates):
        #    return False

        return True


    @classmethod
    @cache(MCKEY_GAME % '{id}', expire=ONE_DAY)
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', id)
        return r and cls(*r)

    @classmethod
    def flush(cls, id):
        mc.delete(MCKEY_GAME % id)

    @classmethod
    def gets(cls, ids):
        games = [cls.get(id) for id in ids if id]
        games = filter(None, games)
        return games

    @classmethod
    @pcache(MCKEY_COURT_GAMES % ('{court_id}', '{datestr}'), count=500, expire=ONE_HOUR*2)
    def get_court_oneday_games(cls, court_id, datestr, start=0, limit=500):
        start_time = datetime.strptime(datestr, '%Y%m%d')
        end_time = start_time + timedelta(1)
        rs = store.fetchall(cls.select_all + ' where court_id=%s '
                            'and start_time>=%s and start_time<%s limit %s, %s',
                            (court_id, start_time, end_time, start, limit))
        if isinstance(rs, tuple):
            return [cls(*game) for game in rs]


    @classmethod
    def flush_schedule(cls, court_id, date):
        datestr = date.strftime('%Y%m%d')
        mc.delete(MCKEY_COURT_GAMES % (court_id, datestr))

    @classmethod
    def gets_by_court_and_date(cls, court_id, start_time, end_time):
        if start_time.date() != end_time.date():
            rs = store.fetchall(cls.select_all + ' where court_id=%s '
                          'and start_time>=%s and start_time<%s',
                          (court_id, start_time, end_time))
            return ([cls(*game) for game in rs] if isinstance(rs, tuple) else rs)

        datestr = start_time.strftime('%Y%m%d')
        games = cls.get_court_oneday_games(court_id, datestr)
        return filter(lambda g: g.start_time >= start_time and g.start_time < end_time, games)

    @classmethod
    def gets_by_venue_and_date(cls, venue_id, start_time, end_time):
        from model.venue import Venue
        venue = Venue.get(venue_id)
        games = []
        for court in venue.courts:
            gs = cls.gets_by_court_and_date(court.id, start_time, end_time)
            games.extend(gs)
        return games

    @classmethod
    def gets_by_manager_and_date(cls, manager_id, start_time, end_time):
        from model.venue import Venue
        venues = Venue.gets_by_manager(manager_id)
        courts = []
        for v in venues:
            courts.extend(v.courts)
        games = []
        for c in courts:
            gs = cls.gets_by_court_and_date(c.id, start_time, end_time)
            games.extend(gs)
        return games

    @classmethod
    def add(cls, court_id, creator_id, book_type, start_time, end_time, comment=None):
        if not court_id or cls.check_can_add(court_id, start_time):
            r = store.transac('insert into ' + cls.table + ' (court_id, creator_id, '
                              'book_type, start_time, end_time, create_time) '
                              'values (%s, %s, %s, %s, %s, %s)',
                              (court_id, creator_id, book_type, start_time, end_time, datetime.now()))

            if r:
                # 清理这个场地这个日期的比赛日程
                if court_id:
                    cls.flush_schedule(court_id, start_time)
                if comment:
                    db.set(GAME_COMMENT_DB_KEY % r, comment)
                return cls.get(r)

    @classmethod
    def check_can_add(cls, court_id, start_time):
        from model.order.game import GameOrder
        sql = cls.select_all + ' where court_id=%s and start_time<=%s and %s<end_time'
        rs = store.fetchall(sql, (court_id, start_time, start_time))
        games = ([cls(*game) for game in rs] if isinstance(rs, tuple) else [])
        for g in games:
            if g.is_created_by_manager():
                admin_order = AdminOrder.get_by_game(g.id)
                if admin_order and admin_order.is_refunded:
                    return True
                return False
            order = GameOrder.get_by_game(g.id)
            if order and not order.is_expire:
                return False
        return True

    def delete(self):
        if not self.can_delete:
            return
        r = store.transac('delete from ' + self.table + ' where id=%s', self.id)
        if r:
            Game.flush_schedule(self.court_id, self.start_time.date())
            Game.flush(self.id)
        return r

    def is_created_by_manager(self):
        return self.book_type in (BOOK_TYPE_MANAGER, BOOK_TYPE_REGULAR)

    def is_created_by_regular(self):
        return self.book_type == BOOK_TYPE_REGULAR

    @classmethod
    def get_manager_book_game_count_by_date(cls, manager_id, date):
        sql = ('select count(id) from ' + cls.table +
               ' where creator_id = %s and book_type = %s '
               'and date(start_time) = %s')
        params = (manager_id, BOOK_TYPE_MANAGER, date)
        r = store.fetchone(sql, params)
        return r and r[0]

    @classmethod
    @cache(MCKEY_VENUE_WEEKLY_GAMES_COUNT % ('{venue_id}', '{date}'), expire=ONE_DAY)
    def get_weekly_participate_count_by_venue(cls, venue_id, date):
        delta = date.weekday()
        start = datetime.combine(date, time()) - timedelta(delta)
        end = start + timedelta(6)
        games = cls.gets_by_venue_and_date(venue_id, start, end)
        return len([game for game in games if game and game.require])

    @classmethod
    def flush_weekly_particapte_count(cls, venue_id, date):
        mc.delete(MCKEY_COURT_GAMES % (venue_id, date))

    @classmethod
    def get_lasted_game(cls, uid, time, count):
        book_games = []
        participate_games = []

        # 获取订场的比赛id。订场成功后如果创建了约球，则该比赛就变成了约球
        book_games_ids = store.fetchall('select game_id from game_order a inner join trade b on a.id = b.id where creator_id=%s and pay_type=%s order by b.id desc', (uid, 1))
        # 获取约球的比赛id
        participate_games_ids = store.fetchall('select game_id from participate_order a inner join trade b on a.id = b.id where creator_id=%s and pay_type=%s order by b.id desc', (uid, 2))
        ids = book_games_ids + participate_games_ids
        games = Game.gets(ids)

        for game in games:
            if game and game.order_completed and game.end_time > time:
                if game.require:
                    participate_games.append(game.jsonify())
                else:
                    book_games.append(game.jsonify())

        return (book_games[0: count], participate_games[0: count])

    def jsonify(self):
        game = {
            'id': self.id,
            'price': self.price,
            'url': self.url,
            'start_time': self.start_time.strftime('%Y-%m-%d %H:%M:%S'),
            'end_time': self.end_time.strftime('%Y-%m-%d %H:%M:%S'),
            'has_started':self.started,
            'sports_type':self.sports_type
        }
        if self.court:
            game['venue'] = self.venue and self.venue.jsonify()
            game['court'] = self.court.jsonify()

        if self.creator:
            game['creator'] = get_normal_user_dict(self.creator)

        if self.require:
            if self.require.address:
                game['address'] = self.require.address

            require = {
                'id': self.require.id,
                'gender': self.require.gender,
                'tennis_level': self.require.tennis_level,
                'max_user_count': self.require.max_users,
                'per_price': float(self.require.price),
                'participates': [get_normal_user_dict(p) for p in self.get_participates_without_creator() if p],
                'is_owner_play': self.require.is_owner_play,
                'desc': self.require.desc,
                'create_time': self.require.create_time.strftime('%Y-%m-%d %H:%M:%S')
            }
            game.update({'yue': require})
        return game
