# -*- coding: utf-8 -*-

from model.gtpns.igt_push import *
from model.gtpns.igetui.template import *
from model.gtpns.igetui.template.igt_base_template import *
from model.gtpns.igetui.template.igt_transmission_template import *
from model.gtpns.igetui.template.igt_link_template import *
from model.gtpns.igetui.template.igt_notification_template import *
from model.gtpns.igetui.template.igt_notypopload_template import *
from model.gtpns.igetui.igt_message import *
from model.gtpns.igetui.igt_target import *
from model.gtpns.igetui.template import *
from config import GETUI_CONFIGURE


#toList接口每个用户返回用户状态开关,true：打开 false：关闭
#os.environ['needDetails'] = 'True'
#CID = "cad13a1feb7309ab4a38384e4b802ea9"

class GtpnsClient(object):
    
    def __init__(self, host, appId, appKey, appSecret, masterSecret):
        self.push = IGeTui(host, appKey, masterSecret)
        self.host = host
        self.appId = appId 
        self.appKey = appKey 
        self.appSecret = appSecret 
        self.masterSecret = masterSecret

    #.PushMessageToSingle接口：支持对单个用户进行推送
    def pushMessageToSingle(self, template, clientId):
        message = IGtSingleMessage()
        message.data = template
        message.isOffline = True
        message.offlineExpireTime = 1000 * 3600 * 12
    
        target = Target()
        target.appId = self.appId
        target.clientId = clientId 
    
        ret = self.push.pushMessageToSingle(message, target)
        print ret
        return ret
    
    
    #.PushMessageToList接口：支持对多个用户进行推送，建议为50个用户
    def pushMessageToList(self, template, clientIds):
        message = IGtListMessage()
        message.data = template
        message.isOffline = True
        message.offlineExpireTime = 1000 * 3600 * 12
        
        targets = list()
        for cid in clientIds:
            target = Target()
            target.appId = self.appId 
            target.clientId = cid 
            targets.append(target)
    
        contentId = self.push.getContentId(message)
        ret =self.push.pushMessageToList(contentId, targets)
        print ret
        return ret
    
    
    #.pushMessageToApp接口：对单个应用下的所有用户进行推送，可根据省份，标签，机型过滤推送
    def pushMessageToApp(self, template):
        message = IGtAppMessage()
        message.data = template
        message.isOffline = True
        message.offlineExpireTime = 1000 * 3600 * 12
        message.appIdList.extend([self.appId])
        message.phoneTypeList.extend(["ANDROID"])
        #message.provinceList.extend(["浙江", "上海"])
        #message.tagList.extend(["开心"])
    
        ret = self.push.pushMessageToApp(message)
        print ret
        return ret
    
    #通知透传模板动作内容
    def notificationTemplate(self, text, title='', transType=1, transContent='', logo='', 
                             logoURL='', isRing=True, isVibrate=True, isClearable=True):
        template = NotificationTemplate()
        template.appId = self.appId 
        template.appKey = self.appKey 
        template.transmissionType = transType 
        template.transmissionContent = transContent
        template.title = title 
        template.text = text 
        template.logo = logo 
        template.logoURL = logoURL 
        template.isRing = isRing
        template.isVibrate = isVibrate
        template.isClearable = isClearable
        #iOS 推送需要的PushInfo字段 前三项必填，后四项可以填空字符串
        #template.setPushInfo(actionLocKey, badge, message, sound, payload, locKey, locArgs, launchImage)
        #template.setPushInfo("open",4,"message","","","","","")
        return template
    
    #通知链接模板动作内容
    def linkTemplate(self, title, text, url, transType=1, transContent='', logo='',
                     isRing=True, isVibrate=True, isClearable=True):
        template = LinkTemplate()
        template.appId = self.appId 
        template.appKey = self.appKey 
        template.title = title 
        template.text = text 
        template.logo = logo 
        template.url = url 
        template.transmissionType = transType
        template.transmissionContent = transContent
        template.isRing = isRing 
        template.isVibrate = isVibrate
        template.isClearable = isClearable
        #iOS 推送需要的PushInfo字段 前三项必填，后四项可以填空字符串
        #template.setPushInfo(actionLocKey, badge, message, sound, payload, locKey, locArgs, launchImage)
        #template.setPushInfo("open",4,"message","test1.wav","","","","")
        return template
    
    #透传模板动作内容
    def transmissionTemplate(self, transContent, transType=1):
        template = TransmissionTemplate()
        template.transmissionType = transType
        template.appId = self.appId 
        template.appKey = self.appKey 
        template.transmissionContent = transContent
        #iOS 推送需要的PushInfo字段 前三项必填，后四项可以填空字符串
        #template.setPushInfo(actionLocKey, badge, message, sound, payload, locKey, locArgs, launchImage)
        #template.setPushInfo("",2,"","","","","","")
        return template
    
    #通知弹框下载模板动作内容
    #TODO rewrite later
    def notyPopLoadTemplate(self, notyTitle, notyContent, popTitle, popContent, loadUrl, 
                            notyIcon='', logoUrl='', isRing=True, isVibrate=True, 
                            isClearable=True, popImg='', loadIcon='', loadTitle=u'下载内容',
                            isAutoInstall=True, isActive=False
                            ):
        template = NotyPopLoadTemplate()
        template.appId = self.appId 
        template.appKey = self.appKey
        template.notyIcon = notyIcon
        template.logoUrl = logoUrl
        template.notyTitle = notyTitle
        template.notyContent= notyContent
        template.isRing = isRing
        template.isVibrate = isVibrate
        template.isClearable = isClearable
    	
        template.popTitle = popTitle
        template.popContent = popContent
        template.popImage = popImg
        template.popButton1 = u"下载"
        template.popButton2 = u"取消"
    
        template.loadIcon = loadIcon 
        template.loadTitle = loadTitle 
        template.loadUrl = loadUrl 
        template.isAutoInstall = isAutoInstall
        template.isActive = isActive 
        return template
    
    #获取用户状态
    def getUserStatus(self, clientId):
        print self.push.getClientIdStatus(self.appId, clientId)
    
    #任务停止功能
    def stopTask(self, contentId):
        print self.push.stop(contentId)	   

gtpns_client = GtpnsClient(
    host = GETUI_CONFIGURE.get('host'), 
    appId = GETUI_CONFIGURE.get('appid'), 
    appKey = GETUI_CONFIGURE.get('appkey'), 
    appSecret = GETUI_CONFIGURE.get('appsecret'),
    masterSecret = GETUI_CONFIGURE.get('mastersecret')
)

admin_gtpns_client = GtpnsClient(
    host = GETUI_CONFIGURE.get('host'),
    appId = GETUI_CONFIGURE.get('appid'),
    appKey = GETUI_CONFIGURE.get('appkey'),
    appSecret = GETUI_CONFIGURE.get('appsecret'),
    masterSecret = GETUI_CONFIGURE.get('mastersecret')
)
