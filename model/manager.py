# -*- coding:utf-8 -*- ＃

from flask.ext.login import UserMixin

from datetime import datetime

from libs.sqlstore import store

from model.consts import (
    K_MANAGER, ROLE_SYSADMIN, ROLE_MERCHANT, ROLE_NAME_MAPPING, ROLE_VENUE_ADMIN
)
from model.photo import Photo, DEFAULT_AVATAR_URL

class Manager(UserMixin):

    table = 'manager'
    kind = K_MANAGER
    select_all = 'select id, name, role, password, telephone, ' +\
                 'company, desc_message, activate, parent_id, create_time, update_time from ' + table
                 

    def __init__(self, id, name, role, password, telephone, company,
                 desc_message, activate, parent_id, create_time, update_time):
        self.id = id
        self.name = name
        self.role = role
        self.password = password
        self.telephone = telephone
        self.company = company
        self.desc_message = desc_message
        self.activate = activate
        self.parent_id = parent_id
        self.create_time = create_time
        self.update_time = update_time

    @property
    def avatar(self):
        p = Photo.gets_by_target(self.id, self.kind, limit=1)
        return p[0] if p else None

    @property
    def avatar_url(self):
        return (self.avatar.url('original') if self.avatar
                else DEFAULT_AVATAR_URL)

    @property
    def is_activate(self):
        return bool(self.activate)

    @property
    def role_cn(self):
        return ROLE_NAME_MAPPING.get(self.role, '未知')

    def __str__(self):
        return '<Manager: id=%s>' % self.id

    def __repr__(self):
        return self.__str__()

    def is_admin(self):
        return self.role == ROLE_SYSADMIN

    def is_owner(self, venue):
        return self.id == venue.manager_id

    def can_manage(self, venue):
        """ower is able to manage his venues;sysadmin is able to manage any venue"""
        return self.is_owner(venue) or self.is_admin()

    @classmethod
    def add(cls, name, role, password, telephone, company, desc_message, activate, parent_id):
        if role not in (ROLE_SYSADMIN, ROLE_MERCHANT):
            return
        r = store.transac(
            'insert ignore into ' + cls.table + '(name, role, '
            'password, telephone, company, desc_message, activate, parent_id, '
            'create_time) values (%s, %s, %s, %s, %s, %s, %s, %s, %s)',
            (name, role, password, telephone, company,
             desc_message, activate, parent_id, datetime.now())
        )
        if r:
            return cls.get(r)

    @classmethod
    def add_child_node(cls, name, role, password, telephone, company, desc_message, activate, parent_id):
        if role != ROLE_VENUE_ADMIN:
            return

        sql = 'replace into {table} (name, role, password, telephone, company, desc_message, activate, parent_id values ' \
              '(%s, %s, %s, %s, %s, %s, %s, %s)'.format(table=cls.table)
        r = store.transac(sql, (name, role, password, telephone, company, desc_message, activate, parent_id))

        if r:
            return cls.get(r)


    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', id)
        return r and cls(*r)

    @classmethod
    def get_by_name(cls, name):
        r = store.fetchone(cls.select_all + ' where name=%s', name)
        return r and cls(*r)

    @classmethod
    def get_by_telephone(cls, telephone):
        r = store.fetchone(cls.select_all + ' where telephone=%s', telephone)
        return r and cls(*r)

    @classmethod
    def get_by_name_or_tel(cls, entity):
        r = store.fetchone(cls.select_all + ' where name=%s or telephone=%s', (entity,entity))
        return r and cls(*r)

    @classmethod
    def gets(cls, ids):
        managers = [cls.get(id) for id in ids if id]
        managers = filter(None, managers)
        return managers 

    @classmethod
    def gets_by_role(cls, role, start=0, limit=20):
        if role not in (ROLE_SYSADMIN, ROLE_MERCHANT):
            return []
        rs = store.fetchall(
            cls.select_all + ' where role=%s limit %s,%s', (role, start, limit)
        )
        if isinstance(rs, tuple):
            managers = [cls(*manager) for manager in rs]
            return managers
                      

    def update(self, name=None, role=None, password=None,
               telephone=None, company=None, desc_message=None):
        self.name = name or self.name
        self.role = role or self.role
        self.password = password or self.password
        self.telephone = telephone or self.telephone
        self.company = company or self.company
        self.desc_message = desc_message or self.desc_message

        r = store.transac('update ' + self.table + ' set name=%s, role=%s, '
                      'password=%s, telephone=%s, company=%s, '
                      'desc_message=%s where id=%s',
                      (self.name, self.role, self.password, self.telephone,
                       self.company, self.desc_message, self.id))
        if r:
            return self

    def delete(self):
        r = store.transac('delete from ' + self.table + ' where id=%s', self.id)
        return r

    def get_id(self):
        return self.id

    @classmethod
    def get_venue_admin(cls, manager_id):
        r = store.fetchone(cls.select_all + ' where parent_id=%s', manager_id)
        return r and cls(*r)

    @classmethod
    def activate_user(cls, manager_id):
        r = store.transac(
            'update {table} set activate=%s where id=%s'.format(table=cls.table),
            (1, manager_id)
        )
        return r

    def jsonify(self):
        from model.venue import Venue

        return {
            "id": self.id,
            "name": self.name.encode('utf-8') if self.name else '',
            "role": self.role,
            "telephone": self.telephone,
            "company": self.company.encode('utf-8') if self.company else '',
            "venues": [venue.jsonify() for venue in Venue.gets_by_manager(self.id)],
            "noti_new_order": True,
        }


class ManagerNotifyConfig(object):

    table='manager_notify_config'
    select_all = 'select id, manager_id, noti_new_order from ' + table

    def __init__(self, id, manager_id, noti_new_order):
        self.id = id
        self.manager_id = manager_id
        self.noti_new_order = noti_new_order

    @classmethod
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where id=%s', id)
        return r and cls(*r)

    @classmethod
    def add(cls, manager_id, noti_new_order):
        sql = 'insert into {table} (manager_id, noti_new_order) values (%s, %s)'.format(table=cls.table)
        r = store.transac(sql, (manager_id, noti_new_order))
        if r:
            return cls.get(r)

    def update(self, **kwargs):
        invalid_fields = ('id',)
        for field in invalid_fields:
            if field in kwargs:
                del kwargs[field]
        values = kwargs.values()
        values.append(self.id)
        r = store.transac(
            'update {table} set ' + ','.join('%s=%%s' % f for f in kwargs) + ' where id=%s',
            values
        )
        return r

    @classmethod
    def get_by_manager(cls, manager_id):
        r = store.fetchone(
            cls.select_all + ' where manager_id=%s',
            manager_id
        )
        return r and cls(*r)
