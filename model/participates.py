# -*- coding:utf-8 -*-

from datetime import datetime
from libs.sqlstore import store
from model.user import User

class Participates(object):
    table = 'participates'
    select_all = 'select id, user_id, telephone, game_id, participate_time from {table}'.format(table=table)

    def __init__(self, id, user_id, telephone, game_id, participate_time):
        self.id = id
        self.user_id = user_id
        self.telephone = telephone
        self.game_id = game_id
        self.participate_time = participate_time

    @classmethod
    def add(cls, user_id, telephone, game_id, participate_time=datetime.now()):
        sql = 'insert into {table}(user_id, telephone, game_id, participate_time) values (%s, %s, %s, %s)'\
            .format(table=cls.table)
        r = store.transac(sql, (user_id, telephone, game_id, participate_time))
        return r and cls(*r)

    @classmethod
    def get_participates_by_game(cls, game_id):
        sql = cls.select_all + ' where game_id=%s'.format(table=cls.table)
        rs = store.fetchall(sql, game_id)
        return rs and [cls(*r) for r in rs]

    @classmethod
    def get_participates_by_game_without_creator(cls, game_id, creator_id):
        sql = cls.select_all + ' where game_id=%s and user_id!=%s'
        rs = store.fetchall(sql, (game_id, creator_id))
        return rs and [cls(*r) for r in rs]

    @classmethod
    def is_participate_game(self, identity, game_id):
        sql = 'select count(*) from {table} where game_id=%s and (user_id=%s or telephone=%s)'
        r = store.fetchone(sql, (game_id, identity, identity))
        return r and bool(r)

    def jsonify(self):
        return {
            'user_id': self.user_id,
            'game_id': self.game_id,
            'telephone': self.telephone,
            'participate_time': self.participate_time.strftime('%Y-%m-%d %H:%M:%S')
        }
