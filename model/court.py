# -*- coding: utf-8 -*-

from datetime import datetime, timedelta, time

from libs.sqlstore import store
from libs.mc import cache, ONE_DAY
from model.consts import COURT_TYPES, COURT_ENVS, HOLIDAY, WORKDAY, WEEKEND, TT_PRICE
from model.venue import Venue
from model.price import Price
from model.member import MemberType
from model.admin_order import AdminOrder
from tools.holiday import verify_holiday, verify_weekend
from consts import ANY_SPORTS_ITEM, TENNIS_ITEM, BADMINTON_ITEM, FOOTBALL_ITEM

MCKEY_COURT = 'court:%s'

class Court(object):

    table = 'court'
    select_all = 'select id, venue_id, size_type, env_type, seq, sports_type, ground_type, has_light, create_time, ' +\
                 'update_time from ' + table

    def __init__(self, id, venue_id, size_type, env_type, seq, sports_type, ground_type, has_light, create_time, update_time):
        self.id = id
        self.venue_id = venue_id
        self.seq = seq
        self.size_type = size_type
        self.env_type = env_type
        self.sports_type = sports_type
        self.ground_type = ground_type
        self._has_light = has_light
        self.create_time = create_time
        self.update_time = update_time

    def __repr__(self):
        return '<Court:id=%s>' % self.id

    @property
    def name(self):
        return u'%s号场' % self.seq

    @property
    def venue(self):
        return Venue.get(self.venue_id)

    @property
    def has_light(self):
        return bool(self._has_light)

    @property
    def city_id(self):
        venue = Venue.get(self.venue_id)
        return venue and venue.city_id

    def isfull(self, date):
        schedule = self.schedule(date)
        start = datetime.combine(date.date(), time())
        start_point = (start + self.venue.start_book_time).hour
        end_point = (start + self.venue.end_book_time).hour
        fullhours = end_point - start_point
        return fullhours == len(schedule)

    def can_book(self, start_time, end_time):
        reason = self.check_can_book(start_time, end_time)
        return not bool(reason)

    def check_can_book(self, start_time, end_time):
        if start_time > end_time:
            return '开始结束时间不正确'
        if start_time < datetime.now():
            return '时间已经过期'
        if end_time.date() != start_time.date():
            if end_time.hour != 0:
                return '时间间隔不能超过一天'
        if not self.venue:
            return '场馆不存在'

        start = datetime.combine(start_time.date(), time())
        start_point = start + self.venue.start_book_time
        end_point = start + self.venue.end_book_time

        if (start_time < start_point or
            end_time <= start_point or
            start_time >= end_point or
            end_time > end_point):
            return '请确认开馆和打烊时间'

        schedule = self.schedule(start.date())
        for point in range(start_time.hour, end_time.hour):
            if point in schedule:
                return '不能重复订场'
        return ''

    def schedule(self, date=datetime.today()):
        from model.game import Game

        datestr = date.strftime('%Y%m%d')
        games = Game.get_court_oneday_games(self.id, datestr)
        result = {}
        for game in games:
            if not game.is_created_by_manager() and not game.order_completed:
                order = game.get_game_order()
                if not order:
                    continue

                if order.is_expire:
                    continue

                if order.is_refunded:
                    continue

            if game.is_created_by_manager():
                admin_game_order = AdminOrder.get_by_game(game.id)
                if admin_game_order and admin_game_order.is_refunded:
                    continue

            start_hour = game.start_time.hour
            end_hour = game.end_time.hour if game.end_time.hour > 0 else 24
            for point in range(start_hour, end_hour):
                result[point] = game.id
        return result



    def price(self, start_time, end_time, price_type=TT_PRICE, member_type_id=None):
        '''开始时间和结束时间必须在同一天'''

        price = 0.00

        if verify_holiday(start_time):
            kind = HOLIDAY
        elif verify_weekend(start_time):
            kind = WEEKEND
        else:
            kind = WORKDAY

        start_point = start_time.hour
        end_point = end_time.hour if end_time.hour != 0 else 24
        venue_id = self.venue_id
        env_type = self.env_type
        ground_type = self.ground_type

        for i in range(start_point, end_point):
            p = Price.get(venue_id, env_type, ground_type, i, kind, price_type)

            if not p:
                p = Price.get_venue_default_price(self.venue_id, kind)

            if not p:
                raise Exception('No price')

            price += float(p.price)

        if member_type_id:
            member_type = MemberType.get(member_type_id)
            discount = member_type.discount

            price = price * discount / 10

        return price

    def update(self, venue_id=None, size_type=None, env_type=None, sports_type=None, ground_type=None, seq=None, has_light=None):
        self.venue_id = venue_id or self.venue_id
        self.size_type = size_type or self.size_type
        self.env_type = env_type or self.env_type
        self.sports_type = sports_type or self.sports_type
        self.ground_type = ground_type or self.ground_type
        self.seq = seq or self.seq
        self._has_light = has_light or self._has_light

        r = store.transac('udpate' + self.table + ' set venue_id=%s, size_type=%s, '
                          'env_type=%s, sports_type=%s, ground_type=%s, seq=%s, has_light=%s where id=%s',
                          (self.venue_id, self.size_type, self.env_type, self.sports_type, self.ground_type,
                           self.seq, self._has_light, self.id))
        if r:
            return self

    def delete(self):
        r = store.transac('delete from ' + self.table + ' where id=%s', self.id)
        return r

    @property
    def type(self):
        return COURT_TYPES.get(self.size_type)

    @property
    def env(self):
        return COURT_ENVS.get(self.env_type)

    def jsonify(self):
        return {
            'id': int(self.id),
            'venue_id': self.venue_id,
            'name': self.name,
            'sports_type': self.sports_type,
            'size_type': self.size_type,
            'env_type': self.env_type,
            'ground_type': self.ground_type,
        }

    @classmethod
    @cache(MCKEY_COURT % '{id}', expire=ONE_DAY)
    def get(cls, id):
        r = store.fetchone(cls.select_all + ' where `id`=%s', id)
        return r and cls(*r)

    @classmethod
    def gets(cls, ids):
        courts = [cls.get(id) for id in ids if id]
        courts = filter(None, courts)
        return courts

    @classmethod
    def get_by_venue(cls, venue_id, start=0, limit=20):
        r = store.fetchall(cls.select_all + ' where `venue_id`=%s'
                        ' limit %s,%s', (venue_id, start, limit))
        if isinstance(r, tuple):
            return [cls(*court) for court in r]

    @classmethod
    def add(cls, venue_id, seq, size_type, env_type, sports_type, ground_type, has_light):
        r = store.transac('insert ignore into ' + cls.table +
                          ' (venue_id, seq, size_type, env_type, sports_type, ground_type, has_light, create_time) '
                          'values (%s, %s, %s, %s, %s, %s, %s, %s)',
                          (venue_id, seq, size_type, env_type, sports_type, ground_type, has_light, datetime.now()))
        if r:
            return cls.get(r)

    @classmethod
    def delete_by_venue(cls, venue_id):
        r = store.transac('delete from ' + cls.table + ' where venue_id=%s', venue_id)
        return r
