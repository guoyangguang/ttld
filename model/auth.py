# -*- coding:utf-8 -*- ＃

from datetime import datetime
from random import getrandbits

from libs.sqlstore import store
from view.utils import get_int_key, gen_public_key


class Auth(object):

    table = "app_auth"
    select_all = 'select id, app_key, secret, app_name, create_time, update_time from ' + table

    def __init__(self, id, app_key, secret,
                 app_name, create_time, update_time):
        self.id = id
        self.app_key = app_key
        self.secret = secret
        self.app_name = app_name
        self.create_time = create_time
        self.update_time = update_time

    @classmethod
    def get(cls, id):
        sql = (cls.select_all + ' where id = %s')
        r = store.fetchone(sql, id)
        if r:
            id, _app_key, _secret, app_name, create_time, update_time = r
            app_key = gen_public_key(_app_key)
            secret = '%016x' % _secret
            return cls(id, app_key, secret, app_name, create_time, update_time)

    @classmethod
    def get_by_app_key(cls, app_key):
        _app_key = get_int_key(app_key)
        sql = (cls.select_all + ' where app_key = %s')
        params = (_app_key,)
        r = store.fetchone(sql, params)
        if r:
            id, _app_key, _secret, app_name, create_time, update_time = r
            app_key = gen_public_key(_app_key)
            secret = '%016x' % _secret
            return cls(id, app_key, secret, app_name, create_time, update_time)

    @classmethod
    def get_by_app_name(cls, app_name):
        sql = cls.select_all + ' where app_name = %s'
        r = store.fetchone(sql, app_name)
        if r:
            id, _app_key, _secret, app_name, create_time, update_time = r
            app_key = gen_public_key(_app_key)
            secret = '%016x' % _secret
            return cls(id, app_key, secret, app_name, create_time, update_time)

    @classmethod
    def add(cls, app_name):
        if not app_name:
            return
        api_key = gen_public_key(getrandbits(60))
        api_secret = '%016x' % getrandbits(64)
        sql = ('insert into ' + cls.table +
               ' (app_key, secret, app_name, create_time)'
               ' values (%s, %s, %s, %s)')
        params = (get_int_key(api_key), int(api_secret, 16), app_name, datetime.now())
                  
        r = store.transac(sql, params)
        if r:
            return cls.get(r)

    @classmethod
    def delete(cls, id):
        sql = 'delete from ' + cls.table + ' where id = %s'
        r = store.transac(sql, (id,))
        return r
