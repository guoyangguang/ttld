# -*- coding: utf-8 -*-

from datetime import datetime

from libs.sqlstore import store
from model.alipay.dut import Sign
from model.user import User
from model.consts import GRADE_NORMAL, USER_GRADES, PAYTYPE_VIP


class Account(object):

    table = 'account'

    def __init__(self, id, user_id, grade, balance):
        self.id = id
        self.user_id = user_id
        self.grade = grade
        self.balance = balance

    def __repr__(self):
        return '<Account:user_id=%s,grade=%s>' % (self.user_id, self.grade_name)

    @property
    def grade_name(self):
        return USER_GRADES.get(self.grade, 'normal')

    @classmethod
    def new(cls, user_id):
        r = store.transac('insert into ' + cls.table +
                           ' (user_id, grade, balance) values (%s, %s, %s)',
                           (user_id, GRADE_NORMAL, 0.00))
        if r:
            return cls.get(user_id)

    @classmethod
    def get(cls, user_id):
        r = store.fetchone('select id, user_id, grade, balance '
                           'from ' + cls.table + ' where user_id=%s', user_id)
        return r and cls(*r)

    @classmethod
    def get_or_add(cls, user_id):
        return cls.get(user_id) or cls.new(user_id)

    @classmethod
    def delete(cls, user_id):
        r = store.transac('delete from ' + cls.table + ' where '
                          'user_id=%s', user_id)
        return r 

    def update_grade(self, grade):
        r = store.transac('update ' + self.table + ' set grade=%s '
                          'where id=%s', (grade, self.id))
        return r

    def inc_balance(self, amount):
        r = store.transac('update ' + self.table +
                          ' set balance = balance + %s where id=%s',
                          (amount, self.id))
        return r

    def reduce_balance(self, amount):
        r = store.transac('update ' + self.table +
                          ' set balance = balance - %s where id=%s',
                          (amount, self.id))
        return r

    def flush_get_balance(self):
        r = store.fetchone('select balance from ' + self.table + ' where id=%s',
                          (self.id,))
        return r and r[0]

    @property
    def alipay_sign(self):
        signs = Sign.gets_by_user_id(self.user_id)
        signs = [s for s in signs if s.is_enabled]
        return signs and signs[0] or None

    @property
    def alipay_account(self):
        return self.alipay_sign and self.alipay_sign.alipay_account_name

    @property
    def express_sign_id(self):
        return self.alipay_sign and self.alipay_sign.id

    @property
    def current_vip_order(self):
        from model.order import Order
        return Order.get_user_lastest_completed_order(self.user_id, PAYTYPE_VIP)

    @property
    def vip_expire_time(self):
        order = self.current_vip_order
        return order and order.expire_time

    @property
    def is_vip_serve(self):
        if self.vip_expire_time and self.vip_expire_time >= datetime.now():
            return True
        return False

    def jsonify(self):
        return {
            'user_id': self.user_id,
            'grade': self.grade_name,
            'vip_expire_time': self.vip_expire_time and self.vip_expire_time.strftime('%Y-%m-%d %H:%M:%S') or '',
            'account_balance': float(self.flush_get_balance())
        }
