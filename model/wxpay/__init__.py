# coding: utf-8

import json
import urllib
import urllib2
import hashlib
from xml.etree import ElementTree
from libs.db import db
from config import TENCOURT
from model.wxpay.consts import *

def build_package(parameter):
    keys = parameter.keys()
    keys.sort()
    joined_string = '&'.join(['%s=%s' % (key.lower(), unicode(parameter[key])) for key in keys])
    joined_string += '&key=' + partnerKey
    m = hashlib.md5(joined_string.encode('utf-8'))
    m.digest()
    signValue = m.hexdigest().upper()
    package = '&'.join(
        ['%s=%s' % (key, urllib.quote(unicode(parameter[key]).encode('utf-8'),safe='')) for key in keys])
    package += '&sign=' + signValue
    return package

def save_notify_data(data):
    db_data = db.get(NOTIFY_DATA_DB_KEY)
    if not db_data:
        db.lcreate(NOTIFY_DATA_DB_KEY)

    db.ladd(NOTIFY_DATA_DB_KEY, data)

# 在mysql中会保存所有财付通的退款记录
def save_refund_data(data):
    pass