#-*- coding: utf-8 -*-
import requests
import hashlib
from xml.etree import ElementTree
from model.consts import PAYMETHOD_WEIXIN
from model.wxpay.consts import partnerId, partnerKey, REFUND_PEM_PATH
from model.order.order_notify import OrderRefundNotify
from tools.celery_logger import refund_logger_log

def refund_from_tenpay(order_id, transaction_id, out_refund_no, total_fee, refund_fee):
    op_user_passwd = 'tt2014ttyd'
    str_for_md5 = 'op_user_id=%s&op_user_passwd=%s&out_refund_no=%s&partner=%s&refund_fee=%s&total_fee=%s&transaction_id=%s&key=%s' % (
        partnerId,
        op_user_passwd,
        out_refund_no,
        partnerId,
        int(refund_fee * 10 * 10),
        int(total_fee * 10 * 10),
        transaction_id,
        partnerKey
    )
    sign = hashlib.md5(str_for_md5).hexdigest().upper()

    data = dict(
      sign=sign,
      partner=partnerId,
      op_user_id=partnerId,
      op_user_passwd=op_user_passwd,
      out_refund_no=out_refund_no,
      transaction_id=transaction_id,
      total_fee=int(total_fee * 10 * 10),
      refund_fee=int(refund_fee * 10 * 10)
    )
    refund_url = 'https://mch.tenpay.com/refundapi/gateway/refund.xml'
    try:
        response = requests.request(method='POST', url=refund_url, data=data, cert=REFUND_PEM_PATH, verify=False)
    except Exception as e:
        refund_logger_log.delay(40, str(e))
    else:
        #retcode, int, 返回状态码,0 表示成功,其他未定义
        #retmsg, string, 返回信息,如非空,为错误原因
        #refund_status, int, 退款状态:4,10:退款成功。3,5,6:退款失败。 8,9,11:退款处理中。 1,2:未确定,需要商户原退款单号重新发起。
        # 7:转入代发,退款到银行发现用户的卡作废或者冻结了,导致原路退款银行卡失败,资金回流到商户的现金帐号,需要商户人工干预,通过线下或者
        # 财付通转账的方式进行退款
        #refund_id, string, 财付通退款单号
        #refund_channel, int, 退款渠道,0:退到财付通、1:退到银行
        #recv_user_id, string, 转账退款接收退款的财付通帐号
        #reccv_user_name, string, 转账退款接收退款的姓名(需与接收退款的财 付通帐号绑定的姓名一致)
        response_text = response.text
        OrderRefundNotify.add(order_id, out_refund_no, PAYMETHOD_WEIXIN, response_text)
        parser = ElementTree.XMLParser(encoding="utf-8")
        eTree = ElementTree.fromstring(response_text, parser=parser)
        retcode = int(eTree.find('retcode').text)
        if retcode == 0:
            refund_id = eTree.find('refund_id').text 
            refund_status = int(eTree.find('refund_status').text)
            if refund_status in (4, 10):
                msg = u'退款成功.'
                is_refunded = True
            elif refund_status in (8, 9, 11):
                msg = u'退款处理中,退款需要1至3个工作日,请您耐心等待.'
                is_refunded = True
            elif refund_status in (3, 5, 6):
                msg = u'退款失败,请您联系客服人工退单.'
                is_refunded = False 
            elif refund_status in (1, 2):
                msg = u'请您重新退单.'
                is_refunded = False 
            elif refund_status == 7:
                msg = u'请您联系客服人工退单.'
                is_refunded = False 
            else:
                msg = u'未知状态,请您联系客服人工退单.'
                is_refunded = False 
            r = dict(is_refunded=is_refunded, msg=msg, refund_id=refund_id)
        else:
            r = dict(is_refunded=False, msg=eTree.find('retmsg').text, refund_id=None)
        r_copy = r.copy() 
        r_copy['transaction_id'] = transaction_id
        r_copy['out_refund_no'] = out_refund_no
        r_copy['total_fee'] = total_fee
        r_copy['refund_fee'] = refund_fee
        refund_logger_log.delay(20, 'tenpay ' +  str(r_copy))
        return r

def fetch_refunding_order(transaction_id):
    str_for_md5 = 'partner=%s&transaction_id=%s&key=%s' % (
        partnerId,
        transaction_id,
        partnerKey
    )
    sign = hashlib.md5(str_for_md5).hexdigest().upper()
    data = dict(
      sign=sign,
      partner=partnerId,
      transaction_id=transaction_id
    )
    order_detail_url = 'https://gw.tenpay.com/gateway/normalrefundquery.xml'
    try:
        response = requests.request(method='GET', url=order_detail_url, params=data,
                                    cert=REFUND_PEM_PATH, verify=False)
    except Exception as e:
        '''TODO sentry log'''
        print(str(e))
    else:
        parser = ElementTree.XMLParser(encoding="utf-8")
        eTree = ElementTree.fromstring(response.text, parser=parser)
        retcode=int(eTree.find('retcode').text)
        if retcode == 0:
            refund_status=int(eTree.find('refund_state_0').text)
            if refund_status in (4, 10):
                msg = u'退款成功.'
            elif refund_status in (8, 9, 11):
                msg = u'退款处理中,退款需要1至3个工作日,请用户耐心等待.'
            elif refund_status in (3, 5, 6):
                msg = u'退款失败,请帮助用户人工退单.'
            elif refund_status in (1, 2):
                msg = u'请告知用户自主重新退单.'
            elif refund_status == 7:
                msg = u'请帮助用户人工退单.'
            else:
                msg = u'未知状态,请帮助用户人工退单.'
            return dict(msg=msg, refund_fee=eTree.find('refund_fee_0').text)
        else:
            return dict(msg=eTree.find('retmsg').text, refund_fee=None)

if __name__ == '__main__':

    print (fetch_refunding_order('1219176801201411276081675183'))
