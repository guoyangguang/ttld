# -*- coding:utf-8 -*-

from datetime import datetime
from libs.sqlstore import store
from model.consts import DEVICE_OS


class Device(object):

    table = 'device'

    def __init__(self, id, uid, token, os, create_time):
        self.id = id
        self.uid = uid
        self.token = token
        self.os = os
        self.create_time = create_time

    def __repr__(self):
        return '<Device:uid=%s,token=%s>' % (self.uid, self.token)

    @property
    def os_str(self):
        return DEVICE_OS.get(self.os) or 'Other OS'

    @classmethod
    def register(cls, uid, token, os, create_time=datetime.now()):
        r = store.transac('insert into ' + cls.table + ' (uid, token, os, create_time) '
                          'values (%s, %s, %s, %s)',
                          (uid, token, os, datetime.now()))
        if r:
            return cls.get(r)

    @classmethod
    def remove(cls, uid):
        r = store.transac('delete from ' + cls.table + ' where '
                          'uid=%s', uid)
        return r

    @classmethod
    def get(cls, id):
        r = store.fetchone('select id, uid, token, os, create_time from ' + cls.table +
                           ' where id=%s', id)
        return r and cls(*r)

    @classmethod
    def get_by_uid(cls, uid):
        r = store.fetchone('select id, uid, token, os, create_time from ' + cls.table +
                           ' where uid=%s', uid)
        return r and cls(*r)

    def jsonify(self):
        return {'user_id': self.uid, 'device_token': self.token, 'os': self.os_str}
