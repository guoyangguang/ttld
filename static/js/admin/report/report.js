(function() {
    require(['jquery', 'lib/bootstrap-datepicker'], function($) {
        var $selectStartDate = $('.select-start-date'),
            $selectEndDate = $('.select-end-date');

        $selectStartDate.datepicker({
            format: 'yyyy-mm-dd'
        });

        $selectEndDate.datepicker({
            format: 'yyyy-mm-dd'
        });
    });
}).call(this);
