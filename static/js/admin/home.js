(function() {
    require(['jquery', 'handlebars', 'bootstrap', 'lib/bootstrap-datepicker', 'lib/fullcalendar'], function($, Handlebars) {
        var _fetchGamesURL = '/admin/venue/{vid}/games/',
            _fetchCourtsURL = '/admin/venue/{vid}/courts/',
            $calendar = $('.cal'),

            $createGameModal = $('.modal-create-game'),
            $createStartTime = $('.start-time', $createGameModal),
            $createEndTime = $('.end-time', $createGameModal),
            $createCourtId = $('.court-id', $createGameModal),
            $createComment = $('.comment', $createGameModal),
            $createCourtName = $('.court-name', $createGameModal),
            $gameInfoModal = $('.modal-game-info'),
            $removeGameBtn = $('.remove-game', $gameInfoModal),

            formatTime = function (time) {
                return $.fullCalendar.formatDate(time, 'yyyy-MM-dd HH:mm:ss');
            },

            showCreateGameModal = function (start, end, courtId, courtName) {
                start = formatTime(start);
                end = formatTime(end);

                $createStartTime.text(start);
                $createEndTime.text(end);
                $createCourtName.text(courtName);
                $createCourtId.val(courtId);

                $createGameModal.modal('show');
            },

            renderGame = function (id, title, start, end, allDay, courtId) {
                start = $.fullCalendar.parseDate(start);
                end = $.fullCalendar.parseDate(end);
                allDay = allDay || false;
                css = '.fc-resourcerow-' + courtId;
                $(css).parents('.cal').fullCalendar('renderEvent',
                    {
                        id: id,
                        title: title,
                        start: start,
                        end: end,
                        allDay: allDay,
                        color: 'red',
                        resource: courtId
                    },
                    true // make the event "stick"
                );
            },

            createGame = function (e) {
                var startTime = $createStartTime.text(),
                    endTime = $createEndTime.text(),
                    courtId = parseInt($createCourtId.val(), 10),
                    comment = $createComment.val(),
                    url = '/admin/court/' + courtId + '/games/',
                    settings = {
                        type: 'POST',
                        data: JSON.stringify({
                            start_time: startTime,
                            end_time: endTime,
                            comment: comment
                        }),
                        contentType: 'application/json; charset=utf-8'
                    };
                    
                e.preventDefault();

                $.ajax(url, settings).done(function (res) {
                    if (res.code != 200) {
                        alert('添加失败，' + res.reason);
                        return
                    }
                    var start = $.fullCalendar.parseDate(startTime),
                        end = $.fullCalendar.parseDate(endTime);
                        allDay = false,
                        id = res.game_id,
                        title = '预留';

                    renderGame(id, title, start, end, allDay, courtId);
                    $calendar.fullCalendar('unselect');
                }).fail(function () {
                    alert('添加失败，请重新添加');
                }).always(function () {
                    $createGameModal.modal('hide');
                });
            },

            removeGame = function (e) {
                e.preventDefault();

                var gameId = $(this).attr('data-gid'),
                    url = '/admin/game/' + gameId + '/',
                    settings = {
                        type: 'DELETE'
                    };

                if ($(this).hasClass('disabled')) {
                    return;
                }

                if (confirm('确认删除？')) {
                    $.ajax(url, settings).done(function (res) {
                        $calendar.fullCalendar('removeEvents', gameId);
                    }).fail(function (res) {
                        alert(res.msg);
                    }).always(function () {
                        $removeGameBtn.addClass('disabled');
                        $gameInfoModal.modal('hide');
                    });
                }
            },

            openGameInfoModal = function (data) {
                var i = 0,
                    pTmpl = '{{#each participates}}\
                               <tr>\
                                 <td>\
                                   <img src="{{avatar}}" alt="{{name}}" width="30" height="30" />\
                                   <span>{{name}}</span>\
                                 </td>\
                                 <td>{{status}}</td>\
                                 <td>{{create_time}}</td>\
                               </tr>\
                             {{/each}}',

                    pTemplate = Handlebars.compile(pTmpl),
                    pData = data.participates,
                    pTotal = pData.length,

                    commentTmpl = '<p>备注：\
                                    <span>{{comment}}</span>\
                                   </p>',
                    commentTemplate = Handlebars.compile(commentTmpl),

                    $infoTable = $('#participates', $gameInfoModal),
                    $infoBody = $('tbody', $infoTable),
                    $infoCourtName = $('.court-name', $gameInfoModal),
                    $infoStartTime = $('.start-time', $gameInfoModal),
                    $infoEndTime = $('.end-time', $gameInfoModal),
                    $infoComment = $('.comment', $gameInfoModal);

                $infoCourtName.text(data.court_name);
                $infoStartTime.text(data.start_time);
                $infoEndTime.text(data.end_time);

                $infoComment.html(commentTemplate(data));
                $infoBody.html(pTemplate(data));

                if (pTotal === 0) {
                    $removeGameBtn.removeClass('disabled');
                    $infoTable.hide().prev('p').hide();
                } else {
                    $infoTable.show().prev('p').show();
                }

                $gameInfoModal.modal('show');
            },

            showGameInfo = function (game, jsEvent, view) {
                var url = '/admin/game/' + game.id + '/',
                    settings = {
                        type: 'GET',
                        dataType: 'json'
                    };

                $removeGameBtn.attr('data-gid', game.id)
                              .addClass('disabled');

                $.ajax(url, settings).done(function (res) {
                    openGameInfoModal(res);
                }).fail(function () {
                    alert('获取比赛信息失败！');
                });
            };

        $.each($calendar, function (i, calendar) {
            var venue_id = $(calendar).data('vid'),
                open_time = $(calendar).data('open_time'),
                close_time = $(calendar).data('close_time'),
                $box = $(calendar).parents('.box'),
                $selectDate = $('.select-date', $box),
                fetchCourtsURL = _fetchCourtsURL.replace('{vid}', venue_id),
                fetchGamesURL = _fetchGamesURL.replace('{vid}', venue_id);

            $(calendar).fullCalendar({
                defaultView: 'resourceDay',
                minTime: open_time,
                maxTime: close_time,
                slotMinutes: 60,
                timeFormat: 'H:mm - {H:mm}',
                editable: true,
                selectable: true,
                allDaySlot: false,
                disableDragging: true,
                disableResizing: true,
                dayNames: ['星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
                dayNamesShort: ['周日', '周一', '周二', '周三', '周四', '周五', '周六'],
                titleFormat: {
                    resourceDay: 'yyyy 年 M 月 d 日，dddd'
                },
                columnFormat: {
                    resourceDay: 'HH:mm'
                },
                select: function (start, end, allDay, jsEvent, view, resource) {
                    if (!allDay) {
                        showCreateGameModal(start, end, resource.id, resource.name);
                    };
                },
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: ''
                },
                buttonText: {
                    today: '今天',
                    month: '月',
                    week: '周',
                    day: '天'
                },
                events: fetchGamesURL,
                resources: fetchCourtsURL,
                eventClick: showGameInfo
            });

            $selectDate.datepicker({
                format: 'yyyy-mm-dd'
            }).on('changeDate', function (e) {
                var year = e.date.getFullYear(),
                    month = e.date.getMonth(),
                    day = e.date.getDate();

                    $(calendar).fullCalendar('gotoDate', year, month, day);
            });
        });

        $('html').on('click', '.submit-create-game', createGame);
        $('html').on('click', '.remove-game', removeGame);
    });
}).call(this);
