(function() {
    require(['jquery'], function($) {
        var $form = $('form'),
            $hasLight = $('.has-light', $form),

            toggleHasLight = function () {
                $hasLight.hide();

                if ($(this).val() == 2) {
                    $hasLight.show();
                }
            };

        $form.on('change', '.env', toggleHasLight);
    });
}).call(this);
