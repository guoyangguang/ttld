(function() {
    require(['jquery'], function($) {
        var removeCourt = function(e) {
            var data = $(this).data(),
                cid = data.cid,
                vid = data.vid,
                url = $(this).attr('href'),
                $row = $(this).parents('tr'),
                settings = {
                    type: 'DELETE',
                    cid: cid,
                    vid: vid
                };

            e.preventDefault();

            if (confirm('确认删除？')) {
                $.ajax(url, settings).done(function() {
                    $row.hide();
                }).fail(function() {
                    alert('删除失败！');
                });
            }
        };

        $('html').on('click', '.remove-court', removeCourt);
    });
}).call(this);
