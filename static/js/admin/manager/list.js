(function() {
    require(['jquery', 'bootstrap', 'flat'], function($) {
        var $form = $('form'),
            redirect = function() {
                var role = $('.select-role', $form).val();
                window.location = '?role=' + role;
            };
        $form.on('change', 'select', redirect);
    });
}).call(this);
