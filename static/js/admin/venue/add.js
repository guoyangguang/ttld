(function() {
    require(['jquery', 'lib/jquery-validate', 'lib/bootstrap-timepicker'], function($) {
        $('#add-venue-form').validate({
            messages: {
                name: '场馆名称不能为空！',
                address: '地址不能为空！',
                telephone: '电话不能为空！'
            },
            errorElement: 'span',
            errorClass: 'help-block error',
            errorPlacement: function (error, element) {
                element.parents('.controls').append(error);
            },
            highlight: function (label) {
                $(label).closest('.control-group').removeClass('error success').addClass('error');
            },
            success: function (label) {
                label.addClass('valid').closest('.control-group').removeClass('error success').addClass('success');
            }
        });

        $('#open-time').timepicker({
            showSeconds: false,
            showMeridian: false,
            defaultTime: '11:00',
        });

        $('#close-time').timepicker({
            showSeconds: false,
            showMeridian: false,
            defaultTime: '23:00',
        });
    });
}).call(this);
