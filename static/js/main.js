(function() {
    require.config({
        baseUrl: '/static/js/'
    });

    define('lib/jquery', 'lib/jquery.min.js');
    define('lib/jquery-validate', ['lib/jquery'], 'lib/jquery.validate.min.js');
    define('lib/bootstrap', ['lib/jquery'], 'lib/bootstrap.min.js');
    define('lib/bootstrap-timepicker', ['lib/bootstrap'], 'lib/bootstrap-timepicker.min.js');
    define('lib/bootstrap-datepicker', ['lib/bootstrap'], 'lib/bootstrap-datepicker.js');
    define('lib/fullcalendar', ['lib/jquery'], 'lib/fullcalendar.js');
    define('lib/handlebars', 'lib/handlebars.js');

    define('jquery', ['lib/jquery'], function() {
        return $;
    });

    define('handlebars', ['lib/handlebars'], function() {
        return Handlebars;
    });

    define('bootstrap', ['lib/bootstrap'], function() {});
    define('flat', ['lib/jquery', 'lib/bootstrap'], 'lib/flat.js');
}).call(this);
