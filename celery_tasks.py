# coding: utf-8

import sys
from celery import Celery
from config import BROKER_URI, BACKEND_URI

reload(sys)
sys.setdefaultencoding('utf-8')

celery_app = Celery(
    'tasks',
    broker=BROKER_URI,
    backend=BACKEND_URI,
    include=['tools.celery_logger', 'tools.postman']
)
