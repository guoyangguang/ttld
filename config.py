# -*- coding: utf-8 -*-

# your configuration here
MYSQL_CONFIG = {
    'db': 'tencourt_test',
    'user': 'travis',
    'password': '',
    'host': '127.0.0.1',
    'port': 3306,
}

HOST = '127.0.0.1'
PORT = '8080'
DEBUG = False
SECRET_KEY = 'JIn398/3yX R~XHH!328jl]LW0X/,?2*(T'

# 用于文件存储，需改为你自己的目录
STORE_DIR = '/opt/fileupload'

PICKLEDB_DIR = '/opt/pickledb'

TENCOURT = 'http://www.tiantianwq.com'
TENCOURT_NAME = '动起来'

SENTRY_DSN = 'http://af8943454822415ca522a80751f918a5:78a042949f7f4460b7e001b54be87103@sentry.tiantianwq.com/2'

MEMCACHED = ["127.0.0.1:11211", ]

BROKER_URI = 'amqp://tencourt:tencourt@localhost:5672/tencourt'
BACKEND_URI = 'db+mysql://root:zzc@localhost:3306/celery'

GETUI_CONFIGURE = dict( 
    appid='WmNDEGTsFfAg3DrtuQnrG2',
    appkey='NzZrLIFKq1AJoSgIrS8BM7',
    appsecret='vFCj0idsTg5jZey9YDPhm',
    mastersecret='OrVLyH0tAg6OvY0am8Ye69',
    host='http://sdk.open.api.igexin.com/apiex.htm'
)

GUNICORN_LOG_DIR = '/opt/deploy/log/gunicorn' 
TENCOURT_LOG_FILE = '/opt/deploy/log/tencourt/tencourt.log'
REFUND_LOG_FILE = '/opt/deploy/log/tencourt/refund.log'

USER_APP_VERSION = '22'
USER_APP_DOWNLOAD_PATH = '%s/static/apk/ttwq.apk' % TENCOURT
MANAGER_APP_VERSION = '3'
MANAGER_APP_DOWNLOAD_PATH = '%s/static/apk/ttwq_manager.apk' % TENCOURT

EHOST = 'smtp.126.com'
ENAME = 'tencourt'
EPASS = 'tencourt@2013'
EDOMAIN = '126.com'

# Redis服务器
CACHE_SERVERS = {
    'entity': ('127.0.0.1', 6379),
}

try:
    from local_config import *
except ImportError:
    pass
