#!/usr/bin/env python

import os
import readline
from pprint import pprint

from flask import *

from app import *
from libs.db import db
from libs.sqlstore import store


os.environ['PYTHONINSPECT'] = 'True'
