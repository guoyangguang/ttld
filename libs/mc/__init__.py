# -*- coding:utf-8 -*-

import memcache

from config import MEMCACHED, DEBUG
from libs.mc.decorator import create_decorators
from libs.mc.debug import LocalMemcache


def hashdict(d):
    """
    make dictionary becomes a immutable tuple (from dae.util)
    """

    if isinstance(d, list):
        return tuple(hashdict(v) for v in d)
    elif isinstance(d, dict):
        return tuple(sorted((k, hashdict(v)) for k, v in d.iteritems()))
    else:
        return d


def create_mc(addr, **kwargs):
    client = memcache.Client(addr, dead_retry=5, socket_timeout=1, **kwargs)
    return client


_clients = {}
def mc_from_config(addr, **kwargs):
    cache_key = hashdict([addr, kwargs])
    mc = _clients.get(cache_key)
    if mc:
        return mc

    if DEBUG:
        mc = LocalMemcache()
    else:
        mc = create_mc(addr, **kwargs)

    _clients[cache_key] = mc
    return mc


mc = mc_from_config(MEMCACHED)


def stub_cache(*args, **kws):
    pass


new_cache = pcache = pcache2 = listcache = cache_in_obj = delete_cache = cache = stub_cache

# some time consts for mc expire
ONE_MINUTE = 60
HALF_HOUR = 1800
ONE_HOUR = 3600
HALF_DAY = ONE_HOUR * 12
ONE_DAY = ONE_HOUR * 24
ONE_WEEK = ONE_DAY * 7
ONE_MONTH = ONE_DAY * 30
ONE_YEAR = ONE_DAY * 365

globals().update(create_decorators(mc))
