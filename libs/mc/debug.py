# -*- coding:utf-8 -*-


class LocalMemcache(object):

    def __init__(self):
        self.dataset = {}

    def set(self, key, val, time=0, compress=True):
        _, version = self.dataset.get(key, (None, -1))
        self.dataset[key] = (val, version + 1)
        return True

    def add(self, key, val):
        if not self.dataset.has_key(key):
            self.dataset[key] = (val, 1)
            return True
        else:
            return False

    def set_multi(self, values, time=0, compress=True, return_failure=False):
        for k, v in values.iteritems():
            _, version = self.dataset.get(k, (None, -1))
            self.dataset[k] = (v, version + 1)
        if return_failure:
            return True, []
        else: return True

    def cas(self, key, val, time=0, cas=0):
        if key in self.dataset:
            _, version = self.dataset.get(key)
            if version == cas:
                self.dataset[key] = (val, version + 1)
                return True
        return False

    def delete(self, key, time=0):
        if key in self.dataset:
            del self.dataset[key]
        return 1

    def delete_multi(self, keys, time=0, return_failure=False):
        for k in keys:
            self.delete(k)
        if return_failure:
            return True, []
        else: return True

    def get(self, key):
        return self.dataset.get(key, (None, 0))[0]

    def gets(self, key):
        return self.dataset.get(key, (None, 0))

    def get_raw(self, key):
        raise NotImplementedError()

    def get_multi(self, keys):
        rets = {}
        for k in keys:
            r = self.dataset.get(k)
            if r is not None:
                rets[k] = r[0]
        return rets

    def get_list(self, keys):
        return [self.dataset.get(k)[0] for k in keys]

    def incr(self, key, val=1):
        raise NotImplementedError()

    def decr(self, key, val=1):
        raise NotImplementedError()

    def clear(self):
        self.dataset.clear()

    def get_last_error(self):
        return 0
