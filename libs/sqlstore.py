# coding: utf-8

import MySQLdb
from MySQLdb import DatabaseError
from DBUtils.PooledDB import PooledDB
from DBUtils.PooledDB import InvalidConnection
from config import MYSQL_CONFIG
# from tools.celery_logger import tencourt_logger_log

class SqlStore(object):

    # 连接池对象
    pool = None

    def __init__(self, config=MYSQL_CONFIG):
        self.config = config
        self._conn = self.__get_conn()
        self._cursor = self._conn.cursor()

    def __get_conn(self):
        """
        @summary: 静态方法，从连接池中取出连接
        @return MySQLdb.connection
        """

        if SqlStore.pool is None:
            db_config = self.config
            SqlStore.pool = PooledDB(MySQLdb, maxcached=5, maxconnections=20, host=db_config['host'],
                port=db_config['port'], user=db_config['user'], passwd=db_config['password'], db=db_config['db'],
                charset='utf8'
            )

        return SqlStore.pool.connection()

    def ensure_conn(self):
        try:
            if not self._conn:
                self._conn = self.__get_conn()
                self._cursor = self._conn.cursor()
        except InvalidConnection:
            self._conn = self.__get_conn()
            self._cursor = self._conn.cursor()

    def execute(self, sql, args=None):
        self.ensure_conn()
        if args and not isinstance(args, (dict, tuple, list)):
            args = (args,)
        try:
            self._cursor.execute(sql, args)
        #like _mysql_exceptions.OperationalError, _mysql_exceptions.IntegrityError, _mysql_exceptions.DataError)
        except DatabaseError as e:
            # tencourt_logger_log.delay(40, str(e))
            return False
        else:
            return (self.get_insert_id() if sql.startswith('insert') else True)

    def get_insert_id(self):
        self._cursor.execute("SELECT @@IDENTITY AS id")
        result = self._cursor.fetchall()
        return result[0][0]

    def commit(self):
        self._conn.commit()

    def rollback(self):
        self._conn.rollback()

    def close(self):
        if self._cursor:
            self._cursor.close()

        if self._conn:
            self._conn.close()

    def fetchone(self, sql, args=None):
        if self.execute(sql, args):
            r = self._cursor.fetchone()
        else:
            r = None
        self.close() 
        return r

    def fetchall(self, sql, args=None):
        if self.execute(sql, args):
            rs = self._cursor.fetchall()
        else:
            rs = None
        self.close() 
        return rs
    
    def transac(self, sql, args=None):
        val = self.execute(sql, args)
        if val:
            self.commit()
            r = val 
        else:
            self.rollback()
            r = None
        self.close() 
        return r 
    

store = SqlStore()
