# -*- coding:utf-8 -*-

import requests
import httplib
import urllib
from urllib import urlencode
from xml.etree import ElementTree
from config import DEBUG,TENCOURT_NAME
import json;

IHUYI_URL = 'http://106.ihuyi.com/webservice/sms.php?method=Submit'
IHUYI_ACCOUNT = 'cf_c382348216'
IHUYI_PASSWORD = '13ten46court78'

LUOSIMAO_URL = "https://sms-api.luosimao.com/v1/send.json"
LUOSIMAO_KEY = 'key-9e56c12f15f3559ffe0f3180d9ef7ec9'

YUNPIAN_HOST = "yunpian.com"
YUNPIAN_VERSION = "v1"
YUNPIAN_KEY = "9978557eddf43450f97d3427d4f3ee8e"
YUNPIAN_SMS_SEND_URI = "/" + YUNPIAN_VERSION + "/sms/send.json"

def send_sms(phone, content):
    return send_by_yunpian(phone, content)

def send_by_ihuyi(phone, content):
    if isinstance(content, unicode):
        content = content.encode('utf-8')

    data = {
        'account': IHUYI_ACCOUNT,
        'password': IHUYI_PASSWORD,
        'mobile': phone,
        'content': content
    }
    headers = {
        'content-type': 'application/x-www-form-urlencoded'
    }
    res = requests.post(IHUYI_URL, data=urlencode(data), headers=headers)

    result = res.text
    if isinstance(result, unicode):
        result = result.encode('utf-8')

    etree = ElementTree.fromstring(result)
    try:
        if etree.getchildren()[0].text == '2':
            return True
    except:
        return False
    return False

def send_by_luosimao(phone, content):
    full_content = "{content}【{sign}】".format(content=content,sign=TENCOURT_NAME)
    resp = requests.post((LUOSIMAO_URL),
	auth=("api", LUOSIMAO_KEY),
    data={
		"mobile": phone,
		"message": full_content
	},timeout=3 , verify=False);
    result =  json.loads(resp.content)
    try:
        if result['code'] == 0:
            return True
    except:
        return False
    return False

def send_by_yunpian(phone, content):
    full_content = "{content}【{sign}】".format(content=content,sign=TENCOURT_NAME)
    params = urllib.urlencode({'apikey': YUNPIAN_KEY, 'text': full_content, 'mobile':phone})
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    conn = httplib.HTTPConnection(YUNPIAN_HOST, port=80, timeout=30)
    conn.request("POST", YUNPIAN_SMS_SEND_URI, params, headers)
    response = conn.getresponse()
    response_str = response.read()
    conn.close()
    try:
        if response.status == 200:
            return True
    except:
        return False
    return False