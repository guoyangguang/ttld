# coding: utf-8

import base64
from functools import wraps

from flask import request

from model.auth import Auth
from model.manager import Manager
from model.venue import Venue
from view.api.error import (
    APIError, INVALID_API_KEY_OR_SECRET, MISSING_AUTHORIZATION,
    INVALID_ACCESS_TOKEN, MISSING_PARAM, MISMATCHED_PASSWORD, USER_NOT_FOUND, INVALID_URL,
    VENUE_NOT_FOUND, FORBIDDEN
)


# 非 GET 请求但也使用 args 传参的类和方法名
ARGS_CLS_FUNC_NAME = [('UserAPI', 'PUT')]


def check_api_key(f):
    @wraps(f)
    def deco(*args, **kwargs):
        key = request.args.get('app_key', '')
        secret = request.args.get('app_secret', '')

        if not (key and secret):
            key = request.form.get('app_key', '')
            secret = request.form.get('app_secret', '')

        if not (key and secret) and request.json:
            key = request.json.get('app_key', '')
            secret = request.json.get('app_secret', '')

        if not (key and secret):
            raise APIError(MISSING_PARAM)

        if not _check_api_key(key, secret):
            raise APIError(INVALID_API_KEY_OR_SECRET)

        return f(*args, **kwargs)
    return deco


def _check_api_key(key, secret):
    if not key or not secret:
        return False

    auth = Auth.get_by_app_key(key)
    if auth and secret == auth.secret:
        return True

    return False


def check_access_token(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        _token = request.headers.get('Authorization')
        if not _token:
            raise APIError(MISSING_AUTHORIZATION)

        # token = _token.split(' ')[1]

        if not _check_access_token(_token):
            raise APIError(INVALID_ACCESS_TOKEN)
        return f(*args, **kwargs)
    return wrapper


# TODO
def _check_access_token(token):
    decode_token = base64.decodestring(token)
    manager_id, password = decode_token.split('tencourt')

    return Manager.get(manager_id).password == password

def check_manager_venue(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        manager_id = request.args.get('manager_id')
        venue_id = request.args.get('venue_id')

        if not manager_id and request.json:
            manager_id = request.json.get('manager_id')

        if not venue_id and request.json:
            venue_id = request.json.get('venue_id')

        if manager_id:
            manager = Manager.get(manager_id)

            if not manager:
                raise APIError(USER_NOT_FOUND)

        if venue_id:
            venue = Venue.get(venue_id)

            if not venue:
                raise APIError(VENUE_NOT_FOUND)

            if manager_id:
                if venue.manager_id != int(manager_id):
                    raise APIError(FORBIDDEN)

        return f(*args, **kwargs)

    return wrapper


def login_required(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        user_name = request.json.get('user_name')
        password = request.json.get('password')
        venue_id = request.json.get('venue_id')
        if not user_name or not password:
            raise(MISSING_PARAM)

        manager = Manager.get_by_name_or_tel(user_name)

        if not manager:
            raise APIError(USER_NOT_FOUND)

        if password != manager.password:
            raise APIError(MISMATCHED_PASSWORD)

        if venue_id:
            venue = Venue.get(venue_id)
            if not venue or (venue and venue.manager_id != manager.id):
                raise APIError(INVALID_URL)

        return f(*args, **kwargs)

    return wrapper
