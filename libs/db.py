import pickledb

from config import PICKLEDB_DIR

db_file = '%s/%s' % (PICKLEDB_DIR, 'tencourt.db')

db = pickledb.load(db_file, True)
