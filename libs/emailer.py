# -*- coding: utf-8 -*-

import smtplib
from email.mime.text import MIMEText
from config import DEBUG, EHOST, ENAME, EPASS, EDOMAIN

def send_mail(emails, subject, content):
    if DEBUG:
        print '客服邮件提醒:'
        print content
        return

    me = '%s@%s' % (ENAME, EDOMAIN)
    message = MIMEText(content, _subtype='plain', _charset='utf8')
    message['Subject'] = subject
    message['From'] = me
    message['To'] = ';'.join(emails)
    try:
        server = smtplib.SMTP()
        server.connect(EHOST)
        server.login(ENAME, EPASS)
        server.sendmail(me, emails, message.as_string())
        server.close()
        return True
    except Exception:
        return False
