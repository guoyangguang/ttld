#-*- coding: utf-8 -*-

import logging
from logging import StreamHandler
from logging.handlers import RotatingFileHandler, SMTPHandler
from config import EHOST, ENAME, EPASS, EDOMAIN

def config_logger(logger, log_file):
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(module)s:%(funcName)s:%(lineno)d %(message)s')

    console_handler = StreamHandler()
    console_handler.setLevel(logging.DEBUG)
    console_handler.setFormatter(formatter)
    file_handler = RotatingFileHandler(log_file, maxBytes=1024 * 256, backupCount=5)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)
    #mail_handler = SMTPHandler(
    #    mailhost=EHOST, 
    #    fromaddr=ENAME + '@126.com', 
    #    toaddrs=['ttld_log@163.com'], 
    #    subject='log from ttld', 
    #    credentials=(ENAME, EPASS), 
    #    secure=None
    #)
    #mail_handler.setLevel(logging.DEBUG)
    #mail_handler.setFormatter(formatter)
    
    logger.addHandler(console_handler)
    logger.addHandler(file_handler)
    #logger.addHandler(mail_handler)

    logger.setLevel(logging.DEBUG)
    
    return logger
