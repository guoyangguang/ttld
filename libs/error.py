# coding: utf-8

from flask import jsonify
from werkzeug.exceptions import Forbidden
from app import app


class ApiForbidden(Forbidden):
    pass


@app.errorhandler(403)
def not_allowed(error):
    response = jsonify(code='403', msg='not allowed')
    response.status_code = 403
    return response
