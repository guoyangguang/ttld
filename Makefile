init_db:
	python -W ignore tools/init_db.py

init_dep:
	pip install -r requirements.txt

init_data: init_db
	python -W ignore tools/init_data.py

dev:
	gunicorn -c gunicorn_dev_config.py app:app

test:
	py.test tests/

clean_pyc:
	@find . -type f -name '*.pyc' -exec rm {} \;
