TenCourt [![Build Status](https://magnum.travis-ci.com/VeryCB/tencourt.png?token=BmcPwyvJE6RQ22FYpY7u&branch=develop)](https://magnum.travis-ci.com/VeryCB/tencourt)
========

TenCourt Web Service

##搭建开发环境

1. 安装virtualenv：
> pip install virtualenv

2. 获取代码
> git clone https://github.com/VeryCB/tencourt.git

3. 进入venv环境
> cd tencourt  
> virtualenv venv  
> . venv/bin/activate  

4. 配置数据库
> cp local_config.py.tmpl local_config.py  
> 然后编辑`local_config.py`，将配置改成自己的，并`确保数据库已经创建`

5. 启动本地服务
> sudo apt-get install libffi-dev
> make init_db  
> make init_dep  
> make init_data  
> make dev  
> 上面这行命令会安装所有依赖，初始化数据库，并启动`tencourt`

6. Enjoy :)

##运行测试

> make test

##API文档


[用户相关](docs/api_user.md)

[场馆相关](docs/api_venue.md)

[订单相关](docs/api_order.md)

[比赛相关](docs/api_game.md)

[商家相关](docs/api_manager.md)

[推送和其他功能](docs/api_util.md)

