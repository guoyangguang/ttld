# -*- coding: utf-8 -*-

from unittest import TestCase
from libs.sqlstore import store

class BaseTestCase(TestCase):
    
    def tearDown(self):
        tables = [
            'device', 'trade', 'game_order', 'participate_order', 'comment',
            'require', 'vip_order', 'charge_order', 'photo_album', 'game',
            'court', 'venue', 'city', 'province', 'photo', 'telephone_verify',
            'token', 'followship', 'user', 'user_password', 'manager',
            'account', 'app_auth', 'venue_price'
        ]
        for table in tables:
            store.transac('delete from `%s`' % table)
