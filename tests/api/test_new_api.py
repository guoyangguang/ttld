# coding: utf-8

import unittest
import pytest
from tools.client import client
from model.user import User
from model.token import Token

class UserRegisterTest(unittest.TestCase):

    def setUp(self):
        self.telephone = '13811685066'

    def get_verify_code(self):
        args = {
            'telephone': self.telephone
        }
        r = client.post('/verify/phone/', data=args)
        assert 'verify_code' in r
        return r.get('verify_code')

    def test_regiter_user(self):
        verify_code = self.get_verify_code()
        args = {
            'telephone': self.telephone,
            'verify_code': verify_code,
            'password': 'zzc',
        }

        r = client.post('/user/register/', args)
        assert 'access_token' in r

    def test_venue_city(self):
        args = {
            'start': 0,
            'count': 10
        }
        r = client.get('/venue/city/', args)

        assert 'code' in r and r['code'] == 200

    def test_game_notify():
        uid = 1000004

        args = {
            'uid': uid,
            'count': 1
        }
        r = client.get('/game/notify/', args)
        assert 'code' in r and r['code'] == 200

    def tearDown(self):
        user = User.get_by_telephone(self.telephone)

        # 删除用户和相关的token
        if user:
            token = Token.get_by_uid(user.id)
            token.delete()
            user.delete()