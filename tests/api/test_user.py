# -*- coding: utf-8 -*-

import pytest

from tools.client import client
from tests.framework import BaseTestCase
from tools.factory import add_or_get_user

from view.utils import encrypt


def get_verify_code():
    telephone = '18663951234'
    args = {
        'telephone': telephone
    }
    r = client.post('/verify/phone/', data=args)
    assert 'verify_code' in r
    return r.get('verify_code')


@pytest.fixture
def add_basic_data(request):
    self = request.instance
    self.user = add_or_get_user('apitest-user')


@pytest.mark.usefixtures('add_basic_data')
class TestUser(BaseTestCase):

    def test_verify_telephone_and_code(self):
        verify_code = get_verify_code()
        args = {
            'telephone': '18663951234',
            'verify_code': verify_code
        }
        r = client.post('/verify/code/', data=args)
        assert 'code' in r and r['code'] == '200'
        assert 'telephone' in r

    def test_reset_password_verify_phone(self):
        args = {'telephone': self.user.telephone}
        r = client.post('/verify/reset-password/', data=args)
        assert 'code' in r and r['code'] == '200'
        assert 'telephone' in r
        assert 'verify_code' in r

    def test_reset_password(self):
        from model.verify import Verify
        from view.utils import encrypt, check_user_password
        verify_code = Verify.gen_code()
        Verify.add(self.user.telephone, verify_code)

        new_password = encrypt('12345678')
        args = {
            'telephone': self.user.telephone,
            'verify_code': verify_code,
            'password': new_password
        }
        r = client.post('/user/reset-password/', data=args)
        assert 'code' in r and r['code'] == '200'
        assert 'access_token' in r
        assert check_user_password(self.user.telephone, new_password)


    def test_add_new_user(self):
        args = {
            'telephone': '18663951234',
            'password': '123456',
            'username': 'daydayfree'
        }
        r = client.post('/user/', data=args)
        assert 'access_token' in r
        self.user_id = r['user_id']
        self.access_token = r['access_token']

    def test_user_login(self):
        from tools.factory import add_or_get_user
        telephone = '18663951234'
        password = '123456'
        user = add_or_get_user('apitest-user1', telephone, password)
        args = {
            'telephone': telephone,
            'password': encrypt(password)
        }
        r = client.post('/user/login/', data=args)
        assert 'code' in r and r['code'] == '200'
        assert 'user' in r
        assert 'access_token' in r

    def test_update_user_infomation(self):
        import os
        from model.photo import DEFAULT_AVATAR_URL
        args = {
            'name': '大麦',
            'birth': '1988-04-01',
            'gender': 'm',
            'tennis_level': 1,
            'tennis_age': 1,
            'intro': '我很拽!'
        }
        f = os.path.join(os.path.dirname(__file__), '../pics/user_avatar.jpg')
        r = client.put('/user/%s/' % self.user.id, args, files={'avatar': open(f, 'rb')})
        assert 'code' in r and r['code'] == 200
        assert 'user' in r

        user = r['user']
        print user
        assert user['name'] == u'大麦'
        assert user['intro'] == u'我很拽!'
        assert user['gender'] == 'm'
        assert user['avatar_url'] != DEFAULT_AVATAR_URL
