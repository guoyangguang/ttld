#encoding=utf-8

from tools.client import client 
from tests.framework import BaseTestCase
from tools.factory import add_or_get_user
from view.api.error import FORBIDDEN
import json

class TestFollowshipAPI(BaseTestCase):
    
    def setUp(self):
        self.user1 = add_or_get_user(name='user1', mobile='18600000001', password='123pw4')
        self.user2 = add_or_get_user(name='user2', mobile='18600000002', password='123pw4')
        self.user3 = add_or_get_user(name='user3', mobile='18600000003', password='123pw4')

    def test_follow_unfollow_api_post_201(self):
        data = {'user_id': self.user1.id, 'followed_uid': self.user2.id}
        response = client.post('/followeds/', data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.headers['content-type'], 'application/json')
        self.assertEqual(response.text, u'{}')

        
    def test_follow_unfollow_api_post_post_403(self):
        data = {'user_id': self.user1.id, 'followed_uid': self.user1.id}
        response = client.post('/followeds/', data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.headers['content-type'], 'application/json')
        body = json.loads(response.text)
        self.assertEqual(body['code'], FORBIDDEN[0])
        self.assertEqual(body['status'], FORBIDDEN[2])

    def test_followeds_api_get_200(self):
        data = {'user_id': self.user1.id, 'followed_uid': self.user2.id}
        client.post('/followeds/', data)
        data = {'user_id': self.user1.id, 'followed_uid': self.user3.id}
        client.post('/followeds/', data)
        data = {'user_id': self.user1.id}
        response = client.get('/followeds/', data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['content-type'], 'application/json')
        body = json.loads(response.text)
        self.assertEqual(len(body), 2)

    def test_followings_api_get_200(self):
        data = {'user_id': self.user2.id, 'followed_uid': self.user1.id}
        client.post('/followeds/', data)
        data = {'user_id': self.user3.id, 'followed_uid': self.user1.id}
        client.post('/followeds/', data)
        data = {'user_id': self.user1.id}
        response = client.get('/followings/', data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['content-type'], 'application/json')
        body = json.loads(response.text)
        self.assertEqual(len(body), 2)

    def test_follow_unfollow_api_delete_204(self):
        data = {'user_id': self.user1.id, 'followed_uid': self.user2.id}
        client.post('/followeds/', data)
        data = {'user_id': self.user1.id, 'followed_uid': self.user3.id}
        client.post('/followeds/', data)

        data = {'user_id': self.user1.id}
        response = client.get('/followeds/', data)
        body = json.loads(response.text)
        self.assertEqual(len(body), 2)

        data = {'user_id': self.user1.id}
        response = client.delete('/followeds/{id}/'.format(id=self.user2.id), data=data)
        self.assertEqual(response.status_code, 204)
        self.assertEqual(response.headers['content-type'], 'application/json')
        self.assertEqual(response.text, u'')


        data = {'user_id': self.user1.id}
        response = client.get('/followeds/', data)
        body = json.loads(response.text)
        self.assertEqual(len(body), 1)
