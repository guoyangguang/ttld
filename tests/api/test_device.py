# -*- coding:utf-8 -*-

import pytest

from tools.factory import add_new_device, add_or_get_user
from tools.client import client
from tests.framework import BaseTestCase


@pytest.fixture
def add_basic_data(request):
    self = request.instance
    self.user = add_or_get_user('apitest-user1', '18663959010')


@pytest.mark.usefixtures('add_basic_data')
class TestDeviceCase(BaseTestCase):

    def test_add_new_device(self):
        token = '132829718442ecced7f12a2b4daed72704374dbfaf7e0c1c5a8c6e7d0523d484'
        args = {
            'user_id': self.user.id,
            'device_token': token,
        }
        r = client.post('/device/register/', args)
        assert 'code' in r and r['code'] == 200
        assert 'device' in r

    def test_get_device(self):
        token = '132829718442ecced7f12a2b4daed72704374dbfaf7e0c1c5a8c6e7d0523d484'
        device = add_new_device(self.user.id, token)

        args = {'user_id': self.user.id}
        r = client.get('/device/', args)
        assert 'code' in r and r['code'] == 200
        assert 'device' in r
