# -*- coding: utf-8 -*-

import pytest

from datetime import datetime, timedelta, time

from tools.client import client
from tools.factory import add_new_court, add_new_manager_booked_game
from tests.framework import BaseTestCase


@pytest.fixture
def add_venue_and_manager(request):
    from tools.factory import add_or_get_manager, add_new_venue
    self = request.instance
    manager = add_or_get_manager('apitest-manager1')
    print 'manager', manager
    self.manager = manager

    name1 = 'apitest-东风艺术区谢特体育中心'
    longitude1 = 116.50777816772461
    latitude1 = 39.957951407349846
    self.venue1 = add_new_venue(name1, manager, latitude1, longitude1)

    name2 = 'apitest-将府公园漫咖啡体育中心'
    longitude2 = 116.460325508324
    latitude2 = 40.015392752252
    self.venue2 = add_new_venue(name2, manager, latitude2, longitude2)

    name3 = 'apitest-博雅体育中心'
    longitude3 = 116.282967286359
    latitude3 = 40.114890435766
    self.venue3 = add_new_venue(name3, manager, latitude3, longitude3)


@pytest.mark.usefixtures('add_venue_and_manager')
class TestVenue(BaseTestCase):

    def test_get_nearby_venues(self):
        args = {
            'longitude': 116.50863647460938,
            'latitude': 39.960714556446824,
            'start': 0,
            'limit': 10
        }
        r = client.get('/venue/nearby/', args)
        assert 'code' in r and r['code'] == 200
        assert 'venues' in r
        ids = [venue['id'] for venue in r['venues']]
        assert self.venue1.id in ids
        assert self.venue2.id in ids

    def test_search_venues(self):
        court = add_new_court(1, self.venue1)
        start_time = datetime.now()
        start = datetime.combine(start_time.date(), time())
        start_point = start + self.venue1.start_book_time
        end_point = start + self.venue1.end_book_time
        add_new_manager_booked_game(self.manager, court, start_point, end_point)

        args = {
            'start_time': start_time.strftime('%Y-%m-%d')
        }
        r = client.get('/venue/search/', args)
        assert 'code' in r and r['code'] == 200
        assert 'venues' in r
        ids = [venue['id'] for venue in r['venues']]
        assert self.venue1.id in ids
