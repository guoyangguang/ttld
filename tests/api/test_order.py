# -*- coding: utf-8 -*-

import pytest

from datetime import datetime, timedelta, time

from tools.client import client
from tests.framework import BaseTestCase
from tools.factory import (
    add_or_get_user, add_or_get_manager, add_new_venue, add_new_court,
    add_new_game_order, add_new_require, add_or_get_account
)


@pytest.fixture
def add_basic_data(request):
    self = request.instance
    self.user = add_or_get_user('apitest-user1', '18663959010')
    self.manager = add_or_get_manager('apitest-manager1')
    self.venue = add_new_venue('apitest-venue1', self.manager)
    self.court = add_new_court(1, self.venue)
    self.order = add_new_game_order(self.user, self.court, False)


@pytest.mark.usefixtures('add_basic_data')
class TestGameOrder(BaseTestCase):

    def test_add_new_game_order(self):
        date = datetime.now()
        start_time = datetime(date.year, date.month, date.day, 10) + timedelta(5)
        end_time = start_time + timedelta(hours=1)
        args = {
            'user_id': self.user.id,
            'court_id': self.court.id,
            'start_time': start_time.strftime('%Y-%m-%d %H:%M:%S'),
            'end_time': end_time.strftime('%Y-%m-%d %H:%M:%S')
        }
        r = client.post('/order/book/', args)
        assert 'code' in r and r['code'] == 200
        assert 'order' in r

    def test_add_exist_game_order(self):
        date = datetime.now()
        start_time = datetime(date.year, date.month, date.day, 10) + timedelta(6)
        end_time = start_time + timedelta(hours=1)
        args = {
            'user_id': self.user.id,
            'court_id': self.court.id,
            'start_time': start_time.strftime('%Y-%m-%d %H:%M:%S'),
            'end_time': end_time.strftime('%Y-%m-%d %H:%M:%S')
        }
        r = client.post('/order/book/', args)
        assert 'code' in r and r['code'] == 200
        assert 'order' in r
        assert 'id' in r['order']
        id1 = r['order']['id']

        r = client.post('/order/book/', args)
        print r
        assert 'code' in r and r['code'] == 200
        assert 'order' in r
        assert 'id' in r['order']
        id2 = r['order']['id']
        assert id1 == id2

    def test_add_game_order_and_account_pay(self):
        account = add_or_get_account(self.user.id)
        account.inc_balance(500.00)

        date = datetime.now()
        start_time = datetime(date.year, date.month, date.day, 10) + timedelta(5)
        end_time = start_time + timedelta(hours=1)
        args = {
            'user_id': self.user.id,
            'court_id': self.court.id,
            'start_time': start_time.strftime('%Y-%m-%d %H:%M:%S'),
            'end_time': end_time.strftime('%Y-%m-%d %H:%M:%S')
        }
        r = client.post('/order/account/book/', args)
        assert 'code' in r and r['code'] == 200
        assert 'order' in r
        assert 'result' in r and r['result'] == 'success'

    def test_get_order(self):
        r = client.get('/order/book/%s' % self.order.id, {})
        print r
        assert 'code' in r and r['code'] == 200
        assert 'order' in r and r['order']['id'] == self.order.id

    def test_get_user_pending_orders(self):
        args = {'user_id': self.user.id}
        r = client.get('/order/user/pending/', args)
        assert 'code' in r and r['code'] == 200
        assert 'orders' in r
        assert self.order.id in [o['id'] for o in r['orders']]

    def test_make_game_participatable(self):
        args = {
            'user_id': self.user.id,
            'game_id': self.order.game.id,
            'max_users': 4,
            'tennis_level': 2
        }
        r = client.post('/game/require/', args)
        assert 'code' in r and r['code'] == 200
        assert 'require' in r

    def test_simple_search_require_related_games(self):
        require = add_new_require(self.user, self.order.game)
        args = {
            'tennis_level': 1
        }
        r = client.get('/game/search/', args)
        assert 'code' in r and r['code'] == 200
        assert 'games' in r
        assert self.order.game.id in [g['id'] for g in r['games']]

    def test_search_require_related_games_by_time(self):
        require = add_new_require(self.user, self.order.game)
        args = {
            'start_time': self.order.game.start_time.strftime('%Y-%m-%d'),
        }
        r = client.get('/game/search/', args)
        assert 'code' in r and r['code'] == 200
        assert 'games' in r
        assert self.order.game.id in [g['id'] for g in r['games']]

    def test_add_participate_order(self):
        court = add_new_court(2, self.venue)
        order = add_new_game_order(self.user, court, True)
        require = add_new_require(self.user, order.game)

        user = add_or_get_user('apitest-user-p1', '18663959110')
        args = {
            'user_id': user.id,
            'game_id': order.game.id
        }

        r = client.post('/order/participate/', args)
        assert 'code' in r and r['code'] == 200
        assert 'order' in r
        fields = ['game', 'creator_id', 'pay_type', 'status', 'price']
        for field in fields:
            assert field in r['order']
        assert 'yue' in r['order']['game']

    def test_add_participate_order_and_account_pay(self):
        user = add_or_get_user('apitest-user-p1', '18663959110')
        account = add_or_get_account(user.id)
        account.inc_balance(500.00)

        court = add_new_court(2, self.venue)
        order = add_new_game_order(self.user, court, True)
        require = add_new_require(self.user, order.game)
        args = {
            'user_id': user.id,
            'game_id': order.game.id
        }
        r = client.post('/order/account/participate/', args)
        assert 'code' in r and r['code'] == 200
        assert 'order' in r
        fields = ['game', 'creator_id', 'status', 'price']
        for field in fields:
            assert field in r['order']
        assert 'yue' in r['order']['game']
        assert 'result' in r and r['result'] == 'success'

    def test_verify_order(self):
        args = {
            'user_id': self.user.id,
            'order_id': self.order.id
        }
        r = client.post('/order/verify/', args)
        assert 'code' in r and r['code'] == 200
        assert 'order' in r
        assert 'price' in r
        assert 'account_balance' in r

    def test_pay_order_by_account_balance(self):
        account = add_or_get_account(self.user.id)
        account.inc_balance(200.00)
        current_balance = account.flush_get_balance()

        args = {
            'order_id': self.order.id,
            'user_id': self.order.creator_id
        }
        r = client.post('/order/payment/account/', args)
        assert 'code' in r and r['code'] == 200
        assert 'result' in r and r['result'] == 'success'
        assert account.flush_get_balance() + self.order.price == current_balance

    def test_add_new_vip_order(self):
        args = {
            'user_id': self.user.id,
            'vip_type': 'sliver'
        }
        r = client.post('/order/vip/', args)
        assert 'code' in r and r['code'] == 200
        assert 'order' in r

    def test_add_new_vip_order_and_account_pay(self):
        account = add_or_get_account(self.user.id)
        account.inc_balance(500.00)

        args = {
            'user_id': self.user.id,
            'vip_type': 'sliver'
        }
        r = client.post('/order/account/vip/', args)
        assert 'code' in r and r['code'] == 200
        assert 'order' in r
        assert 'result' in r and r['result'] == 'success'

    def test_add_new_charge_order(self):
        args = {
            'user_id': self.user.id,
            'amount': 500.00
        }
        r = client.post('/order/charge/', args)
        assert 'code' in r and r['code'] == 200
        assert 'order' in r

    def test_get_user_games(self):
        user = add_or_get_user('apitest-user100')
        court = add_new_court(100, self.venue)
        order = add_new_game_order(user, court, True)
        r = client.get('/game/user/%s/book/' % user.id, {})
        assert 'code' in r and r['code'] == 200
        assert 'games' in r
        gids = [g['id'] for g in r['games']]
        assert order.game.id in gids

    def test_game_order_refund(self):
        date = datetime.now()
        start_time = datetime.combine(date.date(), time()) + timedelta(2) + timedelta(hours=10)

        user = add_or_get_user('apitest-user100')
        court = add_new_court(100, self.venue)
        order = add_new_game_order(user, court, True, start_time)

        account = add_or_get_account(user.id)
        origin_balance = account.flush_get_balance()

        args = {
            'game_id': order.game.id,
            'user_id': order.creator_id
        }
        r = client.post('/order/refund/', data=args)

        assert 'code' in r and r['code'] == 200
        assert 'order' in r and r['order']['status'] == 'refunded'
        right_balance = origin_balance + order.price
        assert 'account_balance' in r and r['account_balance'] == right_balance

        can_book = order.game.court.can_book(order.game.start_time, order.game.end_time)
        assert can_book
