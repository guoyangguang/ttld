#encoding=utf-8

from tests.framework import BaseTestCase
from tools.client import client
from tools.factory import (
    add_or_get_user, add_or_get_manager, add_province, add_city, 
    add_new_venue, add_new_court, add_new_game, add_new_require
)
from flask import json
from datetime import datetime

class TestCommentsAPI(BaseTestCase):

    def setUp(self):
        self.user1 = add_or_get_user('user1', '18600000001', '123pw4')
        self.user2 = add_or_get_user('user2', '18600000002', '123pw4')
        self.manager = add_or_get_manager('manager1')
        self.province = add_province('jiangsu')
        self.city = add_city(self.province.id, 'nanjing', 101010100)
        self.venue = add_new_venue('venue1', self.manager, city_id=self.city.id)
        self.court = add_new_court(1, self.venue)
        self.game = add_new_game(
            self.user1, 
            self.court, 
            datetime(2014, 7, 17, 19, 0, 0), 
            datetime(2014, 7, 17, 19, 0, 0)
        ) 
        self.require = add_new_require(self.user1, self.game)

    def test_post_201(self):
        data = dict(user_id=self.user2.id, body='where the venue?') 
        response = client.post('/game/require/{id}/comments/'.format(id=self.require.id), data=data)
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.headers['content-type'], 'application/json')
        body = json.loads(response.text)
        self.assertIsInstance(body['id'], int)
        self.assertEqual(body['require_id'], self.require.id)
        self.assertEqual(body['user_id'], self.user2.id)
        self.assertEqual(body['body'], data.get('body'))

    def test_post_missing_param(self):
        data = dict(user_id=self.user2.id) 
        response = client.post('/game/require/{id}/comments/'.format(id=self.require.id), data=data)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.headers['content-type'], 'application/json')
        body = json.loads(response.text)
        self.assertEqual(body['mesg'], '缺少参数')

    def test_post_require_not_found(self):
        data = dict(user_id=self.user2.id, body='where the venue?') 
        response = client.post('/game/require/{id}/comments/'.format(id=1000000000000000), data=data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.headers['content-type'], 'application/json')
        body = json.loads(response.text)
        print(body)
        self.assertEqual(body['mesg'], '约球不存在')

    def test_post_user_not_found(self):
        data = dict(user_id=10000000000000000, body='where the venue?') 
        response = client.post('/game/require/{id}/comments/'.format(id=self.require.id), data=data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.headers['content-type'], 'application/json')
        body = json.loads(response.text)
        self.assertEqual(body['mesg'], '用户不存在')

    def test_get_200(self):
        data = dict(user_id=self.user2.id, body='where the venue?') 
        response = client.post('/game/require/{id}/comments/'.format(id=self.require.id), data=data)
        body = json.loads(response.text)
        data = dict(user_id=self.user1.id, body='it is in dayun village', comment_id=body['comment_id']) 
        client.post('/game/require/{id}/comments/'.format(id=self.require.id), data=data)

        params = dict(page=1)
        response = client.get('/game/require/{id}/comments/'.format(id=self.require.id), params)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.headers['content-type'], 'application/json')
        body = json.loads(response.text)
        self.assertEqual(len(body), 2)
        for comment in body:
            if comment.get('replied_user_name'): 
                self.assertEqual(comment.get('replied_user_name'), self.user1.name)

    def test_get_require_not_found(self):
        params = dict(page=1)
        response = client.get('/game/require/{id}/comments/'.format(id=1000000000000000000), params)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.headers['content-type'], 'application/json')
        body = json.loads(response.text)
        self.assertEqual(body['mesg'], '约球不存在')
