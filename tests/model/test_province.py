# -*- coding: utf-8 -*-

from tests.framework import BaseTestCase
from model.province import Province 
from tools.factory import add_province
import unittest

class TestProvince(BaseTestCase):

    def setUp(self):
        self.province = add_province(u'江苏')
    
    def test_table(self):
        self.assertEqual(Province.table, 'province')


    def test_get(self):
        province = self.province.get(self.province.id)
        self.assertEqual(province.id, self.province.id)

    def test_get_returns_None(self):
        province = Province.get(10000000000000000)
        self.assertEqual(province, None)

    def test_gets(self):
        provinces = Province.gets([self.province.id])
        self.assertIsInstance(provinces, list)
        self.assertEqual(len(provinces), 1)
        self.assertEqual(self.province.id, provinces[0].id)
     
    #FIXME None is not filtered
    def test_gets_not_founds_removed(self):
        provinces = Province.gets([self.province.id, 100000000000000000])
        #self.assertEqual(len(provinces), 1)
        self.assertEqual(provinces[0].id, self.province.id)

    def test_save(self): 
        province = Province.save(u'江西')
        self.assertIsInstance(province.id, long)
        self.assertEqual(province.province, u'江西')

if __name__ == '__main__':
    unittest.main()
