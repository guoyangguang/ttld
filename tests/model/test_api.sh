#!/bin/bash
#Simple script test local api or sae api


PORT="8080"
HOST="localhost"
API_URL="http://$HOST:$PORT/api/"

HTTP_HEADER="-H Content-Type:application/json"
HTTP_MULTIPATH_HEADER="-H multipart/form-data"

PHONE_NUMBER="18650751805"

APP_KEY="0d3a7b5dc55a14a327af7219500e3de9"
APP_SECRET="b6e72f651f0ca11c"

APPKEY_POST_ARGS="\"app_key\":\"$APP_KEY\", \"app_secret\":\"$APP_SECRET\""
APPKEY_GET_ARGS="?app_key=$APP_KEY&app_secret=$APP_SECRET"
APPKEY_FORM_ARGS="app_key=$APP_KEY;app_secret=$APP_SECRET"
POST_PHONE="\"telephone\":\"$PHONE_NUMBER\""

echo "$CLIENT_INFO"
echo -e "\n================ <Test TenCourt Api By CURL>===================\n"
echo -e "API_URL:$API_URL\n"


if [ $1 == "verify_phone" ]
then
  method="verify/phone/"
  echo $APPKEY_POST_ARGS
  curl -X POST $HTTP_HEADER -v -d "{$POST_PHONE, $APPKEY_POST_ARGS}" $API_URL$method
fi

if [ $1 == "verify_code" ]
then
  method="verify/code/"
  echo $HTTP_HEADER
  echo $2
  curl -X POST $HTTP_HEADER -v -d "{$POST_PHONE, $APPKEY_POST_ARGS, \"verify_code\":$2}" $API_URL$method
fi


if [ $1 == "reg" ]
then
  method="user/"
  echo $HTTP_HEADER
  curl -X POST $HTTP_HEADER -v -d "{$POST_PHONE, $APPKEY_POST_ARGS, \"username\":\"daydayfreetest1\", \"password\":\"12345\"}" $API_URL$method
fi


if [ $1 == "update_user" ]
then
  method="user/1000000/"
  curl -X PUT $HTTP_MULTIPATH_HEADER -v -F "app_key=$APP_KEY" -F "app_secret=$APP_SECRET" -F "telephone=18663953710" -F "name=Xueming" -F "birth=1988-04-01" -F "gender=m" -F "intro=我很牛!" -F "avatar=@pics/user_avatar.jpg" $API_URL$method
fi

if [ $1 == "lookup" ]
then
  method="user/$2"
  echo $method
  curl -X  $HTTP_HEADER -v -d "{$POST_PHONE, $APPKEY_POST_ARGS}" $API_URL$method
fi

## BookOrder

if [ $1 == "get_book_order" ]
then
  method="order/book/$2"
  echo $method
  curl -X GET $HTTP_HEADER -v -d "{$APPKEY_POST_ARGS}" $API_URL$method
fi

user_id=1000006
court_id=1000000
start_time="2014-01-05 12:00:00"
end_time="2014-01-05 14:00:00"
params="\"court_id\":$court_id, \"user_id\":$user_id, \"start_time\": \"$start_time\", \"end_time\":\"$end_time\""

if [ $1 == "add_book_order" ]
then
  method="order/book/"
  echo $method
  curl -X POST $HTTP_HEADER $API_URL$method -v -d "{$params, $APPKEY_POST_ARGS}"
fi

if [ $1 == "verify_order" ]
then
  method="order/verify/"
  echo $API_URL$method
  params="\"order_id\":1, \"order_type\":\"book\", \"user_id\":1000000"
  curl -X POST $HTTP_HEADER $API_URL$method -v -d "{$params, $APPKEY_POST_ARGS}"
fi

user_id=1000000
game_id=1
age=24
gender='f'
tennis_age=2
tennis_level=2
price=100.00
max_users=3
params="\"user_id\": $user_id, \"game_id\": $game_id, \"age\": $age, \"gender\": \"$gender\", \"tennis_age\": $tennis_age, \"tennis_level\": $tennis_level, \"price\": $price, \"max_users\": $max_users"

if [ $1 == "require" ]
then
  method="game/require/"
  echo $method
  curl -X POST $HTTP_HEADER $API_URL$method -v -d "{$params, $APPKEY_POST_ARGS}"
fi

params="&age=22&tennes_level=1"
if [ $1 == "search-require" ]
then
  method="game/search/"
  echo $API_URL$method
  curl -X GET $API_URL$method$APPKEY_GET_ARGS$params -v
fi

latitude=116.50863647460938
longitude=39.960714556446824
params="&latitude=$latitude&longitude=$longitude&start=0&limit=10"

if [ $1 == "nearby" ]
then
  method="venue/nearby/"
  echo $API_URL$method
  curl -X GET $API_URL$method$APPKEY_GET_ARGS$params -v
fi

uid=1000001
params="&uid=$uid&start=0&limit=10"

if [ $1 == "usual" ]
then
  method="venue/usual/"
  echo $API_URL$method
  curl -X GET $API_URL$method$APPKEY_GET_ARGS$params -v
fi

params="&venue_id=$2&date=$3"

if [ $1 == "schedule" ]
then
  method="venue/schedule/"
  echo $API_URL$method
  curl -X GET $API_URL$method$APPKEY_GET_ARGS$params -v
fi

if [ $1 == "search-venue" ]
then
  params="&q=体育"
  method="venue/search/"
  echo $API_URL$method
  curl -X GET $API_URL$method$APPKEY_GET_ARGS$params -v

  latitude=116.50863647460938
  longitude=39.960714556446824
  params="&latitude=$latitude&longitude=$longitude&start=0&limit=10"
  echo $API_URL$method
  curl -X GET $API_URL$method$APPKEY_GET_ARGS$params -v

  params="&latitude=$latitude&longitude=$longitude&start=0&limit=10&q=体育"
  echo $API_URL$method
  curl -X GET $API_URL$method$APPKEY_GET_ARGS$params -v

  echo $API_URL$method
  curl -X GET $API_URL$method$APPKEY_GET_ARGS -v

fi


if [ $1 == "user-game" ]
then
  method="game/user/1000003/participate/"
  echo $API_URL$method
  curl -X GET $API_URL$method$APPKEY_GET_ARGS -v

  method="game/user/1000001/book/"
  echo $API_URL$method
  curl -X GET $API_URL$method$APPKEY_GET_ARGS -v
fi


if [ $1 == "order_pending" ]
then
  method="order/user/1000006/pending/"
  echo $API_URL$method
  curl -X GET $HTTP_HEADER $API_URL$method$APPKEY_GET_ARGS -v
fi


if [ $1 == "request-vip" ]
then
  method="order/vip/"
  echo $API_URL$method
  params="\"user_id\": $2, \"vip_type\": $3"
  curl -X PUT $HTTP_HEADER $API_URL$method -v -d "{$params, $APPKEY_POST_ARGS}"
fi
