# -*- coding: utf-8 -*-

from datetime import datetime, timedelta, time
from tests.framework import BaseTestCase
import unittest
from model.manager import Manager
from model.province import Province
from model.city import City
from model.user import User
from model.venue import Venue
from model.court import Court


class TestCourt(BaseTestCase):

    def setUp(self):
        self.merchant = Manager.add(
            name='merchant', role=2, password='ttld1234', telephone='18600000001',
            company='tennis company', activate=1, parent_id=None, 
            desc_message='i am the merchant of tennis company'
        )
        self.sysadmin= Manager.add(
            name='sysadmin', role=1, password='ttld1234', telephone='18600000002',
            company='ttld company', activate=1, parent_id=None, desc_message='i am a sysadmin')
        self.province = Province.save(u'北京')
        self.city = City.save(pid=self.province.id, city=u'北京', weather_code=101010) 
        self.user = User.add(telephone=None, status=1, name='user', city_id=self.city.id)

        name = 'test-东风艺术区谢特体育中心'
        longitude = 116.50777816772461
        latitude = 39.957951407349846
        open_time = time(11)
        close_time = time(23)
        self.venue = Venue.add(manager_id=self.merchant.id, city_id=self.city.id, name=name,
            address=u'东风路', telephone='18600000001', latitude=latitude, longitude=longitude,
            open_time=open_time, close_time=close_time)
        self.court1 = Court.add(self.venue.id, size_type=1, env_type=1, seq=1, has_light=1)
        self.court2 = Court.add(self.venue.id, size_type=1, env_type=1, seq=2, has_light=0)
     
    def test_name(self):
        self.assertEqual(self.court1.name, u'1号场')
    
    def test_has_light(self):
        self.assertTrue(self.court1.has_light)
        self.assertFalse(self.court2.has_light)
    
    def test_check_can_book(self):
        val = self.court1.check_can_book(
            start_time = datetime(2016, 8, 12, 17), 
            end_time = datetime(2016, 8, 12, 20)
        )
        self.assertEqual(val, '') 

        val = self.court1.check_can_book(
            start_time = datetime(2016, 8, 12, 17), 
            end_time = datetime(2016, 8, 13, 20)
        )
        self.assertEqual(val, '时间间隔不能超过一天') 

        val = self.court1.check_can_book(
            start_time = datetime(2016, 8, 12, 9), 
            end_time = datetime(2016, 8, 12, 20)
        )
        self.assertEqual(val, '请确认开馆和打烊时间') 
    
    def test_schedule(self):
        pass
    
    def test_price(self):
        pass

    def test_delete(self):
        pass

    #def test_can_book_court(self):
    #    now = datetime.now() + timedelta(1)

    #    start_time = datetime.combine(now.date(), time(6))
    #    end_time = datetime.combine(now.date(), time(7))
    #    can_book = self.court.can_book(start_time, end_time)
    #    self.assertFalse(can_book)

    #    start_time = datetime.combine(now.date(), time(9))
    #    end_time = datetime.combine(now.date(), time(10))
    #    can_book = self.court.can_book(start_time, end_time)
    #    self.assertTrue(can_book)
    #    go1 = add_new_game_order(self.user, self.court, False, start_time, 1)

    #    can_book = self.court.can_book(start_time, end_time)
    #    self.assertFalse(can_book)

    #    start_time = datetime.combine(now.date(), time(10))
    #    end_time = datetime.combine(now.date(), time(14))
    #    can_book = self.court.can_book(start_time, end_time)
    #    self.assertTrue(can_book)

    #    go2 = add_new_game_order(self.user, self.court, False, start_time, 4)

    #    test_start_time = datetime.combine(now.date(), time(11))
    #    test_end_time = datetime.combine(now.date(), time(12))
    #    can_book = self.court.can_book(test_start_time, test_end_time)
    #    self.assertFalse(can_book)

    #def test_get_court_price(self):
    #    now = datetime.now()
    #    start_time = datetime.combine(now.date(), time()) + timedelta(hours=10)
    #    end_time = start_time + timedelta(hours=3)
    #    price = self.court.price(start_time, end_time)
    #    assert price == 3.00

if __name__ == '__main__':
    unittest.main()
