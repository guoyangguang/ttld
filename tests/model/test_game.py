# -*- coding: utf-8 -*-

from datetime import datetime, time

from model.province import Province
from model.city import City
from model.manager import Manager
from model.user import User
from model.venue import Venue
from model.court import Court
from model.game import Game
from model.price import Price
from model.order.game import GameOrder
from model.order import Order
from model.require import Require

from tests.framework import BaseTestCase
import unittest


class TestGame(BaseTestCase):

    def setUp(self):
        self.merchant = Manager.add(
            name='merchant', role=2, password='ttld1234', telephone='18600000001',
            company='tennis company', activate=1, parent_id=None,
            desc_message='i am the merchant of tennis company'
        )
        self.sysadmin= Manager.add(
            name='sysadmin', role=1, password='ttld1234', telephone='18600000002',
            company='ttld company', activate=1, parent_id=None,
            desc_message='i am a sysadmin'
        )
        self.province = Province.save(u'北京')
        self.city = City.save(pid=self.province.id, city=u'北京', weather_code=101010) 
        self.user1 = User.add(telephone=18600000003, status=1, name='user', city_id=self.city.id)
        self.user2 = User.add(telephone=18600000004, status=1, name='user', city_id=self.city.id)

        name = 'test-东风艺术区谢特体育中心'
        longitude = 116.50777816772461
        latitude = 39.957951407349846
        open_time = time(11)
        close_time = time(23)
        self.venue = Venue.add(manager_id=self.merchant.id, city_id=self.city.id, name=name,
            address=u'东风路', telephone='18600000001', latitude=latitude, longitude=longitude,
            open_time=open_time, close_time=close_time)
        self.court1 = Court.add(self.venue.id, size_type=1, env_type=1, seq=1, has_light=1)
        self.court2 = Court.add(self.venue.id, size_type=1, env_type=1, seq=2, has_light=0)
        Price.new(
            venue_id=self.venue.id, env_type=1, size_type=1, point=16, price=100.00, kind=1, price_type=2)
        Price.new(
            venue_id=self.venue.id, env_type=2, size_type=2, point=18, price=200.00, kind=1, price_type=1)
        self.game = Game.add(court_id=self.court1.id, creator_id=self.user1.id, book_type=1, 
            start_time=datetime(2016, 11, 10, 19), end_time=datetime(2016, 11, 10, 20))
        self.order = GameOrder.new(creator_id=self.user1.id, manager_id=self.merchant.id, 
            venue_id=self.venue.id, game_id=self.game.id, court_id=self.court1.id,
            amount=100.00, pay_type=1, pay_time=datetime.now(), create_time=datetime.now(),
            status='P', pay_method=2, gateway=2 
        )
        Order.complete(self.order.id)
        self.require = Require.new(
            creator_id=self.user1.id, game_id=self.game.id,
            age=None, gender=None, tennis_age=None, tennis_level=None,
            price='10.00', max_users=4, is_owner_play=1, desc=None
        )
    
    def test_table(self):
        self.assertEqual(Game.table, 'game')

    def test_price(self):
        self.assertEqual(self.game.price, 100.00) 

    def test_get_participate_orders(self):  
        self.assertEqual(len(self.game.get_participate_orders()), 0)

    def test_get_game_order(self):
        self.assertEqual((self.game.get_game_order()).id, self.order.id) 

    def test_participates(self):
        participates = self.game.participates
        self.assertEqual(len(participates), 1)
        self.assertTrue(self.user1 in participates)

    def test_get_participates_without_creator(self):
        self.assertEqual(len(self.game.get_participates_without_creator()), 0)

    def test_order_completed(self):
        self.assertTrue(self.game.order_completed) 

    def test_started(self):
        self.assertFalse(self.game.started)

    def test_finished(self):
        self.assertFalse(self.game.finished)

    def test_require(self):
        self.assertEqual(self.game.require.id, self.require.id)
    
    def test_can_participate(self):
        self.assertTrue(self.game.can_participate(self.user2))
    
    def test_get_court_oneday_games(self):
        pass
        #games = Game.get_court_oneday_games(court_id=self.court1, datestr='20161110')
        #self.assertEqual(len(games), 1)

if __name__ == '__main__':
    unittest.main()
