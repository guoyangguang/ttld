#encoding=utf-8

from tests.framework import BaseTestCase
from model.comment import Comment
from model.require import Require
from tools.client import client
from tools.factory import (
    add_or_get_user, add_or_get_manager, add_province, add_city, 
    add_new_venue, add_new_court, add_new_game, add_new_require
)
from datetime import datetime
import unittest

class TestComment(BaseTestCase):

    def setUp(self):
        self.user1 = add_or_get_user('user1', '18600000001', '123pw4')
        self.user2 = add_or_get_user('user2', '18600000002', '123pw4')
        self.manager = add_or_get_manager('manager1')
        self.province = add_province('zhejiang')
        self.city = add_city(self.province.id, 'hangzhou', 101010100)
        self.venue = add_new_venue('venue1', self.manager, city_id=self.city.id)
        self.court = add_new_court(1, self.venue)
        self.game = add_new_game(
            self.user1, 
            self.court, 
            datetime(2014, 7, 17, 19, 0, 0), 
            datetime(2014, 7, 17, 19, 0, 0)
        ) 
        self.require = Require.new(
            creator_id=self.user1.id, game_id=self.game.id,
            age=None, gender=None, tennis_age=None, tennis_level=None,
            price='10.00', max_users=4, is_owner_play=1, desc=None
        )

    def test_create(self):
        comment = Comment(body='where is game?')
        val = Comment.create(self.user2, self.require, comment)
        self.assertIsInstance(val.id, long)
        self.assertIsInstance(val.create_time, datetime)
        self.assertIsNone(val.update_time)
        self.assertIsNone(val.comment_id) 

    def test_create_as_a_reply(self):
        comment = Comment.create(self.user2, self.require, Comment(body='where is game?'))
        new_comment = Comment(comment_id=comment.id, body='it is in dayun village')
        val = Comment.create(self.user1, self.require, new_comment)
        self.assertIsInstance(val.id, long)
        self.assertIsInstance(val.create_time, datetime)
        self.assertIsNone(val.update_time)
        self.assertEqual(val.comment_id, comment.id) 

    def test_get_not_found(self):
        comment = Comment.get(1000000000000000000000000)
        self.assertIsNone(comment)

    def test_comments_of(self):
        comment1 = Comment(body='where is game?')
        comment1 = Comment.create(self.user2, self.require, comment1)
        comment2 = Comment(comment_id=comment1.id, body='it is located in dayun village')
        comment2 = Comment.create(self.user1, self.require, comment2)
        comments = Comment.comments_of(self.require)
        self.assertEqual(len(comments), 2)
        ids = list()
        for comment in comments:
            ids.append(comment.id)
        self.assertTrue(comment1.id in ids)
        self.assertTrue(comment2.id in ids)

if __name__ == '__main__':
    unittest.main()
