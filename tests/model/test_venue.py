# -*- coding: utf-8 -*-

import unittest
from tests.framework import BaseTestCase
from model.manager import Manager
from model.user import User
from model.province import Province
from model.city import City
from model.venue import Venue
from model.court import Court
from model.price import Price
from tools.factory import add_new_game_order
import datetime
import time


class TestVenue(BaseTestCase):

    def setUp(self):
        self.merchant = Manager.add(
            name='merchant', role=2, password='ttld1234', telephone='18600000001',
            company='tennis company', activate=1, parent_id=None,
            desc_message='i am the merchant of tennis company'
        )
        self.sysadmin= Manager.add(
            name='sysadmin', role=1, password='ttld1234', telephone='18600000002',
            company='ttld company', activate=1, parent_id=None,
            desc_message='i am a sysadmin'
        )
        self.province = Province.save(u'北京')
        self.city = City.save(pid=self.province.id, city=u'北京', weather_code=101010) 
        self.user = User.add(telephone=None, status=1, name='user1', city_id=self.city.id)

        name1 = 'test-东风艺术区谢特体育中心'
        longitude1 = 116.50777816772461
        latitude1 = 39.957951407349846
        open_time = datetime.time(11)
        close_time = datetime.time(23)
        self.venue1 = Venue.add(manager_id=self.merchant.id, city_id=self.city.id, name=name1,
            address=u'东风路', telephone='18600000001', latitude=latitude1, longitude=longitude1,
            open_time=open_time, close_time=close_time)
        self.court1 = Court.add(self.venue1.id, size_type=1, env_type=1, seq=1, has_light=1)
        self.court2 = Court.add(self.venue1.id, size_type=1, env_type=1, seq=2, has_light=1)
        self.venue1_price1 = Price.new(venue_id=self.venue1.id, env_type=2, size_type=1, point=17,
            price=80.00, kind=1, price_type=2)
        #self.venue1_price2 = Price.new(venue_id=self.venue1.id, env_type=1, size_type=2, point=17,
        #    price=90.00, kind=1, price_type=2)

        name2 = 'test-将府公园漫咖啡体育中心'
        longitude2 = 116.460325508324
        latitude2 = 40.015392752252
        self.venue2 = Venue.add(manager_id=self.merchant.id, city_id=self.city.id, name=name2,
            address=u'将府公园', telephone='18600000001', latitude=latitude2, longitude=longitude2,
            open_time=open_time, close_time=close_time)

        name3 = 'test-博雅体育中心'
        longitude3 = 116.282967286359
        latitude3 = 40.114890435766
        self.venue3 = Venue.add(manager_id=self.merchant.id, city_id=self.city.id, name=name3,
            address=u'博雅路', telephone='18600000001', latitude=latitude2, longitude=longitude2,
            open_time=open_time, close_time=close_time)


    
    def test_table(self):
        self.assertEqual(Venue.table, 'venue')
   
    def test___eq__(self):
        venue = Venue.get(self.venue1.id)
        self.assertTrue(self.venue1.__eq__(venue))
        self.assertFalse(self.venue1.__eq__(self.venue2))
    
    def test___ne__(self):
        venue = Venue.get(self.venue1.id)
        self.assertFalse(self.venue1.__ne__(venue))
        self.assertTrue(self.venue1.__ne__(self.venue2))
    
    def test_isfull(self):
        pass

    def test_court_counts(self):
        self.assertEqual(self.venue1.court_counts, 2)
        self.assertEqual(self.venue2.court_counts, 0)
    
    def test_isAvailable(self):
        pass
    
    def test_gets(self):
        self.assertEqual(
            len(Venue.gets([self.venue1.id, self.venue2.id, self.venue3.id, 100000000000])),
            3
        )
    
    def test_gets_all(self):
        self.assertEqual(len(Venue.gets_all(start=0, limit=3)), 3)
        self.assertEqual(len(Venue.gets_all(start=0, limit=2)), 2)
        self.assertEqual(len(Venue.gets_all(start=0, limit=1)), 1)

    def test_search(self):
        venues = Venue.search(q=u'体育中心')
        self.assertEqual(len(venues), 3)
        venues = Venue.search(q=u'东风')
        self.assertEqual(len(venues), 1)
        self.assertEqual(venues[0].id, self.venue1.id)
        venues = Venue.search(q=u'将府')
        self.assertEqual(len(venues), 1)
        self.assertEqual(venues[0].id, self.venue2.id)
        venues = Venue.search(q=u'博雅')
        self.assertEqual(len(venues), 1)
        self.assertEqual(venues[0].id, self.venue3.id)

    
    def test_add(self):
        name = 'test-朝阳艺术区谢特体育中心'
        longitude = 11.50777816772426
        latitude = 39.957951407349856
        open_time = datetime.time(10)
        close_time = datetime.time(23)
        venue = Venue.add(manager_id=self.merchant.id, name=name, address=u'朝阳路', 
            telephone='18600000001', cellphone='', traffic_info='',
            latitude=latitude, longitude=longitude, open_time=open_time, close_time=close_time,
            intro='', city_id=self.city.id
        )
        self.assertIsInstance(venue.id, long)
        self.assertIsInstance(venue.create_time, datetime.datetime)
        self.assertIsNone(venue.update_time, None)
      
    def test_update(self):
        name = 'test-望京艺术区谢特体育中心'
        longitude = 11.50777816772427
        latitude = 39.957951407349857
        open_time = datetime.time(9)
        close_time = datetime.time(22)
        venue = Venue.add(manager_id=self.merchant.id, name=name, address=u'望京路', 
            telephone='18600000001', cellphone='', traffic_info='',
            latitude=latitude, longitude=longitude, open_time=open_time, close_time=close_time,
            intro='', city_id=self.city.id
        )
        self.assertIsInstance(venue.create_time, datetime.datetime)
        self.assertIsNone(venue.update_time, None)
        
        time.sleep(1)
        new_name = u'test-望京艺术区谢特体育场馆'
        venue.update(name=new_name)
        updated_venue = Venue.get(venue.id)
        self.assertEqual(updated_venue.name, new_name) 
        self.assertIsInstance(updated_venue.update_time, datetime.datetime)
        self.assertEqual(updated_venue.create_time, venue.create_time)
        self.assertNotEqual(updated_venue.update_time, venue.create_time)
    
    def test_delete(self):
        pass
    
    def test_get_cities(self):
        pass
    
    def test_has_price(self):
        pass

    def test_recent_court_situation(self):
        pass

    #def test_get_maximum_price(self):
    #    max = self.venue1.get_maximum_price()
    #    self.assertEqual(max, 90)

    #def test_get_minimum_price(self):
    #    min = self.venue1.get_minimum_price()
    #    self.assertEqual(min, 80)

    #def test_get_nearby_venues(self):
    #    longitude = 116.50863647460938
    #    latitude = 39.960714556446824
    #    venues = get_nearby_venues(latitude, longitude, limit=10)
    #    assert venues
    #    ids = [v.id for v in venues]
    #    assert self.venue1.id in ids
    #    assert self.venue2.id in ids

    #def test_get_regular_venues(self):
    #    order = add_new_game_order(self.user, self.court1)
    #    venues = get_regular_venues(self.user.id)
    #    assert venues
    #    assert self.court1.venue in venues

    #def test_get_venue_schedule(self):
    #    order = add_new_game_order(self.user, self.court1, True)
    #    date = order.game.start_time.date()
    #    start_point = order.game.start_time.hour

    #    schedule = self.court1.schedule(date)
    #    assert schedule
    #    assert schedule[start_point] == order.game.id

if __name__ == '__main__':
    unittest.main()
