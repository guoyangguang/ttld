# -*- coding: utf-8 -*-

import unittest
from datetime import datetime, timedelta, time
from tests.framework import BaseTestCase

from model.account import Account
from model.game import Game
from model.require import Require
from model.order.game import GameOrder
from model.order.participate import ParticipateOrder
from model.order.vip import VIPOrder
from model.order.charge import ChargeOrder
from model.order import Order
from model.order.trade_handler import pay_by_account_balance

from model.consts import (
    BOOK_TYPE_USER, STATUS_PENDING, PAYMETHOD_ALIPAY, GATEWAY_SECURITY,
    PAYTYPE_GAME, PAYTYPE_VIP, PAYTYPE_CHARGE, GRADE_VIP_SLIVER,
    PRICE_VIP_SLIVER, STATUS_REFUNDED
)

from tools.factory import (
    add_or_get_manager, add_or_get_account, add_or_get_user,
    add_new_venue, add_new_court, add_new_game_order,
    add_new_vip_order, add_new_require,
    add_new_participate_order
)


class TestGameOrder(BaseTestCase):

    def setUp(self):
        self.manager = add_or_get_manager('unittest-manager1')
        self.user = add_or_get_user('unittest-user1')
        self.venue = add_new_venue('unittest-venue1', self.manager)
        self.court = add_new_court(1, self.venue)

    def test_add_new_user_booked_game_order(self):
        date = datetime.now()
        start_time = datetime(date.year, date.month, date.day, 10) + timedelta(1)
        end_time = start_time + timedelta(hours=1)

        game = Game.add(self.court.id, self.user.id, BOOK_TYPE_USER, start_time, end_time)
        amount = self.court.price(start_time, end_time)

        r = GameOrder.new(self.user.id, amount, PAYTYPE_GAME, datetime.now(),
                          None, STATUS_PENDING, PAYMETHOD_ALIPAY, GATEWAY_SECURITY,
                          game.id, game.court.id, game.venue.id, game.venue.manager_id)

        assert r
        assert r.creator_id == self.user.id
        assert r.game_id == game.id
        assert r.venue_id == self.venue.id
        assert r.court_id == self.court.id
        assert r.price == amount

    def test_pay_game_order_by_account_balance(self):
        order = add_new_game_order(self.user, self.court)
        account = Account.get(order.creator_id)
        account.inc_balance(500.00)
        current_balance = account.flush_get_balance()
        pay_by_account_balance(order.id)
        order = Order.get(order.id)
        assert order.completed
        assert account.flush_get_balance() + order.price == current_balance

    def test_pay_participate_order_by_account_balance(self):
        user = add_or_get_user('unittest-user100', '18663953112')
        account = Account.get(user.id)
        account.inc_balance(500.00)
        order = add_new_game_order(user, self.court, True)

        add_new_require(user, order.game)

        user2 = add_or_get_user('unittest-user500', '18663953110')
        account2 = Account.get(user2.id)
        account2.inc_balance(500.00)

        origin_balance2 = account2.flush_get_balance()
        porder = add_new_participate_order(user2, order.game)
        assert pay_by_account_balance(porder.id)
        porder = Order.get(porder.id)
        assert porder.completed
        current_balance2 = account2.flush_get_balance()
        assert current_balance2 + porder.price == origin_balance2

    def test_verify_game_createor_account_balance(self):
        user = add_or_get_user('unittest-user100', '18663953112')
        account = Account.get(user.id)
        account.inc_balance(500.00)
        order = add_new_game_order(user, self.court, True)
        current_balance = account.flush_get_balance()
        add_new_require(user, order.game)

        user2 = add_or_get_user('unittest-user500', '18663953110')
        account2 = Account.get(user2.id)
        account2.inc_balance(500.00)

        origin_balance2 = account2.flush_get_balance()
        porder = add_new_participate_order(user2, order.game)
        assert pay_by_account_balance(porder.id)
        porder = Order.get(porder.id)
        assert porder.completed
        current_balance2 = account2.flush_get_balance()
        assert current_balance2 + porder.price == origin_balance2

        current_balance3 = account.flush_get_balance()
        assert current_balance3 == current_balance + porder.price


    def test_get_order(self):
        new_order = add_new_game_order(self.user, self.court)
        order = Order.get(new_order.id)
        assert order
        assert order.creator_id == self.user.id

    def test_add_new_vip_order(self):
        amount = PRICE_VIP_SLIVER
        # TODO user grade staus
        effective_time = datetime.now()
        # TODO vip time delta
        expire_time = effective_time + timedelta(365)
        order = VIPOrder.new(self.user.id, amount, PAYTYPE_VIP, datetime.now(),
                             None, STATUS_PENDING, PAYMETHOD_ALIPAY,
                             GATEWAY_SECURITY, GRADE_VIP_SLIVER,
                             effective_time, expire_time)
        assert order
        assert order.pay_type == PAYTYPE_VIP
        assert order.creator_id == self.user.id

    def test_pay_vip_order_by_account_balance(self):
        order = add_new_vip_order(self.user, GRADE_VIP_SLIVER)
        assert order
        account = Account.get(self.user.id)
        account.inc_balance(500.00)
        current_balance = account.flush_get_balance()
        pay_by_account_balance(order.id)
        order = Order.get(order.id)
        updated_account = Account.get(order.creator_id)
        assert order.completed
        assert updated_account.balance + order.price == current_balance
        assert updated_account.grade == GRADE_VIP_SLIVER

    def test_add_new_charge_order(self):
        amount = 500.00
        order = ChargeOrder.new(self.user.id, amount, PAYTYPE_CHARGE, datetime.now(),
                                None, STATUS_PENDING, PAYMETHOD_ALIPAY,
                                GATEWAY_SECURITY)
        assert order
        assert order.creator_id == self.user.id
        assert float(order.price) == amount

    def test_game_order_is_refundable(self):
        date = datetime.now()
        start_time = datetime.combine(date.date(), time()) + timedelta(2) + timedelta(hours=10)
        order = add_new_game_order(self.user, self.court, False, start_time)
        self.assertFalse(order.completed)
        self.assertFalse(order.is_refundable)

        account = Account.get(self.user.id)
        account.inc_balance(500.00)
        pay_by_account_balance(order.id)

        order = Order.get(order.id)
        self.assertTrue(order.completed)
        self.assertTrue(order.is_refundable)

        add_new_require(self.user, order.game)
        order = Order.get(order.id)
        self.assertTrue(order.is_refundable)

        user2 = add_or_get_user('unittest-user500', '18663953110')
        account2 = Account.get(user2.id)
        account2.inc_balance(500.00)

        porder = add_new_participate_order(user2, order.game)
        pay_by_account_balance(porder.id)

        order = Order.get(order.id)
        self.assertFalse(order.is_refundable)

    def test_game_order_refund(self):
        date = datetime.now()
        start_time = datetime.combine(date.date(), time()) + timedelta(2) + timedelta(hours=10)
        order = add_new_game_order(self.user, self.court, False, start_time)

        account = Account.get(self.user.id)
        account.inc_balance(500.00)
        pay_by_account_balance(order.id)

        origin_balance = account.flush_get_balance()
        order = Order.get(order.id)
        order.refund_by_user()

        order = Order.get(order.id)
        assert order.status == STATUS_REFUNDED
        current_balance = account.flush_get_balance()
        assert current_balance == origin_balance + order.price

if __name__ == '__main__':
    unittest.main()
