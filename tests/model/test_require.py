# -*- coding: utf-8 -*-

from datetime import datetime, time
from tests.framework import BaseTestCase

from model.venue import Venue
from model.require import Require
from tools.factory import (
    add_or_get_manager, add_or_get_user, add_new_venue,
    add_new_court, add_new_game_order, add_new_require
)
import unittest


class TestRequire(BaseTestCase):

    def setUp(self):
        self.user = add_or_get_user('unittest-user1')
        self.manager = add_or_get_manager('unittest-manager1')
        self.venue = add_new_venue('unittest-venue1', self.manager)
        self.court = add_new_court(1, self.venue)
        self.order = add_new_game_order(self.user, self.court)

    def test_add_new_require(self):
        age = 24
        gender = 'f'
        tennis_age = 2
        tennis_level = 4
        max_users = 4
        price = Require.compute_price(200.00, max_users)
        assert price == 50.00

        price = Require.compute_price(250.00, max_users)
        assert price == 63.00

        r = Require.new(self.user.id, self.order.game.id, age, gender, tennis_age,
                        tennis_level, price, max_users)
        assert r
        assert r.price == price
        assert r.game_id == self.order.game.id
        assert r.creator_id == self.user.id
        assert r.tennis_level == tennis_level
        assert Require.delete(r.id)

    def test_search_require(self):
        require = add_new_require(self.user, self.order.game)
        assert require
        assert require.game

        requires = Require.search(age=require.age-1, start_time=require.game.start_time)
        assert requires
        assert require.id in [require.id for require in requires]

        requires = Require.search(tennis_level=require.tennis_level-1)
        assert requires
        assert require.id in [require.id for require in requires]

        requires = Require.search(gender=require.gender)
        assert requires
        assert require.id in [require.id for require in requires]

        requires = Require.search(start_time=datetime(2013, 12, 11))
        assert not requires
        assert require.id not in [require.id for require in requires]

if __name__ == '__main__':
    unittest.main()
