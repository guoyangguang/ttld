# -*- coding: utf-8 -*-

from tests.framework import BaseTestCase
from model.city import City
from tools.factory import add_province, add_city
import unittest

class TestCity(BaseTestCase):

    def setUp(self):
        self.province = add_province(u'江苏')
        self.city = add_city(self.province.id, u'南京', 101010100)
    
    def test_table(self):
        self.assertEqual(City.table, 'city')

    def test__repr__(self):
        self.assertEqual(self.city.__repr__(), '<City:city=%s>' % self.city.city)

    def test_property_province(self):
        self.assertEqual(self.city.province, self.province.province)

    def test_get(self):
        city = self.city.get(self.city.id)
        self.assertEqual(city.id, self.city.id)

    def test_get_returns_None(self):
        city = City.get(10000000000000000)
        self.assertEqual(city, None)

    def test_gets(self):
        cities = City.gets([self.city.id])
        self.assertIsInstance(cities, list)
        self.assertEqual(len(cities), 1)
        self.assertEqual(self.city.id, cities[0].id)

    def test_gets_not_founds_removed(self):
        cities = City.gets([self.city.id, 10000000000000000000000000])
        self.assertEqual(len(cities), 1)
        self.assertEqual(cities[0].id, self.city.id)

    def test_save(self): 
        city = City.save(self.province.id, u'苏州', 101010101)
        self.assertIsInstance(city.id, long)
        self.assertEqual(city.pid, self.province.id)
        self.assertEqual(city.city, u'苏州')

if __name__ == '__main__':
    unittest.main()
