# -*- coding: utf-8 -*-

from tests.framework import BaseTestCase
from model.auth import Auth
import unittest


class TestAuth(BaseTestCase):

    def setUp(self):
        self.app_name = 'test_tencourt'
        self.auth = Auth.add(self.app_name)
        assert self.auth

    def test_get_by_app_key(self):
        auth = Auth.get_by_app_key(self.auth.app_key)

        self.assertEqual(auth.id, self.auth.id)
        self.assertEqual(auth.app_key, self.auth.app_key)
        self.assertEqual (auth.app_secret, self.auth.app_secret)

    def test_get_by_app_name(self):
        auth = Auth.get_by_app_name(self.app_name)

        self.assertEqual(auth.id, self.auth.id)
        self.assertEqual(auth.app_key, self.auth.app_key)
        self.assertEqual(auth.app_secret, self.auth.app_secret)

    def tearDown(self):
        Auth.delete(self.auth.id)

if __name__ == '__main__':
    unittest.main()
