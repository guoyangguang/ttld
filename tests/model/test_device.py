# -*- coding: utf-8 -*-

from tests.framework import BaseTestCase
from model.device import Device
from tools.factory import add_or_get_user
from model.consts import IOS, ANDROID
from datetime import datetime
import unittest

class TestDevice(BaseTestCase):

    def setUp(self):
        self.user = add_or_get_user('user1', mobile='18600000001', password='ttld1234')

    def test_table(self):
        self.assertEqual(Device.table, 'device')

    def test_new(self):
        device = Device.register(
            self.user.id, 
            'MTAwMDAwM3RlbmNvdW50MGQzYTdiNWRjNTVhMTRhMzI3YWY3',
            IOS
        )
        self.assertIsInstance(device.id, long)
        self.assertIsInstance(device.create_time, datetime)
        self.assertEqual(
            device.token,
            'MTAwMDAwM3RlbmNvdW50MGQzYTdiNWRjNTVhMTRhMzI3YWY3'
        )

    def test_remove(self):
        device = Device.register(
            self.user.id, 
            'MTAwMDAwM3RlbmNvdW50MGQzYTdiNWRjNTVhMTRhMzI3YWY3',
            IOS
        )
        self.assertEqual((Device.get_by_uid(self.user.id)).id, device.id)
        r = Device.remove(self.user.id) 
        self.assertTrue(r)
        self.assertIsNone(Device.get_by_uid(self.user.id))
    
    def test_get(self):
        device = Device.register(
            self.user.id, 
            'MTAwMDAwM3RlbmNvdW50MGQzYTdiNWRjNTVhMTRhMzI3YWY3',
            IOS
        )
        self.assertEqual((Device.get(device.id)).id, device.id)
    
    def test_get_returns_none(self): 
        device = Device.get(10000000000000000000000)
        self.assertIsNone(device)

    def test_get_by_uid(self):
        device = Device.register(
            self.user.id, 
            'MTAwMDAwM3RlbmNvdW50MGQzYTdiNWRjNTVhMTRhMzI3YWY3',
            IOS
        )
        self.assertEqual((Device.get_by_uid(self.user.id)).id, device.id)

if __name__ == '__main__':
    unittest.main()
