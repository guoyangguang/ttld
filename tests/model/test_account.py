# -*- coding:utf-8 -*-

from datetime import datetime

from model.user import User
from model.account import Account
from model.order.charge import ChargeOrder
from tools.factory import add_or_get_account
from tests.framework import BaseTestCase
import unittest


class TestAccount(BaseTestCase):

    def setUp(self):
        self.user = User.add(
            telephone='18600000001',
            wx_id='o4XUyuA-dKmBjmpmrfH95YuEpKh1', 
            name='user1' 
        )
        self.user.set_password('ttld1234')
    
    def test_table(self):
        self.assertEqual(Account.table, 'account')
    
    def test_grade_name(self):
        account = self.user.account
        self.assertEqual(account.grade_name, 'normal')
        account.update_grade(2)
        account = Account.get(self.user.id) 
        self.assertEqual(account.grade_name, 'sliver')

    def test_add_new_account(self):
        account = Account.new(self.user.id)
        assert account
        assert account.user_id == self.user.id
        assert account.grade == 1 
        assert account.flush_get_balance() == 0.00
    
    def test_get_account(self):
        add_or_get_account(self.user.id)

        account = Account.get(self.user.id)
        assert account
    
    def test_get_returns_none(self):
        r = Account.get(1000000000000000000)
        self.assertIsNone(r)

    def test_delete_account(self):
        account = add_or_get_account(self.user.id)
        assert Account.delete(account.user_id)
        assert Account.get(self.user.id) == None

    def test_inc_account_balance(self):
        account = add_or_get_account(self.user.id)
        assert account.user_id == self.user.id
        
        assert account.balance == 0.00
        account.inc_balance(200.00)
        assert account.flush_get_balance() == 200.00

    def test_reduce_account_balance(self):
        account = add_or_get_account(self.user.id)
        assert account.user_id == self.user.id

        account.inc_balance(200.00)
        current_balance = account.flush_get_balance()
        assert current_balance == 200.00

        account.reduce_balance(50.00)
        assert account.flush_get_balance() == 150.00
    
    def test_flush_get_balance(self):
        account = Account.new(user_id=self.user.id)
        self.assertEqual(account.flush_get_balance(), account.balance)

    def test_charge(self):
        amount = ChargeOrder.promotion_charge(200)
        assert amount == 220
        amount = ChargeOrder.promotion_charge(600)
        assert amount == 672
        amount = ChargeOrder.promotion_charge(1000)
        assert amount == 1150
        amount = ChargeOrder.promotion_charge(2000)
        assert amount == 2300

if __name__ == '__main__':
    unittest.main()
