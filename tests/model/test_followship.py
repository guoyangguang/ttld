#encoding=utf-8

from tests.framework import BaseTestCase
from model.followship import Followship
from model.user import User
from tools.factory import add_or_get_user
import unittest

class TestFollowship(BaseTestCase):

    def setUp(self):
        self.user1 = add_or_get_user(name='user1', mobile='18600000001', password='123pw4')
        self.user2 = add_or_get_user(name='user2', mobile='18600000002', password='123pw4')
        self.user3 = add_or_get_user(name='user3', mobile='18600000003', password='123pw4')

    def test_follow(self):
        self.assertEqual(len(Followship.followeds(self.user1)), 0)
        val = Followship.follow(self.user1, self.user2)
        self.assertIsInstance(val, User)
        self.assertEqual(self.user2, val)
        val = Followship.follow(self.user1, self.user3)
        self.assertEqual(self.user3, val)
        self.assertEqual(len(Followship.followeds(self.user1)), 2)
        self.assertEqual(True, self.user2 in Followship.followeds(self.user1))
        self.assertEqual(True, self.user3 in Followship.followeds(self.user1))
    
    def test_follow_already_follow(self):
        self.assertEqual(len(Followship.followeds(self.user1)), 0)
        Followship.follow(self.user1, self.user2)
        Followship.follow(self.user1, self.user3)
        self.assertEqual(len(Followship.followeds(self.user1)), 2)

        val = Followship.follow(self.user1, self.user2)
        self.assertEqual(val, self.user2)
        self.assertEqual(len(Followship.followeds(self.user1)), 2)


    def test_unfollow(self):
        Followship.follow(self.user1, self.user2)
        Followship.follow(self.user1, self.user3)
        self.assertEqual(len(Followship.followeds(self.user1)), 2)
        val = Followship.unfollow(self.user1, self.user3)
        self.assertEqual(val, self.user3)
        self.assertEqual(len(Followship.followeds(self.user1)), 1)
        self.assertTrue(self.user2 in Followship.followeds(self.user1))
    
    def test_unfollow_not_follow(self):
        Followship.follow(self.user1, self.user2)
        val = Followship.unfollow(self.user1, self.user3)
        self.assertIsNone(val)
        self.assertEqual(len(Followship.followeds(self.user1)), 1)
        self.assertTrue(self.user2 in Followship.followeds(self.user1))

    def test_followeds(self):
        Followship.follow(self.user1, self.user2)
        Followship.follow(self.user1, self.user3)
        followeds = Followship.followeds(self.user1)
        self.assertEqual(len(followeds), 2)
        self.assertTrue(self.user2 in followeds)
        self.assertTrue(self.user3 in followeds)

    def test_followings(self):
        Followship.follow(self.user1, self.user2)
        Followship.follow(self.user3, self.user2)
        followings = Followship.followings(self.user2)
        self.assertEqual(len(followings), 2)
        self.assertTrue(self.user1 in followings)
        self.assertTrue(self.user3 in followings)

if __name__ == '__main__':
    unittest.main()
