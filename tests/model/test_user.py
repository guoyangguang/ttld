# -*- coding: utf-8 -*-

from tests.framework import BaseTestCase
from model.user import User
from model.account import Account
from datetime import datetime
from view.utils import encrypt
import unittest

class TestUser(BaseTestCase):

    def setUp(self):
        self.user1 = User.add(
            telephone='18600000001',
            wx_id='o4XUyuA-dKmBjmpmrfH95YuEpKh1', 
            name='user1' 
        )
        self.user1.set_password('ttld1234')
        self.user1_account = Account.get_or_add(self.user1.id)

        self.user2 = User.add(
            telephone='18600000002',
            wx_id='o4XUyuA-dKmBjmpmrfH95YuEpKh2', 
            name='user2' 
        )
        self.user2.set_password('ttld1234')

    def test_table(self):
        self.assertEqual(User.table, 'user')

    def test_account(self):
        self.assertEqual(self.user1.account.id, self.user1_account.id)

    def test_account_grade(self):
        self.assertEqual(self.user1.account_grade, self.user1_account.grade)

    def test_account_grade_name(self):
        self.assertEqual(self.user1.account_grade_name, self.user1_account.grade_name)

    def test_account_balance(self):
        self.assertEqual(self.user1.account_balance, self.user1_account.balance)

    def test_is_vip_serve(self):
        self.assertEqual(self.user1.is_vip_serve, self.user1_account.is_vip_serve)

    def test_get(self):
       self.assertEqual(User.get(self.user1.id).id, self.user1.id)

    def test_get_by_telephone(self):
        user = User.get_by_telephone(self.user1.telephone)
        self.assertEqual(user.id, self.user1.id)
        self.assertEqual(user.telephone, self.user1.telephone)
     
    def test_get_by_telephone_returns_none(self):
        user = User.get_by_telephone('13900000000')
        self.assertIsNone(user)

    def test_get_by_wx(self):
        self.assertEqual(User.get_by_wx(self.user1.wx_id).id, self.user1.id)

    def test_get_wx_returns_none(self):
        user = User.get_by_wx('foo')
        self.assertIsNone(user)
    
    def test_gets_not_founds_removed(self):
        users = User.gets([self.user1.id, self.user2.id, '10000000000000000', '111111111111111111'])
        self.assertIsInstance(users, list)
        self.assertEqual(len(users), 2)
        user_ids = [user.id for user in users]
        self.assertTrue(self.user1.id in user_ids)
        self.assertTrue(self.user2.id in user_ids)
    
    def test_add(self):
        wx_id = 'o4XUyuA-dKmBjmpmrfH95YuEpKh3'
        user = User.add(
            telephone='18600000003', 
            wx_id=wx_id, 
            name='user3'
        )
        self.assertIsInstance(user.id, long)
        self.assertEqual(user.wx_id,wx_id) 
        self.assertIsInstance(user.reg_time, datetime)

    def test_add_missing_args_returns_none(self):
        user = User.add(
            telephone=None, 
            wx_id='', 
            name='user3'
        )
        self.assertIsNone(user)

    def test_merge(self):
        pass

    def test_update(self):
        telephone = '18610000003' 
        status = 0 
        name = 'user3'
        birth = datetime.now().date() 
        gender = 'f' 
        tennis_age = 3
        tennis_level = 3.0 
        member_type = 'y' 
        city_id = 2
        
        updated_user1 = self.user1.update(
            telephone=telephone, status=status, name=name,
            birth=birth, gender=gender, tennis_age=tennis_age,
            tennis_level=tennis_level, member_type=member_type, city_id=city_id
        )

        self.assertEqual(updated_user1.telephone, telephone)
        self.assertEqual(updated_user1.wx_id, self.user1.wx_id) 
        self.assertEqual(updated_user1.status, status) 
        self.assertEqual(updated_user1.name, name) 
        self.assertEqual(updated_user1.birth, birth) 
        self.assertEqual(updated_user1.gender, gender) 
        self.assertEqual(updated_user1.user_info.tennis_age, tennis_level)
        self.assertEqual(updated_user1.member_type, member_type) 
        self.assertEqual(updated_user1.city_id, city_id) 
    
    def test_delete(self):
        r = self.user2.delete()
        self.assertTrue(r)
        self.assertIsNone(User.get(self.user2.id))
       
#TODO user is expected to has only user_password row
    def test_set_password(self):
        user = User.add(
            telephone='18600000003',
            wx_id='o4XUyuA-dKmBjmpmrfH95YuEpKh3', 
            name='user3' 
        )
        user.set_password('ttld1234')
        password = user.get_password()
        self.assertEqual(password, 'ttld1234')

    def test_get_password(self):
        password = self.user1.get_password()
        self.assertEqual(password, 'ttld1234')

    def test_is_phone_active(self):
        self.assertFalse(User.is_phone_active('13800000000'))
        self.user1.update(status=1)
        self.assertTrue(User.is_phone_active('18600000001'))
        self.assertFalse(User.is_phone_active('18600000002'))
    
    def test_is_active(self):
        self.assertFalse(User.is_active('13800000000', 'foo'))
        self.user1.update(status=1)
        self.assertTrue(User.is_active('18600000001', 'foo'))
        self.assertFalse(User.is_active('18600000002', 'foo'))

    def test_get_user_book_games_amount(self):
        self.assertEqual(self.user1.get_user_book_games_amount(), 0)
    
    def get_user_participate_games_amount(self):
        self.assertEqual(self.user1.get_user_participate_games_amount(), 0)

if __name__ == '__main__':
    unittest.main()
