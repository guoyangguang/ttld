# -*- coding: utf-8 -*-

from datetime import datetime, time

from model.province import Province
from model.city import City
from model.manager import Manager
from model.user import User
from model.venue import Venue
from model.court import Court
from model.game import Game
from model.price import Price

from tests.framework import BaseTestCase
import unittest


class TestPrice(BaseTestCase):

    def setUp(self):
        self.merchant = Manager.add(
            name='merchant', role=2, password='ttld1234', telephone='18600000001',
            company='tennis company', activate=1, parent_id=None,
            desc_message='i am the merchant of tennis company'
        )
        self.sysadmin= Manager.add(
            name='sysadmin', role=1, password='ttld1234', telephone='18600000002',
            company='ttld company', activate=1, parent_id=None, 
            desc_message='i am a sysadmin'
        )
        self.province = Province.save(u'北京')
        self.city = City.save(pid=self.province.id, city=u'北京', weather_code=101010) 
        self.user = User.add(telephone=18600000003, status=1, name='user', city_id=self.city.id)

        name = 'test-东风艺术区谢特体育中心'
        longitude = 116.50777816772461
        latitude = 39.957951407349846
        open_time = time(11)
        close_time = time(23)
        self.venue = Venue.add(manager_id=self.merchant.id, city_id=self.city.id, name=name,
            address=u'东风路', telephone='18600000001', latitude=latitude, longitude=longitude,
            open_time=open_time, close_time=close_time)
        self.court1 = Court.add(self.venue.id, size_type=1, env_type=1, seq=1, has_light=1)
        self.court2 = Court.add(self.venue.id, size_type=1, env_type=1, seq=2, has_light=0)
        Price.new(
            venue_id=self.venue.id, env_type=1, size_type=1, point=16, price=100.00, kind=1, price_type=2)
        Price.new(
            venue_id=self.venue.id, env_type=2, size_type=2, point=18, price=200.00, kind=1, price_type=1)

    def test_new(self):
        pass

    def test_get(self):
       price = Price.get(venue_id=self.venue.id, env_type=1, size_type=1, point=16, kind=1, price_type=2)
       self.assertEqual(price.price, 100)
       price = Price.get(venue_id=self.venue.id, env_type=2, size_type=2, point=18, kind=1, price_type=1)
       self.assertEqual(price.price, 200)


if __name__ == '__main__':
    unittest.main()
