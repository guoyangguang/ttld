# -*- coding: utf-8 -*-
from tests.framework import BaseTestCase
from model.manager import Manager
from model.province import Province
from model.city import City
from model.venue import Venue
from datetime import datetime, time
from view.utils import encrypt
import unittest

class TestManager(BaseTestCase):

    def setUp(self):
       self.manager1 = Manager.add('sysadmin', 1, 'ttld1234', '18600000001', 'ttld',
                              'i am a sysadmin', 1, None)
       self.manager2 = Manager.add('manager2', 2, 'ttld1234', '18600000002', 'company1',
                              'i am the manager of company1', 1, None)
       self.province = Province.save(u'北京')
       self.city = City.save(pid=self.province.id, city=u'北京', weather_code=101010) 

       name1 = 'test-东风艺术区谢特体育中心'
       longitude1 = 116.50777816772461
       latitude1 = 39.957951407349846
       open_time = time(11)
       close_time = time(23)
       self.venue = Venue.add(manager_id=self.manager2.id, city_id=self.city.id, name=name1,
           address=u'东风路', telephone='18600000002', latitude=latitude1, longitude=longitude1,
           open_time=open_time, close_time=close_time)

    def test_table(self):
        self.assertEqual(Manager.table, 'manager')

    def test_is_admin(self):
        self.assertTrue(self.manager1.is_admin())
        self.assertFalse(self.manager2.is_admin())
    
    def test_is_owner(self):
        self.assertTrue(self.manager2.is_owner(self.venue))
        self.assertFalse(self.manager1.is_owner(self.venue))
    
    def test_can_manage(self):
        self.assertTrue(self.manager1.can_manage(self.venue))
        self.assertTrue(self.manager2.can_manage(self.venue))

    def test_add(self):
       manager = Manager.add('manager3', 1, 'ttld1234', '18600000003', 'company1',
                              'i am an admin from company1', 1, None)
       self.assertIsInstance(manager.id, long)
       self.assertEqual(manager.role, 1)
       self.assertIsInstance(manager.create_time, datetime)
    
    def test_add_invalid_role(self):
        manager = Manager.add('manager3', 100, 'ttld1234', '18600000003', 'company1',
                              'i am an admin from company1', 1, None)
        self.assertIsNone(manager)


    def test_get_by_name(self):
        manager = Manager.get_by_name(self.manager1.name)
        self.assertEqual(manager.id, self.manager1.id)

    def test_get_by_telephone(self):
        manager = Manager.get_by_telephone(self.manager1.telephone)
        self.assertEqual(manager.id, self.manager1.id)
    
    def test_gets(self):
        managers = Manager.gets([self.manager1.id, self.manager2.id, 1000000000]) 
        self.assertIsInstance(managers, list)
        self.assertEqual(len(managers), 2)
        manager_ids = [self.manager1.id, self.manager2.id]
        self.assertTrue(self.manager1.id in manager_ids)
        self.assertTrue(self.manager2.id in manager_ids)

    def test_gets_by_role(self):
        self.manager3 = Manager.add('manager3', 1, 'ttld1234', '18600000003', 'company1',
                                    'i am an admin from company1', 1, None)
        managers = Manager.gets_by_role(1)
        self.assertEqual(len(managers), 2)
        self.assertTrue(self.manager3 in managers)
        self.assertTrue(self.manager1 in managers)

    def test_update(self):
        role = 2 
        password = 'tiantianld1234'
        desc_message = 'i am the manager from company1'
        #self.manager1.update(role=role, password=password, desc_message=desc_message)
        #manager1 = Manager.get(self.manager1.id)
        #self.assertEqual(manager1.role, role)
    
    #def test_delete(self):
    #    id = self.manager2.id
    #    self.assertIsInstance(id, long)
    #    r = self.manager2.delete()
    #    self.assertTrue(r)
    #    r = Manager.get(id)
    #    self.assertIsNone(r)

if __name__ == '__main__':
    unittest.main()
