# -*- coding: utf-8 -*-
import unittest
from tests.framework import BaseTestCase
from tools.factory import add_or_get_user,add_new_game_order,add_or_get_manager,add_new_venue,add_new_court,add_new_require
from datetime import datetime, timedelta, time
from model.province import Province
from model.city import City
from model.recommend import Recommend
from model.user import User
from model.order import Order
from model.account import Account

class TestRecommend(BaseTestCase):

    def setUp(self):
        self.manager = add_or_get_manager('unittest-manager1')
        assert self.manager
        self.province = Province.save(u'北京')
        self.city = City.save(pid=self.province.id, city=u'北京', weather_code=101010) 

        name1 = 'test-东风艺术区谢特体育中心'
        longitude1 = 116.50777816772461
        latitude1 = 39.957951407349846
        open_time = time(9)
        close_time = time(23)
        self.venue = add_new_venue(name1, self.manager, latitude1, longitude1,
                                   open_time, close_time, self.city.id)

        self.court = add_new_court(1, self.venue)
        #self.user = add_or_get_user('unittest-user1')

    def test_recommend_match(self):

        creator = add_or_get_user('unittest-user100', '18663953112')

        order = add_new_game_order(creator, self.court, True)
        add_new_require(creator, order.game)

        users = Recommend.users_for_recommend(order.game)
        count1 = len(users)

        order = add_new_game_order(creator, self.court, True)
        add_new_require(creator, order.game)

        users = Recommend.users_for_recommend(order.game)
        count2 = len(users)

        assert count1 > count2

if __name__ == '__main__':
    unittest.main()
