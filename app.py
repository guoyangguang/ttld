# coding: utf-8

import sys
import os
root_path = os.path.dirname(__file__)
#sys.path.insert(0, root_path)

from flask import Flask, request
from flask.ext.login import LoginManager
from flask.ext.mako import MakoTemplates
from raven.contrib.flask import Sentry
from plim import preprocessor

from config import DEBUG
from view.hook import Hook
from view.admin import admin
from view.api import load_api_views
from view.mobile import mobile
from view.home import home
from view.game import gamebp
from model.manager import Manager

reload(sys)
sys.setdefaultencoding('utf-8')

app = Flask(__name__)
app.config['MAKO_DEFAULT_FILTERS'] = ['decode.utf_8', 'h']
app.config['MAKO_PREPROCESSOR'] = preprocessor
app.config['MAKO_TRANSLATE_EXCEPTIONS'] = False
app.config.from_pyfile('config.py', silent=True)

mako = MakoTemplates(app)
app.template_folder = 'templates'

if not DEBUG:
    sentry = Sentry(app)

app.add_url_rule('/hook/', view_func=Hook.as_view('hook'),
                 methods=['GET', 'POST'])

app.register_blueprint(home)
app.register_blueprint(gamebp)
app.register_blueprint(admin, url_prefix='/admin')
app.register_blueprint(mobile, url_prefix='/mobile')
load_api_views(app)

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'admin.login'


@login_manager.user_loader
def load_user(userid):
    return Manager.get(userid) or None


@app.before_request
def before_request():
    if request.headers.get('User-Agent'):
        if request.headers.get('User-Agent').find('Android') != -1:
            request.android = True
        else:
            request.android = False

if __name__=='__main__':
    app.run(host='0.0.0.0', port=8080)
