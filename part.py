from datetime import datetime
from libs.sqlstore import store
from model.require import Require
from model.order.participate import ParticipateOrder
from model.game import Game
from model.user import User
select_all = 'select id, creator_id, game_id, age, gender, tennis_age, tennis_level, price, max_users, is_owner_play, `desc`, create_time, update_time from `require`'
rs = store.fetchall(select_all + '  where creator_id=%s and create_time>="2014-10-1" and create_time<="2014-10-31"', 1000369)
requires = []
for e in rs:
    req = Require(*e)
    requires.append(req)
results = []
for require in requires:
    data = {}
    data['create_time'] = require.create_time.strftime('%Y-%m-%d %H:%M:%S')
    data['per_price'] = float(require.price)
    game_id = require.game_id
    game = Game.get(game_id)
    venue = game.venue
    data['venue_name'] = venue.name
    data['court_id'] = game.court_id
    ps = ParticipateOrder.gets_by_game(game_id, 0, 10000)
    index = 0
    for p in ps:
        if p and p.status and p.status == 'C':
            index += 1
    data['count'] = index
    results.append(data)
print results
