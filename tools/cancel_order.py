# coding: utf-8

import sys

import os

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import MySQLdb
from datetime import datetime
from optparse import OptionParser
from model.game import Game
from model.consts import PAYTYPE_GAME, PAYTYPE_PARTICIPATE, STATUS_REFUNDED
from tools.postman.sms import send_cancel_order_sms_to_consumer
from config import MYSQL_CONFIG


def cancel_game(game_id):
    game = Game.get(game_id)
    if not game:
        raise Exception('not found game')

    conn = MySQLdb.connect(
        host=MYSQL_CONFIG['host'],
        user=MYSQL_CONFIG['user'],
        passwd=MYSQL_CONFIG['password'],
        db=MYSQL_CONFIG['db'],
        port=int(MYSQL_CONFIG['port']),
        charset='utf8'
    )
    cur = conn.cursor()

    game_order = game.get_game_order()

    if game.require:
        participate_orders = game.get_participate_orders()
        participate_orders = [participate_order.pay_type == PAYTYPE_PARTICIPATE and participate_order.completed \
                              for participate_order in participate_orders]
    else:
        participate_orders = []
    now = datetime.now()

    try:
        if game_order.pay_type == PAYTYPE_GAME and game_order.completed:
            sql = 'update trade set status="%s", refund_time="%s" where id=%s' % (STATUS_REFUNDED, now, game_order.id)
            cur.execute(sql)
            sql = 'update account set balance=balance+%s where user_id=%s' % (game_order.price, game_order.creator_id)
            cur.execute(sql)

        for participate_order in participate_orders:
            sql = 'update trade set status="%s", refund_time="%s" where id=%s' % (STATUS_REFUNDED, now, participate_order.id)
            cur.execute(sql)
            sql = 'update account set balance=balance+%s where user_id=%s' % (participate_order.price, participate_order.creator_id)
            cur.execute(sql)
            sql = 'update account set balance=balance-%s where user_id=%s' % (participate_order.price, game_order.creator_id)
            cur.execute(sql)

        sql = 'delete from `require` where game_id=%s' % game_id
        cur.execute(sql)
        conn.commit()
        send_cancel_order_sms_to_consumer(game_order.creator)
        for participate_order in participate_orders:
            send_cancel_order_sms_to_consumer(participate_order.creator)
    except Exception as e:
        print('Exceptions: Error[%s]' % e)
        conn.rollback()
    finally:
        cur.close()
        conn.close()


if __name__ == '__main__':
    parser = OptionParser()
    parser.add_option('-g', '--game_id', dest='game_id')
    options, args = parser.parse_args()

    if not options.game_id:
        raise Exception('usage: python cancel_game.py -v game_id')

    game_id = int(options.game_id)
    cancel_game(game_id)