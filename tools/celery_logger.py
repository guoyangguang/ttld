#-*- coding: utf-8 -*-

import logging
from libs.log import config_logger
from config import TENCOURT_LOG_FILE
from config import REFUND_LOG_FILE

from celery_tasks import celery_app

tencourt_logger = config_logger(logging.getLogger('tencourt'), TENCOURT_LOG_FILE)
refund_logger = config_logger(logging.getLogger('refund'), REFUND_LOG_FILE)

@celery_app.task
def tencourt_logger_log(level, msg):
    tencourt_logger.log(level, msg)

@celery_app.task
def refund_logger_log(level, msg):
    refund_logger.log(level, msg)
