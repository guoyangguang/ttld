# -*- coding: utf-8 -*-

from datetime import datetime
from tools.holiday.year2014 import HOLIDAY, WORKDAY

# def verify_holiday(dt):
#     date = dt.strftime('%Y-%m-%d')
#     if date in HOLIDAY:
#         return True
#     if dt.weekday() in (5, 6) and date not in WORKDAY:
#         return True
#     return False


def verify_holiday(dt):
    date = dt.strftime('%Y-%m-%d')
    return date in HOLIDAY

    # if dt.weekday() in (5, 6) and date not in WORKDAY:
    #     return True


def verify_weekend(dt):
    date = dt.strftime('%Y-%m-%d')

    return date not in HOLIDAY and dt.weekday() in (5, 6) and date not in WORKDAY