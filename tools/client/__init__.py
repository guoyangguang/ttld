# -*- coding:utf-8 -*-

import json
import requests

from config import TENCOURT
from tools.factory import add_api_key


class TencourtClient(object):

    def __init__(self, app_key, app_secret):
        self.app_key = app_key
        self.app_secret = app_secret

    @property
    def default_params(self):
        return {
            'app_key': self.app_key,
            'app_secret': self.app_secret
        }

    def get(self, method, params, **kwargs):
        params.update(self.default_params)
        url = '%s/api%s' % (TENCOURT, method)
        r = requests.get(url, params=params)
        return r# and r.json()

    def put(self, method, data, **kwargs):
        data.update(self.default_params)
        url = '%s/api%s' % (TENCOURT, method)
        r = requests.put(url, data=data, **kwargs)
        return r and r.json()

    def post(self, method, data, **kwargs):
        data.update(self.default_params)
        url = '%s/api%s' % (TENCOURT, method)
        headers = {'content-type': 'application/json'}
        r = requests.post(url, headers=headers, data=json.dumps(data), **kwargs)
        return r# and r.json()

    def delete(self, method, data, **kwargs):
        data.update(self.default_params)
        url = '%s/api%s' % (TENCOURT, method)
        headers = {'content-type': 'application/json'}
        r = requests.delete(url, headers=headers, data=json.dumps(data), **kwargs)
        return r


auth = add_api_key()
client = TencourtClient(auth.app_key, auth.secret)
