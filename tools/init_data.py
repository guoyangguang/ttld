#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))


from tools.factory import (
    add_or_get_user, add_or_get_manager, add_new_venue, add_new_court,
    add_new_game_order, add_new_require, add_api_key, add_or_get_admin,
    add_new_participate_order, add_new_album, add_photo_into_album,
    add_or_get_account, add_province, add_city, add_new_device
)


if __name__ == '__main__':
    p1 = add_province(u'北京')
    print 'add province %s into table province id=%s' % (u'北京', p1.id)
    c1 = add_city(p1.id, u'北京', 101010100)
    print 'add city %s into city id=%s' % (u'北京', c1.id)
    p2 = add_province(u'上海')
    print 'add province %s into table province id=%s' % (u'上海', p2.id)
    c2 = add_city(p2.id, u'上海', 101020100)
    print 'add city %s into city id=%s' % (u'上海', c2.id)

    admin = add_or_get_admin('admin1')
    print 'add admin id=%s' % admin.id

    manager = add_or_get_manager('manager1')
    print 'add manager id=%s' % manager.id

    user1 = add_or_get_user('user1', '18663953776')
    account1 = add_or_get_account(user1.id)
    account1.inc_balance(50000.00)

    user2 = add_or_get_user('user2', '18663953777')
    account2 = add_or_get_account(user2.id)
    account2.inc_balance(30.00)

    user3 = add_or_get_user('user3', '18663953778')
    account3 = add_or_get_account(user3.id)
    account3.inc_balance(100000.00)

    for user in [user1, user2, user3]:
        print 'add user id=%s' % user.id
    print 'add balance %s in user id=%s' % (50000.00, user1.id)
    print 'add balance %s in user id=%s' % (30.00, user2.id)

    venue1 = add_new_venue('东风艺术区谢特体育中心', manager)
    venue2 = add_new_venue('将府公园漫咖啡体育中心', manager)
    venue3 = add_new_venue(u'测试上海', manager, city_id=2)
    for venue in [venue1, venue2, venue3]:
        print 'add venue id=%s' % venue.id

    album = add_new_album('unittest-album1', venue1)
    add_photo_into_album(album)
    print 'add album and photos in venue id=%s' % venue1.id

    court1 = add_new_court(1, venue1)
    court2 = add_new_court(2, venue1)
    court3 = add_new_court(3, venue1)
    court4 = add_new_court(1, venue2)
    court5 = add_new_court(2, venue2)
    court6 = add_new_court(3, venue2)
    for court in [court1, court2, court3, court4, court5, court6]:
        print 'add court id=%s' % court.id

    # add completed order
    order1 = add_new_game_order(user1, court1)
    print 'add completed game order id=%s' % order1.id

    # add uncompleted order
    order2 = add_new_game_order(user2, court2)
    print 'add an uncompleted game order id=%s' % order2.id

    require1 = add_new_require(user1, order1.game)
    print 'make game id=%s participatable' % order1.game.id

    porder1 = add_new_participate_order(user2, order1.game)
    porder2 = add_new_participate_order(user3, order1.game)
    for order in [porder1, porder2]:
        print 'add user id=%s participte game id=%s' % (order.creator_id, order.game_id)

    auth = add_api_key()
    print 'add api key=%s' % (auth.app_key)

    #add device
    add_new_device(user1.id, '132829718442ecced7f12a2b4daed72704374dbfaf7e0c1c5a8c6e7d0523d484')
    print 'Add device for user:%s' % user1.id
