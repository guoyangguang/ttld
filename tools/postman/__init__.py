# -*- coding:utf-8 -*-
from model.recommend import Recommend
from model.PushNotification import push_client
from model.admin_notify import AdminNotify
from model.notify import Notify
from celery_tasks import celery_app
from tools.postman import sms
from model.consts import USER_CANCEL_REASON,CONFIRM_FAILED_REASON,NEGOTIATE_REASON

@celery_app.task
def notify_confirmed_game(order):
    alert = '恭喜,有一个新用户订场'
    AdminNotify.add_new_book_notify(order.game_id, alert)
    Notify.add_user_order_confirm_notify(order.creator_id, order.game_id, order.id)
    # 给用户和商家发送短信
    sms.send_confirm_success_sms(order)
    push_client.notify_admin_new_book(order, alert)
    push_client.notify_game_order_confirmed(order)
    return

@celery_app.task
def notify_received_gameorder_payment(order):

    sms.send_book_payment_received_mail(order)
    sms.send_book_payment_received_sms(order)
    return

@celery_app.task
def notify_cancel_game(order, reason):
    if reason == USER_CANCEL_REASON:
        sms.send_cancel_book_court_sms(order)
        sms.send_success_cancel_sms(order)
        AdminNotify.add_cancel_book_notify(order.game_id)
        push_client.notify_admin_cancel_book(order)
    elif reason == CONFIRM_FAILED_REASON:
        sms.send_comfirm_failed_sms(order)
        Notify.add_user_order_confirm_fail_notify(user_id=order.creator_id,game_id=order.game_id,order_id=order.id)
        push_client.notify_game_order_confirm_failed(order)
    elif reason == NEGOTIATE_REASON:
        sms.send_success_cancel_sms(order)
    sms.send_operation_refund_sms(order)
    return

@celery_app.task
def notify_charge_success(order, promotion_amount):
    sms.send_success_charge_sms(order,promotion_amount)
    return

@celery_app.task
def notify_join_game(order):
    game = order.game 
    num=len(game.participates)
    alert = '有' + str(num) + '人参加了你的活动,' + (game.require.desc)[0:15]
    Notify.add_join_require_notify(game.creator_id, game.id, order.id, order.creator_id, alert)
    sms.send_join_game_sms(order.game,order.creator)
    push_client.notify_new_participator(order,alert)
    return

@celery_app.task
def notify_new_comment(comment, require):
    if comment.comment_id:
        push_client.notify_comment_owner(comment, require)
        Notify.add_reply_of_comment(require, comment)
    else:
        push_client.notify_require_owner(comment, require)
        Notify.add_comment(require)

@celery_app.task
def notify_recommend_game(game):
    devices = Recommend.devices_for_recommend(game)
    for device in devices:
        result = push_client.notify_recommend_game(game=game, device=device)
        if result:
            Recommend.add(device.uid, game.id)
