# -*- coding:utf-8 -*-
from libs.sms import send_sms
from libs.emailer import send_mail
from config import TENCOURT_NAME,DEBUG
from celery_tasks import celery_app

SERVICE_NUMBER = '010-82310735'
OPERATION_PHONES = [] if DEBUG else ['18503089027', '18710206131', '18910380878', '13718843651']

@celery_app.task
def send_verify_code_sms(phone, code):
    msg = '您的验证码是：%s。请不要把验证码泄露给其他人。如非本人操作，可不用理会！' % code
    send_sms(phone, msg)

def send_book_payment_received_sms(game_order):
    '''
    支付成功后给运营发短信
    '''
    game = game_order.game
    user_telephone = game.creator.telephone.encode('utf-8')
    manager_telephone = game.venue.manager.telephone.encode('utf-8')

    operation_content = '''手机号码为{user_phone}的用户成功预定了{desc},场馆电话为{venue_phone},请运营人员迅速响应确认订单。
'''.format(user_phone=user_telephone, desc=game.desc, venue_phone=manager_telephone)

    for phone in OPERATION_PHONES:
        send_sms(phone, operation_content)

def send_book_success_sms(game_order):
    '''
    原有订场逻辑的确认短信发送
    '''
    game = game_order.game
    user_telephone = game.creator.telephone.encode('utf-8')
    manager_telephone = game.venue.manager.telephone.encode('utf-8')

    user_content = '''恭喜您(尾号{last_num})成功预定了{desc},到场后向管理员出示此短信即可.如有疑问请联系客服:{tt_num},场馆电话:{venue_num},感谢您的支持！
'''.format(last_num=user_telephone[-4:],desc=game.desc,
           tt_num=SERVICE_NUMBER,venue_num=manager_telephone)

    manager_content = '''新的订单：亲爱的商家，尾号{last_num}的用户刚刚预定了{desc}，并已在线付款。如需帮助，请联系我们{tt_num}。合作愉快^_^
'''.format(last_num=user_telephone[-4:],desc=game.desc,
           tt_num=SERVICE_NUMBER)

    send_sms(user_telephone, user_content)
    send_sms(manager_telephone, manager_content)


def send_confirm_success_sms(game_order):
    game = game_order.game
    user_telephone = game.creator.telephone.encode('utf-8')
    manager_telephone = game.venue.manager.telephone.encode('utf-8')

    user_content = '''场地确认成功，到场后向商家出示手机号和此短信即可。{desc}。客服电话:{tt_num}'''.format(desc=game.desc,tt_num=SERVICE_NUMBER)
    manager_content = '''新的订单：亲爱的商家，尾号{last_num}的用户刚刚预定了{desc}，并已在线付款。如需帮助，请联系我们{tt_num}。合作愉快^_^
'''.format(last_num=user_telephone[-4:],desc=game.desc,
           tt_num=SERVICE_NUMBER)

    send_sms(user_telephone, user_content)
    send_sms(manager_telephone, manager_content)

def send_comfirm_failed_sms(game_order):
    game = game_order.game
    user_telephone = game.creator.telephone.encode('utf-8')
    content = '''场地确认失败。抱歉，您预订的{desc}已经爆满，建议您查看附近其他球场进行预定，订场费用已退回银行卡(或账户余额)，具体到账日期以银行政策为准。客服电话:{tt_num}'''\
        .format(desc=game.desc,tt_num=SERVICE_NUMBER)
    send_sms(user_telephone, content)

def send_success_cancel_sms(game_order):
    game = game_order.game
    user_telephone = game.creator.telephone.encode('utf-8')
    content = '''已成功取消订场。您预订的{desc}已经成功取消，订场费用已退回银行卡(或账户余额)，具体到账日期以银行政策为准。客服电话:{tt_num}'''\
        .format(desc=game.desc,tt_num=SERVICE_NUMBER)
    send_sms(user_telephone, content)

def send_success_charge_sms(charge_order,given_amount):
    creator = charge_order.creator
    user_telephone = creator.telephone.encode('utf-8')
    if(given_amount > 0):
        user_content = '''亲爱的用户，您已经成功充值{amount}元。为了感谢您的支持，在此基础上我们赠送给您{given}元，已经一并打入您的{app_name}账户。'''\
            .format(amount=charge_order.amount,given=given_amount,app_name=TENCOURT_NAME)
    else:
        user_content = '''亲爱的用户，您已经成功充值{amount}元，现在可以在{app_name}客户端上查询到。感谢您的支持。'''\
            .format(amount=charge_order.amount,app_name=TENCOURT_NAME)
    send_sms(user_telephone,user_content)

def send_join_game_sms(game,player):
    creator = game.creator

    player_content = '''您刚刚报名参加了{desc}的约球，发起者：({creator_name} 手机号{creator_num})。如有疑问请联系客服:{tt_num}，感谢您的支持
    '''.format(desc=game.desc,creator_name=creator.name,creator_num=creator.telephone,
               tt_num=SERVICE_NUMBER)

    send_sms(player.telephone, player_content)

    creator_content = '''(用户 {player_name} 手机号{player_num})刚刚报名参加了您发起的{desc}的约球，相应费用已打入您的账户余额。如有疑问请联系客服:{tt_num} -感谢您的支持^_^
    '''.format(desc=game.desc,player_name=player.name,player_num=player.telephone,
           tt_num=SERVICE_NUMBER)
    send_sms(creator.telephone, creator_content)

def send_book_payment_received_mail(game_order):
    emails = ['tiantianwangqiu@163.com', 'wangyaoyi@tiantianld.com', 'xiangbin@tiantianld.com',
        'zhangyuanshan@tiantianld.com']
    tpl = '''
订场提醒:
用户：%s
场地：%s%s
时间：%s~%s
消费: %s 元
'''

    game = game_order.game
    venue = game.venue
    court = game.court
    venue_name = venue.name.encode('utf-8')
    court_name = court.name.encode('utf-8')
    user_name = game.creator.name.encode('utf-8')

    content = tpl % (user_name, venue_name, court_name,
                     game.start_time.strftime('%Y-%m-%d %H:%M:%S'),
                     game.end_time.strftime('%Y-%m-%d %H:%M:%S'),
                     game_order.price)

    send_mail(emails, '{app_name}订场提醒'.format(app_name=TENCOURT_NAME), content)



def send_cancel_order_sms_to_consumer(user):
    content = '''亲爱的用户，您提交的退单服务已完成，请您登陆{app_name}APP，查看账户余额，及时确认收到退款。如有任何疑问请速致电{tt_num}， 感谢您对{app_name}的支持，期待下次为您服务。'''\
        .format(tt_num=SERVICE_NUMBER,app_name=TENCOURT_NAME)
    send_sms(user.telephone, content)

def send_cancel_book_court_sms(game_order):
    '''
    原有的取消订场短信通知
    '''
    game = game_order.game
    manager_telephone = game.venue.manager.telephone.encode('utf-8')

    msg_content = '''（退场提醒）亲爱的商家，用户刚刚退订了{desc}，请您注意。如需帮助，请联络我们{tt_num}。-合作愉快^_^
'''.format(desc=game.desc,tt_num=SERVICE_NUMBER)
    send_sms(manager_telephone, msg_content)

def send_operation_refund_sms(game_order):
    game = game_order.game
    venue_name = game.venue.name
    court_name = game.court.name
    manager_tel = game.venue.manager.telephone
    start = game.start_time.strftime('%Y-%m-%d %H:%M'),
    end = game.end_time.strftime('%H:%M')
    user_name = game.creator.name
    user_tel = game.creator.telephone
    amount = game_order.amount
    operation_content = '''（退场提醒）有用户退场。{venue_name}{court_name}{manager_tel},{start}~{end}，用户：{user_name}（{user_tel}），价格：{amount}元。''' \
        .format(venue_name=venue_name, court_name=court_name, manager_tel=manager_tel, start=start, end=end,
                user_name=user_name, user_tel=user_tel, amount=amount)
    for phone in OPERATION_PHONES:
        send_sms(phone, operation_content)
