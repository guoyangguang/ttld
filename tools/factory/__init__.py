# -*- coding:utf-8 -*-

import os
from datetime import datetime, timedelta, time

from libs.sqlstore import store
from model.auth import Auth
from model.consts import (
    ROLE_SYSADMIN, ROLE_MERCHANT, USER_STATUS_ACTIVE, BOOK_TYPE_USER,
    STATUS_PENDING, PAYMETHOD_ALIPAY, GATEWAY_SECURITY,
    PAYTYPE_GAME, PAYTYPE_PARTICIPATE, PAYTYPE_VIP, PAYTYPE_CHARGE,
    VIP_PRICES, BOOK_TYPE_MANAGER, IOS
)
from model.user import User
from model.account import Account
from model.game import Game
from model.manager import Manager
from model.venue import Venue
from model.court import Court
from model.require import Require
from model.order import Order
from model.order.game import GameOrder
from model.order.vip import VIPOrder
from model.order.charge import ChargeOrder
from model.order.participate import ParticipateOrder
from model.album import Album
from model.photo import Photo
from model.province import Province
from model.city import City
from model.device import Device
from view.utils import encrypt
from tools.gen_price import gen_venue_price


def add_api_key():
    app_name = 'tencourt'
    app_key = 953209913815471267
    secret = 13179554945882169628
    store.transac('insert into app_auth (app_key, secret, app_name, create_time) '
                  'values (%s, %s, %s, %s) on duplicate key '
                  'update app_key=%s, secret=%s',
                  (app_key, secret, app_name, datetime.now(), app_key, secret))
    return Auth.get_by_app_name(app_name)

def add_or_get_admin(name):
    manager = Manager.get_by_name(name)
    if manager:
        return manager
    password = encrypt('12345')
    return Manager.add(name=name, role=ROLE_SYSADMIN,
                       password='12345', telephone='18663953776',
                       company='tencourt', desc_message='dev',activate=1,parent_id=None)


def add_or_get_manager(name):
    manager = Manager.get_by_name(name)
    if manager:
        return manager
    password = encrypt('12345')
    return Manager.add(name=name, role=ROLE_MERCHANT,
                       password='12345', telephone='18663953778',
                       company='tencourt', desc_message='dev', activate=1, parent_id=None)


def add_or_get_user(name, mobile=None, password=None):
    from view.utils import encrypt
    mobile = mobile or '18663953779'
    user = User.get_by_telephone(mobile)
    if user:
        return user
    user = User.add(telephone=mobile, status=USER_STATUS_ACTIVE, name=name, gender='f',
                    birth=datetime(1988, 4, 1),
                    tennis_level=4, tennis_age=6)
    user.set_password(encrypt(password or '5uvOXkA8zZLX56kbkXzA2E8es28='))
    add_or_get_account(user.id)
    return user


def add_or_get_account(user_id):
    return Account.get_or_add(user_id)


def add_new_venue(name, manager, latitude=None, longitude=None,
                  open_time=None, close_time=None, city_id=1):
    manager_id = manager.id
    address = '北京朝阳区驼房营18号'
    telephone = '18650751805'
    traffic_info = '11to12'
    latitude = latitude or 39.957951407349846
    longitude = longitude or 116.50777816772461
    open_time = open_time if open_time else time(6)
    close_time = close_time if close_time else time(23)
    intro = '谢特'

    avatar_dir = os.path.join(os.path.dirname(__file__), '../../tests/pics/venue_avatar.jpg')
    avatar = open(avatar_dir, 'r').read()

    venue = Venue.add(manager_id=manager_id, name=name, address=address, telephone=telephone, traffic_info=traffic_info, latitude=latitude,
                      longitude=longitude, open_time=open_time, close_time=close_time, intro=intro, city_id=city_id)
    Photo.add(avatar, venue.id, venue.kind, manager_id)

    # gen venue price
    gen_venue_price(venue.id)
    return venue


def add_new_court(seq, venue):
    return Court.add(venue.id, 1, 1, seq, 1)


def add_new_manager_booked_game(manager, court, start_time, end_time):
    game = Game.add(court.id, manager.id, BOOK_TYPE_MANAGER, start_time, end_time)
    return game

def add_new_game(user, court, start_time, end_time):
    game = Game.add(court.id, user.id, BOOK_TYPE_MANAGER, start_time, end_time)
    return game

def add_new_game_order(user, court, complete=False, start_time=None, hours=1):
    venue = court.venue
    date = datetime.now().replace(hour=12)
    if not start_time:
        start_time = datetime(date.year, date.month, date.day, 10)
        start_time += timedelta(1)
    end_time = start_time + timedelta(hours=hours)

    game = Game.add(court.id, user.id, BOOK_TYPE_USER, start_time, end_time)
    amount = court.price(start_time, end_time)

    # Add game order
    r = GameOrder.new(user.id, amount, PAYTYPE_GAME, datetime.now(),
                      None, STATUS_PENDING, PAYMETHOD_ALIPAY, GATEWAY_SECURITY,
                      game.id, court.id, venue.id, venue.manager_id)

    assert r
    if complete:
        GameOrder.complete(r.id)
    return r


def add_new_require(user, game, max_users=4, tennis_level=1):
    price = Require.compute_price(game.price, max_users)
    r = Require.new(user.id, game.id, 10, 'f', 1, tennis_level,
                    price, max_users, 0, 'test')
    return r


def add_new_participate_order(user, game):
    court = game.court
    price = game.require.price
    return ParticipateOrder.new(user.id, price, PAYTYPE_PARTICIPATE, datetime.now(),
                                None, STATUS_PENDING, PAYMETHOD_ALIPAY,
                                GATEWAY_SECURITY,
                                game.id, court.id, court.venue.id,
                                court.venue.manager_id)


def add_new_vip_order(user, grade):
    amount = VIP_PRICES[grade]
    # TODO user grade staus
    effective_time = datetime.now()
    # TODO vip time delta
    expire_time = effective_time + timedelta(365)
    return VIPOrder.new(user.id, amount, PAYTYPE_VIP, datetime.now(),
                        None, STATUS_PENDING, PAYMETHOD_ALIPAY,
                        GATEWAY_SECURITY, grade,
                        effective_time, expire_time)


def add_new_album(title, venue):
    return Album.add(title, 'A long desc', Album.kind, venue.manager_id, venue.id)


def add_photo_into_album(album):
    path = os.path.join(os.path.dirname(__file__), '../../tests/pics/')
    files = ['venue_photo1.png', 'venue_photo2.png']

    for f in files:
        with open(os.path.join(path, f), 'rb') as i:
            Photo.add(i.read(), album.id, album.kind, album.manager_id)


def add_province(province):
    return Province.save(province)


def add_city(pid, city, weather_code):
    return City.save(pid, city, weather_code)


def add_new_device(user_id, token):
    return Device.register(user_id, token, IOS)
