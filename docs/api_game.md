### 数据样例
#### 比赛: Game

```
{
    "id": 100015,
    "type": 1(用户订场),2(商家自己登记的),3(用户无订场直接发起)
    "creator": (用户数据结构),
    "start_time": "2013-11-28 16:00:00",
    "end_time": "2013-11-28 17:00:00",
    "sport_type": 2,
    "url": "http://www.tencourt.com/game/100015/",
    # 已订场的game的特有数据
    "court": (场地数据结构),
    "venue": (场馆数据结构),    
    "price": 160.00,    
    # 无订场game的特有数据
    "address":场地地址,
    # 活动数据
    "yue": {
    	"per_price": 20,
    	"pay_type": 1是线上，2是线下, 3是免费
        "max_user_count": 3,
        "participates": [用户数据结构列表],
        "is_owner_play": 0/1, (0代表不玩，1代表玩),
        "create_time":"2013-11-20 16:20:00",
        "desc": desc
    }
}
```

#### 过滤比赛
`GET /api/game/search/`

参数:

```
{
    "sports_type" : 1,
    "tennis_level": 3, # 最低要求，可空
    "tennis_age": 2,   # 最低要求，可空
    "age": 24,         # 最低要求，可空
    "gender": "F",     # 性别，可空
    "start_time": "2013-11-12", # 日期，不可空
    "hour": 13         # 时间，可空
    "stage":1,		    # 比赛时段，可空
    "city_id": 1       # 比赛所在城市，默认为1代表北京
    "start": 0
    "count": 10
}
```

返回:

```
{
    "code": 200,
    "games": [比赛数据结构],
    "city": 所在城市，
    "start": start,
    "count": count,
}
```

#### 获取用户订场
`GET /api/game/user/{user_id}/book/`

参数:

```
{
    "start": 0,
    "count": 10
}
```

返回:

```
{
    "code": 200,
    "games": [比赛数据列表],
    "start" start,
    "count": count,
}
```

#### 获取用户参加的约球
`GET /api/game/user/{user_id}/participate/`

参数:

```
{
    "start": 0,
    "count": 10
}
```

返回:

```
{
    "code": 200,
    "games": [比赛数据列表],
    "start": start,
    "count": count,
}
```

#### 获取某场比赛
`GET /api/game/fetch/`

参数:

```
{
    "game_id": 100043,
    "user_id": 123454    
}
```

返回:

```
{
    "code": 200,
    "game": [比赛数据结构],
}

```

#### 将一个比赛转换为约球
`POST /api/game/require/`

参数:

```
{
    "user_id": 123454,
    "game_id": 234212, # 可空
    "tennis_level": 3,
    "max_users": 3,
    "age": 24,    # 可空
    "gender": 'f',  # 可空
    "tennis_age": 2,   # 可空
    "is_owner_play":（0代表不参加，1代表参加）,
    'desc': 约球描述,
    "price": 这场约球的价格,
    "app_version": app版本号，
    "address": 地址信息,
    "sports_type": 1, # 运动类型
    "pay_type": 1, # 支付类型。（免费，线上，线下）
    "start_time": "2014-12-09 14:00:00",
    "end_time": "2014-12-09 15:00:00"
}
```

返回:

```
{
    "code": 200,
    "require": {约球数据结构}
}
```

#### 在一场比赛的约球里发表留言
`POST /api/game/require/<int:id>/comments/`

参数(path里面的id是约球的id):

```
{
    "user_id": 当前用户的id,
    "comment_id": 如果回复某条留言，需要提供某条留言的id,
    'body': 留言
}
```

返回:

```
(
    "code": 200,
    "new_comment":发表的comment数据结构
)
```

#### 取到一场比赛约球的一页留言
`GET /api/game/require/<int:id>/comments/`

参数(path里面的id是约球的id):

```
{

    start:0,
    count:10
}
```

返回:

```
{
    "code": 200,
    "comments":comment列表
}
```

#### 修改约球描述
`POST '/api/require/desc/update/`

参数(path里面的id是约球的id):

```
{
    "user_id": user_id,
    "require_id": require_id,
    "desc": desc
}
```

返回:

```
(
    "code": 200,
    "require": {约球数据结构}
)
```
