##数据定义
#### 管理员用户: Manager
```
{
	"id":1000001,
	"name":"小豆儿",
	"role":1,(1为系统管理员，2为商家管理员,3为商家指定管理员)
	"telephone":"13811798897",
	"company":"泰山网球俱乐部"
	"venues":(2,3类管理员有对应的场馆venue列表)
	"noti_new_order":True
}
```
（其他Venue,Court,Game等和客户端数据定义一致）

##API
所有API，除登录、密码、验证码相关外，有默认参数(user_id)
#### 登录
`POST /api/manager/login/`

参数:

```
{
	"telephone":"13811798897",
	"password":"5dfudnxif7ihekwb",(HMAC_SHA1加密)
}
```

返回:

```
{
	"code":200,
	"access_token":"sdfslkdjflksdj"	,
	"user":(manager数据结构)
}
```

#### 获取用户信息
`GET /api/manager/userinfo`

参数:

```
{
	"fetch_user_id":100005
}
```

返回:

```
{
	"user":(manager)数据结构
}
```

#### 获取验证码

`POST /api/manager/verify/`

参数:

```
{
	"manager_id":"",
	"telephone":""
}
```

返回:

```
{
	"code":200,
	"verify": {
	    "telephone": ""
	    "verify_code": ""
	}
}
```

#### 修改手机号
`POST /api/manager/telephone/`

参数:

```
{
	"verify_code": "872382",
	"telephone": "18611794302",
	"manager_id": manager_id,
	"password": password,
}

```

返回:

```
{
	"code": 200,
	"new_tele":"18611794302"
}
```

#### 重置密码
`POST /api/manager/reset_pwd/`

参数：

```
{
    "password": "sdfslkdjflksdj",
    "telephone": 18663953776,
    "verify_code": "2234234"
}
```

返回：

```
{
    "code": '200',
    "access_token": "xklgsnlabkbmkgkf",
    "user": [用户数据结构]
}
```
#### 修改密码
`POST /api/manager/modify_pwd/`

参数:

```
{
	"old_password":"sdfslkdjflksdj",
	"new_password":"adfslkdjflksdj"
}
```
返回:
```
{
	"code":"200"
}
```

#### 获取某天的场地信息
`GET /api/manager/schedule/`

参数:

```
{
	"date": '2014-06-24',
	"venue_id":100001
}
```

返回:

```
{
    "code": 200,
    "schedule": {
        "current_time": "2014-01-07 01:11:00",
        "date": "2013-11-24",
        "courts": [
            {
                "court": (场地数据结构),
                "schedule": [比赛状态列表]
            },
            # ... dict of (court and schedule)
        ]
    }
}
```

**注: 比赛状态:**

```
{
	#共有数据
    "game_id": 12312312,
    "court_id": 200001,
    "price": 100.00,
    "status": 3, # 2, 1, 0
    "time": "2013-12-21 13:00:00",

    #status字段含义
    COURT_STATUS_BOOK = 1 #动起来用户预定
    COURT_STATUS_RESERVE = 2 #商家预留
    COURT_STATUS_FREE = 3 #可以预定
    COURT_STATUS_PARTICIPATE = 4 #约球
    COURT_STATUS_PARTICIPATE_FULL = 5 #约球人员已满
    COURT_STATUS_EXPIRED = 6 #场地订单过期
    COURT_STATUS_PENDING = 7 #场地订单支付中
    COURT_STATUS_MERCHANT_BOOK = 8 #商家不是会员订场,订单已经完成
    COURT_STATUS_MERCHANT_PENDING = 9 #商家不是会员订场,订单支付中
    COURT_STATUS_MERCHANT_MEMBER_BOOK = 10 #商家会员订场,订单已经完成
    COURT_STATUS_MERCHANT_MEMBER_PENDING = 11 #商家会员订场,订单支付中

    #约球有的数据
    "per_price": 12.00,
    "max_player": 5,
    "current_player": 3
    
    #商家订场
    "ct": (在单元格里显示的内容),
}
```

#### 获取订场列表
'GET /api/manager/games/'

参数:

```
{
	"start": 10,
	"limit": 10,
	"venue_id":1000012
}
```

返回:

```
{
	games:(game列表)
}
```

#### 获取最近退单列表
'GET /api/manager/canceled_game'

参数:

```
{
	"start":10,
	"limit":10,
	"venue_id":1000012
}
```

返回:

```
{
	games:(game列表)
}
```

#### 添加线下订场(预留)
'POST /api/manager/preorder/'

参数:

```
{
	"type":1,(1为订场,2为预留)
	"venue_id":1000012,
	"court_id":1000001,
	"start_time":"2013-09-23 02:00:00",
	"end_time":"2013-09-23 03:00:00",
	"manager_id":123312,
	"user_info":"13811798897",(用户会员或手机号,订场时有)
	"intro":"宝马专场"(预留时有)
}
```

返回:

```
	"code":200,
	"game":(game数据结构)
```

#### 取消线下订场(预留)
'DELETE /api/manager/preorder/'

参数:

```
{
	"game_id":1000035
}
```

返回:

```
{
	"code":200
}
```

#### 确认线下订场
`POST /api/manager/confirm_preorder`

参数:

```
{
	"game_id":1000035
}

```

返回:

```
{
	"code":200
	"game":(game数据结构)
}
```

#### 获取场馆照片
`GET /api/manager/photos/`

参数:

```
{
    "start": 0,
    "count": 1,
    "venue_id": 100043
}
```

返回:

```
{
    "code": 200,
    "venue_photos": {
        "photos": [],
        "photo_id": [],
        "start": 0,
        "count": 1
    }
}
```

#### 上传场馆照片
`POST /api/manager/photos/`

参数:

```
{
	"venue_id":100001,
	"manager_id": 100001,
	"photo":图片内容
}
```

返回:

```
{
    "code": 200,
    "venue_photo": {
        "photo_url": "",
        "photo_id": photo_id,
    }
}
```

#### 设置封面图片
`POST /api/manager/album_cover/`

参数:

```
{
	"stadium_id":100001,
	"photo_id":100002
}
```

返回:

```
{
    "code": 200
}
```

####提醒设置
`POST /api/manager/noti_config`

参数:

```
{
	"noti_new_order": True
}
```

####获取提醒数量
`GET /api/manager/notify_count/`

参数:

```
{
	"manager_id": 100001,
}
```

返回:

```
{
	"count": 20,
}
```

####获取提醒
`GET /api/manager/notify/`

参数:

```
{
	"manager_id": 100001,
	"type":(1是新订单，2是取消订单)
}
```

返回:

```
{
	"notifies":(notify列表)	
}
```

```
notify = {
    'id': 100001,
    'msg': 'hello',
    'type': (1新订单，2取消订单),
    'status': (0未读，1已读),
    'game':(game数据结构)
}

```

####标记提醒为已读
`GET /api/manager/notify_read/`

参数:

```
{
	"manager_id": 100001,
	"notify_id": 100002,	
}
```

返回:

```
{
	code:200	
}
```

####注册设备
`POST /api/manager/device/`

参数:

```
{
	"manager_id": 100001,
	"device_token":"dfa321nd9dsfue"
}
```

返回:

```
{
	"device":(device数据结构)
}
```

####获取设备信息
`GET /api/manager/device/`

参数:

```
{
	"manager_id": 100001,
}
```

返回:

```
{
	"device":(device数据结构)
}
```

####删除注册设备
`DELETE /api/manager/device/`

参数:

```
{
	"manager_id": 100001,
}
```

返回:

```
{
	"result":True
}
```