### 数据样例

#### Order

```
{
    "id": 132132,
    "creator_id": 1000001,
    "price": 120.00,
    "pay_type": "book", # "participate", "vip", "charge"
    "pay_method": (1是支付宝，2是余额支付，3是微信支付，4是银联支付)
    "status": "pending", # "complete"
    "refund_status" : 1,
    "c_time": 1214322343,(订单被创建的时间戳)
    "pc_time": 1314322343，(承诺订单被确认的时间戳)
    
    # 如果是订场或是约球
    "game": (比赛数据结构),
    # 如果是VIP订单或充值订单
    "user": (用户数据结构)
}
```

#### Order的几种状态

| status | desc |
| ------ | ---- |
| pending | 等待支付 |
| expired | 超时未付款 |
| conforming | 确认中 |
| confirm_expired | 确认超时 |
| complete | 被运营确认 |
| refund_pending | 申请退款处理中 |
| refunded | 退款成功 |
| passed | 已结束(已确认并已超过订场时间) |

| refund status | desc |
| ------------- | ---- |
| 1 | 申请发送失败 |
| 2 | 发出申请,并经支付平台确认 |
| 3 | 支付平台驳回了申请 |
| 4 | 最终退款成功 |




### API
#### 我的订场(新增)
`POST /api/my_order/`

参数:

```
{
	"user_id": 100010
}
```

返回:

```
{
    "code": 200,
    "orders": [{order数据格式},...]
}
```

#### 订场请求
`POST /api/order/book/`

参数:

```
{
    "user_id": 1000010,
    "court_id": 2000101,
    "start_time": '2013-10-24 10:00:00',
    "end_time": '2013-10-24 12:00:00',
    "pay_method": (1是支付宝，2是余额支付，3是微信支付，4是银联支付)
}
```

返回:

```
{
    "code": 200,
    "order":(order数据结构)
}
```

#### 参与约球请求
`POST /api/order/participate/`

参数:

```
{
    "game_id": 35324,
    "user_id": 1000001,
    "price": 这场约球的价格,
    "pay_method": (1是支付宝，2是余额支付，3是微信支付，4是银联支付)
}
```

返回:

```
{
    "code": 200,
    "order": (order数据结构)
}
```


#### 充值请求
`POST /api/order/charge/`

参数:

```
{
    "user_id": 1000002
    "amount": 500
    "pay_method": (1是支付宝，2是余额支付，3是微信支付，4是银联支付)
}
```

返回:

```
{
    "code": 200,
    "order": {order数据结构}
}
```

#### 验证订单
`POST /api/order/verify/`

参数:

```
{
    "user_id": 10000010,
    "order_id": 34234234798
    "pay_method": (1是支付宝，2是余额支付，3是微信支付，4是银联支付)
}
```

返回:

```
{
    "code": 200,
    "order": (订单详情),
    "price": 300.00, #(这笔订单的价格),
    "account_balance": 500.00, #(帐号里剩余现金)
}
```

#### 通过账户余额支付
`POST /api/order/payment/account/`

参数:

```
{
    "order_id": 342343749823,
    "user_id": 1000010
}
```

返回:

```
{
    "code": 200,
    "result": "success"
}
```

#### 验证支付结果(当从支付宝返回时, 暂时没有实现)
`POST /wt/order/verify_pay`

参数:

```
{
    "order_id":"34232"
}
```

返回:

```
{
    "result":1
}
```

#### 获取用户未完成的订单(未过期的订场和约球订单)
`GET /api/order/user/pending/`

参数

```
{
    "user_id": 1000001
}
```

返回:

```
{
    "code": 200,
    "orders": [{order数据格式},...]
}
```

#### 获取一个订场订单详情
`GET /api/order/book/{id}`

参数:无
返回:

```
{
    code: 200
    order: {订场数据格式}
}
```

#### 退单
`POST /api/order/refund/`

参数:

```
{
    "game_id": 10000010,
    "user_id": 10000011
}
```

返回:

```
{
    "code": 200,
    "order": (订单详情),
    "price": 300.00, #(这笔订单的价格),
    "account_balance": 500.00, #(帐号里剩余现金)
}
```
