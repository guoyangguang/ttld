###数据样例
#### User

```
{
    "id": "3764320001",
    "telephone": "5666",
    "name": "verycb",
    "avatar_url": "https://www.douban.com/pic/34234343",
    "birth": "1987-08-09",
    "gender": "m",
    "height": 175,
    "weight": 70,
    "intro": "我是一只小飞龙",
    "book_games_amount": 10,
    "participate_games_amount": 10,        
    
    "member_type": 'sliver', # 'normal', 'golden',    
    "vip_expire_time": '2015-02-04 12:00:00',
    "account_balance": 250.00,    
    
    "tennis_level": 2.5,
    "tennis_age": 2,    
    
    "user_info": {
    	"tennis_level": 2.5,
	    "tennis_age": 2,        	
    	"badminton_age": 2.5,
	    "football_age": 2,    	    
	    "football_position": 2,
    },(所有运动的age均是字符串,有"1-","5+"这样的情况)
    
    "participate_count": {
    	"tn": 5,(网球)
    	"bm": 12,(羽毛球)
    	"fb": 20,(足球)
    }
}
```

#### Account

```
{
    "user_id": 1324123123,
    "grade": "normal", # "sliver", "golden"
    "vip_expire_time": '2014-11-11 10:00:00', # 如果账户是VIP账户
    "account_balance": 258.00
}
```

###API
#### 登录
`POST /api/user/login/`

参数:

```
{
    "telephone": "13333324343",
    "password": "123456"
}
```

返回:

```
{
    "code": 200,
    "access_token": "sdfslkdjflksdj",
    "user": (用户信息结构)
}
```


#### 验证用户信息
`POST /api/verify/code/`

参数:

```
{
    "teltephone": "13798364391",
    "verify_code": "exat"
}
```

返回:

```
{
    "code": 200,
    "telephone": "18663953776",
    "r": 0
}
```

#### 获取验证码
`POST /api/verify/phone/`

参数:

```
{
    "telephone": "18663953776"
}
```

返回:

```
{
    "code": 200,
    "telephone": "18663953776",
    "verify_code": "exat"
}
```


#### 注册接口
`POST /api/user/register/`

参数：

```
{
    "telephone": telephone,
    "password": password,
    "verify_code": verify_code
}
```

返回：

```
{
    "code": '200',
    "access_token": access_token,
    "user_id": user_id
}
```

#### 完善资料
`PUT /api/user/{user_id}/`

参数:

```
{
    "name": "xueming",
    "birth": "1987-03-09",
    "gender": "m",
    "intro": "我是一只小飞龙",
    "avatar": 图片内容,
    "tennis_age": 3,
    "tennis_level": 3.5,
    "badminton_age": 3,
    "football_age": 3,
    "football_position": 2,    
}
```

返回:

```
{
    "code": 200,
    "user": (更新后的用户数据结构)
}
```

#### 更新用户城市
`PUT /api/user/city/`

参数:

```
{
    "user_id": "yoyo",
    "city_id": 1,
}
```

返回:

```
{
    "code": 200,
}
```


#### 获取用户信息
`GET /api/user/{id}/`

参数:
  无

返回:

```
{
    "code": 200,
    "user": (用户数据结构)
}
```

#### 获取重置密码的短信验证码
`POST /api/verify/reset-password/`

参数：

```
{
    'telephone': 18663953776
}
```

返回：

```
{
    "code": "200",
    "telephone": 18663953776,
    "verify_code": "2234234"
}
```

#### 重置密码
`POST /api/user/reset-password/`

参数：

```
{
    "password": "reset-password-12345",
    "telephone": 18663953776,
    "verify_code": "2234234"
}
```

返回：

```
{
    "code": '200',
    "access_token": "xklgsnlabkbmkgkf",
    "user": [用户数据结构]
}
```

#### 通过微信账号登录
`POST /api/user/wxlogin/`

参数:

```
{
	"wx_user_id":1321,
	"wx_gender":"m",
}
```

返回:

```
{
	"user":(根据user的name是否为空来判断是否要进入补充资料界面),
    "access_token": "sdfslkdjflksdj"
}
```

#### 微信账号绑定手机号和设置密码
`POST /api/user/add-telephone`

参数:

```
{
    "verify_code": verify_code,
    "telephone": 18623432832,
    "user_id": 100024,
    "password",dafddadfda,
}
```

返回:
```
{
	"user":(用户信息),
}
```

#### 微信账号和现有账号合并

`POST /api/user/merge/`

参数:

```
{
	"merged_userid":100015,
	"host_telephone":18611794302,
	"host_password":"hifundkwerer",
}
```

返回:

```
{
	"user":(用户信息),
}
```

#### 获取用户的比赛通知(返回当前时间在比赛结束前的数据)
`GET /api/game/notify/`

参数:

```
user_id 用户的ID
count 返回订场和约球分别的最大数目
```

返回:

```
{
    "code": 200,
    "games": {
        "book": [比赛数据列表],
        "participate": [比赛数据列表]
    },
    "pending_orders": penging_orders
}
```

### 获取用户未读消息总数
`GET /api/user/notify/count/`

参数:
```
```

返回：

```
{
    "code": 200,
    "user_id": user_id,
    "notify_count": count
}
```

### 获取用户未读消息
`GET /api/user/notify/`

参数:

```
```

返回：

```
{
    "code": 200,
    "user_id": user_id,
    "notifies": [{
        "id": ,
        #"type": (此参数暂未生效 1:有人参与约球,2:比赛即将开始,3:订场被确认，4:订场被取消, 5:收到评论, 6:收到评论的回复)
        "status":(0未读，1已读)
        "msg": (显示内容)
        "game": (比赛数据结构), # 如果有比赛id
        "participant_id": participate_id, # 如果有参与者id
        "order": (订单数据结构) # 如果有订单id
    }]
}
```

### 标记消息为已读
`POST /api/notify/read/`

参数

```
user_id,(用户ID)
notify_id,(消息ID)

```

### 注册设备
`POST /api/device/register/`

参数

```
{
   user_id: '100001',
   device_token: '1123l1k2j3lk1h2lkj3gh1lj23h1lj2h3lk1hl2k3jh1lk23jl1k23'
}
```

返回

```
{
    code: 200,
    device: {
        user_id: '100001',
        device_token: '123jlk12j3lk1h23lk1hj2lk3j1lkj23l1k'
    }
}
```
取消注册设备
`POST /api/device/unregister/`

参数

```
{
   user_id: '100001',
}
```

返回

```
{
    code: 200,
    result:True(false),
}
```

### 获取用户设备信息
`GET /api/device/`

参数

```
{
   user_id: '100001',
}
```

返回

```
{
    code: 200,
    device: {
        user_id: '100001',
        device_token: '123jlk12j3lk1h23lk1hj2lk3j1lkj23l1k'
    }
}
```

**错误码**

| Code    | Message          | status |
| ------- | ---------------- | ------ |
| 1000    | 系统错误         | 400    |
| 1001    | 错误的APPKEY和SECRET| 401  |
| 1002    | 缺少Authorization参数| 403 |
| 1003    | 无效的access token  | 403  |
| 1100    | 手机号码不正确     | 403    |
| 1101    | 验证码发送失败     | 403    |
| 1102    | 密码错误          | 403    |
| 1103    | 用户不存在        | 404    |
| 1104    | 没有找到任何照片   | 404    |
| 1105    | 更新用户信息失败   | 403    |
| 1110    | 该手机号已被注册   | 403    |
| 1111    | 验证码错误        | 403    |
| 1112    | 添加用户失败      | 400    |
| 1113    | 当前账号已经绑定手机号| 400  |
| 4997    | 非法的请求URL     | 400    |
| 4998    | 非法的请求方式     | 400    |
| 4999    | 缺少参数          | 400    |
| 2000    | 订单不存在        | 404    |
| 2001    | 订单类型错误      | 400    |
| 2002    | 订单过期          | 403    |
| 2003    | 订单验证失败    | 405 |
| 2004    | 订单不能再退款   | 405 |
| 2010    | 该订场不能约球    | 403    |
| 2011    | 该订场参与人数已到人数限制| 405|
| 2012    | 比赛不存在        | 404   |
| 2013    | 场地不能被预定    | 403   |
| 2014    | 用户没有权限操作  | 403   |
| 2015    | 比赛已经结束      | 405   |
| 2016    | 用户不符合约球的条件 | 403 |
| 2017    | 用户已经参与约球 | 403 |
| 3001    | 余额不足          | 403   |
| 3002    | 用户账户不存在    | 404   |
| 3003    | 用户已经是VIP会员 | 405   |
