### 安卓版本更新检测接口
`GET /api/user/update/`

参数

```
version_code: '5',
```

返回

```
{
    code: 200,
    last_version: 5,
    file_path: apk下载地址
}
```

## 推送

iOS和Android(还未实现)使用共同的推送数据结构。

注:iOS的APNS的单条信息有256个字节的限制

#### 提醒场主有人参加约球

apns数据结构

```
{
	"aps":{
		"alert":"有一位球友加入了你的约球",
		"badge":1,
	}
	"type":1,
	"game_id":100023,
	"owner_id":100010,
	"new_player_id":100011
}
```	

#### 推荐约球

apns数据结构

```
{
	"aps":{
		"alert":"有球友发起了一场适合你的约球哦，来看看吧",
		"badge":1,
	}
	"type":2,
	"game_id":100003,
}
```

#### 通知场主约球下有新评论

```
{
	"aps":{
		"alert":"你发起的约球有一条新留言",
		"badge":1,
	}
	"type":3,
	"require_id":100003,
	"game_id":100004,	
}
```

#### 通知你的评论被回复

```
{
	"aps":{
		"alert":"有人回复了你在约球里的留言",
		"badge":1,
	}
	"type":4,
	"require_id":100003,
	"game_id":100004,
}
```

#### 提醒新订场被确认

apns数据结构

```
{
	"aps":{
		"alert":"订场被确认",
		"badge":1,
	}
	"type":5,
	"order_id":100002,
	"owner_id":100010,
}
```

#### 提醒新订场被拒绝

apns数据结构

```
{
	"aps":{
		"alert":"订场被拒绝",
		"badge":1,
	}
	"type":6,		
	"order_id":100002,		
	"owner_id":100010,
}
```

	

	
