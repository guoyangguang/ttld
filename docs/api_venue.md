### 数据样例
#### 场馆: Venue

```
{
    "id": 1000001,
    "name": "北航网球馆",
    "address": "海淀区知春路286号",
    "traffic_info": "北京",
    "telephone": "13836382932",
    "latitude": "23.34323",
    "longitude": "34.34343",
    "open_time": "09:00:00",
    "close_time": "24:00:00",
    "avatar_url": "http://tiantianwq.com/photo/1231.jpg",
    "intro": "北航网球馆，位于知春路地铁站附近，是海淀区最掉渣天的网球馆之一",
    "weekly_game_count": 12,(近一周约球数),
    "court_count": 12, (场地数)
}
```

#### 场地: Court

```
{
    "id": 1000011,
    "name": "1号场",
    "venue_id": 1000001,
    "type": "singfield",("doublefield")
}
```

#### 时段状态

```
{
	#共有数据
    "game_id": 12312312,
    "court_id": 200001,
    "price": 100.00,
    "status": 3, # 2, 1, 0
    "time": "2013-12-21 13:00:00",
    "in": 1,(表示小时数的间隔，足球为2，其他为1)
    
    #约球有的数据
    "per_price": 12.00,
    "max_player": 5,
    "current_player": 3
    
    #商家接口参数
    "ct":(供客户端每个单元格显示)
}
```

**时段的status**

| code | desc    |
| ---- | ------- |
| 1    | 已被订场 |
| 2    | 已被预订 |
| 3    | 空闲     |
| 4    | 可以约球 |
| 5    | 已约满   |
| 6    | 已过期   |

#### 体育类别说明
```
网球:2^0
羽毛球:2^1
足球:2^2
乒乓球:2^3
跑步:2^4
滑雪:2^5
游泳:2^6
篮球:2^7
台球:2^8
排球:2^9
```


###API

#### 附近场馆
`GET /api/venue/nearby/`

参数:

```
{
    "latitude": 116.50863647460938,
    "longitude": 39.960714556446824,
    "city_id": 1  # 比赛所在城市，默认为1代表北京
    "sports_type":1,
    "start": 0,
    "count": 10,
}
```

返回:

```
{
    "code": 200,
    "venues": [场馆列表], # 会在场馆中添加一个最近7天订场情况的字段。'recent_court_situation': '充裕'/'紧张'
    "start": start,
    "count": count,
}
```

#### 常去场馆(暂未使用)
`GET /api/venue/usual/`

参数

```
{
    "uid": 1000001,
    "start": 0,
    "count": 10
    "sports_type":1
}
```

返回:

```
{
    "code": 200,
    "venues": [场馆列表],
    "start": start
    "count": count,
}
```

#### 搜索场馆
`GET /api/venue/search/`

参数:

```
{
    "q": "体育",
    "start_time": '2013-12-13 11:00:00', # 可空,日期如果输入不规范返回venues结果为空
    "latitude": 123.00123, # 可空
    "longitude": 90.234435 # 可空
    "city_id": 1  # 比赛所在城市，默认为1代表北京
    "sports_type":1,
    "start": 0
    "count": 10
}
```

返回:

```
{
    "code": 200,
    "venues": [场馆数据结构],
    "start": start
    "count": count,
}
```

#### 获取场馆照片
`GET /api/venue/{venue_id}/photos/`

参数:

```
{
    "start": 0,
    "count": 1
}
```

返回:

```
{
    "code": 200,
    "photos": ["http://xxx.xxx.jpg", ...],
    "start": start,
    "count": count,
}
```

#### 按日期获取场馆场地状态
`GET /api/venue/schedule/`

参数:

```
{
    "venue_id": 100001,
    "date": '2013-11-24'
}
```

返回:

```
{
    "code": 200,
    "schedule": {
        "current_time": "2014-01-07 01:11:00",
        "date": "2013-11-24",
        "courts": [
            {
                "court": (场地数据结构),
                "schedule": [比赛状态列表]
            },
            # ... dict of (court and schedule)
        ]
    }
}
```


#### 获取拥有球场的城市列表
`GET /api/venue/city/`

参数:

```
start 代表起始页数，默认为0
count 代表返回数据个数，默认为10
```

返回:

```
{
    "code": 200,
    "cities": [{
        "city": "城市名称",
        "id": "城市ID",
        "province": "省份名称",
        "weather_code": "城市对应的天气码"
    }],
    "start": start,
    "count": count,
}
```
